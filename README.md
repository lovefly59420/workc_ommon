# RELEASE NOTE
## 4.5.1
- 增加 testing code

## 4.5.0
- 更新 apache commons-imaging 套件依賴版本，原引用 commons-imaging 1.0-snapshots 會導致程式 import class not found 改引用 commons-imaging-1.0-alpha 穩定版本解決

## 4.3.0
- 加入正正主管邏輯 (單一部門，多主管)

## 4.0.0
- 新增 excel 匯出匯入公用 tool
- 導入 common-dependency-manager 1.1.0 (共用管理元件或者工具版本)

## 3.4.1
- 修正當 api host非 domain name  而是域名時，ssl 驗不過的問題
- 修正 list 均分方法，當無資料時，不回傳null
- 升級 lib sslcontext-kickstart -> 7.4.8


## 3.0.0
- 新增 CallApiUtils : 統一 Call Api 工具
- 新增 SkypeBotClient ：傳送訊息到 skype


## 2.39.1

- WkOrgCache 新增 isExsitByID

## 2.39.0

- WkJsoupUtils.clearCssTag2 HTML 轉文字. <ａ> 文字凸顯 ex: 測試連結〈http://xxxx〉 => 【測試連結】〈http://xxxx〉
- 新增列舉 BooleanStatus
- WkOrgUtils 新增 getOrgLevelName 取得組織層級名稱
- WkJsonUtils JSON 轉換效能優化 (修正多筆時，每次 new 一個新物件，造成效能低落問題)

## 2.38.2

- 新增 AbstractEnumConverter 、 PersistableEnum for 公用 converter 抽象化類別

## 2.38.1

- 清理原始碼告警 serialVersionUID

## 2.38.0

- update lib : system-rest-client to 3.11.0
- add class  : SplitListStrConverter
- **WkOrgUtils**
    + **add** prepareDepSidsByRule 依據傳入規則，計算結果 （部門列表）
- **WkUserUtils**
    + **add** findNameByID 以 User ID 查詢使用者名稱

## 2.37.0

- **WkOrgUtils**
    + **adj** findNameBySid 允許傳入NULL，參數若為NULL直接回傳空字串。
- **WkOrgUtils**
    + **adj** findNameBySid 允許傳入NULL，參數若為NULL直接回傳空字串。
- **WkOrgCache**：
    + **adj** findBySid 參數若為NULL增加LOG提示

## 2.35.1

- update lib : jsoup to 1.14.2
- **WkOrgUtils**
    + **adj** prepareBasicDep_Recursive 回傳的上層單位，改為wkOrgCache 再查一次，才會有完整的Org資訊。
- **WkJsoupUtils**
    + **add** clearCssTag2 取得去除網頁標記的純文字內容(支援標記換行，但換行符號需先置換為標記)

## 2.32.1

- **WkFileUtils**
    + **add** readFileToPrettyHTMLByText 讀取文字檔並輸出格式化HTML字串

## 2.26.0

- 新增 SplitListIntConverter, 轉以『,』分隔的字串 String -> List<Integer>
- 新增 WkUserUtil.isUserHasRole , 檢查傳入使用者是否有特定角色

## 2.23.0

- 新增『系統別』相關 class
- 新增 WkVerUtil , 可取得 jar 檔版本
- 新增 WkCommonUtils.findDomainUrlByCompId(compID), 可取得系統網址

## 2.21.0

- **WkOrgUtils**
    + **add** findDepByNameLike 以單位名稱模糊查詢
    + **add** findDepByNameLikeAndActive 以單位名稱模糊查詢 (過濾停用)

## 2.20.1

- **WkUserCache**：新增 findByPrimaryOrgSidsIn 方法，批次尋找帳號所屬單位SID
- **WkUserUtils**：調整 compareUserRelation 方法，邏輯性修正

## 2.20.0

- **WkCommonCache**：程式碼優化
- **WkOrgCache**：
    + **adj** findAllParent 方法新增可傳入 topLevel , 指定取得單位的最大層級
    + **adj** 查詢子單位相關方法, 改為呼叫 orgClient 提供的快取方法
- **SystemDevelopException**：改為 runtime 層級 (改為繼承RuntimeException), 避免程式碼外層需做多作判斷
- **WkOrgUtils**
    + **adj** 原傳入 List、Set 的方法, 改為Collection
    + **add** 新增方法 sortDepSidByOrgTree , 可排序傳入的 depSid
    + **add** 新增方法 getOrgNameByOrgSid，取得單位名稱 (公用,統一意外狀況時回傳的資訊)
- **MailService**：優化 sendHtmlMail 邏輯
- **WkCommonUtils**：調整 compare 方法, 由傳入 List 改為 Collection (提高適應性)
- **WkUserUtils**
    + **add** 新增方法 isActive，可傳入 sid 直接判斷是否為啟用/停用
    + **add** 新增方法 compareUserRelation，比對傳入使用者的隸屬關係

## 2.19.0

- **WkCommonUtils** 新增 prepareExcetionInfo , 兜組『系統錯誤』的訊息 (統一顯示樣式)
- **WkConstrnts** 新增 class , 工作紀錄固定參數
- **WkMessage** 新增 class , 工作紀錄固定回應訊息文字

> by Allen

## 2.18.1

- **WkOrgCache** 修正 findAllParent , 僅能往上取一層的問題

> by Allen

## 2.18.0

- **UserMessageLevel、LogLevel** 同質性過高, 合併為 InfomationLevel

- **WkCommonCache**
    + **adj** 優化排程清快取 log，僅在第一次執行時印出 (避免遺忘此排程)

- **WkOrgCache**
    + **adj** findBySid 增加可傳入 sid type 為 『String』
    + **add** findDirectManagerUserSids ：取得傳入部門，直系向上所有單位主管 (包含傳入部門)

- **WkOrgUtils**
    + **adj** findNameBySid 增加可傳入sid 類型為 Set
    + **add** findNameBySidStrs
    + **add** getOrgName : 統一管理 Org 取得 name 的方式

- **WkUserCache**
    + **add** findBySid 增加可傳入 sid type 為 『String』

- **WkUserUtils**
    + **add** findNameBySidStrs 增加可傳入 sid type 為 『String』
    + **add** prepareUserNameWithDep 兜組 部門+使用者暱稱字串 (ex: E化發展部-專案管理組-Yifan)

> by Allen

## 2.17.0

- add function : WkCommonUtils 增加 【listLeaves】： List 相減
- add function : WkOrgUtils 增加 【findNameByDecorationStyle】： 若為停用單位, 加上停用, 並劃上刪除線
- add function : JsonStringListToConverter 增加 【convertToDatabaseColumn(Set<Integer> values)】：可轉換傳入值 Set<Integer>
- update : WkCommonCache 增加快取存活時間, 排程清過期快取

## 2.16.0

- add function : WkUserCache 增加取得公司別登入權限設定 findSecAuth
- fix : REQ-1391 employee.rest 呼叫錯誤, 以致於無法取得 employee 中的 mail 資料

## 2.15.0

- add function : WkDateUtils 增加判斷String回傳Date功能 timeAnalysis
- add funciton : WkOrgCache 增加取得公司列表功能 findCompanies

## 2.14.1

- update lib : system-rest-client to 3.5.0
- update lib : employee-rest-client to 1.6.0
- add function : WkOrgCache 增加取得單位排序功能 findAllOrgOrderSeqMap, findOrgOrderSeqBySid
- adj WkOrgUtil.sortOrgByOrgTree 改為以 system-rest 回傳資料進行排序 (OrgSort) , 增加運算效率(不需自己算)
- fix 附加檔案下載時, 增加判斷需為『非停用』 (AttachmentServlet) 且清除檔名特殊字元
- fix 附加檔案的檔名有含逗號導致無法瀏覽下載問題

## 2.14.0

- add class : SystemOperationException
- add function : WkCommonUtils.compare()  -> List 比對
- update lib : system-rest-client to 3.4.0

## 2.13.0

- update lib jackson-annotations to 2.7.5
- update lib bpm-vo to 1.9.0
- update lib employee-rest-client to 1.5

## 2.12.0

- 大量更新

## 2.11.1

- 修正 Cache 部分 system-rest-client需套用3.3.1 版，解決當cache無法即時更新時(需求繁忙暫無回應，或網路雍塞)，造成cache無法使用。

## 2.11.0

- add WkCommonCache: 公用快取工具
- add WkEhCacheUtils: EhCache 建立工具
- add WkTreeUtils: 雜項工具
- add WkOrgCache:
  1.findAllDepByCompID 2.findAllDepByCompSid
- add WkCommonUtils.prepareCostMessageStart
- add WkJsonUtils.toJsonWithOutPettyJson 轉為json字串
- add WkOrgUtils.getOrgLevelOrder 依據傳入單位層級，回傳層級順序
- add WkStringUtils.safeConvertStr2Int

## 2.10.2

- 修正 Cache 部分 call system rest 時, 返回 optional 未判斷就 直接做get 造成 exception 訊息的問題

## 2.10.0

- adj WkOrgCache.getChildOrgs: 調整回傳值依據 seq 排序
- add WkCommonUtils.averageAssign: 將 List 均分
- add WkNode: 階層式節點結構物件
- add WkOrgUtils: 工具類-組織相關 1.bulidOrgTreeNode: 依據傳入單位, 建立單位樹 node 2.prepareDepsNameByTreeStyle: 以階層樹方式，兜組傳入單位名稱
  3.sortByOrgTree: 將傳入單位，依照組織樹階層,由上而下依序排序
- add WkJsoupUtils 1.highlightKeyWordByStyle: highlight 關鍵字 （自訂CSS style） 1.highlightKeyWordByClass: highlight 關鍵字
  （自訂CSS class）

## 2.8.0

- upgrade <span style="color:red;">system-rest-client【3.1.0-SNAPSHOT】</span>
- upgrade <span style="color:red;">employee-rest-client【1.3.0】</span>
- 移除 com.cy.work.common.cache 下所有 class 的 cache 機制, 改由 system-rest-client 管理

## 2.6.0

<BR/><span style="color:red;">system-rest-client【2.1.0】->【3.1.0】</span>
<BR/><span style="color:red;">bpm-vo【1.0.0-SNAPSHOT】->【1.8.0】</span>

## 2.3.1 2016/11/15

- 修正使用者By Id 時，不分大小寫。

## 2.3.0 2016/10/27

- 使用附加檔案區分兩類型。
  <BR/><span style="color:red;">package（com.cy.work.common.vo）繼續使用物件</span>
  <BR/><span style="color:red;">package（com.cy.work.common.vo.basic）使用基本型態</span>

## 2.2.0 2016/10/20

- 將附加檔案中使用者及部門改為基本型態。
- Add 角色群組 Cache。
- WkUserWithRolesCache add method getUserSidByRoleSid(Long roleSid)

## 1.0.0

+ work-common ➡ <font color="red">1.0.0</font>
+ common.vo ➡ <font color="red">3.0.0-SNAPSHOT</font>
+ org.springframework ➡ <font color="red">4.2.5.RELEASE</font>
+ 補充common.vo移除檔案
+ 加入markdown文件
