package com.cy.work.common.utils;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.vo.Attachment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class WkAttachUtilsTest {
    @InjectMocks
    private WkAttachUtils wkAttachUtils;


    @Test
    void createEmptyAttachment(){
        class AttachmentVO implements Attachment<String>{
            private String attSid;
            private String fileName;
            private String desc;
            private Org uploadDept;
            private Activation status;
            private Date createdDate;
            private Date updatedDate;
            private User createdUser;
            private User updatedUser;
            private Boolean keyCheckEdit;
            private Boolean keyChecked;

            public AttachmentVO(){};


            @Override
            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null) {
                    return false;
                }
                if (getClass() != obj.getClass()) {
                    return false;
                }
                return true;
            }


            @Override
            public String getSid() {
                return this.attSid;
            }

            @Override
            public void setSid(String sid) {
                this.attSid=sid;
            }

            @Override
            public String getFileName() {
                return this.fileName;
            }

            @Override
            public void setFileName(String fileName) {
                this.fileName=fileName;
            }

            @Override
            public String getDesc() {
                return this.desc;
            }

            @Override
            public void setDesc(String desc) {
                this.desc=desc;
            }

            @Override
            public Org getUploadDept() {
                return this.uploadDept;
            }

            @Override
            public void setUploadDept(Org uploadDept) {
                this.uploadDept=uploadDept;
            }

            @Override
            public Activation getStatus() {
                return this.status;
            }

            @Override
            public void setStatus(Activation status) {
                this.status=status;
            }

            @Override
            public Date getCreatedDate() {
                return this.createdDate;
            }

            @Override
            public void setCreatedDate(Date createdDate) {
                this.createdDate=createdDate;
            }

            @Override
            public Date getUpdatedDate() {
                return this.updatedDate;
            }

            @Override
            public void setUpdatedDate(Date updatedDate) {
                this.updatedDate=updatedDate;
            }

            @Override
            public User getCreatedUser() {
                return this.createdUser;
            }

            @Override
            public void setCreatedUser(User createdUser) {
                this.createdUser=createdUser;
            }

            @Override
            public User getUpdatedUser() {
                return this.updatedUser;
            }

            @Override
            public void setUpdatedUser(User updatedUser) {
                this.updatedUser=updatedUser;
            }

            @Override
            public Boolean getKeyCheckEdit() {
                return this.keyCheckEdit;
            }

            @Override
            public void setKeyCheckEdit(Boolean checkEdit) {
                this.keyCheckEdit=checkEdit;
            }

            @Override
            public Boolean getKeyChecked() {
                return this.keyChecked;
            }

            @Override
            public void setKeyChecked(Boolean checked) {
                this.keyChecked=keyChecked;
            }
        }

        AttachmentVO result1 = new AttachmentVO();
        AttachmentVO result2 = new AttachmentVO();
        AttachmentVO result3 = new AttachmentVO();
        assertEquals(result1,wkAttachUtils.createEmptyAttachment(result1,"test",new User(1),new Org(1)));
        assertNotNull(wkAttachUtils.createEmptyAttachment(result2,"test",new User(2),new Org(2)));
        Attachment<String> result4=wkAttachUtils.createEmptyAttachment(result3,"test",new User(3),new Org(3));
        assertEquals(result4,result3);
    }


    @Test
    void replaceIllegalcharacter(){
        String result1 = "joyTest0727";
        assertEquals(result1,wkAttachUtils.replaceIllegalcharacter("joyTest0727"));
        assertEquals(result1+"＊＊＊",wkAttachUtils.replaceIllegalcharacter("joyTest0727***"));
        assertEquals(result1+"＼＊＊＊",wkAttachUtils.replaceIllegalcharacter("joyTest0727\\***"));
        assertEquals(result1+"＼：／＊？〝〈〉｜",wkAttachUtils.replaceIllegalcharacter("joyTest0727\\:/*?\"<>|"));
        assertEquals(result1+"＼：／＊？〝〈〉｜....",wkAttachUtils.replaceIllegalcharacter("joyTest0727\\:/*?\"<>|...."));
    }
}
