package com.cy.work.common.utils;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.work.common.cache.WkCommonCache;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.google.common.collect.Lists;
import org.junit.BeforeClass;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.reset;

@ExtendWith(MockitoExtension.class)
public class WkCommonContentUtilsTest {


    private MockedStatic<WkUserCache> mockedStatic1;

    private MockedStatic<WkOrgCache> mockedStatic2;

    @BeforeEach
    void initStatic(){
        mockedStatic1=Mockito.mockStatic(WkUserCache.class);
        mockedStatic2=Mockito.mockStatic(WkOrgCache.class);
        WkUserCache wkUserCache = Mockito.mock(WkUserCache.class);
        WkOrgCache wkOrgCache =Mockito.mock(WkOrgCache.class);

        lenient().when(WkUserCache.getInstance()).thenReturn(wkUserCache);
        lenient().when(WkOrgCache.getInstance()).thenReturn(wkOrgCache);

        User user100 = new User();
        user100.setSid(100);
        user100.setName("user100");
        user100.setPrimaryOrg(new SimpleOrg(1));

        User user200 = new User();
        user200.setSid(200);
        user200.setName("user200");
        user200.setPrimaryOrg(new SimpleOrg(2));

        User user300 = new User();
        user300.setSid(300);
        user300.setName("user300");
        user300.setPrimaryOrg(new SimpleOrg(3));

        User user400 = new User();
        user400.setSid(400);
        user400.setName("user400");
        user400.setPrimaryOrg(new SimpleOrg(4));

        User user500 = new User();
        user500.setSid(500);
        user500.setName("user500");
        user500.setPrimaryOrg(new SimpleOrg(5));

        User user600 = new User();
        user600.setSid(600);
        user600.setName("user600");
        user600.setPrimaryOrg(new SimpleOrg(6));

        lenient().when(wkUserCache.findBySid(100)).thenReturn(user100);
        lenient().when(wkUserCache.findBySid(200)).thenReturn(user200);
        lenient().when(wkUserCache.findBySid(300)).thenReturn(user300);
        lenient().when(wkUserCache.findBySid(400)).thenReturn(user400);
        lenient().when(wkUserCache.findBySid(500)).thenReturn(user500);
        lenient().when(wkUserCache.findBySid(600)).thenReturn(user600);

        lenient().when(wkOrgCache.findBySid(1)).thenReturn(new Org(1));
        lenient().when(wkOrgCache.findBySid(2)).thenReturn(new Org(2));
        lenient().when(wkOrgCache.findBySid(3)).thenReturn(new Org(3));
        lenient().when(wkOrgCache.findBySid(4)).thenReturn(new Org(4));
        lenient().when(wkOrgCache.findBySid(5)).thenReturn(new Org(5));
        lenient().when(wkOrgCache.findBySid(6)).thenReturn(new Org(6));

        lenient().when(wkOrgCache.findOrgOrderSeqBySid(1)).thenReturn(1);
        lenient().when(wkOrgCache.findOrgOrderSeqBySid(2)).thenReturn(2);
        lenient().when(wkOrgCache.findOrgOrderSeqBySid(3)).thenReturn(3);
        lenient().when(wkOrgCache.findOrgOrderSeqBySid(4)).thenReturn(4);
        lenient().when(wkOrgCache.findOrgOrderSeqBySid(5)).thenReturn(5);
        lenient().when(wkOrgCache.findOrgOrderSeqBySid(6)).thenReturn(6);
    }

    @AfterEach
    void tearDown(){
        mockedStatic1.close();
        mockedStatic2.close();
    }


    @Test
    void makeupByDataListStyle(){
        String result1 = WkCommonContentUtils.makeupByDataListStyle(Lists.newArrayList("test1","test2"),"empty");
        String result2 = WkCommonContentUtils.makeupByDataListStyle(Lists.newArrayList("test1","test2","test3"),"empty");
        String result3 = WkCommonContentUtils.makeupByDataListStyle(Lists.newArrayList(),"empty");
        String myAnswer1 = "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label>test1</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>test2</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";
        String myAnswer2 = "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label>test1</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>test2</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>test3</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";
        String myAnswer3 = "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<label style='text-align: center; color: rgb(128, 128, 128);'>empty</label>" +
                "</div>";
        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
    }


    @Test
    void prepareDiffMessageForDepartment(){
        String result1=WkCommonContentUtils.prepareDiffMessageForDepartment(Lists.newArrayList(100,200,300),Lists.newArrayList(100,400,500),"新增","移除");
        String myAnswer1 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                        "<br/>" +
                        "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[400]、SID:[500]</div>" +
                        "<br/>" +
                        "<br/>" +
                        "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                        "<br/>" +
                        "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[200]、SID:[300]</div>" +
                        "<br/>";
//        assertEquals(myAnswer1,result1);
    }


    @Test
    void prepareDiffMessageForUser(){
        String result1=WkCommonContentUtils.prepareDiffMessageForUser(Lists.newArrayList(100,200,300),Lists.newArrayList(100,400,500),"新增","移除");
        String myAnswer1 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                        "<br/>" +
                        "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                        "<ol class='ui-datalist-data'>" +
                        "<li class='ui-datalist-item'>" +
                        "<label>>&nbsp;user400</label>" +
                        "<hr style='border-style: ridge'/>" +
                        "</li>" +
                        "<li class='ui-datalist-item'>" +
                        "<label>>&nbsp;user500</label>" +
                        "<hr style='border-style: ridge'/>" +
                        "</li>" +
                        "</ol>" +
                        "</div>" +
                        "<br/>" +
                        "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                        "<br/>" +
                        "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                        "<ol class='ui-datalist-data'><li class='ui-datalist-item'>" +
                        "<label>>&nbsp;user200</label>" +
                        "<hr style='border-style: ridge'/>" +
                        "</li>" +
                        "<li class='ui-datalist-item'>" +
                        "<label>>&nbsp;user300</label>" +
                        "<hr style='border-style: ridge'/>" +
                        "</li>" +
                        "</ol>" +
                        "</div>";

//        assertEquals(myAnswer1,result1);
    }

    @Test
    void prepareDiffMessage() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        WkCommonContentUtilsTest wkCommonContentUtilsTest = new WkCommonContentUtilsTest();
        Method privateMethod = WkCommonContentUtils.class.getDeclaredMethod("prepareDiffMessage", Collection.class,Collection.class,String.class,String.class,boolean.class);
        // 將私有方法設置為可訪問
        privateMethod.setAccessible(true);
        // 出來的是Object，要轉型
        String result1=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(100,400,500),"新增","移除",false);
        String result2=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(100,400,500),"新增","移除",true);
        String result3=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(100,200,300),"新增自定義標題","移除自定義標題",false);
        String result4=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(100,200,300),"新增自定義標題","移除自定義標題",true);
        String result5=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(),Lists.newArrayList(),"新增自定義標題","移除自定義標題",false);
        String result6=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(),Lists.newArrayList(),"新增自定義標題","移除自定義標題",true);
        String result7=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(),Lists.newArrayList(100,400,500),"新增","移除",false);
        String result8=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(),Lists.newArrayList(100,400,500),"新增","移除",true);
        String result9=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(),"新增","移除",false);
        String result10=(String) privateMethod.invoke(wkCommonContentUtilsTest,Lists.newArrayList(100,200,300),Lists.newArrayList(),"新增","移除",true);
        String myAnswer1 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                "<br/>" +
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user400</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user500</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>" +
                "<br/>" +
                "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                "<br/>" +
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'><li class='ui-datalist-item'>" +
                "<label>>&nbsp;user200</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user300</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer2 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                "<br/>" +
                "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[400]、SID:[500]</div>" +
                "<br/>" +
                "<br/>" +
                "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                "<br/>" +
                "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[200]、SID:[300]</div>" +
                "<br/>";

        String myAnswer3 ="";

        String myAnswer4 ="";

        String myAnswer5 ="";

        String myAnswer6 ="";

        String myAnswer7 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                "<br/>" +
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user100</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user400</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user500</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer8 =
                "<font style='font-weight:bold;text-decoration:underline;'>新增:</font>" +
                        "<br/>" +
                        "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[100]、SID:[400]、SID:[500]</div>" +
                        "<br/>";

        String myAnswer9 =
                "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                "<br/>" +
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'><li class='ui-datalist-item'>" +
                "<label>>&nbsp;user100</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user200</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label>>&nbsp;user300</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer10 =
                "<font style='font-weight:bold;text-decoration:underline;'>移除:</font>" +
                "<br/>" +
                "<div style='max-width:300px;overflow:hidden;text-overflow:ellipsis;'>SID:[100]、SID:[200]、SID:[300]</div>" +
                "<br/>";

//        assertEquals(myAnswer1,result1);
//        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
        assertEquals(myAnswer4,result4);
        assertEquals(myAnswer5,result5);
        assertEquals(myAnswer6,result6);
//        assertEquals(myAnswer7,result7);
//        assertEquals(myAnswer8,result8);
//        assertEquals(myAnswer9,result9);
//        assertEquals(myAnswer10,result10);
    }

    @Test
    void prepareUserNameWithDepByDataListStyle(){
        String result1=WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(Lists.newArrayList(1,2,3), OrgLevel.THE_PANEL,false,"-","empty",true);
        String result2=WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(Lists.newArrayList(1,2,3), OrgLevel.THE_PANEL,true,"$$$$$","empty",true);
        String result3=WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(Lists.newArrayList(1,2,3), OrgLevel.THE_PANEL,false,"-","empty",false);
        String result4=WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(null, OrgLevel.THE_PANEL,true,"####","empty",true);

        String myAnswer1=
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label> (1)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (2)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (3)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer2=
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label> (1)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (2)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (3)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer3=
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label></label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label></label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label></label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";

        String myAnswer4=
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<label style='text-align: center; color: rgb(128, 128, 128);'>empty</label>" +
                "</div>";


        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
        assertEquals(myAnswer4,result4);
    }


    @Test
    void prepareDepNameWithDepByDataListStyle(){
        String result1 = WkCommonContentUtils.prepareDepNameWithDepByDataListStyle(Lists.newArrayList(1,2,3),OrgLevel.GROUPS,true,"$$$","empty",true);
        String result2 = WkCommonContentUtils.prepareDepNameWithDepByDataListStyle(null,OrgLevel.GROUPS,true,"aaaa","empty_title",false);

        String myAnswer1 =
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<ol class='ui-datalist-data'>" +
                "<li class='ui-datalist-item'>" +
                "<label> (1)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (2)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "<li class='ui-datalist-item'>" +
                "<label> (3)</label>" +
                "<hr style='border-style: ridge'/>" +
                "</li>" +
                "</ol>" +
                "</div>";


        String myAnswer2 =
                "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>" +
                "<label style='text-align: center; color: rgb(128, 128, 128);'>empty_title</label>" +
                "</div>";

//        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
    }



}
