package com.cy.work.common.utils;

import com.cy.commons.vo.User;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.shared.model.fileset.mappers.MapperUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class WkStringUtilsTest {
    @InjectMocks
    private WkStringUtils wkStringUtils;


    @Test
    void prepareLikeWhereStamentWithIllegalChar(){

    }



    @Test
    void prepareLikeQryStamentWithIllegalChar(){

    }



    @Test
    void prepareUniqueParamName(){

    }



    @Test
    void prepareUniqueParamName2(){

    }



    @Test
    void isEmpty(){
        boolean result1 = WkStringUtils.isEmpty(new User(1));
        boolean result2 = WkStringUtils.isEmpty(new User());
        assertEquals(false,result1);
        assertEquals(false,result2);
    }



    @Test
    void isEmpty2(){
        boolean result1 = WkStringUtils.isEmpty("   123456   ");
        boolean result2 = WkStringUtils.isEmpty("");
        boolean result3 = WkStringUtils.isEmpty(StringUtils.EMPTY);
        boolean result4 = WkStringUtils.isEmpty("123456   ");
        assertEquals(false,result1);
        assertEquals(true,result2);
        assertEquals(true,result3);
        assertEquals(false,result4);
    }



    @Test
    void isEmpty3(){
        boolean result1 = WkStringUtils.isEmpty(Lists.newArrayList(1,2,3));
        boolean result2 = WkStringUtils.isEmpty(Lists.newArrayList("a","b"));
        boolean result3 = WkStringUtils.isEmpty(Lists.newArrayList());
        boolean result4 = WkStringUtils.isEmpty(Sets.newHashSet(1,2,3));
        boolean result5 = WkStringUtils.isEmpty(Queues.newArrayDeque());
        boolean result6 = WkStringUtils.isEmpty(Queues.newArrayDeque(Arrays.asList("apple", "banana", "orange")));
        boolean result7 = WkStringUtils.isEmpty(Queues.newArrayDeque(Lists.newArrayList("apple", "banana", "orange")));
        assertEquals(false,result1);
        assertEquals(false,result2);
        assertEquals(true,result3);
        assertEquals(false,result4);
        assertEquals(true,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
    }



    @Test
    void isEmpty4(){
        boolean result1 = WkStringUtils.isEmpty(Maps.newHashMap());
        Map<String, Integer> hashMap = Maps.newHashMap();
        hashMap.put("apple", 1);
        boolean result2 = WkStringUtils.isEmpty(hashMap);
        assertEquals(true,result1);
        assertEquals(false,result2);
    }



    @Test
    void notEmpty(){
        boolean result1 = WkStringUtils.notEmpty("   123456   ");
        boolean result2 = WkStringUtils.notEmpty("");
        boolean result3 = WkStringUtils.notEmpty(StringUtils.EMPTY);
        boolean result4 = WkStringUtils.notEmpty("123456   ");
        assertEquals(true,result1);
        assertEquals(false,result2);
        assertEquals(false,result3);
        assertEquals(true,result4);
    }



    @Test
    void notEmpty2(){
        boolean result1 = WkStringUtils.notEmpty(new User(1));
        boolean result2 = WkStringUtils.notEmpty(new User());
        assertEquals(true,result1);
        assertEquals(true,result2);
    }



    @Test
    void notEmpty3(){
        boolean result1 = WkStringUtils.notEmpty(Lists.newArrayList(1,2,3));
        boolean result2 = WkStringUtils.notEmpty(Lists.newArrayList("a","b"));
        boolean result3 = WkStringUtils.notEmpty(Lists.newArrayList());
        boolean result4 = WkStringUtils.notEmpty(Sets.newHashSet(1,2,3));
        boolean result5 = WkStringUtils.notEmpty(Queues.newArrayDeque());
        boolean result6 = WkStringUtils.notEmpty(Queues.newArrayDeque(Arrays.asList("apple", "banana", "orange")));
        boolean result7 = WkStringUtils.notEmpty(Queues.newArrayDeque(Lists.newArrayList("apple", "banana", "orange")));
        assertEquals(true,result1);
        assertEquals(true,result2);
        assertEquals(false,result3);
        assertEquals(true,result4);
        assertEquals(false,result5);
        assertEquals(true,result6);
        assertEquals(true,result7);
    }



    @Test
    void notEmpty4(){
        boolean result1 = WkStringUtils.notEmpty(Maps.newHashMap());
        Map<String, Integer> hashMap = Maps.newHashMap();
        hashMap.put("apple", 1);
        boolean result2 = WkStringUtils.notEmpty(hashMap);
        assertEquals(false,result1);
        assertEquals(true,result2);
    }



    @Test
    void isEqual(){
        boolean result1 = WkStringUtils.isEqual("abc","abc");
        boolean result2 = WkStringUtils.isEqual("","abc");
        boolean result3 = WkStringUtils.isEqual("abc","");
        boolean result4 = WkStringUtils.isEqual("    ","    ");
        boolean result5 = WkStringUtils.isEqual("ABC","abc");
        boolean result6 = WkStringUtils.isEqual(" abc","abc");
        boolean result7 = WkStringUtils.isEqual("abc ","abc");
        boolean result8 = WkStringUtils.isEqual("*","＊");
        assertEquals(true,result1);
        assertEquals(false,result2);
        assertEquals(false,result3);
        assertEquals(true,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
        assertEquals(false,result8);
    }



    @Test
    void notEqual(){
        boolean result1 = WkStringUtils.notEqual("abc","abc");
        boolean result2 = WkStringUtils.notEqual("","abc");
        boolean result3 = WkStringUtils.notEqual("abc","");
        boolean result4 = WkStringUtils.notEqual("    ","    ");
        boolean result5 = WkStringUtils.notEqual("ABC","abc");
        boolean result6 = WkStringUtils.notEqual(" abc","abc");
        boolean result7 = WkStringUtils.notEqual("abc ","abc");
        boolean result8 = WkStringUtils.notEqual("*","＊");
        assertEquals(false,result1);
        assertEquals(true,result2);
        assertEquals(true,result3);
        assertEquals(false,result4);
        assertEquals(true,result5);
        assertEquals(true,result6);
        assertEquals(true,result7);
        assertEquals(true,result8);
    }



    @Test
    void isAlpha(){
        boolean result1 = WkStringUtils.isAlpha("abc123");
        boolean result2 = WkStringUtils.isAlpha("ABC123");
        boolean result3 = WkStringUtils.isAlpha("ABC-123");
        boolean result4 = WkStringUtils.isAlpha("#123");
        boolean result5 = WkStringUtils.isAlpha("我是誰");
        boolean result6 = WkStringUtils.isAlpha(",");
        boolean result7 = WkStringUtils.isAlpha("＊");
        assertEquals(true,result1);
        assertEquals(true,result2);
        assertEquals(false,result3);
        assertEquals(false,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
    }



    @Test
    void isNumber(){
        boolean result1 = WkStringUtils.isNumber("123");
        boolean result2 = WkStringUtils.isNumber("ABC123");
        boolean result3 = WkStringUtils.isNumber("ABC-123");
        boolean result4 = WkStringUtils.isNumber("#123");
        boolean result5 = WkStringUtils.isNumber("我是誰");
        boolean result6 = WkStringUtils.isNumber(",");
        boolean result7 = WkStringUtils.isNumber("＊");
        boolean result8 = WkStringUtils.isNumber("-1");
        assertEquals(true,result1);
        assertEquals(false,result2);
        assertEquals(false,result3);
        assertEquals(false,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
        assertEquals(false,result8);
    }



    @Test
    void isCapsEnglish(){
        boolean result1 = WkStringUtils.isCapsEnglish("abc123");
        boolean result2 = WkStringUtils.isCapsEnglish("ZXY");
        boolean result3 = WkStringUtils.isCapsEnglish("ZXx");
        boolean result4 = WkStringUtils.isCapsEnglish("#123");
        boolean result5 = WkStringUtils.isCapsEnglish("我是誰");
        boolean result6 = WkStringUtils.isCapsEnglish(",");
        boolean result7 = WkStringUtils.isCapsEnglish("＊");
        assertEquals(false,result1);
        assertEquals(true,result2);
        assertEquals(false,result3);
        assertEquals(false,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
    }



    @Test
    void isNumberOrCapsEnglish(){
        // 大寫英文or數字
        boolean result1 = WkStringUtils.isNumberOrCapsEnglish("abc123");
        boolean result2 = WkStringUtils.isNumberOrCapsEnglish("ZXY");
        boolean result3 = WkStringUtils.isNumberOrCapsEnglish("ZXx");
        boolean result4 = WkStringUtils.isNumberOrCapsEnglish("#123");
        boolean result5 = WkStringUtils.isNumberOrCapsEnglish("我是誰");
        boolean result6 = WkStringUtils.isNumberOrCapsEnglish(",");
        boolean result7 = WkStringUtils.isNumberOrCapsEnglish("＊");
        boolean result8 = WkStringUtils.isNumberOrCapsEnglish("FH456789");
        boolean result9 = WkStringUtils.isNumberOrCapsEnglish("9514735");
        assertEquals(false,result1);
        assertEquals(true,result2);
        assertEquals(false,result3);
        assertEquals(false,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
        assertEquals(true,result8);
        assertEquals(true,result9);
    }



    @Test
    void isEMail(){
        boolean result1 = WkStringUtils.isEMail("user@gmail.chungyo.com");
        boolean result2 = WkStringUtils.isEMail("我是誰@gmail.chungyo.com");
        boolean result3 = WkStringUtils.isEMail("ㄅㄆ@gmail.chungyo.com");
        boolean result4 = WkStringUtils.isEMail("4567894@gmail.chungyo.com");
        boolean result5 = WkStringUtils.isEMail("sdfsd45645128我@gmail.chungyo.com");
        boolean result6 = WkStringUtils.isEMail("user@sdfsd.456.com.4565.sdf");
        boolean result7 = WkStringUtils.isEMail("user@456456");
        boolean result8 = WkStringUtils.isEMail("user@gmail");
        boolean result9 = WkStringUtils.isEMail("1_____@gmail.aaaa");
        boolean result10 = WkStringUtils.isEMail("11.ss@gmail.aaaa");
        boolean result11 = WkStringUtils.isEMail("11.ss+456@gmail.aaaa");
        boolean result12 = WkStringUtils.isEMail("11.ss+456-asfd@gmail.aaaa");
        boolean result13 = WkStringUtils.isEMail("11...++--ss+_456-asfd@gmail.aaaa");
        assertEquals(true,result1);
        assertEquals(false,result2);
        assertEquals(false,result3);
        assertEquals(true,result4);
        assertEquals(false,result5);
        assertEquals(true,result6);
        assertEquals(false,result7);
        assertEquals(false,result8);
        assertEquals(true,result9);
        assertEquals(true,result10);
        assertEquals(true,result11);
        assertEquals(true,result12);
        assertEquals(true,result13);
    }



    @Test
    void isEMail2() throws UnsupportedEncodingException {
        boolean result1 = WkStringUtils.isEMail("user.12_56-9+@gmail.chungyo.com","UTF-8");
        boolean result2 = WkStringUtils.isEMail("user.12_56-9+@gmail.chungyo.com", StandardCharsets.UTF_8.name());
        String asciiString = new String("user.12_56-9+@gmail.chungyo.com".getBytes(StandardCharsets.US_ASCII));
        boolean result3 = WkStringUtils.isEMail(asciiString, StandardCharsets.US_ASCII.name());
        boolean result4 = WkStringUtils.isEMail("user.12_56-9+我@gmail",StandardCharsets.UTF_8.name());
        boolean result5 = WkStringUtils.isEMail("我是誰@gmail",StandardCharsets.UTF_8.name());
        boolean result6 = WkStringUtils.isEMail("我是誰@gmail.com",StandardCharsets.UTF_8.name());
        boolean result7 = WkStringUtils.isEMail("user.12_56-9+",StandardCharsets.UTF_8.name());
        System.out.println(asciiString);

        assertEquals(true,result1);
        assertEquals(true,result2);
        assertEquals(true,result3);
        assertEquals(false,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
        assertEquals(false,result7);
    }



    @Test
    void isContains(){
        // 判斷str1是否包含str2
        boolean result1 = WkStringUtils.isContains("user.12_56-9+@gmail.chungyo.com",".com");
        boolean result2 = WkStringUtils.isContains("user.12_56-9+@gmail.chungyo.com",".com@");
        boolean result3 = WkStringUtils.isContains("我是誰我在哪我在做甚麼??","麼??");
        boolean result4 = WkStringUtils.isContains("123+-*/`12233456789!@#$#%^&*(","+-*/`1");
        boolean result5 = WkStringUtils.isContains("********","＊");
        boolean result6 = WkStringUtils.isContains("＊＊＊＊","＊＊＊＊＊＊＊");
        assertEquals(true,result1);
        assertEquals(false,result2);
        assertEquals(true,result3);
        assertEquals(true,result4);
        assertEquals(false,result5);
        assertEquals(false,result6);
    }



    @Test
    void safeTrim(){
        // null與空字串時的預設值為""
        String result1 = WkStringUtils.safeTrim("user.12_56-9+@gmail.chungy");
        String result2 = WkStringUtils.safeTrim(null);
        String result3 = WkStringUtils.safeTrim("       ");
        String result4 = WkStringUtils.safeTrim("       joy");
        String result5 = WkStringUtils.safeTrim("joy       ");
        String result6 = WkStringUtils.safeTrim("  j  o  y  ");
        assertEquals("user.12_56-9+@gmail.chungy",result1);
        assertEquals("",result2);
        assertEquals("",result3);
        assertEquals("joy",result4);
        assertEquals("joy",result5);
        assertEquals("j  o  y",result6);
    }



    @Test
    void safeTrim2(){
        String result1 = WkStringUtils.safeTrim("user.12_56-9+@gmail.chungy","null與空字串時的預設值");
        String result2 = WkStringUtils.safeTrim(null,"null與空字串時的預設值");
        String result3 = WkStringUtils.safeTrim("       ","null與空字串時的預設值");
        String result4 = WkStringUtils.safeTrim("       joy","null與空字串時的預設值");
        String result5 = WkStringUtils.safeTrim("joy       ","null與空字串時的預設值");
        String result6 = WkStringUtils.safeTrim("  j  o  y  ","null與空字串時的預設值");
        assertEquals("user.12_56-9+@gmail.chungy",result1);
        assertEquals("null與空字串時的預設值",result2);
        assertEquals("null與空字串時的預設值",result3);
        assertEquals("joy",result4);
        assertEquals("joy",result5);
        assertEquals("j  o  y",result6);
    }



    @Test
    void trimFullWidthSpace(){
        // rim 全形空白 (包含半形)
        String result1 = WkStringUtils.trimFullWidthSpace("joy");
        // 半形空白
        String result2 = WkStringUtils.trimFullWidthSpace(" joy ");
        // 全形空白
        String result3 = WkStringUtils.trimFullWidthSpace("　joy　");

        assertEquals("joy",result1);
        assertEquals("joy",result2);
        assertEquals("joy",result3);
    }



    @Test
    void getExceptionStackTrace(){
        Throwable e1 = new Throwable("測試1");
        Throwable e2 = new Throwable("");
        Error e3 =new Error("測試2");
        Exception e4 = new Exception("測試3");
        IOException e5 = new IOException("測試4");

        String result1 = WkStringUtils.getExceptionStackTrace(e1);
        String result2 = WkStringUtils.getExceptionStackTrace(e2);
        String result3 = WkStringUtils.getExceptionStackTrace(e3);
        String result4 = WkStringUtils.getExceptionStackTrace(e4);
        String result5 = WkStringUtils.getExceptionStackTrace(e5);
        System.out.println(result1);

        assertNotNull(result1);
        assertNotNull(result2);
        assertNotNull(result3);
        assertNotNull(result4);
        assertNotNull(result5);
    }



    @Test
    void removeCtrlChr(){
        // 空字符不同於空格字符
        // 這四種不處理
        //  ASCII:0 -> \0
        //  ASCII:9 -> \t
        //  ASCII:10 -> \n
        //  ASCII:13 -> \r
        String result1=WkStringUtils.removeCtrlChr("aaa");
        String result2=WkStringUtils.removeCtrlChr("");
        String result3=WkStringUtils.removeCtrlChr("    ");
        String result4=WkStringUtils.removeCtrlChr("aaa\0");
        String result5=WkStringUtils.removeCtrlChr("aaa\t");
        String result6=WkStringUtils.removeCtrlChr("aaa\n");
        String result7=WkStringUtils.removeCtrlChr("aaa\r");
        String result8=WkStringUtils.removeCtrlChr("aaa\0\t\n\r");
        // Backspace (BS，編碼值 8) -> \b
        String result9=WkStringUtils.removeCtrlChr("aaa\b");

        assertEquals("aaa",result1);
        assertEquals("",result2);
        assertEquals("    ",result3);
        assertEquals("aaa\0",result4);
        assertEquals("aaa\t",result5);
        assertEquals("aaa\n",result6);
        assertEquals("aaa\r",result7);
        assertEquals("aaa\0\t\n\r",result8);
        assertEquals("aaa",result9);
    }



    @Test
    void isEmojiCharacter(){
        boolean result1=WkStringUtils.isEmojiCharacter('A');
        boolean result2=WkStringUtils.isEmojiCharacter('我');
        boolean result3=WkStringUtils.isEmojiCharacter('5');
        boolean result4=WkStringUtils.isEmojiCharacter('@');
        boolean result5=WkStringUtils.isEmojiCharacter('\u263A');
        boolean result6=WkStringUtils.isEmojiCharacter('\u2701');
        boolean result7=WkStringUtils.isEmojiCharacter('\u2764');
        boolean result8=WkStringUtils.isEmojiCharacter('\u1f60');

        assertEquals(true,result1);
        assertEquals(true,result2);
        assertEquals(true,result3);
        assertEquals(true,result4);
        assertEquals(true,result5);
        assertEquals(true,result6);
        assertEquals(true,result7);
        assertEquals(true,result8);
    }



    @Test
    void contains() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        int[] array1 = {1,23,4,5,69,100};
//        // removeCtrlChr 已經測試功能
//        Method privateMethod = WkStringUtils.class.getDeclaredMethod("contains", Arrays.class, int.class);
//        // 設置可訪問性
//        privateMethod.setAccessible(true);
//        boolean result1 = (boolean)privateMethod.invoke(wkStringUtils,array1,23);
//        assertEquals(true,result1);
    }



    @Test
    void safeGetlength(){

    }



    @Test
    void safeConvertStr2Int(){

    }



    @Test
    void padding(){

    }



    @Test
    void toBase64(){

    }



    @Test
    void fromBase64(){

    }



    @Test
    void padEndEmpty(){

    }



    @Test
    void readLine(){

    }



    @Test
    void readLineShowIndex(){

    }



    @Test
    void fullLeftText(){

    }



    @Test
    void combine(){

    }



    @Test
    void replaceIllegalSqlLikeStrAndAddLikeStr(){

    }



    @Test
    void replaceIllegalSqlLikeStr(){

    }



    @Test
    void checkStringLengthIsOver(){

    }



    @Test
    void isNumeric(){

    }



    @Test
    void contains2(){

    }



    @Test
    void trimLeftChar(){

    }



    @Test
    void unicodeToString(){

    }


}
