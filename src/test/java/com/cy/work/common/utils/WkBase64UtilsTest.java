package com.cy.work.common.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class WkBase64UtilsTest {
    @InjectMocks
    private WkBase64Utils wkBase64Utils;

    @Test
    void encode(){
        String problem = "joy";
        String result= wkBase64Utils.encode(problem);
        // utf-8 -> byte -> 64位元
        // joy -> [106,111,121] -> am95([97,109,57,53])
        String myAnswer = "am95";
        assertEquals(myAnswer,result);
    }


    @Test
    void decode(){
        String problem = "am95";
        String result= wkBase64Utils.decode(problem);
        String myAnswer = "joy";
        assertEquals(myAnswer,result);
    }

}
