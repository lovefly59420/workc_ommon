package com.cy.work.common.utils;

import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.employee.rest.client.simple.SimpleEmployeeClient;
import com.cy.employee.vo.SimpleEmployee;
import com.cy.work.common.enums.WkStrFormatterType;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class WkStrFormatterTest {
    @InjectMocks
    private WkStrFormatter wkStrFormatter;

    @Mock
    private SimpleEmployeeClient simpleEmployeeClient;

    @BeforeEach
    void initSimpleEmployeeClient(){
        SimpleEmployee user1 = new SimpleEmployee("1");
        user1.setExtension("#123456");

        SimpleEmployee user2 = new SimpleEmployee("2");
        user2.setExtension("#2222222");

        lenient().when(simpleEmployeeClient.findByMappedUserSid(1)).thenReturn(Optional.of(user1));
        lenient().when(simpleEmployeeClient.findByMappedUserSid(2)).thenReturn(Optional.of(user2));
    }

//    @AfterEach
//    public void tearDown() {
//        // 清除 WkStrFormatter 實例
//        formatter = null;
//    }



    @Test
    void process(){

    }

    @Test
    void processWithoutErrorLog(){

    }


    @Test
    void process2(){

    }


    @Test
    void trnsParamKeyToContent() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User(1);
        user.setId("10000");
        user.setName("user1");
        SimpleOrg so = new SimpleOrg();
        so.setSid(555);
        user.setPrimaryOrg(so);

        Method privateMethod = WkStrFormatter.class.getDeclaredMethod("trnsParamKeyToContent", String.class,User.class, List.class);
        // 設置可訪問性
        privateMethod.setAccessible(true);
        // 出來的是Object，要轉型
        String result1=(String)privateMethod.invoke(wkStrFormatter,null,user,Lists.newArrayList("警告"));
        String result2=(String)privateMethod.invoke(wkStrFormatter,"",user,Lists.newArrayList("警告"));
        String result3=(String)privateMethod.invoke(wkStrFormatter,"joy",user,Lists.newArrayList("警告"));
        String result4=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.USER_ID.getTypeKeyword(),user,Lists.newArrayList("警告"));
        String result5=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.USER_NAME.getTypeKeyword(),user,Lists.newArrayList("警告"));
        String result6=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.USER_EXT.getTypeKeyword(),user,Lists.newArrayList("警告"));
        String result7=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.SYS_TIME_1.getTypeKeyword(),user,Lists.newArrayList("警告"));
        String result8=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.SYS_DATE_1.getTypeKeyword(),user,Lists.newArrayList("警告"));
        String result9=(String)privateMethod.invoke(wkStrFormatter,WkStrFormatterType.SYS_DATE_2.getTypeKeyword(),user,Lists.newArrayList("警告"));

        assertEquals(null,result1);
        assertEquals("",result2);
        assertEquals("joy",result3);
        assertEquals("10000",result4);
        assertEquals("user1",result5);
        assertEquals("##123456",result6);
        // 盡量避免使用 new Date()，不好測試
        assertNotNull(result7);
        assertNotNull(result8);
        assertNotNull(result9);
    }



    @Test
    void getUserManual(){

    }

}
