package com.cy.work.common.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class WkUserUtilsTest {
    @InjectMocks
    private WkUserUtils wkUserUtils;


    @Test
    void compareUserRelation(){
        int result1 = wkUserUtils.compareUserRelation(1,2, Sets.newHashSet(1, 2, 3, 4));
        int result2 = wkUserUtils.compareUserRelation(1,20, Sets.newHashSet(1, 2, 3, 4));
        int result3 = wkUserUtils.compareUserRelation(10,2, Sets.newHashSet(1, 2, 3, 4));

        assertEquals(0,result1);
        assertEquals(1,result2);
        assertEquals(-1,result3);

    }


    @Test
    void findByNameLike(){

    }


    @Test
    void findAllUserByDepSid(){

    }

    @Test
    void findAllUserByOrgId(){

    }

    @Test
    void findNameBySid(){

    }

    @Test
    void findNameBySid2(){

    }

    @Test
    void findNameBySid3(){

    }

    @Test
    void findNameByID(){

    }

    @Test
    void findNameBySidStrs(){

    }

    @Test
    void findUserPrimaryOrgSid(){

    }

    @Test
    void findUserCompany(){

    }

    @Test
    void findSidById(){

    }

    @Test
    void findUserIDBySid(){

    }

    @Test
    void isActive(){

    }

    @Test
    void isActive2(){

    }

    @Test
    void isExsit(){

    }

    @Test
    void isUserHasRole(){

    }

    @Test
    void isUserHasRole2(){

    }

    @Test
    void isUsersManager(){

    }

    @Test
    void prepareUserNameWithDep(){

    }

    @Test
    void prepareUserNameWithDep2(){

    }

    @Test
    void prepareUserNameWithDep_core(){

    }

    @Test
    void prepareUserNameWithDep3(){

    }

    @Test
    void safetyGetName(){

    }

    @Test
    void isInSimpleUserList(){

    }

}
