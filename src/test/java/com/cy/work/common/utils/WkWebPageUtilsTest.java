package com.cy.work.common.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class WkWebPageUtilsTest {
    @InjectMocks
    private  WkWebPageUtils wkWebPageUtils;


    @Test
    void addWrapTag(){
        String result1=wkWebPageUtils.addWrapTag("testTag");
        String result2=wkWebPageUtils.addWrapTag("");
        String result3=wkWebPageUtils.addWrapTag("white-space: pre;");
        String result4=wkWebPageUtils.addWrapTag("white-space: pre;QQQQQQQQQQQQQQQ;");
        String myAnswer1 = "<span style=\"white-space: pre-wrap;\">testTag</span>";
        String myAnswer2 = "<span style=\"white-space: pre-wrap;\"></span>";
        String myAnswer3 = "<span style=\"white-space: pre-wrap;\">white-space: pre-wrap;</span>";
        String myAnswer4 = "<span style=\"white-space: pre-wrap;\">white-space: pre-wrap;QQQQQQQQQQQQQQQ;</span>";

        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
        assertEquals(myAnswer4,result4);
    }

}
