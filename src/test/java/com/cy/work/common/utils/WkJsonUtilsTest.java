package com.cy.work.common.utils;

import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.employee.vo.SimpleEmployee;
import com.cy.work.common.vo.value.to.ShowInfo;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class WkJsonUtilsTest {
    @InjectMocks
    private WkJsonUtils wkJsonUtils;

    @Test
    void toJson() throws IOException {
        TestClass tc1 = new TestClass(123,"joy");
        TestClass tc2 = new TestClass(123);
        TestClass tc3 = new TestClass("joy");
        TestClass tc4 = new TestClass();

        String result1 = wkJsonUtils.toJson(tc1);
        String result2 = wkJsonUtils.toJson(tc2);
        String result3 = wkJsonUtils.toJson(tc3);
        String result4 = wkJsonUtils.toJson(tc4);
        // 比對時，空格要注意
        String myAnswer1 = "{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}";
        String myAnswer2 = "{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : null\r\n" +
                "}";
        String myAnswer3 = "{\r\n" +
                "  \"id\" : 0,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}";
        String myAnswer4="{\r\n" +
                "  \"id\" : 0,\r\n" +
                "  \"name\" : null\r\n" +
                "}";

        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
        assertEquals(myAnswer4,result4);
    }



    @Test
    void fromJson() throws IOException {
//        String json1 ="{\"id\":123,\"name\":\"joy\"}";
        String json1 ="{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}";
        String json2 = "{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : null\r\n" +
                "}";
        String json3 = "{\r\n" +
                "  \"id\" : 0,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}";
        String json4="{\r\n" +
                "  \"id\" : 0,\r\n" +
                "  \"name\" : null\r\n" +
                "}";

        TestClass result1 = wkJsonUtils.fromJson(json1,TestClass.class);
        TestClass result2 = wkJsonUtils.fromJson(json2,TestClass.class);
        TestClass result3 = wkJsonUtils.fromJson(json3,TestClass.class);
        TestClass result4 = wkJsonUtils.fromJson(json4,TestClass.class);

        TestClass myAnswer1 = new TestClass(123,"joy");
        TestClass myAnswer2 = new TestClass(123);
        TestClass myAnswer3 = new TestClass("joy");
        TestClass myAnswer4 = new TestClass();
        TestClass myAnswer5 = new TestClass();

        System.out.println("result1: "+result1);
        System.out.println("myAnswer1:"+myAnswer1);

        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
        assertEquals(myAnswer3,result3);
        assertEquals(myAnswer4,result4);
    }


    @Test
    void fromJsonToList() throws IOException {
        String json1 ="[{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}]";
        String json2 ="[{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "},"+
                "{\r\n" +
                "  \"id\" : 456,\r\n" +
                "  \"name\" : \"terry\"\r\n" +
                "},"+
                "{\r\n" +
                "  \"id\" : 0,\r\n" +
                "  \"name\" : null\r\n" +
                "}]";
        List<TestClass> result1 = wkJsonUtils.fromJsonToList(json1,TestClass.class);
        List<TestClass> result2 = wkJsonUtils.fromJsonToList(json2,TestClass.class);
        List<TestClass> myAnswer1 = Lists.newArrayList(new TestClass(123,"joy"));
        List<TestClass> myAnswer2 = Lists.newArrayList(new TestClass(123,"joy"),new TestClass(456,"terry"),new TestClass(0,null));

        System.out.println("result2: "+result2);
        System.out.println("myAnswer2:"+myAnswer2);

        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
    }



    @Test
    void fromJsonToHashMapKeyInteger() throws IOException {
        Object o1 = "{\r\n" +
                "  \"id\" : 123,\r\n" +
                "  \"name\" : \"joy\"\r\n" +
                "}";
        Object o2 = "{\r\n" +
                "  \"id\" : 456,\r\n" +
                "  \"name\" : \"terry\"\r\n" +
                "}";
        String json1 ="{\r\n" +
                "  \"1\" : "+o1+",\r\n" +
                "  \"2\" : "+o2+"\r\n" +
                "}";
        System.out.println(json1);
        Map<Integer,Object> result1 = wkJsonUtils.fromJsonToHashMapKeyInteger(json1);
        Map<Integer,Object> myAnswer1 = new HashMap(){
            {
                put(1,"{id=123, name=joy}");
                put(2,"{id=456, name=terry}");
            }
        };
        System.out.println(myAnswer1);
        System.out.println(result1);
        assertEquals(myAnswer1.get(1).toString(),result1.get(1).toString());
        assertEquals(myAnswer1.toString(),result1.toString());
    }

    @Test
    void toPettyJson(){
        TestClass tc1 = new TestClass(123,"joy");
        List<TestClass> tc2 = Lists.newArrayList(new TestClass(123,"joy"),new TestClass(123,"joy"));
        String result1 = wkJsonUtils.toPettyJson(tc1);
        String result2 = wkJsonUtils.toPettyJson(tc2);

        // 格式跟"toJson" 不一樣
        String myAnswer1=
                "{\n" +
                "  \"id\": 123,\n" +
                "  \"name\": \"joy\"\n" +
                "}";
        String myAnswer2=
                "[\n" +
                "  {\n" +
                "    \"id\": 123,\n" +
                "    \"name\": \"joy\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 123,\n" +
                "    \"name\": \"joy\"\n" +
                "  }\n" +
                "]";
        System.out.println(result2);
        System.out.println(myAnswer2);
        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
    }

    @Test
    void toJsonWithOutPettyJson(){
        TestClass tc1 = new TestClass(123,"joy");
        List<TestClass> tc2 = Lists.newArrayList(new TestClass(123,"joy"),new TestClass(123,"joy"));
        String result1 = wkJsonUtils.toJsonWithOutPettyJson(tc1);
        String result2 = wkJsonUtils.toJsonWithOutPettyJson(tc2);

        // 格式跟"toPettyJson" 不一樣
        String myAnswer1="{\"id\":123,\"name\":\"joy\"}";
        String myAnswer2="[{\"id\":123,\"name\":\"joy\"},{\"id\":123,\"name\":\"joy\"}]";
        System.out.println(result2);
        System.out.println(myAnswer2);
        assertEquals(myAnswer1,result1);
        assertEquals(myAnswer2,result2);
    }


    @Test
    void fromJsonByGson(){
        String json1="{\"id\":123,\"name\":\"joy\"}";
        TestClass result1 = (TestClass) wkJsonUtils.fromJsonByGson(json1,TestClass.class);
        TestClass myAnswer1 = new TestClass(123,"joy");
        assertEquals(myAnswer1,result1);
    }


    @Test
    void toPettyJson2(){
        String json1="{\"id\":123,\"name\":\"joy\"}";
        String result1 = wkJsonUtils.toPettyJson(json1);
        String myAnswer1 =
                "{\n" +
                "  \"id\": 123,\n" +
                "  \"name\": \"joy\"\n" +
                "}";
        assertEquals(myAnswer1,result1);
    }

    @Test
    void isJson(){
        String json1="{\"id\":123,\"name\":\"joy\"}";
        String json2="{}";
        String json3="{123}";
        String json4="\"id\":123";
        boolean result1 = wkJsonUtils.isJson(json1);
        boolean result2 = wkJsonUtils.isJson(json2);
        boolean result3 = wkJsonUtils.isJson(json3);
        boolean result4 = wkJsonUtils.isJson(json4);
        assertEquals(true,result1);
        assertEquals(true,result2);
        assertEquals(false,result3);
        assertEquals(false,result4);
    }


    // 使用內部類，需要加 static
    // 在靜態內部類中，Jackson 可以正確地處理反序列化
    // 方法2:Jackson 的 MixIn
    // ObjectMapper objectMapper = new ObjectMapper();
    // objectMapper.enableDefaultTyping(DefaultTyping.NON_FINAL);
    // objectMapper.addMixIn(TestClass.class, TestClassMixIn.class);
    @Data
    public static class TestClass{
        @Getter
        @Setter
        int id;
        @Getter
        @Setter
        String name;
        public TestClass(){}
        public TestClass(int id){
            this.id=id;
        }
        public TestClass(String name){
            this.name=name;
        }

        public TestClass( int id, String name){
            this.id=id;
            this.name=name;
        }
    }



}
