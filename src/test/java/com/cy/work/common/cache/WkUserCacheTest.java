package com.cy.work.common.cache;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.system.rest.client.UserClient;
import com.cy.system.rest.client.vo.SecAuth;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.BeforeClass;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Verify.verify;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WkUserCacheTest {
    @InjectMocks
    private WkUserCache wkUserCache;

    @Mock
    private UserClient userClient;

    private MockedStatic<WkOrgCache> mockedStatic;

    @BeforeEach
    void initStatic(){
        mockedStatic =Mockito.mockStatic(WkOrgCache.class);
        WkOrgCache wkOrgCache = Mockito.mock(WkOrgCache.class);
        lenient().when(wkOrgCache.getInstance()).thenReturn(wkOrgCache);

        Org org1 = new Org(1);
        Org org2 = new Org(2);
        Org org3 = new Org(3);
        lenient().when(wkOrgCache.findBySid(1)).thenReturn(org1);
        lenient().when(wkOrgCache.findBySid(2)).thenReturn(org2);
        lenient().when(wkOrgCache.findBySid(3)).thenReturn(org3);

        Org comp = new Org(1);
        comp.setType(OrgType.HEAD_OFFICE);
        comp.setId("TG");
        lenient().when(wkOrgCache.findById("TG")).thenReturn(comp);
    }

    @AfterEach
    void tearDown(){
        mockedStatic.close();
    }



    @BeforeEach
    void initUserClient(){
        User user1 = new User();
        user1.setName("User1");
        user1.setId("user1");
        user1.setSid(1);
        user1.setUuid("aaaa-0001");
        user1.setStatus(Activation.ACTIVE);

        SimpleOrg so1 = new SimpleOrg();
        so1.setCompanySid(1);
        so1.setSid(1);
        so1.setParentSid(null);
        so1.setManagerSids(Lists.newArrayList(user1.getSid()));
        user1.setPrimaryOrg(so1);

        User user2 = new User();
        user2.setName("User2");
        user2.setId("user2");
        user2.setSid(2);
        user1.setUuid("aaaa-0002");
        user2.setStatus(Activation.ACTIVE);

        SimpleOrg so2 = new SimpleOrg();
        so2.setCompanySid(2);
        so2.setSid(2);
        so2.setParentSid(1);
        so2.setManagerSids(Lists.newArrayList(user2.getSid()));
        user2.setPrimaryOrg(so2);

        User user3 = new User();
        user3.setName("User3");
        user3.setId("user3");
        user3.setSid(3);
        user1.setUuid("aaaa-0003");
        user3.setStatus(Activation.ACTIVE);

        SimpleOrg so3 = new SimpleOrg();
        so3.setCompanySid(3);
        so3.setSid(3);
        so3.setParentSid(2);
        so3.setManagerSids(Lists.newArrayList(user3.getSid()));
        user3.setPrimaryOrg(so3);

        User user4 = new User();
        user4.setName("User4");
        user4.setId("user4");
        user4.setSid(4);
        user1.setUuid("aaaa-0004");
        user4.setStatus(Activation.ACTIVE);

        SimpleOrg so4 = new SimpleOrg();
        so4.setCompanySid(4);
        so4.setSid(4);
        so4.setParentSid(3);
        so4.setManagerSids(Lists.newArrayList(user4.getSid()));
        user4.setPrimaryOrg(so4);


        lenient().when(userClient.findBySid(1)).thenReturn(Optional.of(user1));
        lenient().when(userClient.findBySid(2)).thenReturn(Optional.of(user2));
        lenient().when(userClient.findBySid(3)).thenReturn(Optional.of(user3));
        lenient().when(userClient.findBySid(4)).thenReturn(Optional.of(user4));

        lenient().when(userClient.findAll()).thenReturn(Lists.newArrayList(user1,user2,user3,user4));

        lenient().when(userClient.findById("user1")).thenReturn(Optional.of(user1));
        lenient().when(userClient.findById("user2")).thenReturn(Optional.of(user2));
        lenient().when(userClient.findById("user3")).thenReturn(Optional.of(user3));
        lenient().when(userClient.findById("user4")).thenReturn(Optional.of(user4));

        lenient().when(userClient.findByUuid("aaaa-0001")).thenReturn(Optional.of(user1));
        lenient().when(userClient.findByUuid("aaaa-0002")).thenReturn(Optional.of(user2));
        lenient().when(userClient.findByUuid("aaaa-0003")).thenReturn(Optional.of(user3));
        lenient().when(userClient.findByUuid("aaaa-0004")).thenReturn(Optional.of(user4));

        SecAuth s1 = new SecAuth();
        s1.setCompId("TG");
        s1.setDomainPattern("com.cy");

        SecAuth s2 = new SecAuth();
        s2.setCompId("XS");
        s2.setDomainPattern("com.xs");

        lenient().when(userClient.findSecAuth()).thenReturn(Lists.newArrayList(s1,s2));

        lenient().when(userClient.findByPrimaryOrgSidsIn(Lists.newArrayList(1))).thenReturn(Lists.newArrayList(user1));
        lenient().when(userClient.findByPrimaryOrgSidsIn(Lists.newArrayList(2))).thenReturn(Lists.newArrayList(user2));
        lenient().when(userClient.findByPrimaryOrgSidsIn(Lists.newArrayList(2,3,4))).thenReturn(Lists.newArrayList(user2,user3,user4));
        lenient().when(userClient.findByPrimaryOrgSidsIn(Lists.newArrayList(1,2,3,4))).thenReturn(Lists.newArrayList(user1,user2,user3,user4));
    }


    @Test
    void isExsit(){
        assertEquals(true,wkUserCache.isExsit(1));
        assertEquals(true,wkUserCache.isExsit(2));
        assertEquals(true,wkUserCache.isExsit(3));
        assertEquals(false,wkUserCache.isExsit(55555));
    }

    @Test
    void findAll(){
        List<User> result = Lists.newArrayList(new User(1),new User(2),new User(3),new User(4));
        assertEquals(result,wkUserCache.findAll());
    }


    @Test
    void findBySid(){
        User result1 = new User(1);
        User result2 = new User(2);
        User result3 = new User(3);
        User result4 = new User(4);
        assertEquals(result1,wkUserCache.findBySid(1));
        assertEquals(result2,wkUserCache.findBySid(2));
        assertEquals(result3,wkUserCache.findBySid(3));
        assertEquals(result4,wkUserCache.findBySid(4));
    }


    @Test
    void findBySids(){
        User result1 = new User(1);
        User result2 = new User(2);
        User result3 = new User(3);
        User result4 = new User(4);
        assertEquals(Lists.newArrayList(result1),wkUserCache.findBySids(Lists.newArrayList(1)));
        assertEquals(Lists.newArrayList(result1,result2),wkUserCache.findBySids(Lists.newArrayList(1,2)));
        assertEquals(Lists.newArrayList(result1,result4),wkUserCache.findBySids(Lists.newArrayList(1,4)));
        assertEquals(Lists.newArrayList(result1,result2,result3,result4),wkUserCache.findBySids(Lists.newArrayList(1,2,3,4)));
    }


    @Test
    void findBySidStrs(){
        User result1 = new User(1);
        User result2 = new User(2);
        User result3 = new User(3);
        User result4 = new User(4);
        assertEquals(Lists.newArrayList(result1),wkUserCache.findBySidStrs(Lists.newArrayList("1")));
        assertEquals(Lists.newArrayList(result1,result2),wkUserCache.findBySidStrs(Lists.newArrayList("1","2")));
        assertEquals(Lists.newArrayList(result1,result4),wkUserCache.findBySidStrs(Lists.newArrayList("1","4")));
        assertEquals(Lists.newArrayList(result1,result2,result3,result4),wkUserCache.findBySidStrs(Lists.newArrayList("1","2","3","4")));
    }


    // String sid
    @Test
    void findBySidWithString(){
        User result1 = new User(1);
        User result2 = new User(2);
        User result3 = new User(3);
        User result4 = new User(4);
        assertEquals(result1,wkUserCache.findBySid("1"));
        assertEquals(result2,wkUserCache.findBySid("2"));
        assertEquals(result3,wkUserCache.findBySid("3"));
        assertEquals(result4,wkUserCache.findBySid("4"));
        assertEquals(null,wkUserCache.findBySid("888888"));

    }


    @Test
    void findById(){
        User result1 = new User(1);
        result1.setId("user1");
        User result2 = new User(2);
        result2.setId("user2");
        User result3 = new User(3);
        result3.setId("user3");
        User result4 = new User(4);
        result4.setId("user4");

        assertEquals(result1,wkUserCache.findById("user1"));
        assertEquals(result2,wkUserCache.findById("user2"));
        assertEquals(result3,wkUserCache.findById("user3"));
        assertEquals(result4,wkUserCache.findById("user4"));
        assertEquals(null,wkUserCache.findById("user5555555"));
        assertNull(wkUserCache.findById("user9999999"));
    }


    @Test
    void findByIdAndNotCheckEmpty(){
        User result1 = new User(1);
        result1.setId("user1");
        User result2 = new User(2);
        result2.setId("user2");
        User result3 = new User(3);
        result3.setId("user3");
        User result4 = new User(4);
        result4.setId("user4");

        assertEquals(result1,wkUserCache.findByIdAndNotCheckEmpty("user1"));
        assertEquals(result2,wkUserCache.findByIdAndNotCheckEmpty("user2"));
        assertEquals(result3,wkUserCache.findByIdAndNotCheckEmpty("user3"));
        assertEquals(result4,wkUserCache.findByIdAndNotCheckEmpty("user4"));
        assertEquals(null,wkUserCache.findByIdAndNotCheckEmpty("user5555555"));
        assertNull(wkUserCache.findByIdAndNotCheckEmpty("user9999999"));
    }


    @Test
    void findByUUID(){
        User result1 = new User(1);
        result1.setId("user1");
        result1.setUuid("aaaa-0001");
        User result2 = new User(2);
        result2.setId("user2");
        result2.setUuid("aaaa-0002");
        User result3 = new User(3);
        result3.setId("user3");
        result3.setUuid("aaaa-0003");
        User result4 = new User(4);
        result4.setId("user4");
        result4.setUuid("aaaa-0004");

        assertEquals(result1,wkUserCache.findByUUID("aaaa-0001"));
        assertEquals(result2,wkUserCache.findByUUID("aaaa-0002"));
        assertEquals(result3,wkUserCache.findByUUID("aaaa-0003"));
        assertEquals(result4,wkUserCache.findByUUID("aaaa-0004"));
        assertNull(wkUserCache.findByUUID("xxxxxxxxxxx"));
        assertNull(wkUserCache.findByUUID("aaaa-0005"));
    }


    @Test
    void findUsersByPrimaryOrgSid(){
        User result1 = new User(1);
        result1.setId("user1");
        result1.setUuid("aaaa-0001");
        User result2 = new User(2);
        result2.setId("user2");
        result2.setUuid("aaaa-0002");
        User result3 = new User(3);
        result3.setId("user3");
        result3.setUuid("aaaa-0003");
        User result4 = new User(4);
        result4.setId("user4");
        result4.setUuid("aaaa-0004");

        assertEquals(Lists.newArrayList(result1),wkUserCache.findUsersByPrimaryOrgSid(1));
        assertEquals(Lists.newArrayList(result4),wkUserCache.findUsersByPrimaryOrgSid(4));
        assertEquals(Lists.newArrayList(),wkUserCache.findUsersByPrimaryOrgSid(88888));
    }



    @Test
    void findPrimaryOrgSidByUserSid(){
        assertEquals(1,wkUserCache.findPrimaryOrgSidByUserSid(1));
        assertEquals(2,wkUserCache.findPrimaryOrgSidByUserSid(2));
        assertEquals(3,wkUserCache.findPrimaryOrgSidByUserSid(3));
        assertNull(wkUserCache.findPrimaryOrgSidByUserSid(888777));
    }



    @Test
    void getUsersByPrimaryOrgSid(){
        assertEquals(Lists.newArrayList(new User(1)),wkUserCache.getUsersByPrimaryOrgSid(1));
        assertEquals(Lists.newArrayList(new User(2)),wkUserCache.getUsersByPrimaryOrgSid(2));
        assertEquals(Lists.newArrayList(new User(4)),wkUserCache.getUsersByPrimaryOrgSid(4));

        assertNull(wkUserCache.getUsersByPrimaryOrgSid(null));
        assertEquals(Lists.newArrayList(),wkUserCache.getUsersByPrimaryOrgSid(8888888));
    }



    @Test
    void findByPrimaryOrgSidsIn(){
        List<User> result1=Lists.newArrayList(new User(1));
        List<User> result234=Lists.newArrayList(new User(2),new User(3),new User(4));
        List<User> result1234=Lists.newArrayList(new User(1),new User(2),new User(3),new User(4));
        assertEquals(Lists.newArrayList(result1),wkUserCache.findByPrimaryOrgSidsIn(Lists.newArrayList(1)));
        assertEquals(Lists.newArrayList(result234),wkUserCache.findByPrimaryOrgSidsIn(Lists.newArrayList(2,3,4)));
        assertEquals(Lists.newArrayList(result1234),wkUserCache.findByPrimaryOrgSidsIn(Lists.newArrayList(1,2,3,4)));

        assertEquals(Lists.newArrayList(),wkUserCache.findByPrimaryOrgSidsIn(Lists.newArrayList(888888)));
        assertEquals(Lists.newArrayList(),wkUserCache.findByPrimaryOrgSidsIn(Lists.newArrayList()));
    }



    @Deprecated // 請改用 findUserWithManagerByOrgSids
    @Test
    void findByOrgsWithManager(){
        List<User> result1=Lists.newArrayList(new User(1));
        List<User> result234=Lists.newArrayList(new User(2),new User(3),new User(4));
        List<User> result1234=Lists.newArrayList(new User(1),new User(2),new User(3),new User(4));

        assertEquals(result1,wkUserCache.findByOrgsWithManager(Lists.newArrayList(1),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(result1),wkUserCache.findByOrgsWithManager(Lists.newArrayList(1),null));
        assertEquals(Lists.newArrayList(result1234),wkUserCache.findByOrgsWithManager(Lists.newArrayList(1,2,3,4),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(),wkUserCache.findByOrgsWithManager(Lists.newArrayList(),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(),wkUserCache.findByOrgsWithManager(Lists.newArrayList(888888),Activation.ACTIVE));
    }



    @Test
    void findUserWithManagerByOrgSids(){
        List<User> result1=Lists.newArrayList(new User(1));
        List<User> result234=Lists.newArrayList(new User(2),new User(3),new User(4));
        List<User> result1234=Lists.newArrayList(new User(1),new User(2),new User(3),new User(4));

        assertEquals(Lists.newArrayList(result1),wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(1),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(result1),wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(1),null));
        assertEquals(Lists.newArrayList(result1234),wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(1,2,3,4),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(),wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(),Activation.ACTIVE));
        assertEquals(Lists.newArrayList(),wkUserCache.findUserWithManagerByOrgSids(Lists.newArrayList(888888),Activation.ACTIVE));
    }



    @Test
    void findUserSidByOrgsWithManager(){
        assertEquals(Sets.newHashSet(1),wkUserCache.findUserSidByOrgsWithManager(Lists.newArrayList(1),Activation.ACTIVE));
        assertEquals(Sets.newHashSet(1),wkUserCache.findUserSidByOrgsWithManager(Lists.newArrayList(1),null));
        assertEquals(Sets.newHashSet(1,2,3,4),wkUserCache.findUserSidByOrgsWithManager(Lists.newArrayList(1,2,3,4),Activation.ACTIVE));
        assertEquals(Sets.newHashSet(),wkUserCache.findUserSidByOrgsWithManager(Lists.newArrayList(),Activation.ACTIVE));
        assertEquals(Sets.newHashSet(),wkUserCache.findUserSidByOrgsWithManager(Lists.newArrayList(99999999),Activation.ACTIVE));
    }



    @Test
    void findSecAuth(){
        // 待了解
        // List<SecAuth> secAuths = userClient.findSecAuth();
    }



    @Test
    void findAllUserByCompID(){
        List<User> result1=Lists.newArrayList(new User(1));
        List<User> result234=Lists.newArrayList(new User(2),new User(3),new User(4));
        List<User> result1234=Lists.newArrayList(new User(1),new User(2),new User(3),new User(4));
        assertEquals(result1,wkUserCache.findAllUserByCompID("TG"));
        assertEquals(Lists.newArrayList(),wkUserCache.findAllUserByCompID("BBIN"));
    }



    @Test
    void findAllUserSidByCompID(){
        assertEquals(Sets.newHashSet(1),wkUserCache.findAllUserSidByCompID("TG"));
        assertEquals(Sets.newHashSet(),wkUserCache.findAllUserSidByCompID("BBIN"));
    }



    @Test
    void updateCache(){
        Mockito.verify(userClient,times(0)).updateCache();
        wkUserCache.updateCache();
        Mockito.verify(userClient,times(1)).updateCache();
        wkUserCache.updateCache();
        wkUserCache.updateCache();
        Mockito.verify(userClient,times(3)).updateCache();
    }




}
