package com.cy.work.common.cache;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.OrgSort;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.system.rest.client.DomainClient;
import com.cy.system.rest.client.OrgClient;
import com.cy.system.rest.client.UserClient;
import com.cy.system.rest.client.vo.SecAuth;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkOrgUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.hibernate.mapping.Collection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class WkOrgCacheTest {

    @InjectMocks
    private WkOrgCache wkOrgCache;

    @Mock
    private OrgClient orgClient;

    @Mock
    private DomainClient domainClient;

    @Mock
    private UserClient userClient;

    private final String companyId1 = "TG";
    private final String companyId2 = "XS";
    private final int companySid1 = 1;
    private final int companySid2 = 5;

    @BeforeAll
    static void initStatic(){
        Mockito.mockStatic(WkCommonCache.class);
        Mockito.mockStatic(WkOrgUtils.class);

        WkCommonCache wkCommonCache = Mockito.mock(WkCommonCache.class);
        WkOrgUtils wkOrgUtils = Mockito.mock(WkOrgUtils.class);

        lenient().when(WkCommonCache.getInstance()).thenReturn(wkCommonCache);
        lenient().when(wkCommonCache.getCache(anyString(), anyList(), anyLong() )).thenReturn(null);

        lenient().when(wkOrgUtils.getOrgName(new Org(1))).thenReturn("TG");
        lenient().when(wkOrgUtils.getOrgName(new Org(2))).thenReturn("testParentOrg");
        lenient().when(wkOrgUtils.getOrgName(new Org(3))).thenReturn("testOrg1");
        lenient().when(wkOrgUtils.getOrgName(new Org(5))).thenReturn("XS");

        lenient().when(wkOrgUtils.sortOrgByOrgTree(Lists.newArrayList(new Org(1),
                new Org(2), new Org(3),
                new Org(4), new Org( 5),
                new Org(6), new Org( 7),
                new Org(8), new Org( 9),
                new Org( 10)))).thenReturn(Lists.newArrayList(new Org(1),
                new Org(2), new Org(3),
                new Org(4), new Org( 5),
                new Org(6), new Org( 7),
                new Org(8), new Org( 9),
                new Org( 10)));

        lenient().when(wkOrgUtils.sortOrgByOrgTree(Lists.newArrayList(
                new Org(3),new Org(4),new Org(8),new Org( 10)))).thenReturn(Lists.newArrayList(new Org(3),
                new Org(4),new Org(8),new Org( 10)));

        lenient().when(wkOrgUtils.sortOrgByOrgTree(Lists.newArrayList(
                new Org(2),new Org(3),new Org(4),new Org(8),new Org( 10)))).thenReturn(Lists.newArrayList(new Org(2),new Org(3),
                new Org(4),new Org(8),new Org( 10)));


        lenient().when(wkOrgUtils.sortOrgByOrgTree(Lists.newArrayList(
               new Org( 10)))).thenReturn(Lists.newArrayList(new Org( 10)));

        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(1)).thenReturn(Sets.newHashSet(100,101));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(2)).thenReturn(Sets.newHashSet(200,201));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(3)).thenReturn(Sets.newHashSet(300,301));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(4)).thenReturn(Sets.newHashSet(400,401));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(5)).thenReturn(Sets.newHashSet(500,501));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(6)).thenReturn(Sets.newHashSet(600,601));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(7)).thenReturn(Sets.newHashSet(700,701));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(8)).thenReturn(Sets.newHashSet(800,801));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(9)).thenReturn(Sets.newHashSet(900,901));
        lenient().when(wkOrgUtils.findOrgManagerUserSidByOrgSid(10)).thenReturn(Sets.newHashSet(100,101));

        lenient().when(wkOrgUtils.getOrgLevelOrder(OrgLevel.THE_PANEL)).thenReturn(0);
        lenient().when(wkOrgUtils.getOrgLevelOrder(OrgLevel.MINISTERIAL)).thenReturn(1);
        lenient().when(wkOrgUtils.getOrgLevelOrder(OrgLevel.DIVISION_LEVEL)).thenReturn(2);
        lenient().when(wkOrgUtils.getOrgLevelOrder(OrgLevel.GROUPS)).thenReturn(3);


        lenient().when(wkOrgUtils.isActive(new Org(1))).thenReturn(false);
    }

    /**C D M P
     * 1-2-3
     *    -4-10
     *    -8(X)
     * 5-6-7
     *    -9(X)
     */
    /**
     * 1: 100,101
     * 2: 200,201
     * 3: 300,301
     * 4: 400,401
     * 5: 500,501
     * 6: 600,601
     * 7: 700,701
     * 8: 800,801
     * 9: 900,901
     */
    @BeforeEach
    void initOrgClient() {
        SimpleUser suser100 = new SimpleUser(100);
        SimpleUser suser101 = new SimpleUser(101);
        SimpleUser suser200 = new SimpleUser(200);
        SimpleUser suser201 = new SimpleUser(201);
        SimpleUser suser300 = new SimpleUser(300);
        SimpleUser suser301 = new SimpleUser(301);
        SimpleUser suser400 = new SimpleUser(400);
        SimpleUser suser401 = new SimpleUser(401);
        SimpleUser suser500 = new SimpleUser(500);
        SimpleUser suser501 = new SimpleUser(501);
        SimpleUser suser600 = new SimpleUser(600);
        SimpleUser suser601 = new SimpleUser(601);
        SimpleUser suser700 = new SimpleUser(700);
        SimpleUser suser701 = new SimpleUser(701);
        SimpleUser suser800 = new SimpleUser(800);
        SimpleUser suser801 = new SimpleUser(801);
        SimpleUser suser900 = new SimpleUser(900);
        SimpleUser suser901 = new SimpleUser(901);

        Org company = new Org();
        company.setSid(companySid1);
        company.setId(companyId1);
        company.setName("company");
        company.setParent(null);
        company.setCompany(company.toSimple());
        company.setType(OrgType.HEAD_OFFICE);
        company.setStatus(Activation.ACTIVE);
        company.setLevel(null);
        company.setManagers(Lists.newArrayList(suser100,suser101));


        Org testParentOrg = new Org();
        testParentOrg.setSid(2);
        testParentOrg.setName("testParentOrg");
        testParentOrg.setId("testParentOrg");
        testParentOrg.setParent(company.toSimple());
        testParentOrg.setCompany(company.toSimple());
        testParentOrg.setType(OrgType.DEPARTMENT);
        testParentOrg.setStatus(Activation.ACTIVE);
        testParentOrg.setLevel(OrgLevel.DIVISION_LEVEL);
        testParentOrg.setManagers(Lists.newArrayList(suser200,suser201));

        Org testOrg1 = new Org();
        testOrg1.setSid(3);
        testOrg1.setName("testOrg1");
        testOrg1.setId("testOrg1");
        testOrg1.setParent(testParentOrg.toSimple());
        testOrg1.setCompany(company.toSimple());
        testOrg1.setType(OrgType.DEPARTMENT);
        testOrg1.setStatus(Activation.ACTIVE);
        testOrg1.setLevel(OrgLevel.MINISTERIAL);
        testOrg1.setManagers(Lists.newArrayList(suser300,suser301));

        Org testOrg2 = new Org();
        testOrg2.setName("testOrg2");
        testOrg2.setSid(4);
        testOrg2.setId("testOrg2");
        testOrg2.setParent(testParentOrg.toSimple());
        testOrg2.setCompany(company.toSimple());
        testOrg2.setType(OrgType.DEPARTMENT);
        testOrg2.setStatus(Activation.ACTIVE);
        testOrg2.setLevel(OrgLevel.MINISTERIAL);
        testOrg2.setManagers(Lists.newArrayList(suser400,suser401));

        Org testOrg4 = new Org();
        testOrg4.setName("testOrg4");
        testOrg4.setSid(8);
        testOrg4.setId("testOrg4");
        testOrg4.setParent(testParentOrg.toSimple());
        testOrg4.setCompany(company.toSimple());
        testOrg4.setType(OrgType.DEPARTMENT);
        testOrg4.setStatus(Activation.INACTIVE);
        testOrg4.setLevel(OrgLevel.MINISTERIAL);
        testOrg4.setManagers(Lists.newArrayList(suser800,suser801));

        Org testOrg6 = new Org();
        testOrg6.setName("testOrg10");
        testOrg6.setId("testOrg10");
        testOrg6.setSid(10);
        testOrg6.setParent(testOrg2.toSimple());
        testOrg6.setCompany(company.toSimple());
        testOrg6.setType(OrgType.DEPARTMENT);
        testOrg6.setStatus(Activation.ACTIVE);
        testOrg6.setLevel(OrgLevel.THE_PANEL);
        testOrg6.setManagers(Lists.newArrayList(suser100,suser101));


        Org company2 = new Org();
        company2.setSid(companySid2);
        company2.setId(this.companyId2);
        company2.setName("company2");
        company2.setParent(null);
        company2.setCompany(company2.toSimple());
        company2.setType(OrgType.HEAD_OFFICE);
        company2.setStatus(Activation.ACTIVE);
        company2.setLevel(null);
        company2.setManagers(Lists.newArrayList(suser500,suser501));

        Org testParentOrg2 = new Org();
        testParentOrg2.setSid(6);
        testParentOrg2.setName("testParentOrg2");
        testParentOrg2.setId("testParentOrg2");
        testParentOrg2.setParent(company2.toSimple());
        testParentOrg2.setCompany(company2.toSimple());
        testParentOrg2.setType(OrgType.DEPARTMENT);
        testParentOrg2.setStatus(Activation.ACTIVE);
        testParentOrg2.setLevel(OrgLevel.DIVISION_LEVEL);
        testParentOrg2.setManagers(Lists.newArrayList(suser600,suser601));

        Org testOrg3 = new Org();
        testOrg3.setSid(7);
        testOrg3.setName("testOrg3");
        testOrg3.setId("testOrg3");
        testOrg3.setParent(testParentOrg2.toSimple());
        testOrg3.setCompany(company2.toSimple());
        testOrg3.setType(OrgType.DEPARTMENT);
        testOrg3.setStatus(Activation.ACTIVE);
        testOrg3.setLevel(OrgLevel.MINISTERIAL);
        testOrg3.setManagers(Lists.newArrayList(suser700,suser701));

        Org testOrg5 = new Org();
        testOrg5.setSid(9);
        testOrg5.setName("testOrg5");
        testOrg5.setId("testOrg5");
        testOrg5.setParent(testParentOrg2.toSimple());
        testOrg5.setCompany(company2.toSimple());
        testOrg5.setType(OrgType.DEPARTMENT);
        testOrg5.setStatus(Activation.INACTIVE);
        testOrg5.setLevel(OrgLevel.MINISTERIAL);
        testOrg5.setManagers(Lists.newArrayList(suser900,suser901));

        lenient().when(orgClient.findAll()).thenReturn(Lists.newArrayList(company, testParentOrg, testOrg1, testOrg2, company2, testParentOrg2, testOrg3, testOrg4, testOrg5, testOrg6));

        lenient().when(orgClient.findAllChildrenByParentSid(1)).thenReturn(Lists.newArrayList(testParentOrg, testOrg1, testOrg2, testOrg4, testOrg6));
        lenient().when(orgClient.findAllChildrenByParentSid(2)).thenReturn(Lists.newArrayList(testOrg1, testOrg2, testOrg4, testOrg6));
        lenient().when(orgClient.findAllChildrenByParentSid(3)).thenReturn(Lists.newArrayList());
        lenient().when(orgClient.findAllChildrenByParentSid(4)).thenReturn(Lists.newArrayList(testOrg6));
        lenient().when(orgClient.findAllChildrenByParentSid(5)).thenReturn(Lists.newArrayList(testParentOrg2, testOrg3, testOrg5));
        lenient().when(orgClient.findAllChildrenByParentSid(6)).thenReturn(Lists.newArrayList(testOrg3, testOrg5));
        lenient().when(orgClient.findAllChildrenByParentSid(7)).thenReturn(Lists.newArrayList());
        lenient().when(orgClient.findAllChildrenByParentSid(8)).thenReturn(Lists.newArrayList());
        lenient().when(orgClient.findAllChildrenByParentSid(9)).thenReturn(Lists.newArrayList());

        lenient().when(orgClient.findBySid(1)).thenReturn(Optional.of(company));
        lenient().when(orgClient.findBySid(2)).thenReturn(Optional.of(testParentOrg));
        lenient().when(orgClient.findBySid(3)).thenReturn(Optional.of(testOrg1));
        lenient().when(orgClient.findBySid(4)).thenReturn(Optional.of(testOrg2));
        lenient().when(orgClient.findBySid(5)).thenReturn(Optional.of(company2));
        lenient().when(orgClient.findBySid(6)).thenReturn(Optional.of(testParentOrg2));
        lenient().when(orgClient.findBySid(7)).thenReturn(Optional.of(testOrg3));
        lenient().when(orgClient.findBySid(8)).thenReturn(Optional.of(testOrg4));
        lenient().when(orgClient.findBySid(9)).thenReturn(Optional.of(testOrg5));
        lenient().when(orgClient.findBySid(10)).thenReturn(Optional.of(testOrg6));

        lenient().when(orgClient.findById(this.companyId1)).thenReturn(Optional.of(company));
        lenient().when(orgClient.findById("testParentOrg")).thenReturn(Optional.of(testParentOrg));
        lenient().when(orgClient.findById("testOrg1")).thenReturn(Optional.of(testOrg1));
        lenient().when(orgClient.findById("testOrg2")).thenReturn(Optional.of(testOrg2));
        lenient().when(orgClient.findById(this.companyId2)).thenReturn(Optional.of(company2));
        lenient().when(orgClient.findById("testParentOrg2")).thenReturn(Optional.of(testParentOrg2));
        lenient().when(orgClient.findById("testOrg3")).thenReturn(Optional.of(testOrg3));
        lenient().when(orgClient.findById("testOrg4")).thenReturn(Optional.of(testOrg4));
        lenient().when(orgClient.findById("testOrg5")).thenReturn(Optional.of(testOrg5));
        lenient().when(orgClient.findById("testOrg6")).thenReturn(Optional.of(testOrg6));

        lenient().when(orgClient.findOrgSort()).thenReturn(Lists.newArrayList(new OrgSort(1,1),
                new OrgSort(2, 2), new OrgSort(3, 3),
                new OrgSort(4, 4), new OrgSort(10, 5),
                new OrgSort(8, 6), new OrgSort(5, 7),
                new OrgSort(6, 8), new OrgSort(7, 9),
                new OrgSort(9, 10)));

        lenient().when(orgClient.findChildrenByCache(1)).thenReturn(Lists.newArrayList(testParentOrg,testOrg1,testOrg2,testOrg4,testOrg6));
        lenient().when(orgClient.findChildrenByCache(2)).thenReturn(Lists.newArrayList(testOrg1,testOrg2,testOrg4,testOrg6));
        lenient().when(orgClient.findChildrenByCache(4)).thenReturn(Lists.newArrayList(testOrg6));

        lenient().when(orgClient.findCompanies(Activation.ACTIVE)).thenReturn(Lists.newArrayList(company,company2));

    }

    @BeforeEach
    void initDomainClient() throws Exception {
        lenient().when(domainClient.getDomainUrlByWerpCompId("CY")).thenReturn("com.cy");
        lenient().when(domainClient.getDomainUrlByWerpCompId("XS")).thenReturn("com.xs");
    }

    @BeforeEach
    void initUserClient(){
        SecAuth s1 = new SecAuth();
        s1.setCompId("TG");
        SecAuth s2 = new SecAuth();
        s2.setCompId("XS");

        lenient().when(userClient.findSecAuth()).thenReturn(Lists.newArrayList(s1,s2));
    }



    @Test
    void findAll() {
        List<Org> result = wkOrgCache.findAll();
        assertEquals(10, result.size());
    }

    @Test
    void findAllChild() {
        assertEquals(5, wkOrgCache.findAllChild(1).size());
        assertEquals(4, wkOrgCache.findAllChild(2).size());
        assertEquals(0, wkOrgCache.findAllChild(3).size());
        assertEquals(1, wkOrgCache.findAllChild(4).size());
        assertEquals(3,  wkOrgCache.findAllChild(5).size());
        assertEquals(2, wkOrgCache.findAllChild(6).size());
        assertEquals(0, wkOrgCache.findAllChild(7).size());

        List<Org> result999 = wkOrgCache.findAllChild(999);
        assertEquals(0, result999.size());
    }

    @Test
    void findAllChildSids() {
        Set<Integer> result = wkOrgCache.findAllChildSids(1);
        assertArrayEquals(new Integer[]{2, 3, 4, 8, 10}, result.toArray());
        Set<Integer> result2 = wkOrgCache.findAllChildSids(2);
        assertArrayEquals(new Integer[]{3, 4, 8, 10}, result2.toArray());
        Set<Integer> result3 = wkOrgCache.findAllChildSids(3);
        assertArrayEquals(new Integer[]{}, result3.toArray());
        Set<Integer> result4 = wkOrgCache.findAllChildSids(4);
        assertArrayEquals(new Integer[]{10}, result4.toArray());
        Set<Integer> result5 = wkOrgCache.findAllChildSids(5);
        assertArrayEquals(new Integer[]{6, 7, 9}, result5.toArray());
        Set<Integer> result6 = wkOrgCache.findAllChildSids(6);
        assertArrayEquals(new Integer[]{7, 9}, result6.toArray());
        Set<Integer> result7 = wkOrgCache.findAllChildSids(7);
        assertArrayEquals(new Integer[]{}, result7.toArray());

        Set<Integer> result999 = wkOrgCache.findAllChildSids(999);
        assertArrayEquals(new Integer[]{}, result999.toArray());
    }

    @Test
    void testFindAllChildSids() {
        Set<Integer> result = wkOrgCache.findAllChildSids(Lists.newArrayList(1,2));
        assertArrayEquals(new Integer[]{2, 3, 4, 8, 10}, result.toArray());
        Set<Integer> result2 = wkOrgCache.findAllChildSids(Lists.newArrayList(2,3));
        assertArrayEquals(new Integer[]{3, 4, 8, 10}, result2.toArray());
        Set<Integer> result3 = wkOrgCache.findAllChildSids(Lists.newArrayList(3,4));
        assertArrayEquals(new Integer[]{10}, result3.toArray());
        Set<Integer> result4 = wkOrgCache.findAllChildSids(Lists.newArrayList(1,2,3));
        assertArrayEquals(new Integer[]{2, 3, 4, 8, 10}, result4.toArray());

        Set<Integer> result5 = wkOrgCache.findAllChildSids(Lists.newArrayList(4,5));
        assertArrayEquals(new Integer[]{6, 7, 9, 10}, result5.toArray());
        Set<Integer> result6 = wkOrgCache.findAllChildSids(Lists.newArrayList(5));
        assertArrayEquals(new Integer[]{6, 7, 9}, result6.toArray());
        Set<Integer> result7 = wkOrgCache.findAllChildSids(Lists.newArrayList(6));
        assertArrayEquals(new Integer[]{7, 9}, result7.toArray());

        assertEquals(0, wkOrgCache.findAllChildSids(Collections.emptyList()).size());
    }

    @Test
    void findAllDepByCompID() {
        assertEquals(5, wkOrgCache.findAllDepByCompID(this.companyId1).size());
        assertEquals(3, wkOrgCache.findAllDepByCompID(this.companyId2).size());

        assertEquals(0, wkOrgCache.findAllDepByCompID("xxxx").size());
    }

    @Test
    void findAllDepByCompSid() {
        assertEquals(5, wkOrgCache.findAllDepByCompSid(this.companySid1).size());
        assertEquals(3, wkOrgCache.findAllDepByCompSid(this.companySid2).size());

        assertEquals(0, wkOrgCache.findAllDepByCompSid(99999).size());
    }

    @Test
    void findAllActiveDepByCompSid() {
        assertEquals(0, wkOrgCache.findAllActiveDepByCompSid(this.companySid1).size());
        assertEquals(0, wkOrgCache.findAllActiveDepByCompSid(this.companySid2).size());

        assertEquals(0, wkOrgCache.findAllActiveDepByCompSid(99999).size());
    }

    @Test
    void findAllDepSidByCompID() {
        assertEquals(5, wkOrgCache.findAllDepSidByCompID(this.companyId1).size());
        assertEquals(3, wkOrgCache.findAllDepSidByCompID(this.companyId2).size());

        assertEquals(0, wkOrgCache.findAllDepSidByCompID("xxxxx").size());
    }

    @Test
    void findAllDepSidByCompSid() {
        assertEquals(5, wkOrgCache.findAllDepSidByCompSid(this.companySid1).size());
        assertEquals(3, wkOrgCache.findAllDepSidByCompSid(this.companySid2).size());

        assertEquals(0, wkOrgCache.findAllDepSidByCompSid(9999).size());
    }

    @Test
    void findAllOrgOrderSeqMap() {
        Map<Integer, Integer> result1 =  wkOrgCache.findAllOrgOrderSeqMap();
        assertEquals(10, result1.size());
        assertEquals(1, result1.get(1));
        assertEquals(2, result1.get(2));
        assertEquals(3, result1.get(3));
        assertEquals(4, result1.get(4));
        assertEquals(5, result1.get(10));
        assertEquals(6, result1.get(8));
        assertEquals(7, result1.get(5));
        assertEquals(8, result1.get(6));
        assertEquals(9, result1.get(7));
        assertEquals(10, result1.get(9));
    }

    @Test
    void findAllParent() {
        // 只能是 OrgType.DEPARTMENT
        assertEquals(0, wkOrgCache.findAllParent(1).size());
        assertEquals(0, wkOrgCache.findAllParent(2).size());
        assertEquals(1, wkOrgCache.findAllParent(3).size());
        assertEquals(1, wkOrgCache.findAllParent(4).size());
        assertEquals(1, wkOrgCache.findAllParent(8).size());
        assertEquals(0, wkOrgCache.findAllParent(5).size());
        assertEquals(0, wkOrgCache.findAllParent(6).size());
        assertEquals(1, wkOrgCache.findAllParent(7).size());
        assertEquals(1, wkOrgCache.findAllParent(9).size());
        assertEquals(2, wkOrgCache.findAllParent(10).size());

        assertEquals(0, wkOrgCache.findAllParent(999).size());
    }

    @Test
    void testFindAllParent() {
        assertEquals(0, wkOrgCache.findAllParent(wkOrgCache.findBySid(1)).size());
        assertEquals(0, wkOrgCache.findAllParent(wkOrgCache.findBySid(2)).size());
        assertEquals(1, wkOrgCache.findAllParent(wkOrgCache.findBySid(3)).size());
        assertEquals(1, wkOrgCache.findAllParent(wkOrgCache.findBySid(4)).size());
        assertEquals(1, wkOrgCache.findAllParent(wkOrgCache.findBySid(8)).size());
        assertEquals(0, wkOrgCache.findAllParent(wkOrgCache.findBySid(5)).size());
        assertEquals(0, wkOrgCache.findAllParent(wkOrgCache.findBySid(6)).size());
        assertEquals(1, wkOrgCache.findAllParent(wkOrgCache.findBySid(7)).size());
        assertEquals(1, wkOrgCache.findAllParent(wkOrgCache.findBySid(9)).size());
        assertEquals(2, wkOrgCache.findAllParent(wkOrgCache.findBySid(10)).size());

        assertEquals(0, wkOrgCache.findAllParent(new Org()).size());
    }

    @Test
    void findAllParentSid() {
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(1).toArray());
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(2).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(3).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(4).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(8).toArray());
        assertArrayEquals(new Integer[]{2, 4}, wkOrgCache.findAllParentSid(10).toArray());

        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(5).toArray());
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(6).toArray());
        assertArrayEquals(new Integer[]{6}, wkOrgCache.findAllParentSid(7).toArray());
        assertArrayEquals(new Integer[]{6}, wkOrgCache.findAllParentSid(9).toArray());

        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(9999).toArray());

    }

    @Test
    void testFindAllParentSid() {
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(1)).toArray());
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(2)).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(3)).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(4)).toArray());
        assertArrayEquals(new Integer[]{2}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(8)).toArray());
        assertArrayEquals(new Integer[]{2, 4}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(10)).toArray());

        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(5)).toArray());
        assertArrayEquals(new Integer[]{}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(6)).toArray());
        assertArrayEquals(new Integer[]{6}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(7)).toArray());
        assertArrayEquals(new Integer[]{6}, wkOrgCache.findAllParentSid(wkOrgCache.findBySid(9)).toArray());

        assertEquals(0, wkOrgCache.findAllParentSid(new Org()).size());

    }

    @Test
    void testFindAllParent1() {
        // topLevel以下的直系部門
        assertEquals(0, wkOrgCache.findAllParent(1, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(0, wkOrgCache.findAllParent(2, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(3, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(4, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(8, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(0, wkOrgCache.findAllParent(5, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(0, wkOrgCache.findAllParent(6, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(7, OrgLevel.DIVISION_LEVEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(9, OrgLevel.DIVISION_LEVEL ).size());

        assertEquals(0, wkOrgCache.findAllParent(1, OrgLevel.MINISTERIAL ).size());
        assertEquals(0, wkOrgCache.findAllParent(2, OrgLevel.MINISTERIAL ).size());
        assertEquals(1, wkOrgCache.findAllParent(3, OrgLevel.MINISTERIAL ).size());
        assertEquals(1, wkOrgCache.findAllParent(4, OrgLevel.MINISTERIAL ).size());
        assertEquals(2, wkOrgCache.findAllParent(10, OrgLevel.MINISTERIAL ).size());

        assertEquals(1, wkOrgCache.findAllParent(4, OrgLevel.THE_PANEL ).size());
        assertEquals(2, wkOrgCache.findAllParent(10, OrgLevel.THE_PANEL ).size());
        assertEquals(1, wkOrgCache.findAllParent(4, OrgLevel.GROUPS ).size());
        assertEquals(2, wkOrgCache.findAllParent(10, OrgLevel.GROUPS ).size());

        assertEquals(0, wkOrgCache.findAllParent(null, OrgLevel.DIVISION_LEVEL ).size());
    }

    @Test
    void findById() {
        assertEquals(1, wkOrgCache.findById(this.companyId1).getSid());
        assertEquals(3, wkOrgCache.findById("testOrg1").getSid());
        assertEquals(8, wkOrgCache.findById("testOrg4").getSid());
        assertEquals(5, wkOrgCache.findById(this.companyId2).getSid());

        assertNull(wkOrgCache.findById("XXXXXX"));
        assertNull(wkOrgCache.findById(null));

    }

    @Test
    void findByIdAndNotCheckEmpty() {
        assertEquals(1, wkOrgCache.findByIdAndNotCheckEmpty(this.companyId1).getSid());
        assertEquals(3, wkOrgCache.findByIdAndNotCheckEmpty("testOrg1").getSid());
        assertEquals(8, wkOrgCache.findByIdAndNotCheckEmpty("testOrg4").getSid());
        assertEquals(5, wkOrgCache.findByIdAndNotCheckEmpty(this.companyId2).getSid());

        assertNull(wkOrgCache.findByIdAndNotCheckEmpty("XXXXXX"));
        assertNull(wkOrgCache.findByIdAndNotCheckEmpty(null));
    }

    @Test
    void findBySid() {
        assertEquals(1, wkOrgCache.findBySid(1).getSid());
        assertEquals(3, wkOrgCache.findBySid(3).getSid());
        assertEquals(8, wkOrgCache.findBySid(8).getSid());
        assertEquals(5, wkOrgCache.findBySid(5).getSid());

        assertNull(wkOrgCache.findBySid(999));

    }

    @Test
    void testFindBySid() {
        assertEquals(1, wkOrgCache.findBySid("1").getSid());
        assertEquals(3, wkOrgCache.findBySid("3").getSid());
        assertEquals(8, wkOrgCache.findBySid("8").getSid());
        assertEquals(5, wkOrgCache.findBySid("5").getSid());

        assertNull(wkOrgCache.findBySid("999"));
        assertNull(wkOrgCache.findBySid("test"));

    }

    @Test
    void findBySids() {
        assertEquals(1, wkOrgCache.findBySids(Arrays.asList(1)).get(0).getSid());
    }

    // 問題
    @Test
    void findCompanies() {
        Org company = new Org();
        company.setSid(companySid1);
        company.setId(companyId1);
        company.setName("company");
        company.setParent(null);
        company.setCompany(company.toSimple());
        company.setType(OrgType.HEAD_OFFICE);
        company.setStatus(Activation.ACTIVE);
        company.setLevel(null);

        Org company2 = new Org();
        company2.setSid(companySid2);
        company2.setId(this.companyId2);
        company2.setName("company2");
        company2.setParent(null);
        company2.setCompany(company2.toSimple());
        company2.setType(OrgType.HEAD_OFFICE);
        company2.setStatus(Activation.ACTIVE);
        company2.setLevel(null);

        assertEquals(Lists.newArrayList(company,company2),wkOrgCache.findCompanies());
    }

    @Test
    void findDirectManagerUserSids() {
        // 只會找到 OrgType.DEPARTMENT
        // 如果一開始找的部門 OrgType 不是 OrgType.DEPARTMENT，直接找出該部門的主管
        assertEquals(Sets.newHashSet(100,101),wkOrgCache.findDirectManagerUserSids(1));
        assertEquals(Sets.newHashSet(200,201,800,801),wkOrgCache.findDirectManagerUserSids(8));
        assertEquals(Sets.newHashSet(600,601,900,901),wkOrgCache.findDirectManagerUserSids(9));
        assertEquals(Sets.newHashSet(200,201,400,401,100,101),wkOrgCache.findDirectManagerUserSids(10));
        assertEquals(Sets.newHashSet(),wkOrgCache.findDirectManagerUserSids(777777));
    }

    @Test
    void findDomainUrlByCompId() throws SystemOperationException {
        assertEquals("com.cy",wkOrgCache.findDomainUrlByCompId("CY"));
        assertEquals("com.xs",wkOrgCache.findDomainUrlByCompId("XS"));
        assertEquals(null,wkOrgCache.findDomainUrlByCompId("BBIN"));
        assertEquals(null,wkOrgCache.findDomainUrlByCompId("test04vip"));
    }

    // String result = this.domainClient.getDomainUrlByWerpCompId(compId);
    // 不清楚這個字串，正常時會回傳甚麼東西
    @Test
    void isExistDomainUrlByCompId() throws SystemOperationException {
        // domain
        assertEquals(true,wkOrgCache.isExistDomainUrlByCompId("CY"));
        assertEquals(true,wkOrgCache.isExistDomainUrlByCompId("XS"));
        assertEquals(false,wkOrgCache.isExistDomainUrlByCompId("BBIN"));
    }

    @Test
    void findManagerOrgs() {
        Org company = new Org();
        company.setSid(companySid1);
        company.setId(companyId1);
        company.setName("company");
        company.setParent(null);
        company.setCompany(company.toSimple());
        company.setType(OrgType.HEAD_OFFICE);
        company.setStatus(Activation.ACTIVE);
        company.setLevel(null);

//        assertEquals(Lists.newArrayList(company),wkOrgCache.findManagerOrgs(100));
    }

    @Test
    void findManagerOrgSids() {
        assertEquals(Sets.newHashSet(1,10),wkOrgCache.findManagerOrgSids(100));
        assertEquals(Sets.newHashSet(1,10),wkOrgCache.findManagerOrgSids(101));
        assertEquals(Sets.newHashSet(2),wkOrgCache.findManagerOrgSids(201));
        assertEquals(Sets.newHashSet(8),wkOrgCache.findManagerOrgSids(800));

        assertEquals(Sets.newHashSet(),wkOrgCache.findManagerOrgSids(99999));
    }

    @Test
    void findManagerWithChildOrgSids() {
        assertEquals(Sets.newHashSet(1,2,3,4,8,10),wkOrgCache.findManagerWithChildOrgSids(100));
        assertEquals(Sets.newHashSet(3),wkOrgCache.findManagerWithChildOrgSids(301));
        assertEquals(Sets.newHashSet(4,10),wkOrgCache.findManagerWithChildOrgSids(400));
        assertEquals(Sets.newHashSet(5,6,7,9),wkOrgCache.findManagerWithChildOrgSids(501));
        assertEquals(Sets.newHashSet(),wkOrgCache.findManagerWithChildOrgSids(888888));
    }

    // 問題
    @Test
    void findNameBySid() {
        assertEquals("TG",wkOrgCache.findNameBySid(1));
        assertEquals("testParentOrg",wkOrgCache.findNameBySid(2));
        assertEquals("XS",wkOrgCache.findNameBySid(5));

        assertEquals(null,wkOrgCache.findNameBySid(100));
    }

    @Test
    void testFindNameBySid() {
        assertEquals("TG",wkOrgCache.findNameBySid("1"));
        assertEquals("testParentOrg",wkOrgCache.findNameBySid(" 2 "));
        assertEquals("XS",wkOrgCache.findNameBySid("    5"));

        assertEquals(null,wkOrgCache.findNameBySid("TG"));
        assertNull(wkOrgCache.findNameBySid("XS"));
    }

    @Test
    void findOrgOrderSeq() {
        assertEquals(1,wkOrgCache.findOrgOrderSeq(new Org(1)));
        assertEquals(2,wkOrgCache.findOrgOrderSeq(new Org(2)));
        assertEquals(7,wkOrgCache.findOrgOrderSeq(new Org(5)));
        assertEquals(9,wkOrgCache.findOrgOrderSeq(new Org(7)));
    }

    @Test
    void testFindOrgOrderSeq() {
        assertEquals(1,wkOrgCache.findOrgOrderSeq(new SimpleOrg(1)));
        assertEquals(7,wkOrgCache.findOrgOrderSeq(new SimpleOrg(5)));
        assertEquals(10,wkOrgCache.findOrgOrderSeq(new SimpleOrg(9)));

        assertEquals(Integer.MAX_VALUE,wkOrgCache.findOrgOrderSeq(new SimpleOrg(100)));
    }

    @Test
    void findOrgOrderSeqBySid() {
        assertEquals(1,wkOrgCache.findOrgOrderSeqBySid(1));
        assertEquals(5,wkOrgCache.findOrgOrderSeqBySid(10));
    }

    @Test
    void findOrgSortNumberMapByDepSid() {
        Map<Integer,Integer> result = new HashMap(){
            {
                put(1,1);put(2,2);put(3,3);put(4,4);put(10,5);
                put(8,6);put(5,7);put(6,8);put(7,9);put(9,10);
            }
        };
        assertEquals(result,wkOrgCache.findOrgSortNumberMapByDepSid());
    }

    @Test
    void findParentByOrgLevel() {
        assertEquals(2,wkOrgCache.findParentByOrgLevel(2,OrgLevel.DIVISION_LEVEL));
        assertEquals(2,wkOrgCache.findParentByOrgLevel(10,OrgLevel.DIVISION_LEVEL));
        assertEquals(null,wkOrgCache.findParentByOrgLevel(1,OrgLevel.GROUPS));
    }

    @Test
    void getChildOrgs() {
        assertEquals(Lists.newArrayList(new Org( 10)),wkOrgCache.getChildOrgs(4));
    }

    @Test
    void getChildOrgSids() {
        assertEquals(Lists.newArrayList(new Org(2),new Org(3),new Org(4),new Org(8),new Org( 10)),wkOrgCache.getChildOrgs(1));
        assertEquals(Lists.newArrayList(new Org(3),new Org(4),new Org(8),new Org( 10)),wkOrgCache.getChildOrgs(2));
    }

    @Test
    void isExsit() {
        assertEquals(true,wkOrgCache.isExsit(1));
        assertEquals(true,wkOrgCache.isExsit(2));
        assertEquals(true,wkOrgCache.isExsit(3));
        assertEquals(true,wkOrgCache.isExsit(4));
        assertEquals(true,wkOrgCache.isExsit(5));
        assertEquals(true,wkOrgCache.isExsit(6));
        assertEquals(true,wkOrgCache.isExsit(9));
        assertEquals(false,wkOrgCache.isExsit(16));
    }

    @Test
    void isExsitByID() {
        assertEquals(true,wkOrgCache.isExsitByID(companyId1));
        assertEquals(true,wkOrgCache.isExsitByID(companyId2));
        assertEquals(true,wkOrgCache.isExsitByID("testOrg2"));
        assertEquals(false,wkOrgCache.isExsitByID("BB"));
        assertEquals(false,wkOrgCache.isExsitByID("XXXX"));
    }

    @Test
    void updateCache() {
        verify(orgClient,times(0)).updateCache();
        wkOrgCache.updateCache();
        verify(orgClient,times(1)).updateCache();
        wkOrgCache.updateCache();
        wkOrgCache.updateCache();
        verify(orgClient,times(3)).updateCache();
    }

    @Test
    void findAllSecAuth() {
        // userClient
        SecAuth s1 = new SecAuth();
        s1.setCompId("TG");
        SecAuth s2 = new SecAuth();
        s2.setCompId("XS");

        assertEquals(Lists.newArrayList(s1,s2),wkOrgCache.findAllSecAuth());
    }
}