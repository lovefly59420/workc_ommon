package com.rbt.util.exceloperate.exception;

import com.cy.work.common.enums.InfomationLevel;

import lombok.Getter;

/**
 * Excel Operate 套件處理錯誤時拋出
 * 
 * @author Allen
 */
public class ExcelOperateException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 6519843988004132472L;

    @Getter
    protected InfomationLevel level;
    @Getter
    protected String exceptionMessage;

    /**
     * @param message
     */
    public ExcelOperateException(String message) {
        super(message);

        level = InfomationLevel.INFO;
    }

    /**
     * @param message
     * @param level   訊息等級
     */
    public ExcelOperateException(
            String message,
            InfomationLevel level) {
        super(message);
        this.level = level;
    }

    /**
     * @param message          message
     * @param exceptionMessage 系統的 log 訊息
     * @param level            訊息等級
     */
    public ExcelOperateException(
            String message,
            String exceptionMessage,
            InfomationLevel level) {
        super(message);
        this.exceptionMessage = exceptionMessage;
        this.level = level;
    }
}
