package com.cy.system.rest.client;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.MenuItem;
import com.cy.system.rest.client.config.RestConstants;
import com.cy.system.rest.client.exception.SystemRestClientException;
import com.cy.system.rest.client.exception.SystemRestClientRuntimeException;
import com.cy.system.rest.client.rmi.RestRMI;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.net.URI;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

/**
 * 客制 findAll （原 system-rest-client 未提供此方法）
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class WkMenuItemRMI extends RestRMI<MenuItem, Long> {

    public WkMenuItemRMI() {
        super(RestConstants.MENU_ITEM,
                new TypeReference<MenuItem>() {
                },
                new TypeReference<ArrayList<MenuItem>>() {
                });
    }

    public List<MenuItem> findAll(final Activation status) {
        try {
            if (!super.isAlive()) {
                return Lists.newArrayList();
            }
            URI url = getUriBuilder().path("findAll")
                    .queryParam("queryParam", paraWrapper.encryptQueryParameter(
                            createQueryParameter("status", status)))
                    .build()
                    .toUri();
            log.debug(SYSTEM_REST_PATH + " >> " + url.toString());
            return getListResult(url);
        } catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | RestClientException
                | IOException | SystemRestClientException ex) {
            throw new SystemRestClientRuntimeException(ex.getMessage(), ex);
        }
    }
}
