package com.cy.work.common.constant;

/**
 * 需求單固定定義參數
 */
public class WkConstants {

    /**
     * 主管虛擬 SID (當 user_sid 為此值時, 自動轉換為該單位主管)
     */
    public static final Integer MANAGER_VIRTAUL_USER_SID = -1;

    /**
     * 系統管理者 USER SID
     */
    public static final Integer ADMIN_USER_SID = 1;
}
