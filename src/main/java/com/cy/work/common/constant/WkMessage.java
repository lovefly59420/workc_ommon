package com.cy.work.common.constant;

/**
 * 訊息
 */
public class WkMessage {

    // ========================================================================
    // 頁面顯示訊息
    // ========================================================================
    /**
     * 執行失敗，
     */
    public static final String PROCESS_FAILED = "處理失敗！";

    /**
     * 系統錯誤，請恰資訊人員
     */
    public static final String EXECTION = "系統錯誤，請聯繫資訊人員！";

    /**
     * 資料已被異動，請重新刷新頁面！
     */
    public static final String NEED_RELOAD = "資料已被異動，請重新整理頁面！";

    /**
     * 頁面已逾期，請重新刷新頁面！
     */
    public static final String SESSION_TIMEOUT = "頁面已逾期，請重新整理頁面！";

    // ========================================================================
    // 開發使用
    // ========================================================================
    public static final String DEV_NOT_IMPL = "開發時期錯誤，未實做方法！";
}
