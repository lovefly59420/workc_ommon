/**
 * 
 */
package com.cy.work.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author allen1214_wu
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WkFieldSetting {

    /**
     * 顯示的欄位名稱
     * @return display name
     */
    String displayName();
    
    /**
     * 是否不可為空
     * @return not null
     */
    boolean notNull();
}
