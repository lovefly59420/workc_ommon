/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.mbean;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author marlow_chen
 * 限制起訖日期選擇範圍
 * 不是想像中的好用，會有網路延遲，暫不使用   (2018/09/21 aken
 */
@Scope("request")
@Component
public class CalendarLimitMBean {

    @Getter
    @Setter
    private Date minDate;
    @Getter
    @Setter
    private Date maxDate;

    public void triggerEndDateMin(Date maxDate) {
        this.minDate = maxDate;
    }

    public void triggerStartDateMax(Date minDate) {
        this.maxDate = minDate;
    }

}