/**
 * 
 */
package com.cy.work.common.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;
import nl.altindag.ssl.SSLFactory;

/**
 * @author allen1214_wu
 */
@Component
@Slf4j
public class CallApiUtils {

    // 注意, 不要throw SystemAlertException , 否則連線不通時，會陷入發送警告訊息的無窮迴圈

    /**
     * @param urlStr       ULR String
     * @param headerParams header 參數
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    public HttpEntity httpGet(String urlStr, Map<String, String> headerParams) throws SystemOperationException {

        URI uri = null;
        try {
            uri = new URI(urlStr);
        } catch (URISyntaxException e) {
            String message = "傳入網址有誤! [" + urlStr + "]," + e.getMessage();
            log.error(message, e);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        return httpGet(uri, headerParams, null);
    }

    /**
     * HTTP Get
     * 
     * @param uri           URI
     * @param headerParams  header parameter (option 可為 null)
     * @param requestConfig RequestConfig (option 可為 null)
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    public HttpEntity httpGet(
            URI uri,
            Map<String, String> headerParams,
            RequestConfig requestConfig) throws SystemOperationException {

        // ====================================
        // 檢查
        // ====================================
        if (uri == null) {
            String message = "傳入URI為 null ";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // create HTTP get and set param
        // ====================================
        HttpGet get = new HttpGet();
        get.setURI(uri);

        if (headerParams != null && !headerParams.isEmpty()) {
            for (Entry<String, String> headerParam : headerParams.entrySet()) {
                get.addHeader(headerParam.getKey(), headerParam.getValue());
            }
        }

        // ====================================
        // execute
        // ====================================
        return this.execute(requestConfig, get, null, null);

    }

    /**
     * HTTP Get
     * 
     * @param uri           URI
     * @param headerParams  header parameter (option 可為 null)
     * @param requestEntity HttpEntity
     * @param requestConfig RequestConfig (option 可為 null)
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    public HttpEntity httpPut(
            URI uri,
            Map<String, String> headerParams,
            HttpEntity requestEntity,
            RequestConfig requestConfig) throws SystemOperationException {

        // ====================================
        // 檢查
        // ====================================
        if (uri == null) {
            String message = "傳入URI為 null ";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // create HTTP put and set param
        // ====================================
        HttpPut put = new HttpPut();
        put.setURI(uri);
        if (requestEntity != null) {
            put.setEntity(requestEntity);
        }
        if (headerParams != null && !headerParams.isEmpty()) {
            for (Entry<String, String> headerParam : headerParams.entrySet()) {
                put.addHeader(headerParam.getKey(), headerParam.getValue());
            }
        }

        // ====================================
        // execute
        // ====================================
        return this.execute(requestConfig, put, null, null);

    }

    /**
     * HTTP POST
     * 
     * @param uri           URI
     * @param headerParams  header parameter (option 可為 null)
     * @param requestConfig RequestConfig (option 可為 null)
     * @param requestEntity RequestEntity (option 可為 null)
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    public HttpEntity httpPost(
            URI uri,
            Map<String, String> headerParams,
            RequestConfig requestConfig,
            HttpEntity requestEntity) throws SystemOperationException {
        return httpPost(uri, headerParams, requestConfig, requestEntity, null, null);
    }

    /**
     * HTTP POST
     * 
     * @param uri               URI
     * @param headerParams      header parameter (option 可為 null)
     * @param requestConfig     RequestConfig (option 可為 null)
     * @param requestEntity     RequestEntity (option 可為 null)
     * @param httpClientContext (option 可為 null)
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    public HttpEntity httpPost(
            URI uri,
            Map<String, String> headerParams,
            RequestConfig requestConfig,
            HttpEntity requestEntity,
            HttpClientContext httpClientContext,
            CookieStore cookieStore) throws SystemOperationException {

        // ====================================
        // 檢查
        // ====================================
        if (uri == null) {
            String message = "傳入URI為 null ";
            log.error(message);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // create http get and set param
        // ====================================
        HttpPost post = new HttpPost();
        post.setURI(uri);
        if (requestEntity != null) {
            post.setEntity(requestEntity);
        }
        if (headerParams != null && !headerParams.isEmpty()) {
            for (Entry<String, String> headerParam : headerParams.entrySet()) {
                post.addHeader(headerParam.getKey(), headerParam.getValue());
            }
        }

        // ====================================
        // execute
        // ====================================
        return this.execute(requestConfig, post, httpClientContext, cookieStore);
    }

    /**
     * @param requestConfig     RequestConfig (option 可為 null)
     * @param request           HttpUriRequest
     * @param httpClientContext HttpClientContext
     * @param cookieStore       CookieStore
     * @return HttpEntity
     * @throws SystemOperationException 錯誤時拋出
     */
    private HttpEntity execute(
            RequestConfig requestConfig,
            HttpUriRequest request,
            HttpClientContext httpClientContext,
            CookieStore cookieStore) throws SystemOperationException {

        // ====================================
        // protocol
        // ====================================
        boolean isSSL = false;
        try {
            isSSL = "https".equals(request.getURI().toURL().getProtocol());
        } catch (MalformedURLException e) {
            String message = "處理失敗! " + e.getMessage();
            log.error(message, e);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // ====================================
        // create HttpClient by protocol
        // ====================================
        HttpClient httpClient = null;

        if (requestConfig == null) {
            int CONNECTION_TIMEOUT_MS = 15 * 1000; // 預設15秒

            requestConfig = RequestConfig.custom()
                    .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                    .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                    .build();
        }

        // 依據網址協定, 產生不同的 httpClient
        if (isSSL) {

            // SSL (443)
            SSLFactory sslFactory = SSLFactory.builder()
                    .withTrustingAllCertificatesWithoutValidation()
                    .withUnsafeHostnameVerifier()
                    .build();

            if (cookieStore != null) {
                httpClient = HttpClients.custom()
                        .setDefaultRequestConfig(requestConfig)
                        .setDefaultCookieStore(cookieStore)
                        .setSSLContext(sslFactory.getSslContext())
                        .setSSLHostnameVerifier(sslFactory.getHostnameVerifier())
                        .build();
            } else {
                httpClient = HttpClients.custom()
                        .setDefaultRequestConfig(requestConfig)
                        .setSSLContext(sslFactory.getSslContext())
                        .setSSLHostnameVerifier(sslFactory.getHostnameVerifier())
                        .build();
            }

        } else {

            // HTTP (80)
            if (cookieStore != null) {
                httpClient = HttpClientBuilder.create()
                        .setDefaultCookieStore(cookieStore)
                        .setDefaultRequestConfig(requestConfig)
                        .build();
            } else {
                httpClient = HttpClientBuilder.create()
                        .setDefaultRequestConfig(requestConfig)
                        .build();
            }

        }

        // ====================================
        // call API
        // ====================================
        // execute
        HttpResponse response = null;
        try {
            if (httpClientContext == null) {
                response = httpClient.execute(request);
            } else {
                response = httpClient.execute(request, httpClientContext);
            }

        } catch (IOException e) {
            String message = "呼叫 API 失敗! " + e.getMessage();
            log.error(message, e);
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }

        // 確認回傳狀態
        List<Integer> sucessStatus = Lists.newArrayList(
                HttpStatus.SC_OK, // 200
                HttpStatus.SC_CREATED, // 201
                /** {@code 202 Accepted} (HTTP/1.0 - RFC 1945) */
                HttpStatus.SC_ACCEPTED,
                /** {@code 203 Non Authoritative Information} (HTTP/1.1 - RFC 2616) */
                HttpStatus.SC_NON_AUTHORITATIVE_INFORMATION,
                /** {@code 204 No Content} (HTTP/1.0 - RFC 1945) */
                HttpStatus.SC_NO_CONTENT,
                /** {@code 205 Reset Content} (HTTP/1.1 - RFC 2616) */
                HttpStatus.SC_RESET_CONTENT,
                /** {@code 206 Partial Content} (HTTP/1.1 - RFC 2616) */
                HttpStatus.SC_PARTIAL_CONTENT,
                /**
                 * {@code 207 Multi-Status} (WebDAV - RFC 2518)
                 * or
                 * {@code 207 Partial Update OK} (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?)
                 */
                HttpStatus.SC_MULTI_STATUS,
                HttpStatus.SC_BAD_REQUEST// 400 ()
        );

        Integer responseStatusCode = response.getStatusLine().getStatusCode();
        if (responseStatusCode == null ||
                !sucessStatus.contains(response.getStatusLine().getStatusCode())) {
            String message = "\r\n★呼叫 API 失敗!";
            message += "\r\n★HttpStatus：[" + response.getStatusLine().getStatusCode() + "]";
            if (WkStringUtils.notEmpty(response.getStatusLine().getReasonPhrase())) {
                message += "\r\n★ReasonPhrase:[" + response.getStatusLine().getReasonPhrase() + "]";
            }

            try {
                log.error(message += "\r\n★API URL:[" + request.getURI().toURL().toString() + "]");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                if (response.getEntity() != null) {
                    String responseStr = EntityUtils.toString(response.getEntity(), "UTF-8");
                    responseStr = WkStringUtils.unicodeToString(responseStr);
                    if (WkJsonUtils.isJson(responseStr)) {
                        responseStr = new WkJsonUtils().toPettyJson(responseStr);
                    }
                    message += "\r\n★回應資料：\r\n" + responseStr;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }
        return response.getEntity();
    }
}
