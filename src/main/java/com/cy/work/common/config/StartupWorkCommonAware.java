package com.cy.work.common.config;

import com.cy.work.common.client.skypebot.SkypeBotClient;
import com.cy.work.common.exception.alert.SystemAlertException;
import com.cy.work.common.utils.WkLibChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author allen
 */
@Component
@Slf4j
public class StartupWorkCommonAware implements ApplicationContextAware {

    @Value("${skypeBot.clientId}")
    private String skypeBotClientId;
    @Value("${skypeBot.clientSecret}")
    private String skypeBotClientSecret;
    @Value("${skypeBot.chatId}")
    private String skypeBotChatId;
    @Value("${depEnv.disable.skype.alert}")
    private String disableSkypeAlert;

    @Override
    public void setApplicationContext(ApplicationContext actx) throws BeansException {

        // ====================================
        // 版本檢查
        // ====================================
        log.info("work-common 開始進行依賴版本自檢");
        WkLibChecker wkLibChecker = new WkLibChecker("com.cy.work", "work-common");
        //wkLibChecker.compareLibVersion("com.cy.commons", "system-rest-client");
        //wkLibChecker.compareLibVersion("com.cy.commons", "employee-rest-client");
        //wkLibChecker.compareLibVersion("com.cy.bpm", "bpm-vo");
        wkLibChecker.compareLibVersion("org.apache.httpcomponents", "httpclient");

        // ====================================
        // SkypeBot
        // ====================================
        SkypeBotClient.init(
                this.skypeBotClientId,
                this.skypeBotClientSecret,
                this.skypeBotChatId);

        // ====================================
        // 是否停止發送 Skype Alert
        // ====================================
        SystemAlertException.setSendAlert(!"TRUE".endsWith(disableSkypeAlert));

    }
}
