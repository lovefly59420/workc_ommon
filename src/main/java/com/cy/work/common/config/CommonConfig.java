/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author shaun
 */
@Configuration("com.cy.work.common.config")
@ComponentScan(value = {
        "com.cy.work.common.client.skypebot",
        "com.cy.work.common", 
        "com.cy.employee.rest.client"})
@Import({
        com.cy.commons.config.CommonsUtilConfig.class,
        MailConfig.class
})
public class CommonConfig {

}
