package com.cy.work.common.config;

import com.cy.work.common.utils.WkStringUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@Slf4j
public class MailConfig implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3659193599437886895L;
    private static MailConfig instance;
    @Value("${smtp.mail.server.host}")
    @Getter
    private String mailServerHost;
    @Value("${smtp.mail.server.port}")
    @Getter
    private String mailServerPort;
    @Value("${depEnv.mail.FixReceiver}")
    @Getter
    private String depEnvFixReceiver;

    @Bean
    public static PropertyPlaceholderConfigurer initProp() {
        String configHome = System.getenv("FUSION_CONFIG_HOME");
        Path workCommonPath = Paths.get(configHome, "work-common.properties");
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocations(
                new PathResource[] {
                        new PathResource(workCommonPath) });
        // 開啟後@Value如果有預設值則會永遠使用預設值需注意...
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

    public static MailConfig getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(MailConfig instance) { MailConfig.instance = instance; }

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailServerHost);
        if (!WkStringUtils.isNumber(mailServerPort)) {
            log.error("參數 smtp.mail.server.port 設定錯誤！[{}]", mailServerPort);
            mailSender.setPort(Integer.parseInt(mailServerPort));
        }
        mailSender.setProtocol("smtp");

        return mailSender;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }
}
