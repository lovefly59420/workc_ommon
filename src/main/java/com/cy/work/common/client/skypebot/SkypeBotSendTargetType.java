/**
 * 
 */
package com.cy.work.common.client.skypebot;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public enum SkypeBotSendTargetType {

    Chat("傳送到聊天室", "19:%s@thread.skype"),
    User("傳送到個人", "8:%s");

    @Getter
    private String descr;

    @Getter
    private String apiUrl;

    /**
     * @param descr
     * @param apiUrl
     */
    SkypeBotSendTargetType(String descr, String apiUrl) {
        this.descr = descr;
        this.apiUrl = apiUrl;
    }

    /**
     * 兜組發送訊息API URL
     * 
     * @param type 類別
     * @param id   聊天室 ID 或 個人ID
     * @return API URL string
     */
    public static String prepareSendMessageApiUrl(SkypeBotSendTargetType type, String id) {
        if (type == null) {
            return "";
        }

        String apiUrl = ""
                + "https://smba.trafficmanager.net/apis/v3/conversations/"
                + type.getApiUrl()
                + "/activities";

        return String.format(apiUrl, id);
    }

}
