/**
 * 
 */
package com.cy.work.common.client.skypebot;

import java.io.Serializable;
import java.net.URI;
import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.work.common.api.CallApiUtils;
import com.cy.work.common.cache.WkPersistCache;
import com.cy.work.common.client.skypebot.to.SendMessageRequestTo;
import com.cy.work.common.client.skypebot.to.SendMessageResponseTo;
import com.cy.work.common.client.skypebot.to.TokenResponseTo;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * @author allen1214_wu
 */
@Component
@Slf4j
public class SkypeBotClient implements Serializable, InitializingBean {

    // 注意, 不要throw SystemAlertException , 否則連線不通時，會陷入發送警告訊息的無窮迴圈

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1322380492378634164L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static SkypeBotClient instance;

    public static SkypeBotClient getInstance() { return instance; }

    private static void setInstance(SkypeBotClient instance) { SkypeBotClient.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // API_ROUTE
    // ========================================================================
    /**
     * URL : 取得 token
     */
    private final static String URL_GET_TOKEN = "https://login.microsoftonline.com/common/oauth2/v2.0/token";

    private final static String CACHE_NAME = SkypeBotClient.class.getSimpleName() + "_CACHE";

    // ========================================================================
    // 常數
    // ========================================================================
    /**
     * 不重發時間 30 分鐘
     */
    private final static long INTERVAL_TIME = 30 * 60 * 1000;

    // ========================================================================
    // 變數
    // ========================================================================
    private static String clientId;
    private static String clientSecret;
    private static String chatId;

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private CallApiUtils callApiUtils;
    @Autowired
    private WkJsonUtils wkJsonUtils;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * 初始化
     * 
     * @param clientId
     * @param clientSecret
     * @param chatId
     */
    public static void init(
            String clientId,
            String clientSecret,
            String chatId) {
        SkypeBotClient.clientId = clientId;
        SkypeBotClient.clientSecret = clientSecret;
        SkypeBotClient.chatId = chatId;

        List<String> logMessages = Lists.newArrayList();
        logMessages.add("");
        logMessages.add("SkypeBotClient 初始化");
        logMessages.add("clientId:[" + clientId + "]");
        logMessages.add("clientSecret:[" + (WkStringUtils.isEmpty(clientSecret) ? "為空！" : "秘密") + "]");
        logMessages.add("chatId:[" + chatId + "]");

        log.info(logMessages.stream().collect(Collectors.joining("\r\n")));
    }

    /**
     * 發送訊息給 user
     * 
     * @param targetId    ID
     * @param sendMessage 傳送訊息
     * @throws SystemOperationException 錯誤時拋出
     */
    public void sendMessageToUser(
            String targetId,
            String sendMessage) throws SystemOperationException {

        // ====================================
        // 檢查
        // ====================================
        if (WkStringUtils.isEmpty(SkypeBotClient.clientId)) {
            throw new SystemOperationException("Skybot：clientId 為空! (是否尚未初始化?)");
        }
        if (WkStringUtils.isEmpty(SkypeBotClient.clientSecret)) {
            throw new SystemOperationException("Skybot：clientSecret 為空! (是否尚未初始化?)");
        }

        // ====================================
        // 發送
        // ====================================
        this.sendMessage(
                SkypeBotClient.clientId,
                SkypeBotClient.clientSecret,
                SkypeBotSendTargetType.User,
                targetId,
                sendMessage);
    }

    /**
     * 發送訊息給 聊天室
     * 
     * @param sendMessage 傳送訊息
     * @throws SystemOperationException 錯誤時拋出
     */
    public void sendMessageToChat(
            String sendMessage) throws SystemOperationException {

        // ====================================
        // 檢查
        // ====================================
        if (WkStringUtils.isEmpty(SkypeBotClient.clientId)) {
            throw new SystemOperationException("Skybot：clientId 為空! (是否尚未初始化?)");
        }
        if (WkStringUtils.isEmpty(SkypeBotClient.clientSecret)) {
            throw new SystemOperationException("Skybot：clientSecret 為空! (是否尚未初始化?)");
        }
        if (WkStringUtils.isEmpty(SkypeBotClient.chatId)) {
            throw new SystemOperationException("Skybot：chatId 為空! (是否尚未初始化?)");
        }

        // ====================================
        // 發送
        // ====================================
        this.sendMessage(
                SkypeBotClient.clientId,
                SkypeBotClient.clientSecret,
                SkypeBotSendTargetType.Chat,
                SkypeBotClient.chatId,
                sendMessage);
    }

    /**
     * 發送訊息給 聊天室
     * 
     * @param title   標題
     * @param message 訊息
     * @throws SystemOperationException 錯誤時拋出
     */
    public void sendMessageToChat(String title, String message) throws SystemOperationException {

        List<String> alertMessages = Lists.newArrayList();

        alertMessages.add("\\================================");
        alertMessages.add("【" + title + "】");
        alertMessages.add("\\================================");
        alertMessages.add("\\--訊息--");
        alertMessages.add(message);
        alertMessages.add("");

        this.sendMessageToChat(alertMessages.stream().collect(Collectors.joining("\r\n ")));
    }

    /**
     * 傳送訊息
     * 
     * @param clientId     client Id
     * @param clientSecret client Secret
     * @param targetType   傳送到聊天室或個人
     * @param targetId     聊天室ID/個人ID
     * @param sendMessage  訊息內容
     * @throws SystemOperationException 錯誤時拋出
     */
    public void sendMessage(
            String clientId,
            String clientSecret,
            SkypeBotSendTargetType targetType,
            String targetId,
            String sendMessage) throws SystemOperationException {

        // ====================================
        // 組快取 key
        // ====================================
        String breakLine = "#18255210#";
        String tempContent = sendMessage + "";
        tempContent = tempContent.replaceAll("\r\n", breakLine);
        tempContent = tempContent.replaceAll("\n", breakLine);

        // 去空行、去空白
        tempContent = Lists.newArrayList(tempContent.split(breakLine))
                .stream()
                .map(line -> WkStringUtils.safeTrim(line))
                .filter(line -> WkStringUtils.notEmpty(line))
                .collect(Collectors.joining());

        List<String> keys = Lists.newArrayList(
                targetType.name(),
                targetId,
                tempContent);

        // ====================================
        // 加工處理，避免一直發重複訊息
        // ====================================
        SkypeBotCacheMessage skypeBotCacheMessage = WkPersistCache.getInstance().getCache(CACHE_NAME, keys);
        if (skypeBotCacheMessage != null) {

            // 上次發送『同樣訊息』的間隔
            long interval = System.currentTimeMillis() - skypeBotCacheMessage.getLastSandTime();

            // ====================================
            // 小於重發限制時間，update message , 不新增訊息
            // ====================================
            if (interval < INTERVAL_TIME) {
                skypeBotCacheMessage.reSend();

                sendMessage += "\r\n";
                sendMessage += "\r\n重複次數：" + skypeBotCacheMessage.getReSendCount();
                sendMessage += "\r\n首次時間：" + WkDateUtils.formatDate(
                        new Date(skypeBotCacheMessage.getFirstSandTime()), WkDateUtils.YYYY_MM_DD_HH24_mm_ss_SSS);
                sendMessage += "\r\n末次時間：" + WkDateUtils.formatDate(
                        new Date(skypeBotCacheMessage.getLastSandTime()), WkDateUtils.YYYY_MM_DD_HH24_mm_ss_SSS);
            } else {
                skypeBotCacheMessage = null;
            }
        }

        // ====================================
        //
        // ====================================
        if (skypeBotCacheMessage == null) {
            // 新增一筆訊息
            String currMessageID = this.message(
                    clientId, clientSecret, targetType, targetId, "", sendMessage);

            // 加入快取
            WkPersistCache.getInstance().putCache(CACHE_NAME, keys, new SkypeBotCacheMessage(currMessageID));
        } else {
            // 僅更新訊息
            this.message(
                    clientId, clientSecret, targetType, targetId,
                    skypeBotCacheMessage.getMessageId(), sendMessage);
        }

    }

    /**
     * 更新訊息
     * 
     * @param clientId     client Id
     * @param clientSecret client Secret
     * @param targetType   傳送到聊天室或個人
     * @param targetId     聊天室ID/個人ID
     * @param messageId    要更新的訊息 ID
     * @param newMessage   訊息內容
     * @throws SystemOperationException 錯誤時拋出
     */
    public void updateMessage(
            String clientId,
            String clientSecret,
            SkypeBotSendTargetType targetType,
            String targetId,
            String messageId,
            String newMessage) throws SystemOperationException {
        this.message(clientId, clientSecret, targetType, targetId, messageId, newMessage);
    }

    /**
     * 傳送訊息
     * 
     * @param clientId     client Id
     * @param clientSecret client Secret
     * @param targetType   傳送到聊天室或個人
     * @param targetId     聊天室ID/個人ID
     * @param messageId    訊息ID
     * @param message      訊息
     * @return create message 時，回傳 message ID
     * @throws SystemOperationException 錯誤時拋出
     */
    private String message(
            String clientId,
            String clientSecret,
            SkypeBotSendTargetType targetType,
            String targetId,
            String messageId,
            String message) throws SystemOperationException {

        String procName = "Skype-發送訊息：";
        if (WkStringUtils.notEmpty(messageId)) {
            procName = "Skype-更新訊息：";
        }

        boolean isCreate = WkStringUtils.isEmpty(messageId);

        // ====================================
        // 初始化服務
        // ====================================
        // 給不是 spring 環境用 (EX: main test)
        if (this.callApiUtils == null) {
            this.callApiUtils = new CallApiUtils();
        }

        if (this.wkJsonUtils == null) {
            this.wkJsonUtils = new WkJsonUtils();
        }

        // ====================================
        // 取得 token
        // ====================================
        String token = this.getToken(clientId, clientSecret);

        // ====================================
        // URI
        // ====================================
        URI apiUrl = null;
        try {
            String url = SkypeBotSendTargetType.prepareSendMessageApiUrl(targetType, targetId);
            if (WkStringUtils.notEmpty(messageId)) {
                url += "/" + messageId;
            }
            apiUrl = new URI(url);
        } catch (Exception e) {
            String errMsg = procName + "解析API網址錯誤, 設定值:[" + SkypeBotSendTargetType.prepareSendMessageApiUrl(targetType, targetId) + "]";
            log.error(errMsg);
            throw new SystemOperationException(errMsg);
        }

        Map<String, String> headerParams = Maps.newHashMap();
        headerParams.put("Accept", "application/json");
        headerParams.put(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        // ====================================
        // 組request 資料
        // ====================================
        // body
        StringEntity requestEntity = null;
        try {
            SendMessageRequestTo requestTo = new SendMessageRequestTo();
            requestTo.setText(message);

            // 轉 JSON
            String requestDataJsonStr = WkJsonUtils.getInstance().toJsonWithOutPettyJson(requestTo);
            // 轉 HttpEntity
            requestEntity = new StringEntity(requestDataJsonStr, ContentType.APPLICATION_JSON);

        } catch (Exception e) {
            String errMsg = "處理傳送資料失敗!" + e.getMessage();
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // post
        // ====================================
        HttpEntity httpEntity = null;
        try {
            if (isCreate) {
                // create 用 post
                httpEntity = callApiUtils.httpPost(
                        apiUrl,
                        headerParams,
                        null,
                        requestEntity);
            } else {
                // update 用 put
                httpEntity = callApiUtils.httpPut(
                        apiUrl,
                        headerParams,
                        requestEntity,
                        null);
            }

        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errMsg = "Call API 失敗!" + e.getMessage();
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 當異動訊息時，沒有回傳資料
        // ====================================
        if (!isCreate) {
            return "";
        }

        // ====================================
        // 解析回應資料
        // ====================================
        // 取得 response string
        String responseJsonStr = "";
        try {
            responseJsonStr = EntityUtils.toString(httpEntity, "UTF-8");
        } catch (Exception e) {
            String errMsg = "取得回應資料失敗!" + e.getMessage();
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // 將 JSON 資料轉回 TO
        SendMessageResponseTo responseTo = null;
        try {
            // GSON 解析
            responseTo = (SendMessageResponseTo) wkJsonUtils.fromJsonByGson(
                    responseJsonStr,
                    new TypeToken<SendMessageResponseTo>() {
                    }.getType());
        } catch (Exception e) {
            String errMsg = "解析回應資料失敗!" + e.getMessage() + "response:[\r\n" + responseJsonStr + "]";
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 判斷回傳資料狀態
        // ====================================
        if (WkStringUtils.isEmpty(responseTo.getId())) {
            String errMsg = procName + "失敗!";
            log.error(errMsg + "\r\n" + wkJsonUtils.toPettyJson(responseJsonStr));
            throw new SystemOperationException(errMsg);
        }
        return responseTo.getId();
    }

    /**
     * 取得 token
     * 
     * @param clientId     client ID
     * @param clientSecret client Secret
     * @throws SystemOperationException 錯誤時拋出
     */
    public String getToken(
            String clientId,
            String clientSecret) throws SystemOperationException {

        String procName = "Skype-取得Token：";

        // ====================================
        // 初始化服務
        // ====================================
        // 給不是 spring 環境用 (EX: main test)
        if (this.callApiUtils == null) {
            this.callApiUtils = new CallApiUtils();
        }

        if (this.wkJsonUtils == null) {
            this.wkJsonUtils = new WkJsonUtils();
        }

        // ====================================
        // URI
        // ====================================
        URI apiUrl = null;
        try {
            apiUrl = new URI(URL_GET_TOKEN);
        } catch (Exception e) {
            String errMsg = procName + "解析API網址錯誤, 設定值:[" + URL_GET_TOKEN + "]";
            log.error(errMsg);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 組request 資料
        // ====================================
        // header
        Map<String, String> headerParams = Maps.newHashMap();
        headerParams.put("Accept", "application/json");

        // form-data
        List<NameValuePair> form = Lists.newArrayList();
        form.add(new BasicNameValuePair("grant_type", "client_credentials"));
        form.add(new BasicNameValuePair("client_id", clientId));
        form.add(new BasicNameValuePair("client_secret", clientSecret));
        form.add(new BasicNameValuePair("scope", "https://api.botframework.com/.default"));
        UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(form, Consts.UTF_8);

        // ====================================
        // post
        // ====================================
        HttpEntity httpEntity = null;
        HttpClientContext httpClientContext = new HttpClientContext();
        try {
            httpEntity = callApiUtils.httpPost(
                    apiUrl,
                    headerParams,
                    null,
                    requestEntity,
                    httpClientContext, // 收集 cookie
                    null); // 取得 token 無需驗 coolie
        } catch (SystemOperationException e) {
            throw e;
        } catch (Exception e) {
            String errMsg = procName + "Call API 失敗!" + e.getMessage();
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 解析回應資料
        // ====================================
        // 取得 response string
        String responseJsonStr = "";
        try {
            responseJsonStr = EntityUtils.toString(httpEntity, "UTF-8");
        } catch (Exception e) {
            String errMsg = procName + "取得回應資料失敗!" + e.getMessage();
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // 將 JSON 資料轉回 TO
        TokenResponseTo responseTo = null;
        try {
            // jackson 測試在轉回物件時 JsonProperty 無效
            // syncMmsTokenTo = wkJsonUtils.fromJson(responseJsonStr, SyncMmsTokenTo.class);

            // GSON 解析
            responseTo = (TokenResponseTo) wkJsonUtils.fromJsonByGson(
                    responseJsonStr,
                    new TypeToken<TokenResponseTo>() {
                    }.getType());

        } catch (Exception e) {
            String errMsg = procName + "解析回應資料失敗!" + e.getMessage() + "response:[\r\n" + responseJsonStr + "]";
            log.error(errMsg, e);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 判斷回傳資料狀態
        // ====================================
        if (WkStringUtils.notEmpty(responseTo.getError())) {
            String errMsg = procName
                    + "API 回應『處理失敗』! "
                    + "error:[" + responseTo.getError() + "]"
                    + "error_description:[" + responseTo.getError_description() + "]";

            log.error(errMsg);
            throw new SystemOperationException(errMsg);
        }

        // ====================================
        // 回傳
        // ====================================

        return responseTo.getAccess_token();
    }

    /**
     * main test
     * 
     * @param args 參數無用
     */
    @SuppressWarnings("unused")
    public static void main(String[] args) {

        String clientId = "5425256a-fa9f-4a23-8948-4b237821e90c";
        String clientSecret = "u~O8Q~kglKXt9J813pH_uF-CZ9n6NMNUJBcT4aDu";
        String chatId = "044ae24528c54148834dc2fb3c2b7fa1";
        // String userId = "live:allen1214_wu";
        String userId = "CA.yi-fan501";

        // String

        try {
            // 取得 token
            // String token = new SkypeBotClient().getToken(clientId, clientSecret);
            // log.info("Token:[\r\n{}\r\n]", token);
            //
            // 傳送訊息到聊天室
            new SkypeBotClient().sendMessage(
                    clientId,
                    clientSecret,
                    SkypeBotSendTargetType.Chat,
                    chatId,
                    "傳送到聊天室的測試訊息");

            // // 傳送訊息到個人
            // new SkypeBotClient().sendMessage(
            // clientId,
            // clientSecret,
            // SkypeBotSendTargetType.User,
            // userId,
            // "傳送給" + userId + "的測試訊息");

        } catch (SystemOperationException e) {
            log.error(e.getMessage(), e);
        }

    }

}
