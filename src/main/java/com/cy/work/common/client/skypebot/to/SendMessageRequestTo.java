/**
 * 
 */
package com.cy.work.common.client.skypebot.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Skype 傳送訊息 回應 To
 * 
 * @author allen1214_wu
 */
public class SendMessageRequestTo implements Serializable {


    
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3534354175731367249L;
    
    @Getter
    @Setter
    private String type = "message";
    
    
    /**
     * 訊息
     */
    @Getter
    @Setter
    private String text = "";
}
