/**
 * 
 */
package com.cy.work.common.client.skypebot.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Skype 取得 token 回應 To
 * 
 * @author allen1214_wu
 */
public class TokenResponseTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1982972989639120971L;

    @Getter
    @Setter
    private String token_type;

    @Getter
    @Setter
    private Integer expires_in;

    @Getter
    @Setter
    private Integer ext_expires_in;

    /**
     * token
     */
    @Getter
    @Setter
    private String access_token;

    /**
     * 錯誤
     */
    @Getter
    @Setter
    private String error;

    /**
     * 錯誤說明
     */
    @Getter
    @Setter
    private String error_description;

}
