/**
 * 
 */
package com.cy.work.common.client.skypebot;

import java.io.Serializable;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public class SkypeBotCacheMessage implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 520343410303891693L;

    @Getter
    private String messageId = "";

    @Getter
    private long firstSandTime;

    @Getter
    private long lastSandTime;
    
    @Getter
    private long reSendCount = 1;

    /**
     * @param messageId
     */
    public SkypeBotCacheMessage(String messageId) {
        this.messageId = messageId;
        this.firstSandTime = System.currentTimeMillis();
        this.lastSandTime = System.currentTimeMillis();
    }

    public void reSend() {
        this.lastSandTime = System.currentTimeMillis();
        this.reSendCount += 1;
    }
}
