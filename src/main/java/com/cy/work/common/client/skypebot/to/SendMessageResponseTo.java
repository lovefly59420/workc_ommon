/**
 * 
 */
package com.cy.work.common.client.skypebot.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Skype 傳送訊息 回應 To
 * 
 * @author allen1214_wu
 */
public class SendMessageResponseTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3695657758143638641L;
    
    @Getter
    @Setter
    private String id;
}
