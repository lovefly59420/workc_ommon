package com.cy.work.common.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.OrgSort;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.system.rest.client.DomainClient;
import com.cy.system.rest.client.OrgClient;
import com.cy.system.rest.client.UserClient;
import com.cy.system.rest.client.vo.SecAuth;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkOrgUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Allen
 */
@Slf4j
@Component
public class WkOrgCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -9046443161620866923L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WkOrgCache instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static WkOrgCache getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkOrgCache instance) { WkOrgCache.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient OrgClient orgClient;
    @Autowired
    private transient DomainClient domainClient;
    @Autowired
    private transient UserClient userClient;

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 取得全部組織
     *
     * @return 全部組織
     */
    public List<Org> findAll() {
        return orgClient.findAll();
    }

    /**
     * 取得所有子單位<br/>
     * 1.不包含本身<br/>
     * 2.遞迴取到最下層單位
     *
     * @param parentDepSid
     * @return 所有子單位
     */
    public List<Org> findAllChild(Integer parentDepSid) {
        // 取得下層全部子部門
        List<Org> results = this.orgClient.findAllChildrenByParentSid(parentDepSid);
        // 防呆
        if (results == null) {
            results = Lists.newArrayList();
        }
        return results;
    }

    /**
     * 取得所有子單位 SID<br/>
     * 1.不包含本身<br/>
     * 2.遞迴取到最下層單位
     *
     * @param parentDepSid 父單位 (多筆)
     * @return ChildSids
     */
    public Set<Integer> findAllChildSids(Integer parentDepSid) {
        List<Org> results = findAllChild(parentDepSid);
        if (WkStringUtils.isEmpty(results)) {
            return Sets.newHashSet();
        }

        return results.stream()
                .map(Org::getSid)
                .collect(Collectors.toSet());
    }

    /**
     * 取得所有子單位 SID<br/>
     * 1.不包含本身<br/>
     * 2.遞迴取到最下層單位
     * 
     * @param parentDepSids 父單位 (多筆)
     * @return org sid list
     */
    public Set<Integer> findAllChildSids(Collection<Integer> parentDepSids) {
        if (WkStringUtils.isEmpty(parentDepSids)) {
            return Sets.newHashSet();
        }
        Set<Integer> resultDepSids = Sets.newHashSet();
        Set<Integer> currChildSid = null;
        for (Integer parentDepSid : parentDepSids) {
            currChildSid = this.findAllChildSids(parentDepSid);
            if (WkStringUtils.notEmpty(currChildSid)) {
                resultDepSids.addAll(currChildSid);
            }
        }

        return resultDepSids;

    }

    /**
     * 取得公司下所有的部門
     *
     * @param compID 公司ID
     * @return org list
     */
    public List<Org> findAllDepByCompID(String compID) {
        return this.findAll()
                .stream()
                .filter(org -> OrgType.DEPARTMENT.equals(org.getType()))
                .filter(org -> org.getCompany() != null && WkCommonUtils.compareByStr(org.getCompany()
                        .getId(), compID))
                .collect(Collectors.toList());
    }

    /**
     * 取得公司下所有的組織
     *
     * @param compSid 公司SID
     * @return org list
     */
    public List<Org> findAllDepByCompSid(Integer compSid) {
        return this.findAll()
                .stream()
                .filter(org -> OrgType.DEPARTMENT.equals(org.getType()))
                .filter(
                        org -> org.getCompany() != null && WkCommonUtils.compareByStr(org.getCompany()
                                .getSid(), compSid))
                .collect(Collectors.toList());
    }

    /**
     * 取得公司下所有的組織(排除停用)
     * 
     * @param compSid
     * @return org list
     */
    public List<Org> findAllActiveDepByCompSid(Integer compSid) {
        boolean tmp =WkOrgUtils.isActive(new Org(1));
        return this.findAllDepByCompSid(compSid)
                .stream()
                .filter(org -> WkOrgUtils.isActive(org))
                .collect(Collectors.toList());
    }

    /**
     * 取得公司下所有的組織SID
     *
     * @param compID 公司ID
     * @return org sid list
     */
    public List<Integer> findAllDepSidByCompID(String compID) {
        return this.findAllDepByCompID(compID)
                .stream()
                .map(Org::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 取得公司下所有的組織SID
     *
     * @param compSid 公司SID
     * @return org sid list
     */
    public List<Integer> findAllDepSidByCompSid(Integer compSid) {
        return this.findAllDepByCompSid(compSid)
                .stream()
                .map(Org::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 取得所有單位的絕對排序資料
     *
     * @return Map < orgSid, seqNumber >
     */
    public Map<Integer, Integer> findAllOrgOrderSeqMap() {

        String cacheName = "WkOrgCache_findAllOrgOrderSeqMap";

        // 快取逾期時間 1 分鐘
        Map<Integer, Integer> results = WkCommonCache.getInstance()
                .getCache(cacheName, Lists.newArrayList(), 1 * 1000);

        if (results != null) {
            return results;
        }

        List<OrgSort> orgSorts = this.orgClient.findOrgSort();
        if (WkStringUtils.isEmpty(orgSorts)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "取得單位排序資料為空");
            return Maps.newHashMap();
        }

        results = orgSorts.stream()
                .collect(Collectors.toMap(OrgSort::getOrgSid, OrgSort::getOrgSortNumber));
        WkCommonCache.getInstance()
                .putCache(cacheName, Lists.newArrayList(), results);
        return results;
    }

    /**
     * 取得直系向上所有部門 (不包含本身)
     *
     * @param org
     * @return org list
     */
    public List<Org> findAllParent(Org org) {
        if (org == null || org.getSid() == null) {
            return Lists.newArrayList();
        }
        return this.findAllParent(org.getSid());
    }

    /**
     * 取得直系向上所有部門 (不包含本身)
     *
     * @param depSid orgSid
     * @return org list
     */
    public List<Org> findAllParent(Integer depSid) {

        Org dep = this.findBySid(depSid);
        if (dep == null) {
            return Lists.newArrayList();
        }

        if (dep.getParent() == null) {
            return Lists.newArrayList();
        }

        Org parent = dep.getParent() == null ? null
                : this.findBySid(dep.getParent().getSid());

        List<Org> results = Lists.newArrayList();

        // 往上找到頂
        while (parent != null && OrgType.DEPARTMENT.equals(parent.getType())) {
            results.add(parent);
            if (parent.getParent() != null) {
                parent = this.findBySid(parent.getParent().getSid());
            } else {
                parent = null;
            }
        }
        return results;
    }

    /**
     * 取得直系向上所有部門 (不包含本身)
     * 
     * @param depSid 部門 sid
     * @return 直系向上所有部門 sid
     */
    public Set<Integer> findAllParentSid(Integer depSid) {
        return findAllParent(depSid).stream().map(Org::getSid).collect(Collectors.toSet());
    }

    /**
     * 取得直系向上所有部門 (不包含本身)
     * 
     * @param dep 部門
     * @return 直系向上所有部門 sid
     */
    public Set<Integer> findAllParentSid(Org dep) {
        return findAllParent(dep).stream().map(Org::getSid).collect(Collectors.toSet());
    }

    /**
     * 取得所有直系上層部門, 過濾部門層級需 <= topLevel
     *
     * @param depSid   部門 sid
     * @param topLevel 最高單位層級
     * @return org list
     */
    public List<Org> findAllParent(
            Integer depSid,
            OrgLevel topLevel) {
        // 1. 取得所有直系上層部門
        // 2. 過濾部門層級需 <= topLevel
        return this.findAllParent(depSid)
                .stream()
                .filter(org -> WkOrgUtils.compareOrgLevel(org.getLevel(), topLevel) <= 0)
                .collect(Collectors.toList());

    }

    /**
     * 以 ID 查詢 ORG 資料
     *
     * @param orgId orgId
     * @return Org
     */
    public Org findById(String orgId) {
        if (WkStringUtils.isEmpty(orgId)) {
            return null;
        }
        try {
            Optional<Org> optional = orgClient.findById(orgId);
            if (optional.isPresent()) {
                return optional.get();
            }
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 org id :[" + orgId + "]");
            return null;
        } catch (Exception e) {
            log.error("orgClient.findById error", e);
        }
        return null;
    }

    /**
     * 以 ORG ID 查詢 , 但查不到資料時，不印 LOG (給查詢BPM 對應單位ID用)
     *
     * @param orgId
     * @return Org
     */
    public Org findByIdAndNotCheckEmpty(String orgId) {
        if (WkStringUtils.isEmpty(orgId)) {
            return null;
        }
        try {
            Optional<Org> optional = this.orgClient.findById(orgId);
            return optional.orElse(null);
        } catch (Exception e) {
            log.error("orgClient.findById error", e);
        }
        return null;
    }

    /**
     * 以 SID 查詢 ORG 資料
     *
     * @param sid
     * @return Org
     */
    public Org findBySid(Integer sid) {
        if (sid == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 org sid 為 null");
            return null;
        }

        try {
            Optional<Org> optional = orgClient.findBySid(sid);
            if (optional.isPresent()) {
                return optional.get();
            }
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 org sid :[" + sid + "]");
        } catch (Exception e) {
            log.error("orgClient.findBySid error", e);
        }
        return null;
    }

    /**
     * 以SID 查詢 ORG 資料
     *
     * @param sid orgSid
     * @return Org
     */
    public Org findBySid(String sid) {
        sid = WkStringUtils.safeTrim(sid);
        if (WkStringUtils.isEmpty(sid)) {
            return null;
        }
        if (!WkStringUtils.isNumber(sid)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 org sid 不是數字:[" + sid + "]", 10);
            return null;
        }

        return findBySid(Integer.parseInt(sid));
    }

    /**
     * 以SID 查詢 ORG 資料 (多筆)
     *
     * @param sids orgSids
     * @return org list
     */
    public List<Org> findBySids(Collection<Integer> sids) {
        if (WkStringUtils.isEmpty(sids)) {
            return Lists.newArrayList();
        }

        List<Org> orgs = Lists.newArrayList();
        for (Integer sid : sids) {
            Org org = this.findBySid(sid);
            if (org == null) {
                continue;
            }
            orgs.add(org);
        }

        return orgs;
    }

    /**
     * 取得公司列表
     *
     * @return compOrg org list
     */
    public List<Org> findCompanies() {
        try {
            return orgClient.findCompanies(Activation.ACTIVE);
        } catch (Exception e) {
            log.error("findCompanies ERROR", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 取得傳入部門，直系向上所有單位主管 (包含傳入部門)
     *
     * @param depSid org sid
     * @return org sid set
     */
    public Set<Integer> findDirectManagerUserSids(Integer depSid) {

        Set<Integer> directManagerUserSids = Sets.newHashSet();

        // 取得傳入單位資料
        Org dep = this.findBySid(depSid);
        if (dep == null) {
            return directManagerUserSids;
        }

        // 取得單位主管+副主管
        Set<Integer> managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(depSid);
        if (WkStringUtils.notEmpty(managerUserSids)) {
            directManagerUserSids.addAll(managerUserSids);
        }

        // 取得直系向上單位，所有主、副主管
        List<Org> allParent = findAllParent(depSid);
        for (Org parentDep : allParent) {
            managerUserSids = WkOrgUtils.findOrgManagerUserSidByOrgSid(parentDep.getSid());
            if (WkStringUtils.notEmpty(managerUserSids)) {
                directManagerUserSids.addAll(managerUserSids);
            }
        }

        return directManagerUserSids;
    }

    /**
     * 依據公司別代碼, 取得網址
     *
     * @param compId 公司別代碼
     * @return 公司別的 domain URL
     * @throws SystemOperationException 取得失敗時拋出
     */
    public String findDomainUrlByCompId(String compId) throws SystemOperationException {
        try {
            return this.domainClient.getDomainUrlByWerpCompId(compId);
        } catch (Exception e) {
            String message = String.format("取得 WERP網址失敗! compId:[%s], error:[%s]", compId, e.getMessage());
            throw new SystemOperationException(message, InfomationLevel.ERROR);
        }
    }

    /**
     * 是否存在傳入公司ID的 domain 資料
     * 
     * @param compId
     * @return true/false
     * @throws SystemOperationException 錯誤時拋出
     */
    public boolean isExistDomainUrlByCompId(String compId) throws SystemOperationException {
        try {
            String result = this.domainClient.getDomainUrlByWerpCompId(compId);
            return WkStringUtils.notEmpty(result);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 取得使用者管理的部門
     *
     * @param userSid
     * @return org list
     */
    public List<Org> findManagerOrgs(Integer userSid) {

        // ====================================
        // 取得全部單位 (cache)
        // ====================================
        List<Org> allOrgs = this.orgClient.findAll();
        if (WkStringUtils.isEmpty(allOrgs)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 收集管理部門
        // ====================================
        Set<Org> managerOrgs = Sets.newHashSet();
        for (Org org : allOrgs) {

            // 檢查是否為部門主管
            if (WkStringUtils.isEmpty(org.getManagers())) {
                if (org.getCompany() != null
                        && "TG".equals(org.getCompany().getId())
                        && !org.getName().contains("停用")) {
                    //log.warn("取得部門主管為空![{}-{}]", org.getSid(), org.getName());
                }
            } else {
                // 檢查是否為部門主管
                if (WkUserUtils.isInSimpleUserList(org.getManagers(), userSid)) {
                    managerOrgs.add(org);
                    continue;
                }
            }

            // 檢查是否為部門副主管
            if (WkUserUtils.isInSimpleUserList(org.getDeputyManagers(), userSid)) {
                managerOrgs.add(org);
                continue;
            }
        }

        // 回傳
        return managerOrgs.stream().collect(Collectors.toList());
    }

    /**
     * 取得使用者管理的部門 (為主、副主管)
     *
     * @param userSid
     * @return org sid list
     */
    public Set<Integer> findManagerOrgSids(Integer userSid) {
        return this.findManagerOrgs(userSid)
                .stream()
                .map(Org::getSid)
                .collect(Collectors.toSet());
    }

    /**
     * 查詢使用者所有管理的單位, 包含管理單位子部門
     *
     * @param userSid user sid
     * @return 所有管理單位包含管理單位子部門 SID
     */
    public Set<Integer> findManagerWithChildOrgSids(Integer userSid) {
        // 查詢所有管理單位
        Set<Integer> depSids = this.findManagerOrgSids(userSid)
                .stream()
                .collect(Collectors.toSet());

        if (WkStringUtils.isEmpty(depSids)) {
            return Sets.newHashSet();
        }
        // 收集所有管理單位的子單位
        Set<Integer> allChildDepSids = Sets.newHashSet();
        for (Integer depSid : depSids) {
            Set<Integer> childDepSids = this.findAllChildSids(depSid);
            if (WkStringUtils.notEmpty(childDepSids)) {
                allChildDepSids.addAll(childDepSids);
            }
        }

        // 加入所有子單位
        if (WkStringUtils.notEmpty(allChildDepSids)) {
            depSids.addAll(allChildDepSids);
        }

        return depSids;
    }

    /**
     * 查詢單位名稱
     *
     * @param sid org sid
     * @return 單位名稱
     */
    public String findNameBySid(Integer sid) {
        Org org = this.findBySid(sid);
        return WkOrgUtils.getOrgName(org);
    }

    /**
     * 查詢單位名稱
     *
     * @param sid org sid
     * @return 單位名稱
     */
    public String findNameBySid(String sid) {
        Org org = this.findBySid(sid);
        return WkOrgUtils.getOrgName(org);
    }

    /**
     * 取得單位的絕對排序
     *
     * @param org Org
     * @return 單位的絕對排序序號
     */
    public Integer findOrgOrderSeq(Org org) {
        if (org == null || org.getSid() == null) {
            return Integer.MAX_VALUE;
        }

        return this.findOrgOrderSeqBySid(org.getSid());
    }

    /**
     * 取得單位的絕對排序
     *
     * @param org SimpleOrg
     * @return 單位的絕對排序序號
     */
    public Integer findOrgOrderSeq(SimpleOrg org) {
        if (org == null || org.getSid() == null) {
            return Integer.MAX_VALUE;
        }

        return this.findOrgOrderSeqBySid(org.getSid());
    }

    /**
     * 取得單位的絕對排序
     *
     * @param sid
     * @return 排序序號
     */
    public Integer findOrgOrderSeqBySid(Integer sid) {
        List<OrgSort> orgSorts = this.orgClient.findOrgSort();
        if (WkStringUtils.isEmpty(orgSorts)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 org id :[" + sid + "]");
            return Integer.MAX_VALUE;
        }
        for (OrgSort orgSort : orgSorts) {
            if (WkCommonUtils.compareByStr(sid, orgSort.getOrgSid())) {
                return orgSort.getOrgSortNumber();
            }
        }
        WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 org 排序資料 orgid :[" + sid + "]");
        return Integer.MAX_VALUE;
    }

    /**
     * 取得全部部門的排序
     * 
     * @return Map[OrgSid,排序序號]
     */
    public Map<Integer, Integer> findOrgSortNumberMapByDepSid() {
        // ====================================
        // 快取處理
        // ====================================
        // 快取名稱
        String cacheName = this.getClass().getSimpleName() + "_findOrgSortNumberMapByDepSid";
        // 由快取中取回 (逾時時間 60 秒)
        Map<Integer, Integer> orgSortNumberMapByDepSid = WkCommonCache.getInstance().getCache(cacheName, null, 60000);

        if (orgSortNumberMapByDepSid != null) {
            return orgSortNumberMapByDepSid;
        }

        // ====================================
        // 取得部門排序 map
        // ====================================
        // 將全部資料整裡為Map<OrgSid,排序序號>
        orgSortNumberMapByDepSid = Maps.newHashMap();
        for (OrgSort orgSort : this.orgClient.findOrgSort()) {
            orgSortNumberMapByDepSid.put(orgSort.getOrgSid(), orgSort.getOrgSortNumber());
        }
        // 放入快取
        WkCommonCache.getInstance().putCache(cacheName, null, orgSortNumberMapByDepSid);

        return orgSortNumberMapByDepSid;

    }

    /**
     * 查詢上層單位，直至目標層級 (OrgLevel)<br/>
     * <br/>
     * 1.若傳入單位即為目標層級(OrgLevel) => 回傳自己<br/>
     * 2.若傳入單位高於目標層級(OrgLevel) => 回傳 null<br/>
     * 3.若往上找不到符合目標的層級 => 回傳 null
     *
     * @param orgSid         單位 sid
     * @param targetOrgLevel 要查詢的目標層級
     * @return org sid
     */
    public Integer findParentByOrgLevel(
            Integer orgSid,
            OrgLevel targetOrgLevel) {

        Org srcOrg = this.findBySid(orgSid);
        if (srcOrg == null) {
            return null;
        }

        // 傳入 org 不是部門
        if (!OrgType.DEPARTMENT.equals(srcOrg.getType())) {
            return null;
        }

        // 傳入單位層級 為 標的層級 => 回傳傳入單位
        if (Objects.equals(WkOrgUtils.getOrgLevelOrder(srcOrg.getLevel()), WkOrgUtils.getOrgLevelOrder(targetOrgLevel))) {
            return orgSid;
        }

        // 傳入單位層級 大於 標的層級 => 回傳 null
        if (WkOrgUtils.getOrgLevelOrder(srcOrg.getLevel()) > WkOrgUtils.getOrgLevelOrder(targetOrgLevel)) {
            return orgSid;
        }

        // 取得上層單位
        SimpleOrg parentOrg = srcOrg.getParent();
        // 無上層單位時, 回傳 null
        if (parentOrg == null) {
            return null;
        }

        // 遞迴往上查詢
        return findParentByOrgLevel(parentOrg.getSid(), targetOrgLevel);
    }

    /**
     * 取得傳入單位的子單位 (單一層不往下遞迴尋找)
     *
     * @param parentOrgSid
     * @return org sid list
     */
    public List<Org> getChildOrgs(Integer parentOrgSid) {
        if (parentOrgSid == null) {
            return Lists.newArrayList();
        }

        List<Org> orgs = orgClient.findChildrenByCache(parentOrgSid);

        if (WkStringUtils.isEmpty(orgs)) {
            return Lists.newArrayList();
        }

        // 排序
        return WkOrgUtils.sortOrgByOrgTree(orgs);
    }

    /**
     * 取得傳入單位的子單位 (單一層不往下遞迴尋找)
     *
     * @param parentOrgSid
     * @return org sid list
     */
    public Set<Integer> getChildOrgSids(Integer parentOrgSid) {
        List<Org> result = this.getChildOrgs(parentOrgSid);
        if (WkStringUtils.isEmpty(result)) {
            return Sets.newHashSet();
        }
        return result.stream()
                .map(Org::getSid)
                .collect(Collectors.toSet());
    }

    /**
     * 單位是否存在
     *
     * @param orgSid org Sid
     * @return y/n
     */
    public boolean isExsit(Integer orgSid) {
        if (orgSid == null) {
            return false;
        }
        Optional<Org> optional = orgClient.findBySid(orgSid);
        return optional.isPresent();
    }

    /**
     * 檢查單位是否存在
     *
     * @param orgID org ID
     * @return true / false
     */
    public boolean isExsitByID(String orgID) {
        if (WkStringUtils.isEmpty(orgID)) {
            return false;
        }
        Optional<Org> optional = orgClient.findById(orgID);
        return optional.isPresent();
    }

    /**
     * 立即清除快取
     */
    public void updateCache() {
        orgClient.updateCache();
    }

    /**
     * 取得 SEC_AUTH 資料
     * 
     * @return secAuth list
     */
    public List<SecAuth> findAllSecAuth() {
        return this.userClient.findSecAuth();
    }
}
