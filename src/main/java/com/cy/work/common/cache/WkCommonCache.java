package com.cy.work.common.cache;

import com.cy.work.common.exception.SystemDevelopException;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.CacheModel;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 公用快取工具
 *
 * @author allen1214_wu
 */
@Component
@Slf4j
public class WkCommonCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -740018924014573595L;

    // ====================================
    // implement InitializingBean
    // ====================================
    private static WkCommonCache instance;

    /**
     * get WkCommonCache Instance
     *
     * @return WkCommonCache
     */
    public static WkCommonCache getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkCommonCache instance) { WkCommonCache.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ====================================
    // 環境參數
    // ====================================
    /**
     * 快取最大存活時間 5分鐘 = 5 * 60 * 1000 豪秒 (TTS)
     */
    @Getter
    private final long timeToLiveMilliseconds = (5 * 60 * 1000);
    /**
     * 執行清除過期快取間隔時間
     */
    private final long clearOverdueCacheFixedDelay = (1 * 60 * 1000);

    // ====================================
    // 變數區
    // ====================================
    /**
     * 註記第一次清除快取
     */
    private boolean isFirstClearCache = true;
    /**
     * 快取容器
     */
    private Map<String, CacheModel> cacheDataMap = Maps.newHashMap();

    // ====================================
    // 方法區
    // ====================================

    /**
     * 將資料放入快取
     *
     * @param uniqueCacheName 快取名稱(唯一, 全系統共用)
     * @param keys            快取資料的 key
     * @param obj             快取資料
     */
    public void putCache(
            String uniqueCacheName,
            List<String> keys,
            Object obj) {
        this.cacheDataMap.put(this.prepareKey(uniqueCacheName, keys), new CacheModel(obj));
    }

    /**
     * 取得快取資料
     *
     * @param <T>                回傳物件 class
     * @param uniqueCacheName    快取名稱(唯一, 全系統共用)
     * @param keys               快取資料的 key
     * @param overdueMillisecond 逾期時間 (毫秒)
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T> T getCache(
            String uniqueCacheName,
            List<String> keys,
            long overdueMillisecond) {

        if (overdueMillisecond > timeToLiveMilliseconds) {
            throw new SystemDevelopException("傳入的資料逾期時間，大於TTS (資料最大存活時間) " + ":overdueMillisecond:" + overdueMillisecond
                    + ",timeToLiveMilliseconds:" + timeToLiveMilliseconds);
        }

        try {

            // 依據傳入類型, 取得快取容器
            CacheModel cacheModel = this.cacheDataMap.get(this.prepareKey(uniqueCacheName, keys));

            // 無資料時回傳 null
            if (cacheModel == null) {
                return null;
            }
            // 超過快取時間
            if (System.currentTimeMillis() - cacheModel.getTime() > overdueMillisecond) {
                return null;
            }

            return (T) cacheModel.getData();
        } catch (Exception e) {
            log.error("取得快取資料失敗!", e);
        }
        return null;
    }

    /**
     * @param uniqueCacheName 快取名稱(唯一, 全系統共用)
     * @param keys            快取資料的 key
     * @return key
     */
    private String prepareKey(
            String uniqueCacheName,
            List<String> keys) {
        if (WkStringUtils.isEmpty(keys)) {
            return uniqueCacheName;
        }
        return uniqueCacheName + "|" + String.join("_+_", keys);
    }

    /**
     * 清除已逾期快取, 超過最大存活時間
     */
    @Scheduled(fixedDelay = clearOverdueCacheFixedDelay)
    public void clearOverdueCache() {

        // 取得系統時間
        Long sysTime = System.currentTimeMillis();
        // 建立收集容器
        Map<String, CacheModel> newCacheDataMap = Maps.newHashMap();

        // 逐筆剔除逾期資料
        for (Entry<String, CacheModel> entry : this.cacheDataMap.entrySet()) {
            CacheModel cacheModel = entry.getValue();
            if (cacheModel == null) {
                continue;
            }

            // 超過快取時間
            if (sysTime - cacheModel.getTime() > timeToLiveMilliseconds) {
                continue;
            }

            // 收集未於其資料
            newCacheDataMap.put(entry.getKey(), entry.getValue());
        }

        // 替換收集容器
        Map<String, CacheModel> oldCacheDataMap = this.cacheDataMap;
        this.cacheDataMap = newCacheDataMap;
        oldCacheDataMap.clear();
        oldCacheDataMap = null;

        // 為避免遺忘此排程，故第一次運作時，印出執行訊息
        if (isFirstClearCache) {
            log.info(String.format("【排程】清除 WkCommonCache 開始運作, 執行間隔時間=%s sec, 資料TTS=%s sec",
                    (this.clearOverdueCacheFixedDelay / 1000), (this.timeToLiveMilliseconds / 1000)));
            isFirstClearCache = false;
        }
    }

    /**
     * 清除所有快取
     */
    public void clearAllCache() {
        // 替換收集容器
        Map<String, CacheModel> oldCacheDataMap = this.cacheDataMap;
        this.cacheDataMap = Maps.newHashMap();
        oldCacheDataMap.clear();
        oldCacheDataMap = null;
    }

}
