package com.cy.work.common.cache;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.MenuItem;
import com.cy.commons.vo.simple.SimpleRole;
import com.cy.commons.vo.special.UserWithRoles;
import com.cy.system.rest.client.MenuItemClient;
import com.cy.system.rest.client.WkMenuItemRMI;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkTreeUtils;
import com.cy.work.common.vo.WkItem;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author Allen
 */
@Component
public class WkMenuItemCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3239083199413020290L;
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WkMenuItemCache instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static WkMenuItemCache getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkMenuItemCache instance) { WkMenuItemCache.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient WkMenuItemRMI wkMenuItemRMI;
    @Autowired
    private transient MenuItemClient menuItemClient;

    // ========================================================================
    //
    // ========================================================================
    private String uniqueCacheName = this.getClass().getSimpleName();

    private long overdueMillisecond = (5 * 60 * 1000);

    // ========================================================================
    // 方法區
    // ========================================================================

    /**
     * 取得全部 MenuItem
     *
     * @return 全部組織
     */
    public List<MenuItem> findAll() {

        // ====================================
        // 先由快取中取得
        // ====================================
        List<String> cacheKeyS = Lists.newArrayList("findAll");
        List<MenuItem> allMenuItem = WkCommonCache.getInstance().getCache(
                this.uniqueCacheName,
                cacheKeyS,
                this.overdueMillisecond);

        if (allMenuItem != null) {
            return allMenuItem;
        }

        // ====================================
        // 快取中沒有時，打API查詢
        // ====================================
        // call API
        allMenuItem = this.wkMenuItemRMI.findAll(Activation.ACTIVE);
        // 放入快取
        WkCommonCache.getInstance().putCache(this.uniqueCacheName, cacheKeyS, allMenuItem);

        return allMenuItem;
    }

    /**
     * 取得全部 MenuItem
     *
     * @return 全部組織
     */
    public List<MenuItem> findByRoleSid(final Long roleSid, final Activation status) {
        // ====================================
        // 先由快取中取得
        // ====================================
        List<String> cacheKeyS = Lists.newArrayList("findByRoleSid", roleSid+"", status+"");
        List<MenuItem> menuItemsByRole = WkCommonCache.getInstance().getCache(
                this.uniqueCacheName,
                cacheKeyS,
                this.overdueMillisecond);

        if (menuItemsByRole != null) {
            return menuItemsByRole;
        }

        // ====================================
        // 快取中沒有時，打API查詢
        // ====================================
        // call API
        menuItemsByRole = this.menuItemClient.findByRoleSid(roleSid, Activation.ACTIVE);
        // 放入快取
        WkCommonCache.getInstance().putCache(this.uniqueCacheName, cacheKeyS, menuItemsByRole);

        return menuItemsByRole;
    }

    /**
     * 查詢 user 有權限的 MenuItem
     * 
     * @param userSid user sid
     * @return user 有權限的 MenuItem
     */
    public Set<MenuItem> findByUserSid(final Integer userSid) {

        // ====================================
        // 查詢 USER 角色
        // ====================================
        UserWithRoles userWithRoles = WkUserWithRolesCache.getInstance().findBySid(userSid);
        if (userWithRoles == null || WkStringUtils.isEmpty(userWithRoles.getRoles())) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集角色對應的權限
        // ====================================
        Set<MenuItem> allMenuItems = Sets.newHashSet();
        for (SimpleRole simpleRole : userWithRoles.getRoles()) {
            // 排除停用角色
            if (!Activation.ACTIVE.equals(simpleRole.getStatus())) {
                continue;
            }

            List<MenuItem> currMenuItem = this.findByRoleSid(simpleRole.getSid(), Activation.ACTIVE);
            if (WkStringUtils.notEmpty(currMenuItem)) {
                allMenuItems.addAll(currMenuItem);
            }
        }

        return allMenuItems;
    }

    /**
     * 以階層樹方式，兜組user有權限的 menu 上項目
     * 
     * @param userSid user Sid
     * @return 階層樹
     */
    public String prepareMenuItemContentTreeStyle(Integer userSid) {

        // ====================================
        // 查詢所有的 menu item
        // ====================================
        List<MenuItem> allMenuItem = this.findAll();

        // ====================================
        // 取得使用者有權限的 menuItem
        // ====================================
        Set<String> showItemSids = this.findByUserSid(userSid).stream()
                // 去除沒有網址的
                .filter(menuItem -> WkStringUtils.notEmpty(menuItem.getUrl()))
                // 轉 menu item sid
                .map(menuItem -> menuItem.getSid() + "")
                // 去除重複
                .collect(Collectors.toSet());

        // ====================================
        // MenuItem 轉 WkItem
        // ====================================
        // 全部轉 WkItem
        List<WkItem> allItems = allMenuItem.stream()
                .filter(menuItem -> !"1".equals(menuItem.getSid() + "")) // 去除 root
                .map(menuItem -> this.menuItemToWkItem(menuItem, showItemSids))
                .collect(Collectors.toList());

        // 塞入 parent
        Map<String, WkItem> itemsMapBySid = allItems.stream()
                .collect(Collectors.toMap(WkItem::getSid, WkItem::getThis));

        for (WkItem wkItem : allItems) {
            String parentSid = wkItem.getOtherInfo().get("parentSid");
            wkItem.setParent(itemsMapBySid.get(parentSid));
        }
        

        // ====================================
        // 兜組顯示內容
        // ====================================

        return WkTreeUtils.prepareSelectedItemNameByTreeStyle(allItems, showItemSids, 2000);

    }

    private WkItem menuItemToWkItem(MenuItem menuItem, Set<String> showItemSids) {

        WkItem wkItem = new WkItem(
                menuItem.getSid() + "",
                menuItem.getName(),
                Activation.ACTIVE.equals(menuItem.getStatus()));

        wkItem.setItemSeq(Long.parseLong(menuItem.getSequence() + ""));
        wkItem.getOtherInfo().put("parentSid", menuItem.getParent() == null ? null : menuItem.getParent().getSid() + "");

        if (showItemSids.contains(wkItem.getSid())) {
            String showContent = "【%s】(<span style='color:blueviolet; text-decoration: none;'>%s</span>)";
            showContent = String.format(
                    showContent,
                    menuItem.getName(),
                    menuItem.getUrl());
            wkItem.setShowTreeName(showContent);
        }

        return wkItem;
    }

}
