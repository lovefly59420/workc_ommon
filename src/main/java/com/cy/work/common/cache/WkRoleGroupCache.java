package com.cy.work.common.cache;

import com.cy.commons.vo.RoleGroup;
import com.cy.system.rest.client.RoleGroupClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * 角色群組 Cache
 *
 * @author kasim
 */
@Slf4j
@Component
public class WkRoleGroupCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3713592047603543025L;
    /**
     *
     */
    private static WkRoleGroupCache instance;
    @Autowired
    private RoleGroupClient roleGroupClient;

    public WkRoleGroupCache() {
    }

    /**
     * @return instance
     */
    public static WkRoleGroupCache getInstance() {
        return instance;
    }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkRoleGroupCache instance) {
        WkRoleGroupCache.instance = instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    /**
     * 取得全部角色群組
     *
     * @return RoleGroup list
     */
    public List<RoleGroup> findAll() {
        return roleGroupClient.findAll();
    }

    /**
     * 查詢 By sid
     *
     * @param roleGroupSid role group sid
     * @return RoleGroup list
     */
    public RoleGroup findBySid(Long roleGroupSid) {
        if (roleGroupSid == null) {
            return null;
        }
        try {

            Optional<RoleGroup> optional = roleGroupClient.findBySid(roleGroupSid);
            if (optional.isPresent()) {
                return optional.get();
            }
            log.warn("找不到 RoleGroup sid :[" + roleGroupSid + "]");
        } catch (Exception e) {
            log.error("roleGroupClient.findBySid error", e);
        }
        return null;

    }

    /**
     * 立即清除快取
     */
    public void updateCache() {
        roleGroupClient.updateCache();
    }

}
