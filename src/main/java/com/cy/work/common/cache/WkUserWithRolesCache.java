package com.cy.work.common.cache;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleRole;
import com.cy.commons.vo.special.UserWithRoles;
import com.cy.system.rest.client.special.UserWithRolesClient;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Slf4j
@Component
public class WkUserWithRolesCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4857928452318500792L;
    /**
     *
     */
    private static WkUserWithRolesCache instance;
    @Autowired
    private UserWithRolesClient userWithRolesClient;

    /**
     * @return instance
     */
    public static WkUserWithRolesCache getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkUserWithRolesCache instance) { WkUserWithRolesCache.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    /**
     * 取得使用者所有角色的SID (過濾停用)
     *
     * @param userSid user sid
     * @return UserRole sid list
     */
    public List<Long> findActiveUserRoleSidsByUser(Integer userSid) {
        // 使者用角色
        UserWithRoles userWithRoles = this.findBySid(userSid);
        List<SimpleRole> roles = Lists.newArrayList();
        if (userWithRoles != null) {
            roles = userWithRoles.getRoles();
        }
        if (WkStringUtils.isEmpty(roles)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "使用者無角色資料 user sid :[" + userSid + "]");
        }

        return roles.stream()
                .filter(role -> Activation.ACTIVE.equals(role.getStatus()))
                .map(SimpleRole::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 查詢全部
     *
     * @return UserWithRoles list
     */
    public List<UserWithRoles> findAll() {
        List<UserWithRoles> results = userWithRolesClient.findAll();
        if (results == null) {
            return Lists.newArrayList();
        }
        return results;
    }

    /**
     * @param userId user ID
     * @return UserWithRoles
     */
    public UserWithRoles findById(String userId) {

        if (WkStringUtils.isEmpty(userId)) {
            return null;
        }

        try {
            Optional<UserWithRoles> optional = userWithRolesClient.findById(userId);
            if (optional.isPresent()) {
                return optional.get();
            }
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 user id :[" + userId + "]");

        } catch (Exception e) {
            log.error("userWithRolesClient.findById error", e);
        }
        return null;

    }

    /**
     * 以 user sid 查詢
     *
     * @param userSid user sid
     * @return UserWithRoles
     */
    public UserWithRoles findBySid(Integer userSid) {
        if (userSid == null) {
            return null;
        }
        try {
            Optional<UserWithRoles> optional = this.userWithRolesClient.findBySid(userSid);
            if (optional.isPresent()) {
                return optional.get();
            }
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 user sid :[" + userSid + "]");

        } catch (Exception e) {
            log.error("userWithRolesClient.findBySid error", e);
        }
        return null;
    }

    /**
     * 查詢使用者所擁有的所有角色
     *
     * @param userSid         user Sid
     * @param loginCompanySid 登入公司別
     * @return SimpleRole
     */
    public List<SimpleRole> findUserRole(
            Integer userSid,
            Integer loginCompanySid,
            Activation roleStatus) {
        // ====================================
        // 查詢 UserWithRoles
        // ====================================
        UserWithRoles userWithRoles = this.findBySid(userSid);
        if (userWithRoles == null) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得角色並依據公司別過濾
        // ====================================
        if (WkStringUtils.isEmpty(userWithRoles.getRoles())) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "使用者不存在任何角色 user sid :[" + userSid + "]");
            return Lists.newArrayList();
        }

        return userWithRoles.getRoles()
                .stream()
                .filter(simpleRole -> simpleRole.getStatus().equals(roleStatus))
                .filter(simpleRole -> WkCommonUtils.compareByStr(loginCompanySid, simpleRole.getCompanySid()))
                .collect(Collectors.toList());

    }

    /**
     * 查詢使用者所擁有的所有角色 (僅取得非停用角色)
     *
     * @param userSid         user Sid
     * @param loginCompanySid 登入公司別
     * @return role sid
     */
    public List<Long> findUserRoleSids(
            Integer userSid,
            Integer loginCompanySid) {
        return this.findUserRole(userSid, loginCompanySid, Activation.ACTIVE)
                .stream()
                .map(SimpleRole::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 查詢所有角色 by user and 登入公司別 (僅取得非停用角色)
     *
     * @param userSid        user sid
     * @param loginCompanyID 登入公司 ID
     * @return 角色 sids
     */
    public List<Long> findRoleSidsByUserAndLoginCompID(
            Integer userSid,
            String loginCompanyID) {
        Org comp = WkOrgCache.getInstance()
                .findById(loginCompanyID);
        if (comp == null) {
            return Lists.newArrayList();
        }

        Integer loginCompanySid = comp.getSid();

        return this.findUserRole(userSid, loginCompanySid, Activation.ACTIVE)
                .stream()
                .map(SimpleRole::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 查詢使用者所擁有的所有角色名稱 (僅取得非停用角色)
     *
     * @param userSid        user Sid
     * @param loginCompanyID 登入公司別
     * @return role name
     */
    public List<String> findRoleNamesByUserAndLoginCompID(
            Integer userSid,
            String loginCompanyID) {

        Org comp = WkOrgCache.getInstance()
                .findById(loginCompanyID);
        if (comp == null) {
            return Lists.newArrayList();
        }

        Integer loginCompanySid = comp.getSid();

        return this.findUserRole(userSid, loginCompanySid, Activation.ACTIVE)
                .stream()
                .map(SimpleRole::getName)
                .collect(Collectors.toList());
    }

    /**
     * 取得使用者所有角色的SID
     *
     * @param userSid
     * @return role sid list
     */
    public List<Long> findUserRoleSidsByUser(Integer userSid) {
        // 使者用角色
        UserWithRoles userWithRoles = this.findBySid(userSid);
        List<SimpleRole> roles = Lists.newArrayList();
        if (userWithRoles != null) {
            roles = userWithRoles.getRoles();
        }
        if (WkStringUtils.isEmpty(roles)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "使用者無角色資料 user sid :[" + userSid + "]");
        }

        return roles.stream()
                .map(SimpleRole::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 取得使用者的所有角色名稱
     *
     * @param userSid user sid
     * @return role name list
     */
    public List<String> findRolesNameByUserSid(Integer userSid) {
        // 使者用角色
        UserWithRoles userWithRoles = this.findBySid(userSid);
        List<SimpleRole> roles = Lists.newArrayList();
        if (userWithRoles != null) {
            roles = userWithRoles.getRoles();
        }
        if (WkStringUtils.isEmpty(roles)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "使用者無角色資料 user sid :[" + userSid + "]");
        }

        return roles.stream()
                .map(SimpleRole::getName)
                .collect(Collectors.toList());
    }

    /**
     * 取得擁有此角色的使用者Sid
     *
     * @param roleSid role sid
     * @return user sid list
     */
    public List<Integer> findUserByRoleSid(Long roleSid) {
        if (roleSid == null) {
            return Lists.newArrayList();
        }
        // Long startTime = System.currentTimeMillis();
        try {
            // 過濾所有資料, 並取得 user SID
            List<Integer> results = findAll().stream()
                    // 過濾掉已停用 user
                    .filter(userWithRoles -> Activation.ACTIVE.equals(userWithRoles.getStatus()))
                    // 過濾掉沒有此角色的 user
                    .filter(userWithRoles -> {
                        // 防呆
                        if (userWithRoles == null || WkStringUtils.isEmpty(userWithRoles.getRoles())) {
                            return false;
                        }
                        // 檢核此 user 是否有此角色
                        for (SimpleRole role : userWithRoles.getRoles()) {
                            if (WkCommonUtils.compareByStr(roleSid + "", role.getSid() + "")) {
                                return true;
                            }
                        }
                        return false;
                    })
                    .map(UserWithRoles::getSid)
                    .collect(Collectors.toList());

            if (results == null) {
                return Lists.newArrayList();
            }

            return results;

        } catch (Exception e) {
            log.error("依角色取得所有 user 時發生錯誤!", e);
        }

        return Lists.newArrayList();
    }

    /**
     * 以角色名稱取得有此角色的 user
     * @param targetRoleName 角色名稱
     * @param loginCompanySid 公司 sid
     * @param active 停用或啟用 user (傳入 null 時，不過濾)
     * @return user sid list
     */
    public Set<Integer> findUserSidByRoleName(String targetRoleName, Integer loginCompanySid, Activation active) {

        // ====================================
        // 比對公司別、角色名稱
        // ====================================
        List<User> matchUsers = findAll().stream()
                .filter(userWithRoles -> {
                    // 防呆
                    if (userWithRoles == null || WkStringUtils.isEmpty(userWithRoles.getRoles())) {
                        return false;
                    }

                    // 檢核此 user 是否有此角色
                    for (SimpleRole simpleRole : userWithRoles.getRoles()) {
                        // 比對公司
                        if (!WkCommonUtils.compareByStr(loginCompanySid, simpleRole.getCompanySid())) {
                            continue;
                        }
                        // 比對角色名稱
                        if (WkCommonUtils.compareByStr(targetRoleName, simpleRole.getName())) {
                            return true;
                        }
                    }
                    return false;
                })
                .map(userWithRoles -> WkUserCache.getInstance().findBySid(userWithRoles.getSid()))
                .collect(Collectors.toList());

        if (WkStringUtils.isEmpty(matchUsers)) {
            return Sets.newHashSet();
        }

        // ====================================
        // 過濾 active
        // ====================================
        if (active != null) {
            matchUsers = matchUsers.stream()
                    .filter(user -> WkUserUtils.isActive(user))
                    .collect(Collectors.toList());

        }

        // ====================================
        // 整理 userSid
        // ====================================
        return matchUsers.stream()
                .map(User::getSid)
                .collect(Collectors.toSet());

    }

    /**
     * 立即清除快取
     */
    public void updateCache() {
        userWithRolesClient.updateCache();
    }

}
