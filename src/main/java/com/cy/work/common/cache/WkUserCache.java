package com.cy.work.common.cache;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.system.rest.client.UserClient;
import com.cy.system.rest.client.vo.SecAuth;
import com.cy.work.common.client.skypebot.SkypeBotClient;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author allen
 */
@Slf4j
@Component
public class WkUserCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4524755553243565L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WkUserCache instance;

    /**
     * @return instance
     */
    public static WkUserCache getInstance() { return instance; }

    private static void setInstance(WkUserCache instance) { WkUserCache.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private UserClient userClient;

    // ========================================================================
    // 方法
    // ========================================================================
    /**
     * user是否存在
     *
     * @param userSid user Sid
     * @return y/n
     */
    public boolean isExsit(Integer userSid) {
        if (userSid == null) {
            return false;
        }
        Optional<User> optional = userClient.findBySid(userSid);
        return optional.isPresent();
    }

    /**
     * 查詢系統中所有 user (為空時回傳 empty list)
     *
     * @return user list
     */
    public List<User> findAll() {
        List<User> users = userClient.findAll();
        if (users == null) {
            users = Lists.newArrayList();
        }

        return users;
    }

    /**
     * 以 User Sid 查詢 User 資料檔
     *
     * @param userSid userSid
     * @return User
     */
    public User findBySid(Integer userSid) {
        if (userSid == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 user sid 為 null");
            return null;
        }
        try {
            Optional<User> optional = userClient.findBySid(userSid);
            if (optional.isPresent()) {
                return optional.get();
            }

            if (userSid >= 0) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 user sid :[" + userSid + "]");
            }

        } catch (Exception e) {
            log.error("userClient error", e);
        }
        return null;
    }

    /**
     * 以 User Sid 查詢 User 資料檔 (多筆)
     *
     * @param userSids userSids
     * @return user list
     */
    public List<User> findBySids(Collection<Integer> userSids) {

        if (WkStringUtils.isEmpty(userSids)) {
            return Lists.newArrayList();
        }

        List<User> users = Lists.newArrayList();
        for (Integer userSid : userSids) {
            User user = this.findBySid(userSid);
            if (user != null) {
                users.add(user);
            }
        }
        return users;
    }

    /**
     * 以 User Sid 查詢 User 資料檔 (多筆)
     *
     * @param userSids userSids
     * @return user list
     */
    public List<User> findBySidStrs(Collection<String> userSids) {

        if (WkStringUtils.isEmpty(userSids)) {
            return Lists.newArrayList();
        }

        List<User> users = Lists.newArrayList();
        for (String userSid : userSids) {
            if (!WkStringUtils.isNumber(userSid)) {
                continue;
            }
            User user = this.findBySid(userSid);
            if (user != null) {
                users.add(user);
            }
        }
        return users;
    }

    /**
     * 以 user Sid 查詢
     *
     * @param userSid
     * @return user
     */
    public User findBySid(String userSid) {
        userSid = WkStringUtils.safeTrim(userSid);
        if (WkStringUtils.isEmpty(userSid)) {
            return null;
        }
        if (!WkStringUtils.isNumber(userSid)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 org sid 不是數字:[" + userSid + "]");
            return null;
        }

        return findBySid(Integer.parseInt(userSid));
    }

    /**
     * 以 user ID 查詢
     *
     * @param userId user ID
     * @return user
     */
    public User findById(String userId) {
        if (WkStringUtils.isEmpty(userId)) {
            return null;
        }

        try {
            Optional<User> optional = userClient.findById(userId);
            if (optional.isPresent()) {
                return optional.get();
            }

            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 userId :[" + userId + "]");
        } catch (Exception e) {
            log.error("userClient error", e);
        }
        return null;
    }

    /**
     * 以 user ID 查詢 , 但查不到資料時，不印 LOG (給查詢BPM 對應單位ID用)
     *
     * @param userId user ID
     * @return user
     */
    public User findByIdAndNotCheckEmpty(String userId) {
        if (WkStringUtils.isEmpty(userId)) {
            return null;
        }

        try {
            Optional<User> optional = userClient.findById(userId);
            if (optional.isPresent()) {
                return optional.get();
            }
        } catch (Exception e) {
            log.error("userClient error", e);
        }
        return null;
    }

    /**
     * 以 UUID 查詢
     *
     * @param uuid
     * @return user
     */
    public User findByUUID(String uuid) {
        if (WkStringUtils.isEmpty(uuid)) {
            return null;
        }

        try {
            Optional<User> optional = userClient.findByUuid(uuid);
            if (optional.isPresent()) {
                return optional.get();
            }

            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 user uuid :[" + uuid + "]");
        } catch (Exception e) {
            log.error("userClient error", e);
        }
        return null;
    }

    /**
     * 查詢單位下的使用者 (不過濾停用)
     *
     * @param primaryOrgSid
     * @return user list
     */
    public List<User> findUsersByPrimaryOrgSid(Integer primaryOrgSid) {
        return this.findAll()
                .stream()
                .filter(each -> each.getPrimaryOrg() != null && each.getPrimaryOrg()
                        .getSid() != null)
                .filter(each -> each.getPrimaryOrg()
                        .getSid()
                        .equals(primaryOrgSid))
                .collect(Collectors.toList());
    }

    /**
     * 取得 user 的主要部門
     * 
     * @param userSid user sid
     * @return 主要部門 sid (找不到時，回傳null)
     */
    public Integer findPrimaryOrgSidByUserSid(Integer userSid) {
        // ====================================
        // 查詢 user 資料
        // ====================================
        User user = findBySid(userSid);
        if (user == null) {
            return null;
        }

        // ====================================
        // 檢核
        // ====================================
        String errorMessage = "";

        if (user.getPrimaryOrg() == null
                || user.getPrimaryOrg().getSid() == null) {
            errorMessage = "user:[" + userSid + "] 沒有 PrimaryOrg 資料!";
            return null;
        }

        Org primaryOrg = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());
        if (primaryOrg == null) {
            errorMessage = "user:[" + userSid + "] 的 PrimaryOrg 找不到 PrimaryOrgSid:[" + user.getPrimaryOrg().getSid() + "]!";
            log.warn("User:[{}-{}] 找不到 PrimaryOrg:[{}] 資料! ", user.getSid(), user.getName(), user.getPrimaryOrg().getSid());
            return null;
        }

        if (WkStringUtils.notEmpty(errorMessage)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, errorMessage);
            try {
                SkypeBotClient.getInstance().sendMessageToChat("系統警告", errorMessage);
            } catch (SystemOperationException e) {
                log.error(e.getMessage(), e);
            }
            return null;
        }

        // ====================================
        // 回傳
        // ====================================
        return user.getPrimaryOrg().getSid();
    }

    /**
     * 查詢單位下的使用者 (僅回傳Active、過濾停用)
     *
     * @param primaryOrgSid
     * @return user list
     */
    public List<User> getUsersByPrimaryOrgSid(Integer primaryOrgSid) {
        if (primaryOrgSid == null) {
            return null;
        }
        try {
            return userClient.findAll()
                    .stream()
                    .filter(each -> each.getPrimaryOrg() != null && each.getPrimaryOrg()
                            .getSid() != null)
                    .filter(each -> each.getPrimaryOrg()
                            .getSid()
                            .equals(primaryOrgSid))
                    .filter(each -> Activation.ACTIVE.equals(each.getStatus()))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("getUsersByPrimaryOrgSid error : ", e);
        }
        return Lists.newArrayList();
    }

    /**
     * 查詢單位下的 user
     *
     * @param primaryOrgSids
     * @return user list
     */
    public List<User> findByPrimaryOrgSidsIn(Collection<Integer> primaryOrgSids) {
        if (WkStringUtils.isEmpty(primaryOrgSids)) {
            return Lists.newArrayList();
        }
        try {
            return userClient.findByPrimaryOrgSidsIn(Lists.newArrayList(primaryOrgSids));
        } catch (Exception e) {
            log.error("findByPrimaryOrgSidsIn error : ", e);
        }
        return Lists.newArrayList();
    }

    @Deprecated // 請改用 findUserWithManagerByOrgSids
    public List<User> findByOrgsWithManager(
            Collection<Integer> orgSids,
            Activation active) {
        return this.findUserWithManagerByOrgSids(orgSids, active);
    }

    /**
     * 查詢傳入部門下, 所有的 user , 包含代理主管 (不包含以下部門)
     *
     * @param orgSids 部門 sid
     * @param active  過濾帳號狀態 (傳入 null 時不過濾)
     * @return user list
     */
    public List<User> findUserWithManagerByOrgSids(
            Collection<Integer> orgSids,
            Activation active) {

        if (WkStringUtils.isEmpty(orgSids)) {
            return Lists.newArrayList();
        }

        Set<User> users = Sets.newHashSet();

        // ====================================
        // 查詢傳入部門，所有的 user (不包含以下部門)
        // ====================================
        List<User> usersByPrimaryOrg = this.findByPrimaryOrgSidsIn(orgSids);
        if (WkStringUtils.notEmpty(usersByPrimaryOrg)) {
            users.addAll(usersByPrimaryOrg);
        }

        // ====================================
        // 加入部門主管 (兼職時，上面方法撈不到)
        // ====================================
        // 取得部門 manager （加入代理部門主管->主管的主要部門不在此單位）
        for (Integer orgSid : orgSids) {
            // 查詢部門資料
            Org org = WkOrgCache.getInstance().findBySid(orgSid);
            if (org == null) {
                continue;
            }

            // 部門主管
            if (WkStringUtils.notEmpty(org.getManagers())) {
                for (SimpleUser simpleUser : org.getManagers()) {
                    User manager = this.findBySid(simpleUser.getSid());
                    if (manager != null) {
                        users.add(manager);
                    }
                }
            }

            // 部門副主管
            if (WkStringUtils.notEmpty(org.getDeputyManagers())) {
                for (SimpleUser deputyManagerSimpleUser : org.getDeputyManagers()) {
                    User deputyManager = this.findBySid(deputyManagerSimpleUser.getSid());
                    if (deputyManager != null) {
                        users.add(deputyManager);
                    }
                }
            }
        }

        // ====================================
        // 過濾 active 狀態
        // ====================================
        // 未傳入狀態時, 直接回傳
        if (active == null) {
            return Lists.newArrayList(users);
        }

        // 有傳入狀態時，進行過濾
        return users.stream()
                .filter(user -> active.equals(user.getStatus()))
                .collect(Collectors.toList());

    }

    /**
     * 查詢傳入部門下, 所有的 user , 包含代理主管 (不會重複)
     *
     * @param orgSids 部門 sid
     * @param active  過濾帳號狀態 (傳入 null 時不過濾)
     * @return user sid list
     */
    public Set<Integer> findUserSidByOrgsWithManager(
            Collection<Integer> orgSids,
            Activation active) {
        return this.findUserWithManagerByOrgSids(orgSids, active)
                .stream()
                .map(User::getSid)
                .distinct()
                .collect(Collectors.toSet());
    }

    /**
     * 查詢系統中公司別登入權限設定 (為空時回傳 empty list)
     *
     * @return SecAuth list
     */
    public List<SecAuth> findSecAuth() {
        List<SecAuth> secAuths = userClient.findSecAuth();
        if (secAuths == null) {
            secAuths = Lists.newArrayList();
        }

        return secAuths;
    }

    /**
     * 查詢公司下所有 user
     *
     * @param compID 公司 ID
     * @return user list
     */
    public List<User> findAllUserByCompID(String compID) {

        // 轉換公司 ID -> SID
        compID = WkStringUtils.safeTrim(compID);
        Org comp = WkOrgCache.getInstance()
                .findById(compID);
        if (comp == null) {
            return Lists.newArrayList();
        }

        // 以使用者所屬公司過濾
        return this.findAll().stream()
                .filter(user -> {
                    if (user.getPrimaryOrg() == null) {
                        return false;
                    }
                    return WkCommonUtils.compareByStr(comp.getSid(), user.getPrimaryOrg()
                            .getCompanySid());
                })
                .collect(Collectors.toList());
    }

    /**
     * 查詢公司下所有 user
     *
     * @param compID 公司 ID
     * @return user sid list
     */
    public Set<Integer> findAllUserSidByCompID(String compID) {
        return findAllUserByCompID(compID).stream()
                .map(User::getSid)
                .collect(Collectors.toSet());
    }

    /**
     * 立即清除快取
     */
    public void updateCache() {
        userClient.updateCache();
    }
}
