package com.cy.work.common.cache;

import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.vo.CacheModel;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 公用長快取
 *
 * @author allen1214_wu
 */
@Component
@Slf4j
public class WkPersistCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -740018924014573595L;

    // ====================================
    // implement InitializingBean
    // ====================================
    private static WkPersistCache instance;

    /**
     * get WkCommonCache Instance
     *
     * @return WkCommonCache
     */
    public static WkPersistCache getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkPersistCache instance) { WkPersistCache.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ====================================
    // 變數區
    // ====================================
    /**
     * 快取容器
     */
    private Map<String, CacheModel> cacheDataMap = Maps.newHashMap();

    // ====================================
    // 方法區
    // ====================================

    /**
     * 將資料放入快取
     *
     * @param uniqueCacheName 快取名稱(唯一, 全系統共用)
     * @param keys            快取資料的 key
     * @param obj             快取資料
     */
    public void putCache(
            String uniqueCacheName,
            List<String> keys,
            Object obj) {
        this.cacheDataMap.put(this.prepareKey(uniqueCacheName, keys), new CacheModel(obj));
    }

    /**
     * 取得快取資料
     *
     * @param <T>             回傳物件 class
     * @param uniqueCacheName 快取名稱(唯一, 全系統共用)
     * @param keys            快取資料的 key
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T> T getCache(
            String uniqueCacheName,
            List<String> keys) {

        try {

            // 依據傳入類型, 取得快取容器
            CacheModel cacheModel = this.cacheDataMap.get(this.prepareKey(uniqueCacheName, keys));

            // 無資料時回傳 null
            if (cacheModel == null) {
                return null;
            }

            return (T) cacheModel.getData();
        } catch (Exception e) {
            log.error("取得快取資料失敗!", e);
        }
        return null;
    }

    /**
     * @param uniqueCacheName 快取名稱(唯一, 全系統共用)
     * @param keys            快取資料的 key
     * @return key
     */
    private String prepareKey(
            String uniqueCacheName,
            List<String> keys) {
        if (WkStringUtils.isEmpty(keys)) {
            return uniqueCacheName;
        }
        return uniqueCacheName + "|" + String.join("_+_", keys);
    }

    /**
     * 清除已逾期快取, 超過最大存活時間
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void scheduleClearCache() {

        log.info(String.format("【排程】清除 WkPersistCache 開始運作, 執行時間每日1點"));
        this.clearAllCache();

    }

    /**
     * 清除所有快取
     */
    public void clearAllCache() {
        // 替換收集容器
        Map<String, CacheModel> oldCacheDataMap = this.cacheDataMap;
        this.cacheDataMap = Maps.newHashMap();
        oldCacheDataMap.clear();
        oldCacheDataMap = null;
    }

}
