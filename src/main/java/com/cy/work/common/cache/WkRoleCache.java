/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.cache;

import com.cy.commons.vo.Role;
import com.cy.system.rest.client.RoleClient;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author brain0925_liao
 */
@Component
@Slf4j
public class WkRoleCache implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1487233409898935066L;
    /**
     *
     */
    private static WkRoleCache instance;
    @Autowired
    private RoleClient roleClient;

    /**
     * @return WkRoleCache
     */
    public static WkRoleCache getInstance() {
        return instance;
    }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkRoleCache instance) {
        WkRoleCache.instance = instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    /**
     * 查詢全部 role 資料
     *
     * @return role list
     */
    public List<Role> findAll() {
        return roleClient.findAll();
    }

    /**
     * 以 sid 查詢
     *
     * @param roleSid role sid
     * @return Role
     */
    public Role findBySid(Long roleSid) {
        if (roleSid == null) {
            return null;
        }
        try {
            Optional<Role> optional = roleClient.findBySid(roleSid);
            if (optional.isPresent()) {
                return optional.get();
            }
            log.warn("找不到角色 sid :[" + roleSid + "]");
        } catch (Exception e) {
            log.error("roleClient.findBySid error", e);
        }
        return null;
    }

    /**
     * 以角色名稱查詢
     *
     * @param roleName
     * @param compID
     * @return role list
     */
    public List<Role> findByRoleNameAndCompID(
            String roleName,
            String compID) {
        if (WkStringUtils.isEmpty(roleName)) {
            log.warn("找不到角色名稱 :[{}]", roleName);
            return Lists.newArrayList();
        }

        List<Role> roles = this.roleClient.findAll();
        if (roles == null) {
            return Lists.newArrayList();
        }

        return roles.stream()
                .filter(role -> role.getCompany() != null)
                .filter(role -> WkCommonUtils.compareByStr(role.getCompany()
                        .getId(), compID))
                .filter(role -> WkCommonUtils.compareByStr(role.getName(), roleName))
                .collect(Collectors.toList());

    }

    /**
     * 以角色名稱查詢
     *
     * @param roleName 角色名稱
     * @param compID   公司別 ID
     * @return role sid list
     */
    public List<Long> findSidByRoleNameAndCompID(
            String roleName,
            String compID) {
        return this.findByRoleNameAndCompID(roleName, compID)
                .stream()
                .map(Role::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 立即清除快取
     */
    public void updateCache() {
        roleClient.updateCache();
    }

}
