package com.cy.work.common.enums;

import com.cy.work.common.utils.WkStringUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 緊急度
 *
 * @author shaun
 */
@Slf4j
public enum UrgencyType {

    GENERAL("一般"), URGENT("緊急");

    @Getter
    private String value;

    UrgencyType(String value) {
        this.value = value;
    }

    /**
     * 以名稱取得對應 UrgencyType
     *
     * @param str type name
     * @return UrgencyType
     */
    public static UrgencyType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (UrgencyType item : UrgencyType.values()) {
                if (item.name()
                        .equals(str)) {
                    return item;
                }
            }
        }
        return null;
    }

    /**
     * 以index取得對應 UrgencyType
     *
     * @param index index
     * @return UrgencyType
     */
    public static UrgencyType safeIndexOf(Integer index) {
        if (index == null) {
            log.warn("UrgencyType 轉換傳入 index 為空");
            return null;
        }
        if (index < 0 || index > UrgencyType.values().length - 1) {
            log.warn("UrgencyType 轉換傳入 index 錯誤:[{}]", index);
            return null;
        }
        return UrgencyType.values()[index];
    }

}
