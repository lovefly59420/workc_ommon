/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.enums;

import lombok.Getter;

/**
 * LOG 等級
 */
public enum InfomationLevel {

    DEBUG("除錯"),
    INFO("資訊"),
    WARN("警告"),
    ERROR("錯誤");

    @Getter
    private String desc;

    /**
     * @param desc
     */
    InfomationLevel(String desc) {
        this.desc = desc;
    }
}
