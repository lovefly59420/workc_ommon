package com.cy.work.common.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public enum WkStrFormatterType {

    USER_ID("#USER_ID#", "使用者ID"),
    USER_NAME("#USER_NAME#", "使用者名稱"),
    DEPT_0("#DEPT_0#", "使用者所屬部門(最高串到組)"),
    DEPT_1("#DEPT_1#", "使用者所屬部門(最高串到部)"),
    DEPT_2("#DEPT_2#", "使用者所屬部門(最高串到處)"),
    USER_EXT("#USER_EXT#", "使用者分機，會回傳 【#1234】, 未設定時回傳空字串"),
    SYS_DATE_1("#SYS_DATE_1#", "系統日 (YYYY/MM/DD)"),
    SYS_DATE_2("#SYS_DATE_2#", "系統日 (YYYY-MM-DD)"),
    SYS_TIME_1("#SYS_TIME_1#", "系統時間 (HH:mm:ss)"),

    ;

    /** 類別辨識關鍵字 */
    @Getter
    private final String typeKeyword;
    /** 說明 */
    @Getter
    private final String descr;

    WkStrFormatterType(String typeKeyword, String descr) {
        this.typeKeyword = typeKeyword;
        this.descr = descr;
    }

    /**
     * 將傳入的關鍵字字串，轉為列舉
     * 
     * @param typeKeyword 關鍵字
     * @return WkStrFormatterType
     */
    public static WkStrFormatterType trnsFromTypeKeyword(String typeKeyword) {
        String currTypeKeyword = WkStringUtils.safeTrim(typeKeyword).toUpperCase();
        for (WkStrFormatterType strFormatterType : WkStrFormatterType.values()) {
            if (strFormatterType.getTypeKeyword().equals(currTypeKeyword)) {
                return strFormatterType;
            }
        }
        return null;
    }

}
