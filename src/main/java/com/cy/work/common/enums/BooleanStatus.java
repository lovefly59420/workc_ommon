/**
 *
 */
package com.cy.work.common.enums;

import lombok.Getter;

/**
 * @author allen1214_wu
 *
 */
public enum BooleanStatus {

    TRUE("是"),
    FALSE("否");

    /** 欄位名稱 */
    @Getter
    private final String descr;

    BooleanStatus(String descr) {
        this.descr = descr;
    }

}
