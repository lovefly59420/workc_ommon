/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.enums;

import com.cy.work.common.utils.WkStringUtils;

import lombok.Getter;

/**
 * 閱讀狀態碼
 *
 * @author shaun
 */
public enum ReadRecordType {

    //讀取過之後，單據有新的異動
    WAIT_READ("待閱讀"),
    //從未讀取
    UN_READ("未閱讀"),
    //讀取過之後，單據沒有新的異動
    HAS_READ("已閱讀"),
    SYSTEM_UPDATE("系統更新"),
    //待閱讀+未閱讀
    NEED_READ("需閱讀（未＆待閱讀）");

    @Getter
    private String value;

    ReadRecordType(String value) {
        this.value = value;
    }
    
    /**
     * @param str 列舉 name 字串
     * @return RequireStatusType
     */
    public static ReadRecordType safeValueOf(String str) {
        if (WkStringUtils.notEmpty(str)) {
            for (ReadRecordType enumType : ReadRecordType.values()) {
                if (enumType.name().equals(str)) {
                    return enumType;
                }
            }
        }
        return null;
    }

}
