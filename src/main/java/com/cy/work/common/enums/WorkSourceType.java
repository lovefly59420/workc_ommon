/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.enums;

import lombok.Getter;

/**
 * 送測來源
 *
 * @author shaun
 */
public enum WorkSourceType {

    TECH_ISSUE("案件單"),
    TECH_REQUEST("需求單");

    @Getter
    private String value;

    WorkSourceType(String value) {
        this.value = value;
    }
}
