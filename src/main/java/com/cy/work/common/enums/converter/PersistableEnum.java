package com.cy.work.common.enums.converter;

public interface PersistableEnum<T> {
    /**
     * A mapping from an enum value to a type T (usually a String, Integer etc.).
     */
    T getCode();
}
