/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.enums;

import lombok.Getter;

/**
 * 模組群組
 * 
 * @author allen1214_wu
 */
public enum WerpModuleGroup {
    FIN("財務專案(Financial)", 1),
    HR("人資專案(Human Resources System)", 2),
    WORK("工作記錄專案(Work Record)", 3),
    COMMON("共用專案(Common)", 4),
    OTHER("其他專案(Other)", 5);

    @Getter
    private String label;

    @Getter
    private int sortSeq;

    private WerpModuleGroup(String label, int sortSeq) {
        this.label = label;
        this.sortSeq = sortSeq;
    }
}
