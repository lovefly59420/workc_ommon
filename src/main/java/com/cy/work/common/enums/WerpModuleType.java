package com.cy.work.common.enums;

import lombok.Getter;

/**
 * 模組代號
 * 
 * @author allen1214_wu
 */
public enum WerpModuleType {

    // 財務專案(Financial)
    ACC(WerpModuleGroup.FIN, "總帳", "financial"),
    ADM(WerpModuleGroup.FIN, "請款", "financial"),
    AST(WerpModuleGroup.FIN, "固資", "financial"),
    PUR(WerpModuleGroup.FIN, "請採驗", "pur"),
    FM(WerpModuleGroup.FIN, "公基金", "provident"),
    CAP(WerpModuleGroup.FIN, "資金", "cap"),

    // 人資專案(Human Resources System)
    NHR(WerpModuleGroup.HR, "人事", ""),
    HRA(WerpModuleGroup.HR, "考勤", ""),
    ZCKIO(WerpModuleGroup.HR, "線上簽到退", ""),
    WOT(WerpModuleGroup.HR, "加班單", ""),
    SR(WerpModuleGroup.HR, "上下班提醒", ""),
    ACSABNORMA(WerpModuleGroup.HR, "忘簽到退", ""),
    HRI(WerpModuleGroup.HR, "保險", ""),

    // 訓練、評核、報名
    PDS(WerpModuleGroup.HR, "績效評核", "pds"),
    PAS(WerpModuleGroup.HR, "試用期評核", "pas"),
    RGS(WerpModuleGroup.HR, "報名", "rgs"),
    ANNO(WerpModuleGroup.HR, "公告", "anno"),
    QUS(WerpModuleGroup.HR, "問卷", "qus"),
    MAS(WerpModuleGroup.HR, "月考核", "month-assessment"),
    WAS(WerpModuleGroup.HR, "週考核", "week-assessment"),

    // 工作記錄專案(Word Record)
    TECH(WerpModuleGroup.WORK, "案件單", "tech.issue"),
    REQ(WerpModuleGroup.WORK, "需求單", "tech.request"),
    WORKCOMMU(WerpModuleGroup.WORK, "工作聯絡單", "work-connect"),
    WORKRPT(WerpModuleGroup.WORK, "工作報告", ""),
    CSM(WerpModuleGroup.WORK, "客戶留言", "customer-msg"),
    PET(WerpModuleGroup.WORK, "簽呈", "petition"),

    // 共用專案(Common)
    IPMGR(WerpModuleGroup.WORK, "IP控管", ""),
    SYS(WerpModuleGroup.WORK, "System WEB", ""),

    // 其他專案(Other)
    CUSVIST(WerpModuleGroup.WORK, "客戶來訪", "custvisit"),
    MM(WerpModuleGroup.WORK, "會議管理", ""),
    ;

    /**
     * 模組分類
     */
    @Getter
    private WerpModuleGroup group;

    /**
     * 顯示名稱
     */
    @Getter
    private String label;

    /**
     * 
     */
    @Getter
    private String urlPropKeyPrefix;

    /**
     * REST 控制項目說明
     */
    @Getter
    private String restHandleDesc;

    private WerpModuleType(
            WerpModuleGroup group,
            String label,
            String urlPropKeyPrefix) {
        this.group = group;
        this.label = label;
        this.urlPropKeyPrefix = urlPropKeyPrefix;
    }
}
