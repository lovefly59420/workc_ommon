package com.cy.work.common.enums.converter;

import javax.persistence.AttributeConverter;
import java.util.function.Function;

/**
 * 公用 Enum Converter 抽象類別
 *
 * @param <E>
 * @param <T>
 * @author allen1214_wu
 */
public abstract class AbstractEnumConverter<E extends Enum<E> & PersistableEnum<T>, T>
        implements AttributeConverter<E, T> {

    private Function<T, E> fromCodeToEnum;

    protected AbstractEnumConverter(Function<T, E> fromCodeToEnum) {
        this.fromCodeToEnum = fromCodeToEnum;
    }

    @Override
    public T convertToDatabaseColumn(E persistableEnum) {
        return persistableEnum == null ? null : persistableEnum.getCode();
    }

    @Override
    public E convertToEntityAttribute(T code) {
        return code == null ? null : fromCodeToEnum.apply(code);
    }
}
