package com.cy.work.common.enums;

import lombok.Getter;

/**
 * The Enum InstanceStatus,單據狀態.
 *
 * @author Mars
 */
public enum InstanceStatus {

    NEW_INSTANCE("新建檔", "PAPER000"),
    WAITING_FOR_APPROVE("待簽核", "PAPER001"),
    APPROVING("進行中", "PAPER002"),
    APPROVED("核准", "PAPER003"),
    RECONSIDERATION("再議", "PAPER004"),
    INVALID("作廢", "PAPER005"),
    NOT_ALLOW("不准", "PAPER006"),
    TELLER_CHECK("出納確認", "PAPER007"),
    ACCOUNTANT_CHECK("會計確認", "PAPER008"),
    REJECT_TO_RESIGN("退回重簽", "PAPER009"),
    WAIT_FOR_ATTACHMENT("需附檔", "PAPER010"),
    CANCEL("取消", "PAPER100"),
    CLOSED("結案", "PAPER999"),
    CANCEL_AND_CLOSED("取消 並 結案", "PAPER101"),
    TEMPORARY_INSTANCE("暫存檔", "PAPER011"),
    NOT_INPUT("尚未填寫", "PAPER012"),
    AUTO_CLOSED("自動結案", "PAPER013"),
    REQUIRE_SUSPENDED_CANCLE("需求暫緩退件", "PAPER014"),
    ;

    @Getter
    private String label;
    @Getter
    private String value;

    InstanceStatus(
            String label,
            String value) {
        this.label = label;
        this.value = value;
    }

    public static InstanceStatus fromPaperCode(String pCode) {
        if (pCode == null) {
            return null;
        }

        switch (pCode) {
            case "PAPER000":
                return InstanceStatus.NEW_INSTANCE;
            case "PAPER001":
                return InstanceStatus.WAITING_FOR_APPROVE;
            case "PAPER002":
                return InstanceStatus.APPROVING;
            case "PAPER003":
                return InstanceStatus.APPROVED;
            case "PAPER004":
                return InstanceStatus.RECONSIDERATION;
            case "PAPER005":
                return InstanceStatus.INVALID;
            case "PAPER006":
                return InstanceStatus.NOT_ALLOW;
            case "PAPER007":
                return InstanceStatus.TELLER_CHECK;
            case "PAPER008":
                return InstanceStatus.ACCOUNTANT_CHECK;
            case "PAPER009":
                return InstanceStatus.REJECT_TO_RESIGN;
            case "PAPER010":
                return InstanceStatus.WAIT_FOR_ATTACHMENT;
            case "PAPER011":
                return InstanceStatus.TEMPORARY_INSTANCE;
            case "PAPER012":
                return InstanceStatus.NOT_INPUT;
            case "PAPER100":
                return InstanceStatus.CANCEL;
            case "PAPER101":
                return InstanceStatus.CANCEL_AND_CLOSED;
            case "PAPER999":
                return InstanceStatus.CLOSED;
            case "PAPER013":
                return InstanceStatus.AUTO_CLOSED;
            case "PAPER014":
                return InstanceStatus.REQUIRE_SUSPENDED_CANCLE;
            default:
                throw new IllegalArgumentException("無法轉換 " + pCode + " 為 " + InstanceStatus.class
                        .getSimpleName());
        }
    }

    public InstanceStatus valueOfPapercode(String papercode) {
        return InstanceStatus.fromPaperCode(papercode);
    }

    public String getResourceName() {
        return this.getClass()
                .getSimpleName() + "." + this.name();
    }
}
