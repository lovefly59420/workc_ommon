/**
 *
 */
package com.cy.work.common.enums;

import lombok.Getter;

/**
 * @author allen1214_wu
 *
 */
public enum ShowMode {

    LIST("列表模式"),
    TREE("樹模式");

    /** 欄位名稱 */
    @Getter
    private final String label;

    ShowMode(String label) {
        this.label = label;
    }

}
