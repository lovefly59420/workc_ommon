package com.cy.work.common.utils;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * 版本取得工具
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkVerUtil {
    /**
     * 快取
     */
    private static HashMap<String, String> verCache = Maps.newHashMap();

    /**
     * 查詢版本
     * 
     * @return 版本字串
     */
    public static String findVersion() {

        if (!verCache.containsKey("version")) {
            final Properties properties = new Properties();
            try {
                properties.load(WkVerUtil.class.getClassLoader()
                        .getResourceAsStream("pomParmTransfer.properties"));

                for (Entry<Object, Object> valuePair : properties.entrySet()) {
                    verCache.put(valuePair.getKey() + "", valuePair.getValue() + "");
                    log.info(valuePair.getKey() + "[" + valuePair.getValue() + "]");
                }
            } catch (IOException e) {
                log.error("取得pom設定資訊失敗！", e);
            }
        }

        String ver = verCache.get("version");
        if (WkStringUtils.isEmpty(ver)) {
            log.error("取得 version 資訊失敗!");
        }

        return ver;

    }

    /**
     * 在 UAT 環境上無效. 暫時取消
     * <p>
     * 取得程式庫建置版本別<br/>
     * 注意: jar 檔未放在 web-inf lib 下者無效 (例如放在 tomcat 目錄下)<br/>
     *
     * @param groupId    group ID
     * @param artifactId artifact ID
     * @return version string
     */
    public static String findLibVersion(
            String groupId,
            String artifactId) {

        // ====================================
        // 快取
        // ====================================
        String verCacheKey = groupId + ":" + artifactId;
        if (verCache.containsKey(verCacheKey)) {
            return verCache.get(verCacheKey);
        }

        // ====================================
        // 取得 pom.properties
        // ====================================
        InputStream stream = null;
        try {
            stream = prepareResourceAsStream(groupId, artifactId, "pom.properties");
        } catch (Exception e) {
            log.error("取得 pom.properties 失敗", e);
            return "";
        }

        // ====================================
        // 解析檔案
        // ====================================
        try {
            Properties properties = new Properties();
            properties.load(stream);

            if (properties.containsKey("version")) {
                String version = properties.getProperty("version");
                verCache.put(verCacheKey, version);
                return version;
            }
            log.error("找不到 version 設定!");
        } catch (Exception e) {
            String msg = String.format(
                    "取得 lib 版本失敗, groupId:%s, artifactId:%s, 可能原因:\r\n"
                            + "1.未導入lib\r\n"
                            + "2.jar 不是由 maven 建置\r\n"
                            + "3.jar 沒有放在專案底下(例如 tomcat server 下的共用 lib)\r\n"
                            + "4.放在 tomcat lib 底下的 jar 檔無法生效 (server 安全鎖定) \r\n"
                            + "5.web 專案, 沒有產生 pom.properties \r\n"
                            + "6.其他",
                    groupId,
                    artifactId);
            log.error(msg, e);
        }
        return "";
    }

    /**
     * 取得 Resource
     * 
     * @param groupId    group Id
     * @param artifactId artifact Id
     * @param fileName   檔案名稱
     * @return InputStream
     * @throws Exception 錯誤時拋出
     */
    public static InputStream prepareResourceAsStream(
            String groupId,
            String artifactId,
            String fileName) throws Exception {

        // ====================================
        // 組資源路徑
        // ====================================
        String filePath = String.format(
                "META-INF/maven/%s/%s/%s",
                groupId,
                artifactId,
                fileName);

        // log.debug("resource path:[{}]", filePath);

        InputStream stream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(filePath);
        if (stream != null && stream.available() > 0) {
            // log.debug("read by ContextClassLoader().getResourceAsStream");
            return stream;
        }

        stream = new WkVerUtil().getClass().getResourceAsStream(filePath);
        if (stream != null && stream.available() > 0) {
            // log.debug("read by this.getClass().getResourceAsStream");
            return stream;
        }

        stream = ClassLoader.getSystemResourceAsStream(filePath);
        if (stream != null && stream.available() > 0) {
            // log.debug("read by ClassLoader.getSystemResourceAsStream");
            return stream;
        }

        throw new Exception("所有取檔方式皆失敗");
    }
}
