package com.cy.work.common.utils;

import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.constant.WkMessage;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 雜項工具
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkCommonUtils {

    @Getter
    private static Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    /**
     * 將 List 均分 (sourceList = 5 , partCount = 2, 會得到 3 組 list =>2,2,1)
     *
     * @param <T>        T
     * @param sourceList object list
     * @param partCount  每組數量
     * @return result
     */
    public static <T> List<List<T>> averageAssign(List<T> sourceList, int partCount) {

        if (WkStringUtils.isEmpty(sourceList)) {
            return Lists.newArrayList();
        }
        if (partCount <= 0) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入的partCount有誤:[" + partCount + "]");
            return Lists.newArrayList();
        }

        List<List<T>> result = new ArrayList<List<T>>();

        int sourceSize = sourceList.size();
        int size = (sourceList.size() / partCount) + 1;
        for (int i = 0; i < size; i++) {
            List<T> subset = new ArrayList<T>();
            for (int j = i * partCount; j < (i + 1) * partCount; j++) {
                if (j < sourceSize) {
                    subset.add(sourceList.get(j));
                }
            }
            result.add(subset);
        }
        return result;
    }

    /**
     * 以 JSON 方式複製物件
     *
     * @param <T>    T
     * @param source 複製來源
     * @param type   new TypeToken<T>() {}.getType()
     * @return clone object
     */
    public static <T> T cloneObject(
            T source,
            Type type) {
        if (source == null) {
            return source;
        }

        String jsonStr = gson.toJson(source);
        return gson.fromJson(jsonStr, type);
    }

    /**
     * 回傳轉為 String 後比對結果
     *
     * @param o1 object 1
     * @param o2 object 2
     * @return true / false
     */
    public static boolean compareByStr(
            Object o1,
            Object o2) {
        return (o1 + "").equals(o2 + "");
    }

    /**
     * Collection 比較
     *
     * @param <T>         T
     * @param collection1 collection1
     * @param collection2 collection2
     * @return true / false
     */
    public static <T> boolean compare(
            Collection<T> collection1,
            Collection<T> collection2) {

        // 複製一份並去除重複 (轉set)
        Set<T> temp1 = Sets.newHashSet();
        if (collection1 != null) {
            temp1 = collection1.stream()
                    .collect(Collectors.toSet());
        }
        Set<T> temp2 = Sets.newHashSet();
        if (collection2 != null) {
            temp2 = collection2.stream()
                    .collect(Collectors.toSet());
        }

        // 先比對數量
        if (temp1.size() != temp2.size()) {
            return false;
        }

        // 逐一筆對
        for (T t1 : temp1) {
            if (!temp2.contains(t1)) {
                return false;
            }
        }

        for (T t2 : temp2) {
            if (!temp1.contains(t2)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 比對日期 (精確到時間)
     * 
     * @param date1 date 1
     * @param date2 date 2
     * @return true:比對成功
     */
    public static boolean compareDateTime(
            Date date1,
            Date date2) {

        Long time1 = (date1 == null) ? -1 : date1.getTime();
        Long time2 = (date2 == null) ? -1 : date2.getTime();

        return compareByStr(time1, time2);
    }

    /**
     * List 相減 (不影響傳入 list)
     *
     * @param <T>
     * @param src    來源 (被減 list)
     * @param target 標的 (減)
     * @return result List
     */
    public static <T> Collection<T> listLeaves(
            Collection<T> src,
            Collection<T> target) {
        Collection<T> currSrc = Lists.newArrayList(src);
        if (WkStringUtils.notEmpty(target)) {
            currSrc.removeAll(target);
        }
        return currSrc;
    }

    /**
     * Collection 比對，回傳新增項目，和減少項目
     *
     * @param <T>         動態型別
     * @param srcItems    來源項目 Collection
     * @param targetItems 目標項目 Collection
     * @return ItemsCollectionDiffTo 比對結果
     */
    public static <T> ItemsCollectionDiffTo<T> itemCollectionDiff(
            Collection<T> srcItems,
            Collection<T> targetItems) {

        // ====================================
        // 新增項目
        // ====================================
        Collection<T> plusItems = Lists.newArrayList();
        if (WkStringUtils.notEmpty(targetItems)) {
            plusItems.addAll(targetItems);
        }

        // 新清單 - 舊清單 = 新增項目
        if (WkStringUtils.notEmpty(srcItems)) {
            plusItems.removeAll(srcItems);
        }

        // ====================================
        // 減少項目
        // ====================================
        Collection<T> reduceItems = Lists.newArrayList();
        if (WkStringUtils.notEmpty(srcItems)) {
            reduceItems.addAll(srcItems);
        }

        // 舊清單 - 新清單 = 減少項目
        if (WkStringUtils.notEmpty(targetItems)) {
            reduceItems.removeAll(targetItems);
        }

        // 資料封裝並回傳
        return new ItemsCollectionDiffTo<>(plusItems, reduceItems);
    }

    /**
     * 準備『耗時』log 文字 - 計時開始
     *
     * @param procTitle 處理項目 title
     * @return result content
     */
    public static String prepareCostMessageStart(String procTitle) {
        return "【" + procTitle + "】：開始";
    }

    /**
     * 準備處理耗時訊息字串
     *
     * @param startTime 起使時間
     * @param procTitle 處理 title
     * @return result content
     */
    public static String prepareCostMessage(
            Long startTime,
            String procTitle) {
        if (startTime == null) {
            return "";
        }
        return "【" + procTitle + "】：共耗時:" + calcCostSec(startTime) + "秒";
    }

    /**
     * 計算消耗秒數
     *
     * @param startTime 開始時間
     * @return result content
     */
    public static String calcCostSec(Long startTime) {
        double costTime = (System.currentTimeMillis() - startTime);
        costTime = costTime / 1000;
        return costTime + "";
    }

    /**
     * 印出 log, 並顯示 StackTrace (用於被呼叫物件自己寫 log 時, 印出上層呼叫者, 方便追蹤程式)
     *
     * @param infomationLevel InfomationLevel
     * @param message         message
     */
    public static void logWithStackTrace(
            InfomationLevel infomationLevel,
            String message) {
        logWithStackTrace(infomationLevel, message, 10);
    }

    /**
     * 印出 log, 並顯示 StackTrace (用於被呼叫物件自己寫 log 時, 印出上層呼叫者, 方便追蹤程式)
     *
     * @param infomationLevel InfomationLevel
     * @param message         message
     */
    public static void logWithStackTrace(
            InfomationLevel infomationLevel,
            String message,
            int showLine) {

        int startLine = 2;
        int endLine = 2 + showLine;

        // 取得執行到這一行，所產生的呼叫堆疊
        StackTraceElement[] st = Thread.currentThread().getStackTrace();

        // 扣除本方法所產生的堆疊資訊後，依據所傳入的參數（行數）， 印出呼叫者所在的位置
        List<String> stackTraces = Lists.newArrayList();
        if (st != null && st.length > 0) {
            for (int i = startLine; i < endLine && i < st.length; i++) {
                stackTraces.add(st[i].toString());
            }
        }

        String logMessage = message + "\r\n\t" + String.join("\r\n\t", stackTraces);

        switch (infomationLevel) {
        case INFO:
            log.info(logMessage);
            break;
        case WARN:
            log.warn(logMessage);
            break;
        case ERROR:
            log.error(logMessage);
            break;
        default:
            log.debug(logMessage);
        }
    }

    public static void logWithStackTraceByFullClassNamePrefix(
            InfomationLevel infomationLevel,
            String message,
            String fullClassNamePrefix) {

        // 取得執行到這一行，所產生的呼叫堆疊
        StackTraceElement[] st = Thread.currentThread().getStackTrace();

        // 扣除本方法所產生的堆疊資訊後，依據所傳入的參數（行數）， 印出呼叫者所在的位置
        List<String> stackTraces = Lists.newArrayList();

        if (st != null && st.length > 0) {
            int lineIndex = -1;
            for (StackTraceElement stackTraceElement : Lists.newArrayList(st)) {
                lineIndex += 1;
                if (lineIndex < 2) {
                    continue;
                }
                String stackTraceLine = stackTraceElement.toString();
                if (stackTraceLine.startsWith(fullClassNamePrefix)) {
                    stackTraces.add(stackTraceLine);
                }
            }
        }

        String logMessage = "\r\n" + message + "\r\n\t" + String.join("\r\n\t", stackTraces);

        switch (infomationLevel) {
        case INFO:
            log.info(logMessage);
            break;
        case WARN:
            log.warn(logMessage);
            break;
        case ERROR:
            log.error(logMessage);
            break;
        default:
            log.debug(logMessage);
        }
    }

    /**
     * 兜組『系統錯誤』的訊息
     *
     * @param e
     * @return exception info
     */
    public static String prepareExcetionInfo(Throwable e) {
        return WkMessage.EXECTION + " [" + e.getMessage() + "]";
    }

    /**
     * 依據公司別代碼, 取得網址
     *
     * @param compId 公司別代碼
     * @return 公司別的 domain URL
     * @throws SystemOperationException 取得失敗時拋出
     */
    public static String findDomainUrlByCompId(String compId) throws SystemOperationException {
        return WkOrgCache.getInstance()
                .findDomainUrlByCompId(compId);
    }

    /**
     * 安全的轉為 Stream (防止原 Collection 為 null)
     *
     * @param <T>        T
     * @param collection collection
     * @return Stream
     */
    public static <T> Stream<T> safeStream(Collection<T> collection) {
        return Optional.ofNullable(collection)
                .map(Collection::stream)
                .orElseGet(Stream::empty);
    }

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    public static void memonyMonitor() {
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        log.info("Free memory is megabytes: " + bytesToMegabytes(runtime.freeMemory()));
        log.info("Used memory is megabytes: " + bytesToMegabytes(memory));
    }
}
