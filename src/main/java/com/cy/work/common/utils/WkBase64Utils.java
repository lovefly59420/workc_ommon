/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.utils;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

/**
 * @author shaun
 */
@Component
public class WkBase64Utils implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7867325354138737697L;
    private static WkBase64Utils instance;

    public static WkBase64Utils getInstance() {
        if (instance == null) {
            instance = new WkBase64Utils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // WkBase64Utils.instance = this;
    }

    /**
     * 網路通訊編碼
     *
     * @param msg message string
     * @return result
     */
    public String encode(String msg) {
        byte[] msgBytes = msg.getBytes(StandardCharsets.UTF_8);
        byte[] base64MsgBytes = Base64.encodeBase64(msgBytes);
        return new String(base64MsgBytes);
    }

    /**
     * 網路通訊解碼
     *
     * @param msg message string
     * @return result
     */
    public String decode(String msg) {
        return new String(Base64.decodeBase64(msg), StandardCharsets.UTF_8);
    }

}
