/**
 *
 */
package com.cy.work.common.utils;

import com.cy.work.common.exception.SystemDevelopException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.MemoryUnit;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * EhCache 建立工具
 *
 * @author allen1214_wu
 */
@Slf4j
@Component
public class WkEhCacheUtils {

    // ====================================
    // 預設參數
    // ====================================
    // Configured maximum number of entries for the local memory heap.
    private final int maxEntriesLocalHeap = 10000;

    // 記憶體儲存規則
    // LRU - least recently used
    // LFU - least frequently used
    // FIFO - first in first out, the oldest element by creation time
    // CLOCK - ? API 上沒寫
    private final MemoryStoreEvictionPolicy memoryStoreEvictionPolicy = MemoryStoreEvictionPolicy.LFU;

    // Sets whether elements are eternal
    // 設為 true時, 將覆蓋 TTS 和 TTL設定
    private final boolean eternal = false;

    // maximum bytes on disk expressed as a percentage (百分比)
    // 最大磁碟使用量
    private final int maxBytesLocalDisk = 10;

    /**
     *
     */
    private Map<String, String> cacheDescMapByName = Maps.newHashMap();

    /**
     * @param cacheManager      CacheManager
     * @param cacheName         快取名稱
     * @param cacheDesc         快取說明
     * @param timeToLiveSeconds 資料逾期時間(TTS) - 超過此時間必定被清除 (設為0即不清快取)
     * @param timeToIdleSeconds idle逾期時間(TTL) - idle 時間大於此值即被清除 (設為0即不清快取)
     */
    public void addCache(
            CacheManager cacheManager,
            String cacheName,
            String cacheDesc,
            int timeToLiveSeconds,
            int timeToIdleSeconds) {

        // ====================================
        // 檢核
        // ====================================
        if (cacheManager == null) {
            throw new SystemDevelopException("傳入的 CacheManager 為 null !");
        }

        if (cacheManager.getCacheNames() != null
                && Lists.newArrayList(cacheManager.getCacheNames())
                        .contains(cacheName)) {
            throw new SystemDevelopException("Cache建立失敗, 傳入的 Cache name 已存在 :【" + cacheName + "】");
        }

        // ====================================
        // 建立 config
        // ====================================
        CacheConfiguration config = new CacheConfiguration(cacheName, maxEntriesLocalHeap)
                .memoryStoreEvictionPolicy(memoryStoreEvictionPolicy)
                .eternal(eternal)
                .timeToIdleSeconds(timeToIdleSeconds)
                .timeToLiveSeconds(timeToLiveSeconds)
                .maxBytesLocalDisk(maxBytesLocalDisk, MemoryUnit.MEGABYTES);

        config.setCopyOnRead(true);
        config.setCopyOnWrite(true);

        // ====================================
        // 加入
        // ====================================
        cacheManager.addCache(new Cache(config));

        cacheDescMapByName.put(cacheName, cacheDesc);

        log.info("init EhCache【" + cacheDesc + " [" + cacheName + "]】, "
                + "TTS=" + timeToLiveSeconds + ",TTL=" + timeToIdleSeconds);
    }

    /**
     * 顯示所有 EhCache 使用量
     *
     * @param cacheManager CacheManager
     */
    public void showAllEhCacheSize(CacheManager cacheManager) {

        if (cacheManager.getCacheNames() == null) {
            return;
        }

        for (String cacheName : Lists.newArrayList(cacheManager.getCacheNames())) {
            Cache dataListCache = cacheManager.getCache(cacheName);
            if (dataListCache != null) {
                log.info("Size of 【" + cacheDescMapByName.get(cacheName) + " [" + cacheName + "]】 : "
                        + dataListCache.getSize());
            }
        }
    }

}
