/**
 * 
 */
package com.cy.work.common.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.system.rest.client.OrgClient;
import com.cy.system.rest.client.vo.OrgTransMappingTo;

/**
 * @author allen1214_wu
 */
@Component
public class WkOrgTransMappingUtils implements Serializable, InitializingBean {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6858747633554176867L;

    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WkOrgTransMappingUtils instance;

    /**
     * getInstance
     *
     * @return instance
     */
    public static WkOrgTransMappingUtils getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkOrgTransMappingUtils instance) { WkOrgTransMappingUtils.instance = instance; }

    /**
     * afterPropertiesSet
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務區
    // ========================================================================
    @Autowired
    private transient OrgClient orgClient;

    // ========================================================================
    // 方法區
    // ========================================================================
    /**
     * 查詢組織異動前後對應設定 by 公司ID、組織異動生效日
     * 
     * @param compId        公司ID
     * @param effectiveDate 組織異動生效日
     * @return OrgTransMappingTo list
     */
    public List<OrgTransMappingTo> findOrgTrnsMapping(
            String compId,
            Date effectiveDate) {
        return this.orgClient.findOrgTrnsMapping(compId, effectiveDate);
    }

    /**
     * 查詢組織異動前後對應設定 中所有資料的『組織異動生效日』(distinct)
     * 
     * @param compId 公司ID
     * @return date list
     */
    public List<Date> findOrgTransMappingEffectiveDates(
            String compId) {
        return this.orgClient.findOrgTransMappingEffectiveDates(compId);
    }

}
