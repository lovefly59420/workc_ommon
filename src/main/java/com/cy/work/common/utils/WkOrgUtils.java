package com.cy.work.common.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.enums.OrgType;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleOrg;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.eo.AbstractDepRuleEo;
import com.cy.work.common.vo.WkNode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;

/**
 * 工具類：組織
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkOrgUtils {

    /**
     * 單位層級排序
     */
    private static final List<OrgLevel> ORG_LEVEL_ORDER = Lists.newArrayList(
            OrgLevel.THE_PANEL,
            OrgLevel.MINISTERIAL,
            OrgLevel.DIVISION_LEVEL,
            OrgLevel.GROUPS);

    /**
     * 依據傳入單位, 建立單位樹 node
     *
     * @param comp              公司別
     * @param whitelistByDepSid 可用單位
     * @return 建立單位樹 node
     */
    public static WkNode bulidOrgTreeNode(
            Org comp,
            Collection<Integer> whitelistByDepSid) {

        WkNode rootNode = new WkNode(comp);

        if (comp == null) {
            log.warn("傳入公司為 null");
            return rootNode;
        }

        bulidOrgTreeNode_recursion(rootNode, whitelistByDepSid);

        return rootNode;
    }

    /**
     * 遞迴建立單位樹
     *
     * @param node              node
     * @param whitelistByDepSid 白名單 SID
     * @return 是否包含真實節點
     */
    private static boolean bulidOrgTreeNode_recursion(
            WkNode node,
            Collection<Integer> whitelistByDepSid) {
        // 防呆
        if (node == null || node.getNodeData() == null) {
            return false;
        }

        // 取得本節點單位資料
        Org currNodeDep = (Org) node.getNodeData();

        // ====================================
        // 取得所有子單位資料
        // ====================================
        List<Org> childDeps = WkOrgCache.getInstance().getChildOrgs(currNodeDep.getSid());

        if (WkStringUtils.isEmpty(childDeps)) {
            return false;
        }

        // ====================================
        // 逐筆處理子單位
        // ====================================
        boolean isHaveTrueDataChildNode = false;
        for (Org childDep : childDeps) {

            // 建立子單位節點
            WkNode childNode = new WkNode(childDep);
            childNode.setParent(node);

            // 判斷是否包含在傳入的列表內
            boolean isPathNode = whitelistByDepSid == null
                    || !whitelistByDepSid.contains(childDep.getSid());
            // 傳入白名單列表不為空，且不包含在內, 則為路徑節點 (不是真實資料,僅為階層)
            childNode.setPathNode(isPathNode);

            // 遞迴建立子節點, 並判斷是否此節點下有真實資料節點
            boolean isCurrChildNodeHaveTureDataChildNode = bulidOrgTreeNode_recursion(childNode, whitelistByDepSid);

            // 若本身為路徑節點, 底下又無真實資料節點的話, 移除此節點
            if (isPathNode && !isCurrChildNodeHaveTureDataChildNode) {
                childNode.setParent(null);
                continue;
            }

            // 紀錄傳入節點下的子節點中包含真實節點
            isHaveTrueDataChildNode = true;
        }

        return isHaveTrueDataChildNode;
    }

    /**
     * 比較單位層級
     *
     * @param srcLevel    比較值
     * @param targetLevel 被比較值
     * @return 等於:0 , 小於:-1, 大於:1
     */
    public static Integer compareOrgLevel(
            OrgLevel srcLevel,
            OrgLevel targetLevel) {
        Integer src = getOrgLevelOrder(srcLevel);
        Integer target = getOrgLevelOrder(targetLevel);
        return src.compareTo(target);
    }

    /**
     * @param parentSid       父部門 sid
     * @param relationDepsMap 關係部門 Map
     * @param distance        隔代距離
     */
    private static void findChildsForRelationDeps(
            Integer parentSid,
            Map<Integer, Integer> relationDepsMap,
            int distance) {

        List<Org> childOrgs = WkOrgCache.getInstance().getChildOrgs(parentSid);

        for (Org childOrg : childOrgs) {
            relationDepsMap.put(childOrg.getSid(), distance);
            findChildsForRelationDeps(childOrg.getSid(), relationDepsMap, distance + 1);
        }
    }

    /**
     * 以單位名稱模糊查詢<br/>
     * 1.僅回傳類型為『部門』的單位
     *
     * @param companySid 公司 sid
     * @param depName    查詢單位名稱 (like)
     * @return 回傳類型為『部門』的單位
     */
    public static List<Org> findDepByNameLike(
            Integer companySid,
            String depName) {
        // 取得全部單位資料
        List<Org> allOrgs = WkOrgCache.getInstance().findAll();

        // 過濾
        // 1.需為『部門』
        // 2.該部門為傳入公司別
        allOrgs = allOrgs.stream()
                .filter(org -> OrgType.DEPARTMENT.equals(org.getType()))
                .filter(org -> WkCommonUtils.compareByStr(companySid, org.toSimple()
                        .getCompanySid()))
                .collect(Collectors.toList());

        // 為空時回傳全部
        if (WkStringUtils.isEmpty(depName)) {
            return allOrgs;
        }
        // 去空白
        final String compareDepName = WkStringUtils.safeTrim(depName);

        // 過濾單位名稱
        return allOrgs.stream()
                .filter(org -> (org.getName() + "").contains(compareDepName))
                .collect(Collectors.toList());

    }

    /**
     * 以單位名稱模糊查詢<br/>
     * 1.僅回傳類型為『部門』的單位<br/>
     * 2.僅回傳『非停用』單位
     *
     * @param companySid 公司 sid
     * @param depName    查詢單位名稱 (like)
     * @return Org list
     */
    public static List<Org> findDepByNameLikeAndActive(
            Integer companySid,
            String depName) {
        // 過濾單位名稱
        return findDepByNameLike(companySid, depName).stream()
                .filter(org -> Activation.ACTIVE.equals(org.getStatus()))
                .collect(Collectors.toList());
    }

    /**
     * 若為停用單位, 加上停用, 並劃上刪除線
     *
     * @param depSid    org sid
     * @param addParent 是否加上上層部門名稱
     * @return 單位名稱
     */
    public static String findNameByDecorationStyle(
            Integer depSid,
            boolean addParent) {

        if (depSid == null) {
            return "";
        }
        // ====================================
        // 取得部門資料
        // ====================================

        Org dep = WkOrgCache.getInstance().findBySid(depSid);

        if (dep == null) {
            return "";
        }

        // ====================================
        // 部門名稱 (本身+上層)
        // ====================================
        String currDepStr = "";
        if (addParent && dep.getParent() != null) {
            currDepStr += getOrgName(dep.getParent().toOrg());
            currDepStr += " - <br/>";
        }
        currDepStr += "<span style='font-weight: bold;'>" + getOrgName(dep) + "</span>";

        // ====================================
        // 組完整顯示字串
        // ====================================
        String showContent = "";

        if (Activation.ACTIVE.equals(dep.getStatus())) {
            showContent += currDepStr;

        } else {
            showContent += "<span class='WS1-1-2' style='text-decoration: line-through;'>(停用)&nbsp;";
            showContent += "<span class='WS1-1-1' style='text-decoration: none;'>" + currDepStr + "</span>";
            showContent += "</span>";
        }

        return showContent;
    }

    /**
     * 若傳入部門為停用時，對傳入的『部門文字資訊』加上停用標示，並劃上刪除線
     * 
     * @param depSid         org sid
     * @param depInfoContent 原始部門資訊
     * @return string
     */
    public static String addDecorationStyleIfInactive(Integer depSid, String depInfoContent) {
        // 查不到對應部門資訊時，不對傳入文字作處理
        Org org = WkOrgCache.getInstance().findBySid(depSid);
        if (org == null) {
            return depInfoContent;
        }

        // 非停用時，不對傳入文字作處理
        if (isActive(org)) {
            return depInfoContent;
        }

        return "<span class='WS1-1-2' style='text-decoration: line-through;'>(停用)&nbsp;"
                + "<span class='WS1-1-1' style='text-decoration: none;'>" + depInfoContent + "</span>"
                + "</span>";
    }

    /**
     * @param deps      org list (有序)
     * @param delimiter 分隔符
     * @return 部門名稱1、部門名稱2、.....
     */
    public static String findNameByOrgs(
            List<Org> deps,
            String delimiter) {

        // ====================================
        // String 轉 Integer
        // ====================================
        List<Integer> depSids = Lists.newArrayList();

        for (Org dep : deps) {
            if (dep == null || dep.getSid() == null) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 List<Org> 中有 null", 10);
                continue;
            }
            depSids.add(dep.getSid());
        }

        // ====================================
        // findNameBySid
        // ====================================
        return findNameBySid(depSids, delimiter, false);
    }

    /**
     * 多單位轉單行顯示字串 sid 轉 org name<br/>
     * 1.不包含組裝單位 sid<br/>
     * 2.會排序傳入單位
     *
     * @param depSids   部門 sid
     * @param delimiter 分隔符號
     * @return 部門名稱1、部門名稱2、.....
     */
    public static String findNameBySid(
            Collection<Integer> depSids,
            String delimiter) {
        return findNameBySid(Lists.newArrayList(depSids), delimiter, false);
    }

    /**
     * 多單位轉單行顯示字串 sid 轉 org name (list 型態)<br/>
     * 將自動排序傳入部門
     *
     * @param depSids      部門 sid (有序)
     * @param delimiter    分隔符號
     * @param isWithOrgSid 是否組裝部門 sid
     * @return 部門名稱1、部門名稱2、.....
     */
    public static String findNameBySid(
            Collection<Integer> depSids,
            String delimiter,
            boolean isWithOrgSid) {
        return findNameBySid(depSids, delimiter, isWithOrgSid, true);
    }

    /**
     * 多單位轉單行顯示字串 sid 轉 org name (list 型態)
     *
     * @param depSids      部門 sid (有序)
     * @param delimiter    分隔符號
     * @param isWithOrgSid 是否組裝部門 sid
     * @param isSort       是否排序
     * @return 部門名稱1、部門名稱2、.....
     */
    public static String findNameBySid(
            Collection<Integer> depSids,
            String delimiter,
            boolean isWithOrgSid,
            boolean isSort) {

        if (WkStringUtils.isEmpty(depSids)) {
            return "";
        }

        // 去除重複
        depSids = depSids.stream()
                .distinct()
                .collect(Collectors.toList());

        // 排序
        if (isSort) {
            depSids = sortByOrgTree(depSids);
        }

        // 逐筆取得部門名稱
        List<String> depNames = Lists.newArrayList();

        for (Integer depSid : depSids) {
            // 以 sid 取得單位檔物件
            Org dep = WkOrgCache.getInstance().findBySid(depSid);
            // 解析單位名稱
            String name = getOrgName(dep);

            if (WkStringUtils.isEmpty(name)) {
                // 為空時，代表資料有問題 印出SID
                // (org == null、name is empty)
                name = "SID:[" + depSid + "]";

            } else if (isWithOrgSid) {
                // 指定加上 SID
                name = depSid + " " + name;
            }

            depNames.add(name);
        }

        return String.join(delimiter, depNames);
    }

    // =============================================================================================
    // 多單位轉單行顯示字串
    // =============================================================================================

    /**
     * 多單位轉單行顯示字串
     *
     * @param depSids   org sid list
     * @param delimiter 分隔字串
     * @param maxWidth  最大寬度
     * @return org name
     */
    public static String findNameBySid(
            Collection<Integer> depSids,
            String delimiter,
            Integer maxWidth) {
        //
        String result = findNameBySid(depSids, delimiter);
        if (WkStringUtils.isEmpty(result)) {
            return "";
        }
        String omittedCss = "overflow:hidden;"
                + "text-overflow:ellipsis;"
                // + "display:-webkit-box;"
                // + "-webkit-line-clamp: 2;"
                // + "-webkit-box-orient: vertical;"
                + "";

        return "<div style='max-width:" + maxWidth + "px;" + omittedCss + "'>" + result + "</div>";
    }

    /**
     * 查詢單位名稱
     *
     * @param depSid 單位 sid
     * @return 單位名稱
     */
    public static String findNameBySid(Integer depSid) {
        if (depSid == null) {
            return "";
        }

        Org dep = WkOrgCache.getInstance().findBySid(depSid);
        // 解析單位名稱
        return getOrgName(dep);
    }

    /**
     * 多單位轉單行顯示字串 sid 轉 org name<br/>
     * 1.不包含組裝單位 sid<br/>
     * 2.會排序傳入單位
     *
     * @param depSidStrs 部門 sid (有序)
     * @param delimiter  分隔符號
     * @return org names
     */
    public static String findNameBySidStrs(
            Collection<String> depSidStrs,
            String delimiter) {

        // ====================================
        // String 轉 Integer
        // ====================================
        List<Integer> depSids = Lists.newArrayList();

        for (String depSidStr : depSidStrs) {
            depSidStr = WkStringUtils.safeTrim(depSidStr);
            if (WkStringUtils.isEmpty(depSidStr)) {
                continue;
            }
            if (!WkStringUtils.isNumber(depSidStr)) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 org sid 不是數字:[" + depSidStr + "]", 10);
                continue;
            }
            depSids.add(Integer.parseInt(depSidStr));
        }

        // ====================================
        // findNameBySid
        // ====================================
        return findNameBySid(depSids, delimiter, false);
    }

    // =============================================================================================
    // 取得上下直系關係單位, 並標註距離
    // =============================================================================================

    /**
     * 取得上下直系關係單位, 並標註距離
     * 
     * @param depSid org sid
     * @return Map<DepSids, 距離>
     */
    public static Map<Integer, Integer> findRelationDepsAndDistance(Integer depSid) {

        Map<Integer, Integer> relationDepsMap = Maps.newConcurrentMap();

        // 轉成Org物件
        Org currDep = WkOrgCache.getInstance().findBySid(depSid);
        if (currDep == null) {
            log.warn("findRelationDepsAndDistance - 找不到傳入單位資料! orgSid[" + depSid + "]");
            return relationDepsMap;
        }

        // ====================================
        // 往上收集父單位
        // ====================================
        // 與自己的【隔代距離】 由 0 開始算, 這樣往上的單位會比往下的單位優先值高
        int distance = 0;
        while (currDep.getParent() != null) {
            currDep = currDep.getParent()
                    .toOrg();
            relationDepsMap.put(currDep.getSid(), distance);
            distance += 1;
        }

        // ====================================
        // 往下收集所有子單位
        // ====================================
        findChildsForRelationDeps(depSid, relationDepsMap, 1);

        return relationDepsMap;
    }

    /**
     * 取得與傳入單位相關的單位 1.向上直系 2.向下所有單位 3.以隔代距離排序 4.向父單位排序距離優先值大於子單位 (父單位距離-1)
     *
     * @param depSid org sid
     * @return org sid list
     */
    public static List<Integer> findRelationDepsAndSortByDistance(Integer depSid) {

        // ====================================
        // 查詢
        // ====================================
        Map<Integer, Integer> relationDepsMap = findRelationDepsAndDistance(depSid);

        // ====================================
        // 以隔代距離排序
        // ====================================
        List<Integer> relationDeps = relationDepsMap.keySet()
                .stream()
                .sorted(Comparator.comparingInt(relationDepsMap::get))
                .collect(Collectors.toList());

        return relationDeps;
    }

    /**
     * 取得與傳入單位相關的單位 1.向上直系 2.向下所有單位 3.以隔代距離排序 4.向父單位排序距離優先值大於子單位 (父單位距離-1)
     *
     * @param userSid user sid
     * @return org sid list
     */
    public static List<Integer> findUserRelationDepsAndSortByDistance(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null) {
            return Lists.newArrayList();
        }
        return findRelationDepsAndSortByDistance(user.getPrimaryOrg().getSid());
    }

    /**
     * 依據傳入單位層級, 回傳層級順序
     *
     * @param level OrgLevel
     * @return 回傳層級順序(找不到則回傳 - 1)
     */
    public static Integer getOrgLevelOrder(OrgLevel level) {
        if (level == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入OrgLevel為 [null]", 10);
            return -1;
        }
        try {
            return ORG_LEVEL_ORDER.indexOf(level);
        } catch (Exception e) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入OrgLevel未定義:[" + level.name() + "]", 10);
        }
        return -1;
    }

    /**
     * 取得 org name
     * 
     * @param org org
     * @return org name
     */
    public static String getOrgName(SimpleOrg org) {
        if (org == null) {
            return "";
        }
        return getOrgName(org.toOrg());
    }

    /**
     * 取得單位名稱 (公用,統一意外狀況時回傳的資訊)
     *
     * @param org org
     * @return org name
     */
    public static String getOrgName(Org org) {
        if (org == null) {
            return "";
        }
        if (0 == org.getSid()) {
            return "WERP";
        }

        // 避免傳入 Org 只是容器，沒有其他資訊
        Org currOrg = WkOrgCache.getInstance().findBySid(org.getSid());
        if (currOrg == null) {
            return "";
        }
        return currOrg.getOrgNameWithParentIfDuplicate();
    }

    /**
     * 取得單位名稱 (公用,統一意外狀況時回傳的資訊)
     *
     * @param orgSid org sid
     * @return org name
     */
    public static String getOrgNameByOrgSid(Integer orgSid) {
        return getOrgName(WkOrgCache.getInstance().findBySid(orgSid));
    }

    /**
     * 是否為啟用
     *
     * @param orgSid
     * @return y/n
     */
    public static boolean isActive(Integer orgSid) {
        Org org = WkOrgCache.getInstance().findBySid(orgSid);
        return isActive(org);

    }

    /**
     * 是否為啟用
     *
     * @param org Org
     * @return y/n
     */
    public static boolean isActive(Org org) {
        if (org == null) {
            return false;
        }
        return Activation.ACTIVE.equals(org.getStatus());
    }

    /**
     * 組多筆 org 名稱成為 單一 字串<br/>
     * EX:單位一、單位二、單位甲
     *
     * @param orgs      org collection
     * @param delimiter 分割符號
     * @return orgs info
     */
    public static String joinOrgName(
            Collection<Org> orgs,
            String delimiter) {
        return joinOrgName(orgs, delimiter, false);
    }

    /**
     * 組多筆 org 名稱成為 單一 字串<br/>
     * ex:單位一、單位二、單位甲
     *
     * @param orgs         org collection
     * @param delimiter    分割符號
     * @param isWithOrgSid 是否組裝 org sid
     * @return orgs info
     */
    public static String joinOrgName(
            Collection<Org> orgs,
            String delimiter,
            boolean isWithOrgSid) {

        if (WkStringUtils.isEmpty(orgs)) {
            return "";
        }

        // 逐筆取得部門名稱
        List<String> depNames = Lists.newArrayList();
        for (Org dep : orgs) {
            if (dep == null) {
                continue;
            }

            String name = getOrgName(dep);
            if (isWithOrgSid) {
                name = dep.getSid() + " " + name;
            }

            depNames.add(name);
        }

        return String.join(delimiter, depNames);
    }

    /**
     * 取得組織層級名稱
     *
     * @param org Org
     * @return 組織層級名稱
     */
    public static String getOrgLevelName(Org org) {
        if (org == null || org.getLevel() == null) {
            return "";
        }

        switch (org.getLevel()) {
        case THE_PANEL:
            return "組";
        case MINISTERIAL:
            return "部";
        case DIVISION_LEVEL:
            return "處";
        case GROUPS:
            return "群";
        default:
            log.error("未定義的orgType:[" + org.getLevel() + "]");
            break;
        }
        return "";
    }

    // =============================================================================================
    // 美化 麵包屑樣式的部門名稱
    // =============================================================================================

    /**
     * 美化顯示部門名稱
     *
     * @param depName  部門名稱
     * @param splitStr 分隔符號
     * @return string
     */
    public static String makeupDepName(
            String depName,
            String splitStr) {
        depName = WkStringUtils.safeTrim(depName);

        if (WkStringUtils.isEmpty(depName)) {
            return "";
        }

        return makeupDepName(depName, splitStr, "STYLE_CONTENT_TRUE_DATA");
    }

    /**
     * 美化顯示部門名稱
     * 
     * @param depName          部門名稱
     * @param splitStr         分隔符號
     * @param trueContentStyle 加上 CSS style
     * @return result string
     */
    public static String makeupDepName(
            String depName,
            String splitStr,
            String trueContentStyle) {
        depName = WkStringUtils.safeTrim(depName);

        if (WkStringUtils.isEmpty(depName)) {
            return "";
        }

        String[] nameAry = depName.split(splitStr);

        String prefixName = "";
        if (nameAry.length > 1) {
            prefixName += "<span style='color:gray'>【";
            prefixName += depName.substring(0, depName.lastIndexOf(splitStr));
            prefixName += "】:</span>";
        }

        String pureDepName = String.format(
                "<span style='font-weight:bold;%s'>%s</span>",
                WkStringUtils.notEmpty(trueContentStyle) ? trueContentStyle : "",
                nameAry[nameAry.length - 1]);

        return prefixName + pureDepName;
    }

    // =============================================================================================
    // 收束為基本單位
    // =============================================================================================

    /**
     * 取得 特定 層級基本部門 且 OrgType 為部門（DEPARTMENT） 若傳入單位的『上層單位』，已超過特定 層級基本部門，則回傳原單位
     *
     * @param currDepSid  部門 sid
     * @param targetLevel 收束標的層級
     * @return org
     */
    public static Org prepareBasicDep(
            Integer currDepSid,
            OrgLevel targetLevel) {

        // ====================================
        // 取得傳入單位資料
        // ====================================
        Org currDep = WkOrgCache.getInstance().findBySid(currDepSid);
        // 找不到傳入單位
        if (currDep == null) {
            return null;
        }
        // ====================================
        // 遞迴取得基本部門
        // ====================================
        return prepareBasicDep_Recursive(currDep, null, targetLevel);
    }

    /**
     * 取得 特定 層級基本部門 且 OrgType 為部門（DEPARTMENT） 若傳入單位的『上層單位』，已超過特定 層級基本部門，則回傳原單位
     *
     * @param currDep     currDep
     * @param preDep      preDep
     * @param targetLevel 收束標的層級
     * @return org
     */
    private static Org prepareBasicDep_Recursive(
            Org currDep,
            Org preDep,
            OrgLevel targetLevel) {

        // ====================================
        // 檢核
        // ====================================
        // 傳入單位非部門 回傳上一層 (若無上一層, 則為 null)
        if (!OrgType.DEPARTMENT.equals(currDep.getType())) {
            return preDep;
        }

        // ====================================
        // 判斷傳入單位
        // ====================================
        // 目標層級
        Integer targetOrgLevel = WkOrgUtils.getOrgLevelOrder(targetLevel);
        // 傳入單位層級
        Integer currOrgLevel = WkOrgUtils.getOrgLevelOrder(currDep.getLevel());

        // 傳入單位 = 目標層級時，回傳傳入單位 (命中)
        if (Objects.equals(currOrgLevel, targetOrgLevel)) {
            return currDep;
        }

        // 傳入單位 > 目標層級，
        if (currOrgLevel > targetOrgLevel) {
            // 若上一層不為空，則回傳上一層
            // EX 目標為部級, 但傳入的組級直接掛在處底下，此時直接回傳組
            if (preDep != null) {
                return preDep;
            }

            // 上層為空，代表傳入部門本身已大於標的層級，則回傳本身
            return currDep;
        }

        // ====================================
        // 傳入單位還未到達指定層級, 取上層部門
        // currOrgLevel < targetOrgLevel
        // ====================================
        // 無上層部門時，回傳傳入單位
        if (currDep.getParent() == null) {
            log.debug("取基本(" + targetLevel.name() + ")部門，因parent==null，不再往上尋找。 basicSid：" + currDep);
            if (preDep != null) {
                return preDep;
            } else {
                return currDep;
            }
        }

        return prepareBasicDep_Recursive(WkOrgCache.getInstance().findBySid(currDep.getParent()
                .getSid()), currDep, targetLevel);
    }

    // =============================================================================================
    // 兜組 『麵包屑樣式』的部門名稱
    // =============================================================================================

    /**
     * 部門名稱麵包屑
     *
     * @param orgSid         部門 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param splitStr       部門間分隔符號
     * @return result string
     */
    public static String prepareBreadcrumbsByDepName(
            Integer orgSid,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String splitStr) {

        if (!ORG_LEVEL_ORDER.contains(topLevel)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.ERROR, "傳入 OrgLevel 有誤:[" + topLevel + "]", 10);
            return "";
        }

        if (orgSid == null) {
            return "";
        }

        int topLevelOrder = getOrgLevelOrder(topLevel);

        return prepareBreadcrumbsByDepName(
                orgSid,
                topLevelOrder,
                isForceUpLevel,
                splitStr,
                true,
                "");
    }

    /**
     * 部門名稱麵包屑 + 美化
     *
     * @param orgSid         org sid
     * @param topLevel       組裝部門最高單位層級
     * @param isForceUpLevel 是否強制多一層
     * @param splitStr       切分字元
     * @return HTML content
     */
    public static String prepareBreadcrumbsByDepNameAndMakeup(
            Integer orgSid,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String splitStr) {

        // 組麵包屑樣式
        String breadcrumb = prepareBreadcrumbsByDepName(orgSid, topLevel, isForceUpLevel, splitStr);
        // 標的單位凸顯樣式
        return makeupDepName(breadcrumb, splitStr, "");
    }

    /**
     * 部門名稱麵包屑 + 美化 + 停用 style 部門 style
     *
     * @param orgSid         org sid
     * @param topLevel       組裝部門最高單位層級
     * @param isForceUpLevel 是否強制多一層
     * @param splitStr       切分字元
     * @return HTML content
     */
    public static String prepareBreadcrumbsByDepNameAndMakeupAndDecorationStyle(
            Integer orgSid,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String splitStr) {

        // 組麵包屑樣式 + 美化
        String depInfo = prepareBreadcrumbsByDepNameAndMakeup(orgSid, topLevel, isForceUpLevel, splitStr);
        // 判斷為停用單位時，加入停用的 style
        return addDecorationStyleIfInactive(orgSid, depInfo);
    }

    /**
     * @param orgSid         org sid
     * @param topLevel       組裝部門最高單位層級
     * @param isForceUpLevel 是否強制多一層
     * @param splitStr       切分字元
     * @param isFirstDep     是否為原始傳入部門
     * @param srcResultStr   上一次處理的結果
     * @return result string
     */
    private static String prepareBreadcrumbsByDepName(
            Integer orgSid,
            int topLevel,
            boolean isForceUpLevel,
            String splitStr,
            boolean isFirstDep,
            String srcResultStr) {

        // 查詢傳入公司別資料
        Org org = WkOrgCache.getInstance().findBySid(orgSid);

        // 若找不到 - 不回傳本層
        if (org == null) {
            return "[" + orgSid + "]" + splitStr + srcResultStr;
        }

        // 取得傳入部門等級
        int currOrgLevel = Integer.MAX_VALUE;
        // 為部門時, 才需要取
        if (OrgType.DEPARTMENT.equals(org.getType())) {
            currOrgLevel = WkOrgUtils.getOrgLevelOrder(org.getLevel());
            if (currOrgLevel == -1) {
                log.info("\r\n" + new com.google.gson.GsonBuilder().setPrettyPrinting()
                        .create()
                        .toJson(org));
            }
        }

        // 如果原始傳入部門等級本身就大於等於設定等級, 直接回傳本層
        if (isFirstDep && currOrgLevel >= topLevel) {
            if (isForceUpLevel) {
                // 如果需要強制顯示上一層, 則+1
                topLevel = currOrgLevel + 1;
            } else {
                return getOrgName(org);
            }
        }

        // 若為傳入單位不是部門, 不回傳本層
        if (!OrgType.DEPARTMENT.equals(org.getType())) {
            return srcResultStr;
        }

        // 複製一份
        String newResultStr = srcResultStr + "";

        // 將回傳字串加上本層
        if (WkStringUtils.notEmpty(newResultStr)) {
            newResultStr = splitStr + newResultStr;
        }
        newResultStr = getOrgName(org) + newResultStr;

        // 無父部門, 或資料異常, 直接回傳本層
        if (org.getParent() == null
                || org.getParent().getLevel() == null) {
            return newResultStr;
        }

        // 已到頂層 -> 回傳本層
        if (currOrgLevel == topLevel) {
            return newResultStr;
        }

        // 還未到頂層, 繼續遞迴
        if (currOrgLevel < topLevel) {
            return prepareBreadcrumbsByDepName(
                    org.getParent().getSid(),
                    topLevel,
                    isForceUpLevel,
                    splitStr,
                    false,
                    newResultStr);
        }

        // 以下傳入部門等級, 已大於最高部門等級 currOrgLevel > topLevel
        // 以下邏輯為非循序性部門設定 例: 群 -> 部 -> 組 (少掉處)
        // 開啟非循序性部門往上一層flag時, 才回傳本層
        if (!isFirstDep && currOrgLevel > topLevel && isForceUpLevel) {
            return newResultStr;
        }

        return srcResultStr;
    }

    // =============================================================================================
    // 兜組 『階層樹樣式』的部門名稱
    // =============================================================================================
    /**
     * 以階層樹方式，兜組傳入單位名稱
     *
     * @param depSids 要顯示單位的 SID
     * @param maxLine 最大顯示長度 (避免階層樹往下排過長,超過時會往右邊排)
     * @return result content
     */
    public static String prepareDepsNameByTreeStyle(
            Collection<Integer> depSids,
            int maxLine) {
        if (WkStringUtils.isEmpty(depSids)) {
            return "";
        }

        String compID = "";
        for (Integer depSid : depSids) {
            Org org = WkOrgCache.getInstance().findBySid(depSid);
            if (org != null
                    && org.getCompany() != null
                    && WkStringUtils.notEmpty(org.getCompany().getId())) {
                compID = org.getCompany().getId();
                break;
            }
        }

        if (WkStringUtils.isEmpty(compID)) {
            List<String> depSidStrs = depSids.stream()
                    .map(sid -> sid + "")
                    .collect(Collectors.toList());
            log.warn("傳入單位:[" + String.join("、", depSidStrs) + "]找不到所屬公司");
            return "";
        }

        return prepareDepsNameByTreeStyle(compID, depSids, maxLine);
    }

    /**
     * 以階層樹方式，兜組傳入單位名稱
     *
     * @param compID  公司別 (ID)
     * @param depSids 要顯示單位的 SID
     * @param maxLine 最大顯示長度 (避免階層樹往下排過長,超過時會往右邊排)
     * @return result content
     */
    public static String prepareDepsNameByTreeStyle(
            String compID,
            Collection<Integer> depSids,
            int maxLine) {

        Org comp = WkOrgCache.getInstance().findById(compID);
        if (comp == null) {
            log.warn("找不到傳入公司ID![" + compID + "]");
            return "";
        }
        // 防呆
        if (maxLine < 1) {
            maxLine = 0;
        }

        // 建立單位樹
        WkNode rootNode = bulidOrgTreeNode(comp, depSids);

        // 兜組單位顯示文字
        List<String> contentLines = prepareDepsNameByTreeStyle_recursion(-1, rootNode);

        // ====================================
        // 必免清單過長, 依據最大行數分組
        // ====================================
        //
        List<List<String>> contentParts = Lists.newArrayList();
        // 初始值訂為超過最大值, 讓第一次進入迴圈就產生一個新的 part
        int number = maxLine + 1;
        // 目前 part 在容器中的 index
        int currPartIndex = 0;
        for (String contentLine : contentLines) {
            // 超過行數時, 放到下一個 part
            if (number > maxLine) {
                contentParts.add(Lists.newArrayList());
                // 更新目前正在使用的 part index
                currPartIndex = contentParts.size() - 1;
                number = 1;
            }
            contentParts.get(currPartIndex).add(contentLine);
            number++;
        }

        // ====================================
        // 兜組全部內容
        // ====================================
        StringBuilder htmlContent = new StringBuilder();
        // 外框線
        htmlContent.append("<table style='border:2px #cccccc solid; margin-top:10px'><tr>");
        if (WkStringUtils.notEmpty(contentParts)) {
            for (List<String> eachContentLinePart : contentParts) {
                // 強制不折行、文字置頂
                htmlContent.append("<td style='white-space:nowrap;vertical-align:text-top;'>");
                htmlContent.append(String.join("<br/>", eachContentLinePart));
                htmlContent.append("</td>");
            }
        }
        htmlContent.append("</tr></table>");

        return htmlContent.toString();
    }

    /**
     * 兜組內容
     * 
     * @param level level
     * @return result content
     */
    public static String prepareDepsNameByTreeStyle_getPreContent(int level) {
        if (level == 0) {
            return "";
        }
        StringBuilder preContent = new StringBuilder();
        for (int i = 0; i < level; i++) {
            preContent.append("&nbsp;&nbsp;&nbsp;&nbsp;");
        }
        preContent.append("|__");
        return preContent.toString();
    }

    /**
     * 遞迴
     * 
     * @param level level
     * @param node  node
     * @return result content
     */
    private static List<String> prepareDepsNameByTreeStyle_recursion(
            int level,
            WkNode node) {

        List<String> resultContents = Lists.newArrayList();

        // ====================================
        // 防呆
        // ====================================
        if (node == null || node.getNodeData() == null || !(node.getNodeData() instanceof Org)) {
            return resultContents;
        }

        // ====================================
        // 部門名稱
        // ====================================
        Org dep = (Org) node.getNodeData();

        String depName = getOrgName(dep);
        if (Activation.INACTIVE.equals(dep.getStatus())) {
            depName = "(停用)" + depName;
        }

        // ====================================
        // 兜組完整 CSS 內容
        // ====================================
        String nodeContent = "";
        if (level > -1) {
            nodeContent += prepareDepsNameByTreeStyle_getPreContent(level);
            if (node.isPathNode()) {
                // 非真實節點(階層路徑),不醒目處理
                nodeContent += "<span style='color:gray;opacity:0.5;'>" + depName + "</span>";
            } else {
                // 真實節點, 醒目顯示
                String style = "font-weight:bold;";
                if (Activation.INACTIVE.equals(dep.getStatus())) {
                    style += "text-decoration:line-through;color:red;opacity:0.7;";
                }
                nodeContent += "<i class=\"fa fa-star\" aria-hidden=\"true\"></i>";
                nodeContent += "<span style='" + style + "'>" + depName + "</span>";
            }
        }

        if (node.getParentNode() != null) {
            resultContents.add(nodeContent);
        }

        // ====================================
        // 遞迴處理子節點
        // ====================================
        if (WkStringUtils.isEmpty(node.getChildNodes())) {
            return resultContents;
        }

        for (WkNode childNode : node.getChildNodes()) {
            resultContents.addAll(prepareDepsNameByTreeStyle_recursion(level + 1, childNode));
        }
        return resultContents;
    }

    // =============================================================================================
    // 單位排序相關
    // =============================================================================================
    /**
     * 取得排序序號
     *
     * @param sortSeqMapByOrgSid MAP[org sid, 排序序號]
     * @param depSid             org sid
     * @return 排序序號
     */
    public static Integer prepareOrgSortSeq(
            Map<Integer, Integer> sortSeqMapByOrgSid,
            Integer depSid) {
        if (!sortSeqMapByOrgSid.containsKey(depSid)) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "找不到傳入的 org 排序資料 orgSid :[" + depSid + "]", 10);
            return Integer.MAX_VALUE;
        }
        return sortSeqMapByOrgSid.get(depSid);
    }

    /**
     * 依照組織樹排序
     *
     * @param depSids org sid collection
     * @return 已排序的 list
     */
    public static List<Integer> sortByOrgTree(Collection<Integer> depSids) {

        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得所有單位排序序號
        // ====================================
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        // ====================================
        // 回傳排序後資料
        // ====================================
        return depSids.stream()
                .sorted(Comparator.comparing(
                        depSid -> prepareOrgSortSeq(sortSeqMapByOrgSid, depSid)))
                .collect(Collectors.toList());
    }

    /**
     * 依照組織樹排序
     *
     * @param depSids            org sid list
     * @param sortSeqMapByOrgSid map[部門sid,該部門排序順序]
     * @return org sid list
     */
    public static List<Integer> sortByOrgTree(
            Collection<Integer> depSids,
            Map<Integer, Integer> sortSeqMapByOrgSid) {

        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 回傳排序後資料
        // ====================================
        return depSids.stream()
                .sorted(Comparator.comparing(
                        depSid -> prepareOrgSortSeq(sortSeqMapByOrgSid, depSid)))
                .collect(Collectors.toList());
    }

    /**
     * 依照組織樹排序
     *
     * @param depSids 傳入部門 sid set
     * @return 回傳排序好的 depSid list
     */
    public static List<Integer> sortDepSidByOrgTree(Collection<Integer> depSids) {
        if (WkStringUtils.isEmpty(depSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得所有單位排序序號
        // ====================================
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        // ====================================
        // 回傳排序後資料
        // ====================================
        return depSids.stream()
                .sorted(Comparator.comparing(
                        depSid -> prepareOrgSortSeq(sortSeqMapByOrgSid, depSid)))
                .collect(Collectors.toList());
    }

    /**
     * 依照組織樹排序
     *
     * @param deps org list
     * @return org list
     */
    public static List<Org> sortOrgByOrgTree(List<Org> deps) {
        if (WkStringUtils.isEmpty(deps)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 取得所有單位排序序號
        // ====================================
        Map<Integer, Integer> sortSeqMapByOrgSid = WkOrgCache.getInstance().findAllOrgOrderSeqMap();

        // ====================================
        // 回傳排序後資料
        // ====================================
        return deps.stream()
                .sorted(Comparator.comparing(
                        dep -> prepareOrgSortSeq(sortSeqMapByOrgSid, dep.getSid())))
                .collect(Collectors.toList());
    }

    // =============================================================================================
    // 上層單位相關
    // =============================================================================================
    /**
     * 僅留下最上層的父單位 (含以下時，縮減資料用)
     *
     * @param depSids org sid list
     * @return top org sid
     */
    public static Set<Integer> collectToTopmost(Collection<Integer> depSids) {

        Set<Integer> topmostDeps = Sets.newHashSet();

        // 為空時跳出
        if (WkStringUtils.isEmpty(depSids)) {
            return topmostDeps;
        }

        for (Integer depSid : depSids) {
            if (depSid == null) {
                continue;
            }

            // 取得此單位上層所有的直系單位
            Set<Integer> directParentDepSids = WkOrgCache.getInstance().findAllParent(depSid)
                    .stream()
                    .map(Org::getSid)
                    .collect(Collectors.toSet());

            // 檢查此單位的任何一個直系父單位，是否已經包含在列表中了
            // 若是-> 則代表此單位可剔除 (因為列表中已經有此單位的父單位了)
            if (!Collections.disjoint(depSids, directParentDepSids)) {
                continue;
            }

            // 此單位為列表中,自己的直系最上層單位, 故加入
            topmostDeps.add(depSid);
        }

        return topmostDeps;
    }

    /**
     * 傳入最上層單位，算出含以下單位 (包含本身)
     *
     * @param topmostDepSids      最上層單位 (無需有隸屬關係)
     * @param isUseInactiveFilter 是否將『停用單位』過濾掉
     * @return org sid list
     */
    public static Set<Integer> prepareAllDepsByTopmost(
            Collection<Integer> topmostDepSids,
            boolean isUseInactiveFilter) {
        // 防呆
        if (WkStringUtils.isEmpty(topmostDepSids)) {
            return Sets.newHashSet();
        }

        // 所有單位的收集容器
        Set<Integer> topWithChildOrgSids = Sets.newHashSet();

        for (Integer topOrgSid : topmostDepSids) {
            // 無法解析的單位跳過
            if (!WkOrgCache.getInstance().isExsit(topOrgSid)) {
                continue;
            }
            // 加入最上層單位
            topWithChildOrgSids.add(topOrgSid);

            // 加入子單位
            Set<Integer> childOrgSids = WkOrgCache.getInstance().findAllChildSids(topOrgSid);
            if (WkStringUtils.notEmpty(childOrgSids)) {
                topWithChildOrgSids.addAll(childOrgSids);
            }
        }

        // 過濾掉『停用』單位 （如果需要的話）
        if (isUseInactiveFilter) {
            return topWithChildOrgSids.stream()
                    .filter(WkOrgUtils::isActive)
                    .collect(Collectors.toSet());
        }

        // 不用過濾停用單位
        return topWithChildOrgSids;
    }

    /**
     * 取得直系上層
     *
     * @param depSid   部門 sid
     * @param orgLevel 目標部門等級
     * @return org sid list
     */
    public static Set<Integer> prepareParentDepUntilOrgLevel(
            Integer depSid,
            OrgLevel orgLevel) {

        // ====================================
        // 取得資料並判斷
        // ====================================
        // 取得 org 資料
        Org org = WkOrgCache.getInstance().findBySid(depSid);

        // 防呆
        if (org == null || org.getParent() == null) {
            return Sets.newHashSet();
        }

        // 排除非部門
        if (!OrgType.DEPARTMENT.equals(org.getType())
                || !OrgType.DEPARTMENT.equals(org.getParent().getType())) {
            return Sets.newHashSet();
        }

        // 目標級別
        Integer targetOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(orgLevel);
        // 取得父部門的部門級別
        Integer parentOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(org.getParent().getLevel());
        // 防呆
        if (targetOrgLevelIndex == -1 || parentOrgLevelIndex == -1) {
            return Sets.newHashSet();
        }

        // 父單位已大於目標
        if (parentOrgLevelIndex > targetOrgLevelIndex) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集直系往上部門
        // ====================================
        Set<Integer> resultDepSids = Sets.newHashSet();
        // 父單位等於目標, 加入清單
        if (parentOrgLevelIndex.equals(targetOrgLevelIndex)) {
            resultDepSids.add(org.getParent().getSid());
        }
        // 父單位小於目標, 遞迴取得上層
        if (parentOrgLevelIndex < targetOrgLevelIndex) {
            // 加入父層
            resultDepSids.add(org.getParent().getSid());

            // 遞迴
            Set<Integer> tempDepSids = prepareParentDepUntilOrgLevel(
                    org.getParent().getSid(),
                    orgLevel);

            // 回傳不為空時, 加入結果
            if (WkStringUtils.notEmpty(tempDepSids)) {
                resultDepSids.addAll(tempDepSids);
            }
        }

        // 轉 list 回傳
        return resultDepSids;
    }

    /**
     * 取得基礎單位
     *
     * @param targetDepSid 目標單位 SID
     * @param orgLevel     基礎單位OrgLevel
     * @return 基礎單位 sid
     */
    public static Org prepareBaseDep(
            Integer targetDepSid,
            OrgLevel orgLevel) {
        return WkOrgCache.getInstance().findBySid(prepareBaseDepSid(targetDepSid, orgLevel));
    }

    /**
     * 取得基礎單位
     *
     * @param targetDepSid 目標單位 SID
     * @param orgLevel     基礎單位OrgLevel
     * @return 基礎單位 sid
     */
    public static Integer prepareBaseDepSid(
            Integer targetDepSid,
            OrgLevel orgLevel) {

        // ====================================
        // 取得資料並判斷
        // ====================================
        // 取得 org 資料
        Org org = WkOrgCache.getInstance().findBySid(targetDepSid);

        // 防呆
        if (org == null || org.getParent() == null) {
            return targetDepSid;
        }

        // 目標級別
        Integer targetOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(orgLevel);
        // 取得自己部門級別
        Integer selfOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(org.getLevel());
        // 取得父部門的部門級別
        Integer parentOrgLevelIndex = WkOrgUtils.getOrgLevelOrder(org.getParent().getLevel());
        // 防呆
        if (targetOrgLevelIndex == -1 || parentOrgLevelIndex == -1) {
            return targetDepSid;
        }

        // 自己的部門級別等於基礎部門, 回傳自己
        if (selfOrgLevelIndex.equals(targetOrgLevelIndex)) {
            return targetDepSid;
        }

        // 父單位已大於目標 回傳自己
        if (parentOrgLevelIndex > targetOrgLevelIndex) {
            return targetDepSid;
        }

        // 父單位等於目標 回傳父單位
        if (parentOrgLevelIndex.equals(targetOrgLevelIndex)) {
            return org.getParent().getSid();
        }

        // 父單位小於目標, 遞迴取得上層
        return prepareBaseDepSid(org.getParent().getSid(), orgLevel);
    }

    /**
     * 比對是否符合 DepRule
     * 
     * @param depRuleEo    AbstractDepRuleEo
     * @param targetDepSid 比對部門 sid
     * @return true/false
     */
    public static boolean isMatchDepRule(
            AbstractDepRuleEo depRuleEo,
            Integer targetDepSid) {

        // 防呆
        if (targetDepSid == null) {
            return false;
        }

        // 計算部門規則
        Set<Integer> canUserDepSids = WkOrgUtils.prepareDepSidsByRule(
                depRuleEo.getDepSids(),
                depRuleEo.isDepIsBelow(),
                depRuleEo.getExceptDepSids(),
                depRuleEo.isExceptDepIsBelow());

        // 防呆
        if (WkStringUtils.isEmpty(canUserDepSids)) {
            return false;
        }

        return canUserDepSids.contains(targetDepSid);
    }

    /**
     * 依據傳入規則，計算結果 （部門列表）
     *
     * @param depSids          正向列表單位
     * @param depIsBelow       正向列表單位是否含以下
     * @param exceptDepSids    除外單位
     * @param exceptDepIsBelow 除外單位是否含以下
     * @return org sid list
     */
    public static Set<Integer> prepareDepSidsByRule(
            List<Integer> depSids,
            boolean depIsBelow,
            List<Integer> exceptDepSids,
            boolean exceptDepIsBelow) {
        // 未設定
        if (WkStringUtils.isEmpty(depSids)) {
            return Sets.newHashSet();
        }

        // ====================================
        // 收集部門
        // ====================================
        Set<Integer> resultDepSids = Sets.newHashSet();

        for (Integer depSid : depSids) {
            // 加入 單位
            resultDepSids.add(depSid);

            // 含以下時，加入單位的子單位
            if (depIsBelow) {
                Set<Integer> childDepSids = WkOrgCache.getInstance()
                        .findAllChildSids(depSid);

                if (WkStringUtils.notEmpty(childDepSids)) {
                    resultDepSids.addAll(childDepSids);
                }
            }
        }

        // ====================================
        // 移除例外部門
        // ====================================
        if (WkStringUtils.notEmpty(exceptDepSids)) {
            for (Integer exceptDepSid : exceptDepSids) {
                // 移除例外單位
                resultDepSids.remove(exceptDepSid);

                // 含以下時，移除例外單位的子單位
                if (exceptDepIsBelow) {
                    Set<Integer> childDepSids = WkOrgCache.getInstance().findAllChildSids(exceptDepSid);

                    if (WkStringUtils.notEmpty(childDepSids)) {
                        resultDepSids.removeAll(childDepSids);
                    }
                }
            }
        }
        return resultDepSids;
    }

    // ========================================================================
    // 副主管邏輯
    // ========================================================================
    /**
     * 取得單位直系往上所有主管 (包含傳入單位)
     * 
     * @param targetOrgSid org sid
     * @return manager sid set
     */
    public static Set<Integer> findOrgAndDirectParentOrgManagerByOrgSid(Integer targetOrgSid) {
        // 查詢傳入單位主管
        Set<Integer> allManagerSids = findOrgManagerUserSidByOrgSid(targetOrgSid);

        // 查詢直系往上單位
        Set<Integer> parentOrgSids = WkOrgCache.getInstance().findAllParentSid(targetOrgSid);

        // 逐筆處理上層單位
        for (Integer parentOrgSid : parentOrgSids) {
            // 查詢傳入單位主管
            Set<Integer> managerSids = findOrgManagerUserSidByOrgSid(parentOrgSid);
            if (WkStringUtils.notEmpty(parentOrgSid)) {
                allManagerSids.addAll(managerSids);
            }
        }

        return allManagerSids;
    }

    /**
     * 取得單位主管+副主管
     * 
     * @param orgSid org sid
     * @return 單位主管+副主管 sid set
     */
    public static Set<Integer> findOrgManagerUserSidByOrgSid(Integer orgSid) {
        Org org = WkOrgCache.getInstance().findBySid(orgSid);
        if (org == null) {
            return Sets.newHashSet();
        }

        Set<Integer> managerUserSids = Sets.newHashSet();
        // 取得部門主管
        if (WkStringUtils.notEmpty(org.getManagers())) {
            for (SimpleUser manager : org.getManagers()) {
                if (manager != null) {
                    managerUserSids.add(manager.getSid());
                }
            }
        }

        // 取得部門副主管
        if (WkStringUtils.notEmpty(org.getDeputyManagers())) {
            for (SimpleUser deputyManager : org.getDeputyManagers()) {
                managerUserSids.add(deputyManager.getSid());
            }
        }

        return managerUserSids;
    }

    /**
     * 是否為單位主管 (正、副主管都算)
     * 
     * @param orgSid  單位 sid
     * @param userSid 要比對的使用者 sid
     * @return true/false
     */
    public static boolean isDepManager(Integer orgSid, Integer userSid) {
        // 取得單位主管
        Set<Integer> managerUserSids = findOrgManagerUserSidByOrgSid(orgSid);
        // 比對
        return managerUserSids.contains(userSid);
    }

    /**
     * 是否為單位正主管
     * 
     * @param orgSid  單位 sid
     * @param userSid 要比對的使用者 sid
     * @return true/false
     */
    public static boolean isMainDepManager(Integer orgSid, Integer userSid) {
        Org org = WkOrgCache.getInstance().findBySid(orgSid);
        if(org ==null) {
            return false;
        }
        return WkUserUtils.isInSimpleUserList(org.getManagers(), userSid);
    }

    /**
     * 是否為單位副主管
     * 
     * @param orgSid  單位 sid
     * @param userSid 要比對的使用者 sid
     * @return true/false
     */
    public static boolean isDeputyDepManager(Integer orgSid, Integer userSid) {
        Org org = WkOrgCache.getInstance().findBySid(orgSid);
        if (org == null) {
            return false;
        }
        if (WkStringUtils.isEmpty(org.getDeputyManagers())) {
            return false;
        }

        for (SimpleUser user : org.getDeputyManagers()) {
            if (WkCommonUtils.compareByStr(userSid, user.getSid())) {
                return true;
            }
        }

        return false;
    }
}
