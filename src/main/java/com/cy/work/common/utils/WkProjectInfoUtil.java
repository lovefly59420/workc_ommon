package com.cy.work.common.utils;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * 專案資訊取得工具
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkProjectInfoUtil {

    public final static String VERSION = "version";
    public final static String ARTIFACT_ID = "artifactId";

    /**
     * 快取
     */
    private static HashMap<String, String> propCache = Maps.newHashMap();

    /**
     * 是否成功讀取 properties 檔案
     */
    private static boolean isSuccessLoad = false;

    static {
        final Properties properties = new Properties();
        try {
            log.info("start load project info ");
            properties.load(WkProjectInfoUtil.class.getClassLoader()
                    .getResourceAsStream("application.properties"));

            for (Entry<Object, Object> valuePair : properties.entrySet()) {
                propCache.put(valuePair.getKey() + "", valuePair.getValue() + "");
                log.info(valuePair.getKey() + "[" + valuePair.getValue() + "]");
            }

            isSuccessLoad = true;
        } catch (IOException e) {
            log.error("取得pom設定資訊失敗！", e);
        }
    }

    /**
     * 取得屬性值
     *
     * @param propName 屬性名稱
     * @return 屬性 value
     */
    public static String findPropValue(String propName) {
        if (!isSuccessLoad) {
            throw new RuntimeException("啟動時讀取檔案 application.properties 失敗，故無法取得專案屬性！");
        }

        if (!propCache.containsKey(propName)) {
            throw new RuntimeException("不存在屬性:[" + propName + "]");
        }

        return propCache.get(propName);
    }
}
