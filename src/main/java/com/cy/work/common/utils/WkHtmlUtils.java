/**
 *
 */
package com.cy.work.common.utils;

/**
 * @author allen1214_wu
 *
 */
public class WkHtmlUtils {

    /**
     * 對傳入內容新增刪除線樣式
     *
     * @param srcContent 傳入內容
     * @param prfixStr
     * @return HTML content
     */
    public static String addStrikethroughStyle(
            String srcContent,
            String prfixStr) {
        return "<span class='WS1-1-2' style='text-decoration: line-through;'>"
                + "(" + prfixStr + ")&nbsp;"
                + "<span style='color:#000000; text-decoration: none;'>"
                + srcContent
                + "</span>"
                + "</span>";
    }

    /**
     * @param srcContent
     * @param className
     * @return HTML content
     */
    public static String addClass(
            String srcContent,
            String className) {
        return "<span class='" + className + "'>"
                + srcContent
                + "</span>";
    }

    // ========================================================================
    // UXGuideline_WS1 色碼
    // http://confluence.iwerp.net:8090/pages/viewpage.action?pageId=37684838
    // ========================================================================

    /**
     * 紅 #E20F0F 警示、預警、錯誤狀態、財務支出 、文字框標記色
     *
     * @param srcContent
     * @return HTML content
     */
    public static String addRedClass(String srcContent) {
        return addClass(srcContent, "WS1-1-2");
    }

    /**
     * 紅(粗體) #E20F0F 警示、預警、錯誤狀態、財務支出 、文字框標記色
     * @param srcContent
     * @return HTML content
     */
    public static String addRedBlodClass(String srcContent) {
        return addClass(srcContent, "WS1-1-2b");
    }

    /**
     * 淺藍 #009ceb 主醒目
     * @param srcContent
     * @return HTML content
     */
    public static String addBlueClass(String srcContent) {
        return addClass(srcContent, "WS1-1-3");
    }

    /**
     * 淺藍(粗體) #009ceb 主醒目
     * @param srcContent
     * @return HTML content
     */
    public static String addBlueBlodClass(String srcContent) {
        return addClass(srcContent, "WS1-1-3b");
    }

    /**
     * 綠 #008e00 財務收入、輔醒目
     * @param srcContent
     * @return HTML content
     */
    public static String addGreenClass(String srcContent) {
        return addClass(srcContent, "WS1-1-4");
    }

    /**
     * 綠(粗體) #008e00 財務收入、輔醒目
     * @param srcContent
     * @return HTML content
     */
    public static String addGreenBlodClass(String srcContent) {
        return addClass(srcContent, "WS1-1-4b");
    }

    /**
     * 紫 #5f2cbf 第三醒目色
     * @param srcContent
     * @return HTML content
     */
    public static String addPurpleClass(String srcContent) {
        return addClass(srcContent, "WS1-1-5");
    }

    /**
     * 紫(粗體) #5f2cbf 第三醒目色
     * @param srcContent
     * @return HTML content
     */
    public static String addPurpleBlodClass(String srcContent) {
        return addClass(srcContent, "WS1-1-5b");
    }

}
