/**
 * 
 */
package com.cy.work.common.utils;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

/**
 * @author allen1214_wu
 */
public class WkSqlUtils {

    /**
     * 準備多關鍵字的查詢 SQL condition
     * 
     * @param columnName  欄位名稱
     * @param keywordsStr 關鍵字字串 (以空白分隔)
     * @return SQL condition string
     */
    public static String prepareConditionSQLByMutiKeyword(String columnName, String keywordsStr) {
        if (WkStringUtils.isEmpty(keywordsStr)) {
            return "";
        }

        String delimiter = "%' AND " + columnName + " LIKE '%";
        String prefix = " (" + columnName + " LIKE '%";
        String suffix = "%') ";

        return Lists.newArrayList(keywordsStr.split(" ")) // 以空白切分關鍵字
                .stream()
                // 去除重複關鍵字
                .distinct()
                // 去除覆蓋非法字元
                .map(keyword -> replaceIllegalSqlLikeStr(keyword))
                // 去除純空白
                .filter(keyword -> WkStringUtils.notEmpty(keyword))
                // 組合語句
                .collect(Collectors.joining(delimiter, prefix, suffix));
    }

    /**
     * 準備多關鍵字查詢 SQL condition (是否存在於其他 table 的指定欄位)
     * 
     * @param targetTableName   標的 table
     * @param bindColumns       標的 table 和 外部 table 的綁定欄位 Entry[標的table欄位, 外部table欄位]
     * @param keywordColumnName 要比對關鍵字的欄位
     * @param keywordsStr       關鍵字字串
     * @return SQL condition string
     */
    public static String prepareExistsInOtherTableConditionSQLForMutiKeywordSearch(
            String targetTableName,
            Map<String, String> bindColumns,
            String keywordColumnName,
            String keywordsStr) {

        List<String> whereConditions = Lists.newArrayList();

        // 兜組綁定欄位where條件 (標的table column = 外部 table column)
        if (bindColumns != null && !bindColumns.isEmpty()) {
            for (Entry<String, String> bindColumn : bindColumns.entrySet()) {
                whereConditions.add(targetTableName + "." + bindColumn.getKey() + " = " + bindColumn.getValue());
            }
        }

        // 兜組關鍵字查詢 where 條件
        whereConditions.add(prepareConditionSQLByMutiKeyword(targetTableName + "." + keywordColumnName, keywordsStr));

        return ""
                + "     EXISTS ( "
                + "         SELECT 1 "
                + "           FROM " + targetTableName + " " + targetTableName
                + "          WHERE " + String.join(" AND ", whereConditions)
                + "             ) ";

    }

    /**
     * @param targetTableName table name
     * @param bindColumns     相互綁定 欄位
     * @return SQL condition string
     */
    public static String prepareExistsInOtherTableConditionSQL(
            String targetTableName,
            Map<String, String> bindColumns) {

        List<String> whereConditions = Lists.newArrayList();

        // 兜組綁定欄位where條件 (標的table column = 外部 table column)
        if (bindColumns != null && !bindColumns.isEmpty()) {
            for (Entry<String, String> bindColumn : bindColumns.entrySet()) {
                whereConditions.add(targetTableName + "." + bindColumn.getKey() + " = " + bindColumn.getValue());
            }
        }

        return ""
                + "     EXISTS ( "
                + "         SELECT 1 "
                + "           FROM " + targetTableName + " " + targetTableName
                + "          WHERE " + String.join(" AND ", whereConditions)
                + "             ) ";
    }

    // ========================================================================
    // 移植 ReqularPattenUtils
    // ========================================================================
    private final static String REPLACE_FUZZY_PATTEN = "(%)+|(_)";
    private final static String HELF_EMPTY = " ";
    private final static String FULL_EMPTY = "　";
    private final static String PERCENT = "%";
    private final static String BACK_SLASH = "\\\\";
    private final static String REPLACE_CHAR = "_";

    /**
     * 覆蓋SQL Query Like的非法字元
     *
     * @param str
     * @return SQL like string
     */
    public static String replaceIllegalSqlLikeStr(String str) {

        if (WkStringUtils.isEmpty(str)) {
            return "";
        }
        if (str.length() == 1 || isAllSameSpChar(str)) {
            return replaceSpStr(str);
        }
        String replaceHelfEmpty = str.replaceAll(HELF_EMPTY, REPLACE_CHAR);
        String replaceFullEmpty = replaceHelfEmpty.replaceAll(FULL_EMPTY, REPLACE_CHAR);
        String replacePercent = replaceFullEmpty.replaceAll(PERCENT, REPLACE_CHAR);
        String replaceBackslash = replacePercent.replaceAll(BACK_SLASH, PERCENT);
        return replaceBackslash;
    }

    private static String replaceSpStr(String str) {
        if (str.contains("\\")) {
            return str.replaceAll("\\\\", "\\\\\\\\");
        }
        return str.replaceAll(REPLACE_FUZZY_PATTEN, "\\\\$1$2");
    }

    /**
     * 檢測是否全部為相同字元
     *
     * @param str target string
     * @return true/false
     */
    private static boolean isAllSameSpChar(String str) {
        if (!str.contains("%") && !str.contains("_") && !str.contains("\\")) {
            return false;
        }
        char[] charArray = str.toCharArray();
        char prevC = 0;
        for (char c : charArray) {
            if (prevC == 0) {
                prevC = c;
                continue;
            }
            if (prevC != c) {
                return false;
            }
            prevC = c;
        }
        return true;
    }
}
