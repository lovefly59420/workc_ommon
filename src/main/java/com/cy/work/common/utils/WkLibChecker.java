/**
 *
 */
package com.cy.work.common.utils;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import java.io.InputStream;
import java.util.Map;

/**
 * @author allen1214_wu
 *
 */
@Slf4j
public class WkLibChecker {

    Map<String, Dependency> srcDependencys;

    /**
     * 建構子
     *
     * @param srcGroupId    來源 lib 的 GroupId
     * @param srcArtifactId 來源 lib 的 ArtifactId
     */
    public WkLibChecker(
            String srcGroupId,
            String srcArtifactId) {

        // ====================================
        // 取得 lib 中的 pom.xml
        // ====================================
        InputStream stream = null;

        try {
            stream = WkVerUtil.prepareResourceAsStream(srcGroupId, srcArtifactId, "pom.xml");
        } catch (Exception e) {
            log.error("取得 pom.xml 失敗, 此 WkLibChecker 無法生效!", e);
            return;
        }

        // ====================================
        // 解析檔案
        // ====================================
        try {
            org.apache.maven.model.Model model = new MavenXpp3Reader().read(stream);
            if (model == null) {
                throw new Exception(" model 回傳 null ");
            }

            if (model.getDependencies() == null) {
                throw new Exception(" lib 沒有設定依賴 (dependency) ");
            }

            this.srcDependencys = Maps.newHashMap();
            for (Dependency dependency : model.getDependencies()) {
                srcDependencys.put(
                        this.prepareKey(
                                dependency.getGroupId(),
                                dependency.getArtifactId()),
                        dependency);
            }

        } catch (Exception e) {
            log.error("解析 pom.xml 失敗, 此 WkLibChecker 無法生效!", e);
        }
    }

    /**
     * @param groupId
     * @param artifactId
     */
    public void compareLibVersion(
            String groupId,
            String artifactId) {

        if (this.srcDependencys == null) {
            log.error("WkLibChecker 已初始化失敗. 此方法無法運作");
            return;
        }

        // 取得傳入檢查條件, 對應的 Dependency 設定
        Dependency dependency = this.srcDependencys.get(this.prepareKey(groupId, artifactId));
        if (dependency == null) {
            log.error("檢查 lib 未設定此依賴 groupId:[{}], artifactId:[{}]", groupId, artifactId);
            return;
        }

        String realUseVersion = WkVerUtil.findLibVersion(groupId, artifactId);
        if (WkStringUtils.isEmpty(realUseVersion)) {
            log.error("取得系統實際使用 lib 版本失敗 groupId:[{}], artifactId:[{}]", groupId, artifactId);
            return;
        }

        if (realUseVersion.equals(dependency.getVersion())) {
            log.info("版本檢查 ok  groupId:[{}], artifactId:[{}], version:[{}]", groupId, artifactId, realUseVersion);
        } else {
            log.error(
                    "版本檢查不相符"
                            + "，pom設定的依賴lib版本, 與AP實際載入的lib版本不同！\r\n"
                            + "  groupId:[{}], artifactId:[{}]: pom設定依賴版本:[{}], 實際載入版本[{}]",
                    groupId, artifactId,
                    dependency.getVersion(),
                    realUseVersion);
        }
    }

    public String prepareKey(
            String groupId,
            String artifactId) {
        return groupId + ":" + artifactId;
    }

}
