package com.cy.work.common.utils;

import com.google.common.collect.Lists;

import java.util.*;

/**
 *
 */
public class WkMathUtils {

    /**
     * list 求差集
     *
     * @param n   source list
     * @param m   target list
     * @param <T> target class
     * @return 差異
     */
    public static <T> List<T> getDifferenceSet(
            List<T> n,
            List<T> m) {
        // 轉化最長列表
        Set<T> set = new HashSet<>(n.size() > m.size() ? n : m);
        // 迴圈最短列表
        for (T t : n.size() > m.size() ? m : n) {
            if (set.contains(t)) {
                set.remove(t);
            } else {
                set.add(t);
            }
        }
        return Lists.newArrayList(set);
    }

    /**
     * list 求交集
     *
     * @param n   source list
     * @param m   target list
     * @param <T> target class
     * @return 交集
     */
    public static <T> List<T> getIntersection(
            List<T> n,
            List<T> m) {
        Set<T> setN = new HashSet<>(n);
        Set<T> setM = new HashSet<>(m);
        setN.retainAll(setM);
        return Lists.newArrayList(setN);
    }

    /**
     * list 合併
     *
     * @param n   source list
     * @param m   target list
     * @param <T> target class
     * @return 合併結果
     */
    public static <T> List<T> getUnion(
            List<T> n,
            List<T> m) {
        Set<T> setN = new HashSet<>(n);
        Set<T> setM = new HashSet<>(m);
        setN.addAll(setM);
        return Lists.newArrayList(setN);
    }

    /**
     * 陣列求差集
     *
     * @param n   source
     * @param m   target
     * @param <T> target class
     * @return 差集
     */
    public static <T> T[] getDifferenceSet(
            T[] n,
            T[] m) {
        List<T> list = WkMathUtils.getDifferenceSet(Arrays.asList(n), Arrays.asList(m));
        return list.toArray(Arrays.copyOf(n, list.size()));
    }

    /**
     * 陣列求交集
     *
     * @param n   source
     * @param m   target
     * @param <T> target class
     * @return 交集
     */
    public static <T> T[] getIntersection(
            T[] n,
            T[] m) {
        List<T> list = WkMathUtils.getIntersection(Arrays.asList(n), Arrays.asList(m));
        return list.toArray(Arrays.copyOf(n, list.size()));
    }

    /**
     * 陣列並集
     *
     * @param n   source
     * @param m   target
     * @param <T> target class
     * @return 聯集
     */
    public static <T> T[] getUnion(
            T[] n,
            T[] m) {
        List<T> list = WkMathUtils.getUnion(Arrays.asList(n), Arrays.asList(m));
        return list.toArray(Arrays.copyOf(n, list.size()));
    }

    /**
     * 測試
     * 
     * @param args xx
     */
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> list1 = new ArrayList<>(Arrays.asList(3, 4, 5, 6));
        System.out.println("list 差集" + getDifferenceSet(list, list1));
        System.out.println("list 並集" + getUnion(list, list1));
        System.out.println("list 交集" + getIntersection(list, list1));
        Integer[] array = new Integer[] { 1, 2, 3, 4 };
        Integer[] array1 = new Integer[] { 3, 4, 5, 6 };
        // 差集[1, 2, 5, 6]
        System.out.println("array 差集" + Arrays.toString(getDifferenceSet(array, array1)));
        // 並集[1, 2, 3, 4, 5, 6]
        System.out.println("array 並集" + Arrays.toString(getUnion(array, array1)));
        // 交集[3, 4]
        System.out.println("array 交集" + Arrays.toString(getIntersection(array, array1)));

    }

}
