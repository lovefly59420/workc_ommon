/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.utils;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.vo.Attachment;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * @author shaun
 */

@Component
public class WkAttachUtils implements InitializingBean, Serializable {

    /**
     *
     */
    public static final char[] FILE_NOT_ALLOWED_CHARACTERS = { '\\', ':', '/', '*', '?', '\"', '<', '>', '|' };
    /**
     *
     */
    public static final char[] FILE_NOT_ALLOWED_CHARACTERS_REPLACE = { '＼', '：', '／', '＊', '？', '〝', '〈', '〉', '｜' };
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7136088332223521284L;
    private static WkAttachUtils instance;

    public static WkAttachUtils getInstance() {
        if (instance == null) {
            instance = new WkAttachUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // WkAttachUtils.instance = this;
    }

    public Attachment<String> createEmptyAttachment(
            Attachment<String> attach,
            String fileName,
            User executor,
            Org dep) {
        attach.setSid(UUID.randomUUID().toString());
        attach.setUploadDept(dep);
        attach.setStatus(Activation.ACTIVE);
        attach.setCreatedUser(executor);
        attach.setCreatedDate(new Date());
        attach.setFileName(fileName);
        attach.setKeyChecked(Boolean.TRUE);
        return attach;
    }

    /**
     * 將非法的檔案名稱字元, 換成全形
     *
     * @param fileName 檔案名稱
     * @return 新的檔案名稱
     */
    public String replaceIllegalcharacter(String fileName) {
        if (WkStringUtils.isEmpty(fileName)) {
            return fileName;
        }

        String newFileName = "";
        // 逐字比對
        for (char ch : fileName.toCharArray()) {
            // 比對此字元是否位於非法字元列表
            for (int i = 0; i < FILE_NOT_ALLOWED_CHARACTERS.length; i++) {
                char IllegalChar = FILE_NOT_ALLOWED_CHARACTERS[i];
                // 若比對為非法字元, 則更換為對應合法字元
                if (ch == IllegalChar) {
                    ch = FILE_NOT_ALLOWED_CHARACTERS_REPLACE[i];
                    break;
                }
            }

            newFileName += ch;
        }

        if (fileName.getBytes().length != newFileName.getBytes().length) {
            String message = String.format("檔案名稱中有非法字元，已進行置換\r\n原檔名：【%s】\r\n新檔名：【%s】", fileName, newFileName);
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, message, 10);
            return newFileName;
        }
        return fileName;
    }

}
