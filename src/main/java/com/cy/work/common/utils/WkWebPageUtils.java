package com.cy.work.common.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author kasim
 */
@Component
public class WkWebPageUtils implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4326206530929722770L;

    private static WkWebPageUtils instance;

    public static WkWebPageUtils getInstance() {
        if (instance == null) {
            instance = new WkWebPageUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        // WkWebPageUtils.instance = this;
    }

    /**
     * 取代使用者複製貼上的標籤格式
     *
     * @param value target str
     * @return String
     */
    public String addWrapTag(String value) {

        String newValue = "<span style=\"white-space: pre-wrap;\">";
        newValue += value.replace("white-space: pre;", "white-space: pre-wrap;");
        newValue += "</span>";
        return newValue;
    }

}
