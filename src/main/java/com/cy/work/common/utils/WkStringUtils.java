package com.cy.work.common.utils;

import com.cy.work.common.vo.value.to.LikeSqlQryStamentTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 共用工具 - 字串處理類
 */
@Component
@Slf4j
public class WkStringUtils implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4791917444921308464L;
    private static WkStringUtils instance;

    /**
     * 模糊查詢要取代的 regex
     */
    private final String REPLACE_FUZZY_PATTEN = "(%)+|(_)";

    public static WkStringUtils getInstance() {
        if (instance == null) {
            instance = new WkStringUtils();
        }
        return instance;
    }

    /**
     * 解決模糊查詢時, 有非法字元問題<br/>
     * 使用方式:<br/>
     * 原 SQL + 'AND ' + prepareLikeWhereStamentWithIllegalChar(targetColumnName, likeConditionValueStr, parameters)
     *
     * @param targetColumnName      目標欄位名稱
     * @param likeConditionValueStr 模糊查詢字串 (使用者輸入)
     * @param parameters            SQL參數 MAP (會將整理好的值放入)
     * @return string
     */
    public static String prepareLikeWhereStamentWithIllegalChar(
            String targetColumnName,
            String likeConditionValueStr,
            Map<String, Object> parameters) {

        // ====================================
        // 當模糊查詢值為空時, 回傳讓條件永遠為 true
        // ====================================
        if (WkStringUtils.isEmpty(likeConditionValueStr)) {
            return " 1=1 ";
        }

        // ====================================
        // 準備轉換過後資料
        // ====================================
        LikeSqlQryStamentTo to = prepareLikeQryStamentWithIllegalChar(likeConditionValueStr);

        // ====================================
        // 準備 SQL 參數名稱
        // ====================================
        // 1.以傳入目標欄位為準, 將"."取代為 "_"
        String paramName = WkStringUtils.safeTrim(targetColumnName)
                .replaceAll("\\.", "_");
        // 2.檢查已存在參數內是否已經有一樣的名稱
        paramName = WkStringUtils.prepareUniqueParamName(paramName, parameters);

        // ====================================
        // 組裝 SQL , 並將參數值放入 map
        // ====================================
        // 將參數值放入 map
        parameters.put(paramName, to.getNewConditionStr());

        // 回傳組裝好的 SQL
        return " " + targetColumnName + " LIKE :" + paramName + " " + to.getLikeSqlSubFixStr() + " ";
    }

    /**
     * 解決模糊查詢時, 有非法字元問題
     *
     * @param likeConditionStr 模糊查詢字串
     */
    public static LikeSqlQryStamentTo prepareLikeQryStamentWithIllegalChar(String likeConditionStr) {

        // ====================================
        // 參數
        // ====================================
        // 需要處理的特殊字元
        String SPC_CHAR = "*%_[]^'";
        // 可供挑選的跳脫字元
        String ESCAPE_CHAR = "+-*/!@#$^&*()_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        // like 的後綴
        String statementSuffixString = "";
        // 被處理過的 新條件字串
        String newQryParameter = "%" + likeConditionStr + "%";

        // ====================================
        // 檢查無需處理
        // ====================================
        // 檢查傳入參數為空
        if (likeConditionStr == null || likeConditionStr.length() == 0) {
            return new LikeSqlQryStamentTo(newQryParameter, statementSuffixString);
        }

        // 檢查是否有需要處理的字元
        boolean hasSpecial = false;
        for (char spcChar : SPC_CHAR.toCharArray()) {
            if (likeConditionStr.indexOf(spcChar) > -1) {
                hasSpecial = true;
                break;
            }
        }

        // 無特殊字元時跳出
        if (!hasSpecial) {
            return new LikeSqlQryStamentTo(statementSuffixString, newQryParameter);
        }

        // 尋找可以作為跳脫字元的 char
        String currEscapeChar = null;
        for (char escapeChar : ESCAPE_CHAR.toCharArray()) {
            if (likeConditionStr.indexOf(escapeChar) < 0) {
                currEscapeChar = escapeChar + "";
                break;
            }
        }

        // 檢查找不到合適的跳脫字元
        if (currEscapeChar == null) {
            newQryParameter = WkStringUtils.instance.replaceIllegalSqlLikeStrAndAddLikeStr(likeConditionStr);
            log.warn("like 條件字串有特殊字元, 但找不到合適的跳脫字元 paramater:[" + likeConditionStr + "]");
            return new LikeSqlQryStamentTo(statementSuffixString, newQryParameter);
        }

        // ====================================
        // 組新參數和 like 後綴
        // ====================================
        // 組後綴 SQL
        statementSuffixString = " ESCAPE '" + currEscapeChar + "' ";

        // 初始化 (迴圈不可用 +=)
        StringBuilder currNewQryParameter = new StringBuilder();

        for (char qryChar : likeConditionStr.toCharArray()) {
            if (SPC_CHAR.indexOf(qryChar) > -1) {
                currNewQryParameter.append(currEscapeChar + qryChar);
            } else {
                currNewQryParameter.append(qryChar);
            }
        }

        newQryParameter = "%" + currNewQryParameter.toString() + "%";

        return new LikeSqlQryStamentTo(statementSuffixString, newQryParameter);
    }

    /**
     * 避免參數名稱衝突
     *
     * @param defaultName 原始名稱
     * @param parameters  已存在的參數 MAP
     * @return 原始名稱運算後結果
     */
    public static String prepareUniqueParamName(
            String defaultName,
            Map<String, Object> parameters) {
        if (!parameters.containsKey(defaultName)) {
            return defaultName;
        }
        int index = 0;

        while (true) {
            String newParamName = defaultName + index;
            if (!parameters.containsKey(newParamName)) {
                return newParamName;
            }
            index++;
        }
    }

    /**
     * 避免參數名稱衝突
     *
     * @param defaultName 原始名稱
     * @param parameters  已存在的參數 MAP
     * @return 原始名稱運算後結果
     */
    public static String prepareUniqueParamName(
            String defaultName,
            List<String> parameters) {
        if (!parameters.contains(defaultName)) {
            return defaultName;
        }
        int index = 0;

        while (true) {
            String newParamName = defaultName + index;
            if (!parameters.contains(newParamName)) {
                return newParamName;
            }
            index++;
        }
    }

    /**
     * 檢核字串是否為空
     *
     * @param object 傳入物件
     * @return true or false
     */
    public static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        }
        if (!"".equals(object.toString().trim())) {
            return false;
        }
        return true;
    }

    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }
        if (!"".equals(str.trim())) {
            return false;
        }
        return true;
    }

    /**
     * 檢核 Collection 是否為空
     *
     * @param collection collection
     * @return true or false
     */
    public static boolean isEmpty(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 檢核MAP為空
     * 
     * @param map
     * @return true or false
     */
    public static boolean isEmpty(Map<?, ?> map) {
        if (map == null || map.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 檢核字串是否不為空
     *
     * @param str
     * @return true or false
     */
    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 檢核物件是否不為空
     *
     * @param obj
     * @return 是/否
     */
    public static boolean notEmpty(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!"".equals(obj.toString().trim())) {
            return true;
        }
        return false;
    }

    /**
     * 檢核Collection是否為不為空
     *
     * @param collection 傳入物件
     * @return true or false
     */
    public static boolean notEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * 檢核MAP不為空
     * 
     * @param map
     * @return true or false
     */
    public static boolean notEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * 判斷字串相等
     *
     * @param srcStr    srcStr
     * @param targetStr targetStr
     * @return 是否相等
     */
    public static boolean isEqual(
            String srcStr,
            String targetStr) {
        if (srcStr == null && targetStr == null) {
            return true;
        }
        if (srcStr == null) {
            srcStr = "";
        }
        if (targetStr == null) {
            targetStr = "";
        }

        return srcStr.equals(targetStr);
    }

    // =============================================================================
    // 處理SQL 字串
    // =============================================================================

    /**
     * 判斷字串不等
     *
     * @param srcStr    srcStr
     * @param targetStr targetStr
     * @return 是否『不』相等
     */
    public static boolean notEqual(
            String srcStr,
            String targetStr) {
        return !isEqual(srcStr, targetStr);
    }

    /**
     * 判斷英數字
     *
     * @param val string
     * @return 是/否
     */
    public static boolean isAlpha(String val) {
        if (val == null) {
            return false;
        }
        for (int i = 0; i < val.length(); i++) {
            char ch = val.charAt(i);
            if ((ch < 'a' || ch > 'z')
                    && (ch < 'A' || ch > 'Z')
                    && (ch < '0' || ch > '9')) {
                return false;
            }
        }
        return true;
    }

    /**
     * 是否為數字
     * 
     * @param val string
     * @return 是/否
     */
    public static boolean isNumber(String val) {
        if (val == null) {
            return false;
        }
        for (int i = 0; i < val.length(); i++) {
            char ch = val.charAt(i);
            if (ch < '0' || ch > '9') {
                return false;
            }
        }
        return true;
    }

    /**
     * 判斷是否為大寫英文字母
     *
     * @param val
     * @return 是/否
     */
    public static boolean isCapsEnglish(String val) {
        if (val == null) {
            return false;
        }
        for (int i = 0; i < val.length(); i++) {
            char ch = val.charAt(i);

            if (ch < 'A' || ch > 'Z') {
                return false;
            }
        }
        return true;
    }

    // =============================================================================
    // is 判斷
    // =============================================================================

    /**
     * 是否為英文或數字
     *
     * @param val 傳入字串
     * @return 是/否
     */
    public static boolean isNumberOrCapsEnglish(String val) {
        if (val == null) {
            return false;
        }
        for (int i = 0; i < val.length(); i++) {
            char ch = val.charAt(i);

            if ((ch < 'A' || ch > 'Z') && (ch < '0' || ch > '9')) {
                return false;
            }
        }
        return true;
    }

    /**
     * E-mail格式檢查
     *
     * @param email 帶檢核字串
     * @return 是/否
     */
    public static boolean isEMail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return WkStringUtils.safeTrim(email)
                .matches(regex);
    }

    /**
     * E-mail格式檢查
     *
     * @param data     待檢查字串
     * @param ENCODING 字串編碼
     * @return 是/否
     * @throws UnsupportedEncodingException 指定編碼錯誤時拋出
     */
    public static boolean isEMail(
            String data,
            String ENCODING) throws UnsupportedEncodingException {
        if (data != null && !data.trim()
                .equalsIgnoreCase("")) {
            data = data.trim();
            char[] charArray = data.toCharArray();
            for (char c : charArray) {
                if (String.valueOf(c).getBytes(ENCODING).length != 1) {
                    return false;// Email格式錯誤;
                }
            }
            if (!data.contains("@")) {
                return false;// Email格式錯誤;
            }
            String[] email = data.split("@");
            if (email.length != 2) {
                return false;// Email格式錯誤;
            }
            if ((email[0].contains(" ")) || (email[1].contains(" "))) {
                return false;// Email格式錯誤;
            }
            if (!data.contains(".")) {
                return false;// Email格式錯誤;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * 判斷str1是否包含str2
     *
     * @param str1 字串1
     * @param str2 字串2
     * @return 是/否
     */
    public static boolean isContains(
            String str1,
            String str2) {
        if (str1 == null) {
            str1 = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return str1.toUpperCase()
                .contains(str2.toUpperCase());
    }

    /**
     * trim 字串 (避免傳入值為null)
     *
     * @param s 串入值
     * @return 執行 trim 後的 字串
     */
    public static String safeTrim(Object s) {
        return safeTrim(s, "");
    }

    /**
     * trim 字串 (避免傳入值為null)
     *
     * @param s          傳入字
     * @param defaultStr 預設字串
     * @return 執行 trim 後的 字串
     */
    public static String safeTrim(
            Object s,
            String defaultStr) {
        if (s == null || isEmpty(s)) {
            return defaultStr;
        }
        return s.toString()
                .trim();
    }

    /**
     * Trim 全形空白 (包含半形)
     *
     * @param str 待處理字串
     * @return 處理結果
     */
    public static String trimFullWidthSpace(String str) {
        String result = "";
        if (str != null && !"".equalsIgnoreCase(str)) {
            int length = str.length();
            int len = length;
            int st = 0;
            char[] val = str.toCharArray();
            while ((st < len) && ((val[st] == ' ') || (val[st] == '　'))) {// 前面的半型空白和全型空白
                st++;
            }
            while ((st < len) && ((val[len - 1] == ' ') || (val[len - 1] == '　'))) {// 後面的半型空白和全型空白
                len--;
            }
            result = ((st > 0) || (len < length)) ? str.substring(st, len) : str;
        }
        return result;
    }

    /**
     * 將ExceptionStackTrace轉為字串
     *
     * @param e Throwable Exception 物件 (Throwable 以下繼承的類型均可傳入)
     * @return Exception Stack Trace
     */
    public static String getExceptionStackTrace(Throwable e) {
        if (e == null) {
            return "Exception object is null";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2048);
        e.printStackTrace(new PrintStream(byteArrayOutputStream));
        return new String(byteArrayOutputStream.toByteArray());
    }

    /**
     * 移除傳入字串中的控制字元
     *
     * @param str 待處理字串
     * @return 處理結果字串
     */
    public static String removeCtrlChr(String str) {

        // 為空時不用處理
        if (WkStringUtils.isEmpty(str)) {
            return str;
        }

        int[] escapeAscii = new int[] { 0, 9, 10, 13 };

        // 重組欄位值 （移除控制字元）
        StringBuilder newValue = new StringBuilder();
        for (char chr : str.toCharArray()) {
            // 轉為 ASCII code
            int ascii = (int) chr;
            // 為控制字元時移除
            // https://zh.wikipedia.org/wiki/ASCII
            if (ascii <= 31 || ascii == 127) {
                if (!contains(escapeAscii, ascii)) {
                    log.debug("移除控制字元:" + ascii);
                    continue;
                }
            }

            newValue.append(chr);
        }
        return newValue.toString();
    }

    /**
     * 表情符號判斷 <br/>
     * from : https://cloud.tencent.com/developer/article/1639973
     *
     * @param codePoint 待識別的 char
     * @return 是否為表情符號
     */
    public static boolean isEmojiCharacter(char codePoint) {
        char[] targets = new char[] { 0x0, 0x9, 0xA, 0xD };

        for (char target : targets) {
            if (target == codePoint) {
                return true;
            }
        }

        if (codePoint >= 0x20 && codePoint <= 0xD7FF) {
            return true;
        }

        if (codePoint >= 0xE000 && codePoint <= 0xFFFD) {
            return true;
        }

        if (codePoint >= 0x10000 && codePoint <= 0x10FFFF) {
            return true;
        }

        return false;
    }

    /**
     * int array 比對
     *
     * @param array
     * @param v
     * @return
     */
    private static boolean contains(
            final int[] array,
            final int v) {
        for (int i : array) {
            if (i == v) {
                return true;
            }
        }
        return false;
    }

    /**
     * 安全方法:取得字串長度<br/>
     * 排除因字串為 null 時, 產生的 exception
     *
     * @param str 字串
     * @return 字串長度
     */
    public static Integer safeGetlength(String str) {
        if (str == null) {
            return 0;
        }
        return str.length();
    }

    public static List<Integer> safeConvertStr2Int(List<String> strs) {
        List<Integer> ints = Lists.newArrayList();
        if (WkStringUtils.isEmpty(strs)) {
            return ints;
        }

        for (String str : strs) {
            try {
                ints.add(Integer.parseInt(str));
            } catch (Exception e) {
                log.warn("safeConvertStr2Int - 傳入 str 資料錯誤!:[" + str + "] => by pass");
            }
        }
        return ints;
    }

    /**
     * Padding Char
     *
     * @param str         原始字串
     * @param paddingChar paddingChar
     * @param length      padding length
     * @param isLeft      是否補在字串左邊
     * @throws Exception
     */
    public static String padding(
            String str,
            char paddingChar,
            int length,
            boolean isLeft) {

        if (str == null) {
            str = "";
        }

        String paddingCharStr = "";
        for (int z = 0; z < length * 2; z++) {
            paddingCharStr += paddingChar;
        }

        // 多字元判斷
        for (int i = 0; i < str.length(); i++) {
            String c = str.substring(i, i + 1);
            if (c.getBytes().length > 1) {
                length = length - 2;
            } else {
                length = length - 1;
            }
        }

        if (length < 1) {
            return str;
        }

        if (isLeft) {
            return paddingCharStr.substring(0, length) + str;
        }
        return str + paddingCharStr.substring(0, length);
    }

    /**
     * 轉為 base64 編碼
     *
     * @param str 字串
     * @return base64 encode string
     */
    public static String toBase64(String str) {
        byte[] encodedBytes = Base64.encodeBase64(str.getBytes());
        String encodedStr = new String(encodedBytes);
        // System.out.println(encodedStr); // aGVsbG8gd29ybGQ=
        return encodedStr;
    }

    // =============================================================================
    // Trim
    // =============================================================================

    /**
     * 反解 base64 編碼
     *
     * @param base64EncodedStr base64 編碼後的字串
     * @return 未編碼字串
     */
    public static String fromBase64(String base64EncodedStr) {
        byte[] decodedBytes = Base64.decodeBase64(base64EncodedStr);
        String decodedStr = new String(decodedBytes);
        // System.out.println(decodedStr); // hello world
        return decodedStr;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // WkStringUtils.instance = this;
    }

    /**
     * 以byte為基準點進行padEnd
     *
     * @param str
     * @param maxLen
     * @param endString
     * @return result string
     */
    public String padEndEmpty(
            String str,
            int maxLen,
            String endString) {
        int finishLen = maxLen - str.getBytes().length;
        int calcCnt = finishLen / 4;
        int calcCnt2 = finishLen % 4;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        for (int i = 0; i < calcCnt2; i++) {
            sb.append(" ");
        }
        for (int i = 0; i < calcCnt; i++) {
            sb.append("\t");
        }
        sb.append(endString);
        return sb.toString();
    }

    /**
     * 處理文字方塊
     *
     * @param text
     * @param intervalSpace
     * @return result content
     */
    public String readLine(
            String text,
            String intervalSpace) {
        StringBuilder sb = new StringBuilder();
        String str;
        BufferedReader reader = new BufferedReader(new StringReader(text));
        try {
            while ((str = reader.readLine()) != null) {
                sb.append(intervalSpace);
                sb.append(intervalSpace)
                        .append(str)
                        .append("\n");
            }
            reader.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return sb.toString();
    }

    // =============================================================================
    // Exception 轉字串
    // =============================================================================

    /**
     * 處理文字方塊..第一行插入序號
     *
     * @param text
     * @param intervalSpace
     * @param areaIdx
     * @return result content
     */
    public String readLineShowIndex(
            String text,
            String intervalSpace,
            int areaIdx) {
        StringBuilder sb = new StringBuilder();
        String str;
        BufferedReader reader = new BufferedReader(new StringReader(text));
        try {
            int idx = 0;
            while ((str = reader.readLine()) != null) {
                sb.append(intervalSpace);
                if (idx++ == 0) {
                    sb.append(Strings.padStart(String.valueOf(areaIdx + 1), 3, ' '))
                            .append(".");
                } else {
                    sb.append("    ");
                }
                sb.append(intervalSpace)
                        .append(str)
                        .append("\n");
            }
            reader.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return sb.toString();
    }

    // =============================================================================
    // 字串處理
    // =============================================================================

    /**
     * 將文字補滿所需要長度
     *
     * @param str
     * @param len
     * @param glue
     * @return str
     */
    public String fullLeftText(
            String str,
            int len,
            String glue) {
        if (Strings.isNullOrEmpty(str)) {
            str = "";
        }
        if (str.length() < len) {
            for (int i = 0; i < len - str.length(); i++) {
                str = glue + str;
            }
        }
        return str;
    }

    /**
     * 將List集合組成一字串
     *
     * @param list : 字串集合物件
     * @param glue : 區隔符
     * @return 結果字串
     */
    public String combine(
            List<String> list,
            String glue) {
        if (list == null || list.isEmpty()) {
            return "";
        }
        int k = list.size();
        StringBuilder out = new StringBuilder();
        out.append(list.get(0));
        for (int x = 1; x < k; ++x) {
            out.append(glue)
                    .append(list.get(x));
        }
        return out.toString();
    }

    /**
     * 覆蓋SQL Query Like的非法字元並增加前後百分比
     *
     * @param str 來源字串
     * @return 結果
     */
    public String replaceIllegalSqlLikeStrAndAddLikeStr(String str) {
        return "%" + replaceIllegalSqlLikeStr(str) + "%";
    }

    /**
     * 覆蓋SQL Query Like的非法字元
     *
     * @param str
     * @return
     */
    private String replaceIllegalSqlLikeStr(String str) {
        return str.replaceAll(REPLACE_FUZZY_PATTEN, "'$1$2'");
    }

    /**
     * 檢查字串長度是否超過限定的長度
     *
     * @param str
     * @param limitLength
     * @return boolean
     */
    public boolean checkStringLengthIsOver(
            String str,
            int limitLength) {
        if (str.trim()
                .length() > limitLength) {
            return true;
        }
        return false;
    }

    /**
     * 檢查是否只有數字
     *
     * @param str
     * @return boolean
     */
    public boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    // =============================================================================
    // base 64
    // =============================================================================

    /**
     * 判斷str1是否包含str2
     *
     * @param str1
     * @param str2
     * @return String
     */
    public boolean contains(
            String str1,
            String str2) {
        if (str1 == null) {
            str1 = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        return str1.toUpperCase()
                .contains(str2.toUpperCase());
    }

    /**
     * 將左邊的某一字元去除
     *
     * @param str 原始字串
     * @param ch  要去除的字元
     * @return result string
     */
    public String trimLeftChar(
            String str,
            char ch) {

        if (str == null) {
            return "";
        }

        String sch = String.valueOf(ch);
        for (int i = 0; i < str.length(); i++) {
            if (!sch.equals(String.valueOf(str.charAt(i)))) {
                return str.substring(i);
            }
        }
        return str;
    }

    /**
     * UNICODE to string
     * 
     * @param str string
     * @return UNICODE to string
     */
    public static String unicodeToString(String str) {
        Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            String group = matcher.group(2);
            ch = (char) Integer.parseInt(group, 16);
            String group1 = matcher.group(1);
            str = str.replace(group1, ch + "");
        }
        return str;
    }

}
