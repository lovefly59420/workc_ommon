package com.cy.work.common.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.cglib.core.Converter;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class WkEntityUtils implements InitializingBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3839638423525836935L;
    private static WkEntityUtils instance;

    public static WkEntityUtils getInstance() {
        if (instance == null) {
            instance = new WkEntityUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        WkEntityUtils.instance = this;
    }

    /**
     * 注意：不支援基本类型与装箱类型，int->Integer，Integer->int <br/>
     * Field名稱 Field型態 皆需相同(不會autoboxing)(Cglib BeanCopier)
     *
     * @param source
     * @param target
     * @return target
     */
    public Object copyProperties(
            Object source,
            Object target) {
        BeanCopier copier = BeanCopier.create(source.getClass(), target.getClass(), false);
        copier.copy(source, target, null);
        return target;
    }

    /**
     * 當Filed名稱相同但Field型態不同時，可透過轉換器(Cglib BeanCopier)進行複製
     * <p/>
     * Converter 解釋 <BR/>
     * public convert(Object o, Class type, Object o1){...}<BR/>
     * o = 來源的值物件<BR/>
     * type = 目標的值屬性<BR/>
     * o1 = 存取的方法名稱
     * <p/>
     * 重要
     * <p/>
     * 使用Converter時必需將所有要Copy的值型態都考慮到，才能完整複製。
     *
     * @param source
     * @param target
     * @param converter
     * @return target
     */
    public Object copyProperties(
            Object source,
            Object target,
            Converter converter) {
        BeanCopier copier = BeanCopier.create(source.getClass(), target.getClass(), true);
        copier.copy(source, target, converter);
        return target;
    }

}
