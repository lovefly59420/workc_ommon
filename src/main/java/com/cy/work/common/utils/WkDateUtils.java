package com.cy.work.common.utils;

import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kasim
 */
@Slf4j
@Component
public class WkDateUtils implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2372141707806833817L;
    public static DateTimeFormatter YYYY_MM_DD_HH24_MI = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm");
    public static DateTimeFormatter YYYY_MM_DD_HH24_mm_ss = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
    public static DateTimeFormatter YYYY_MM_DD_HH24_mm_ss_SSS = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss:SSS");
    public static DateTimeFormatter YYYY_MM_DD_HH24_mm_ss2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter YYYY_MM_DD = DateTimeFormat.forPattern("yyyy/MM/dd");
    public static DateTimeFormatter YYYY_MM_DD2 = DateTimeFormat.forPattern("yyyy-MM-dd");
    public static DateTimeFormatter YYYY_MM = DateTimeFormat.forPattern("yyyy/MM");
    public static DateTimeFormatter HH24_mm_ss = DateTimeFormat.forPattern("HH:mm:ss");

    /**
     * 判斷String格式轉換為Date
     *
     * @param dateStr
     * @return date
     */
    public static Date timeAnalysis(String dateStr) {
        Date result = new Date();

        Pattern timestampPattern = Pattern.compile(
                "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$");
        Matcher timestampMatcher = timestampPattern.matcher(dateStr);

        Pattern longPattern = Pattern.compile("^\\d+$");
        Matcher longMatcher = longPattern.matcher(dateStr);
        try {
            if (timestampMatcher.matches()) {
                result = DateTime.parse(dateStr, YYYY_MM_DD_HH24_mm_ss2)
                        .toDate();
            } else if (longMatcher.matches()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                result = sdf.parse(sdf.format(new Date(Long.parseLong(dateStr))));
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return result;
    }

    /**
     * 轉換當天時分秒為00:00:00.0
     *
     * @param date
     * @return Date
     */
    public static Date convertToOriginOfDay(Date date) {
        if (date == null) {
            return null;
        }
        DateTime dateTime = new DateTime(date);

        return dateTime.withTime(0, 0, 0, 0)
                .toDate();
    }

    /**
     * 轉換當天時分秒為23:59:59.999
     *
     * @param date
     * @return Date
     */
    public static Date convertToEndOfDay(Date date) {
        if (date == null) {
            return null;
        }
        DateTime dateTime = new DateTime(date);

        return dateTime.withTime(23, 59, 59, 999)
                .toDate();
    }

    /**
     * 轉換為當月第一天，時分秒為00:00:00.0
     *
     * @param date
     * @return Date
     */
    public static Date convertToOriginOfMonth(Date date) {
        if (date == null) {
            return null;
        }
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.dayOfMonth()
                .withMinimumValue();
        dateTime = dateTime.withTime(0, 0, 0, 0);
        return dateTime.toDate();
    }

    /**
     * 轉換為當月最後一天，時分秒為23:59:59.999
     *
     * @param date
     * @return Date
     */
    public static Date convertToEndOfMonth(Date date) {
        if (date == null) {
            return null;
        }
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.dayOfMonth()
                .withMaximumValue();
        dateTime = dateTime.withTime(23, 59, 59, 999);
        return dateTime.toDate();
    }

    /**
     * 判斷是否為系統日
     *
     * @param date
     * @return true/false
     */
    public static boolean isSystemDate(Date date) {
        if (date != null) {
            return YYYY_MM_DD.print(date.getTime())
                    .equals(YYYY_MM_DD.print(new Date().getTime()));
        }
        return false;
    }

    /**
     * 是否為同一天
     *
     * @param date1 date1
     * @param date2 date2
     * @return true/false
     */
    public static boolean isSameDate(
            Date date1,
            Date date2) {
        if (date1 != null && date2 != null) {
            return YYYY_MM_DD.print(date1.getTime())
                    .equals(YYYY_MM_DD.print(date2.getTime()));
        }
        return false;
    }

    /**
     * 比較 srcDate > targetDate (有任一傳入日期為 null, 則回傳 defaultValue)
     *
     * @param srcDate
     * @param targetDate
     * @return true/false
     */
    public static boolean isAfter(
            Date srcDate,
            Date targetDate,
            boolean defaultValue) {
        if (srcDate == null || targetDate == null) {
            return defaultValue;
        }
        return srcDate.after(targetDate);
    }

    /**
     * 比較 srcDate < targetDate (有任一傳入日期為 null, 則回傳 false)
     *
     * @param srcDate
     * @param targetDate
     * @return true/false
     */
    public static boolean isBefore(
            Date srcDate,
            Date targetDate,
            boolean defaultValue) {
        if (srcDate == null || targetDate == null) {
            return defaultValue;
        }
        return srcDate.before(targetDate);
    }

    /**
     * 依據傳入的 DateTimeFormatter, 回傳字串型態日期
     *
     * @param date
     * @param dateTimeFormatter
     * @return date text content
     */
    public static String formatDate(
            Date date,
            DateTimeFormatter dateTimeFormatter) {
        if (date == null) {
            return "";
        }
        return dateTimeFormatter.print(date.getTime());
    }

    /**
     * date to long
     *
     * @param date
     * @param whenNullDefault
     * @return time
     */
    public static Long safeToTimeMillis(
            Date date,
            Long whenNullDefault) {
        if (date != null) {
            return date.getTime();
        }
        return whenNullDefault;
    }

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return date string
     */
    public static String transStrByStartDate(Date startDate) {
        return new DateTime(startDate).toString("yyyy-MM-dd") + " 00:00:00";
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return date string
     */
    public static String transStrByEndDate(Date endDate) {
        return new DateTime(endDate).toString("yyyy-MM-dd") + " 23:59:59";
    }

    /**
     * 增加1秒
     *
     * @param date
     * @return Date
     */
    public static Date addOneSec(Date date) {
        if (date == null) {
            return null;
        }
        return new Date(date.getTime() + 1000);
    }

    /**
     * 日期加減 (天數)
     *
     * @param srcDate 原始日期
     * @param days    要增加(+)或減少(-)的天數
     * @return Date
     */
    public static Date plusDays(
            Date srcDate,
            int days) {
        DateTime dateTime = new DateTime(srcDate);
        dateTime = dateTime.plusDays(days);
        return dateTime.toDate();
    }

    /**
     * 轉為當天最早的時間
     *
     * @param date
     * @return Date
     */
    public static Date toStartDay(Date date) {
        DateTime dateTime = new DateTime(date);
        // 下面這個方法用錯了，是用來調整時區差異的 by allen
        // dateTime = dateTime.withEarlierOffsetAtOverlap();
        // 改為下面這個方法
        dateTime = dateTime.withTimeAtStartOfDay();
        return dateTime.toDate();
    }

    /**
     * 轉為當天最晚的時間
     *
     * @param date
     * @return Date
     */
    public static Date toEndDay(Date date) {
        DateTime dateTime = new DateTime(date);
        // 下面這個方法用錯了，是用來調整時區差異的 by allen
        // dateTime = dateTime.withLaterOffsetAtOverlap();
        // 改為下面這個方法
        dateTime = dateTime.withTimeAtStartOfDay().plusDays(1).minus(1);
        return dateTime.toDate();
    }

    /**
     * 查詢啟始日的時間調整為00-00-00
     *
     * @param startDate
     * @return date
     */
    public Date transStartDate(Date startDate) {
        return new LocalDate(startDate).toDate();
    }

    /**
     * 查詢結束日的時間調整為23-59-59
     *
     * @param endDate
     * @return Date
     */
    public Date transEndDate(Date endDate) {
        DateTime endTime = new DateTime(new LocalDate(endDate).plusDays(1)
                .toDate()).minusMillis(1);
        return endTime.toDate();
    }

    /**
     * 轉換Fogbugz日期參數為data型態
     *
     * @param dateStr
     * @return Date
     */
    public Date transDateByFogbugz(String dateStr) {
        if (Strings.isNullOrEmpty(dateStr)) {
            return null;
        }
        String temp = dateStr;
        if (dateStr.length() > 19) {
            if (("+").equals(dateStr.substring(10, 11))) {
                temp = dateStr.replace("+", "T");
            } else if ((" ").equals(dateStr.substring(10, 11))) {
                temp = dateStr.replace(" ", "T");
            }
        }
        try {
            DateTime date = DateTime.parse(temp);
            date = date.withZone(DateTimeZone.forID("Asia/Taipei"));
            return date.toDate();
        } catch (Exception ex) {
            log.error("轉換Fogbugz日期參數發生錯誤：" + dateStr);
        }
        return null;
    }

    /**
     * 日期+時間合併
     *
     * @param date
     * @param timeStr 字串格式僅接受 HHmm
     * @return Date
     */
    public static Date mergeDtByCondition(Date date, String timeStr) {
        if (date == null) {
            date = createNowDateTime();
        }
        timeStr = timeStr.replace(":", "").replace("：", "");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.HOUR_OF_DAY, parserTimeHourStr(timeStr));
        cal.add(Calendar.MINUTE, parserTimeMinStr(timeStr));
        return cal.getTime();
    }

    /**
     * 解析班別時間字串<BR/>
     * 回傳整數數值
     * HH
     *
     * @param timeStr
     * @return Hour
     */
    public static int parserTimeHourStr(String timeStr) {
        int idxStart = 0;
        int idxEnd = 2;
        Integer time;
        try {
            time = Integer.valueOf(timeStr.substring(idxStart, idxEnd));
        } catch (NumberFormatException e) {
            log.error("分析時間字串【時】錯誤!! 時間字串 : " + timeStr);
            time = 0;
        }
        return time;
    }

    /**
     * 解析時間字串中的 『分』
     * 
     * @param timeStr 時間字串 (HH:mm)
     * @return 分
     */
    public static int parserTimeMinStr(String timeStr) {
        int idxStart = 2;
        int idxEnd = 4;
        Integer time;
        try {
            time = Integer.valueOf(timeStr.substring(idxStart, idxEnd));
        } catch (NumberFormatException e) {
            log.error("分析時間字串【分】錯誤!! 時間字串 : " + timeStr);
            time = 0;
        }
        return time;
    }

    /**
     * 取得現在日期時間 Date
     * 
     * @return Date
     */
    public static Date createNowDateTime() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
