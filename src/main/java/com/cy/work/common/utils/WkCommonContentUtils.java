/**
 *
 */
package com.cy.work.common.utils;

import com.cy.commons.enums.OrgLevel;
import com.cy.work.common.vo.value.to.ItemsCollectionDiffTo;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;

/**
 * 用於都組內容的工具
 *
 * @author allen
 */
public class WkCommonContentUtils {

    // ============================================================================================================
    // show by style
    // ============================================================================================================

    /**
     * 將 String Collection 美化為 data List 樣式
     *
     * @param itemInfos
     * @param emptyMessage
     * @return format content
     */
    public static String makeupByDataListStyle(
            Collection<String> itemInfos,
            String emptyMessage) {

        String htmlContent = "<div class='ui-widget-content' style='width: -webkit-fill-available; margin-top:10px; padding:20px 5px 5px 5px'>";

        if (WkStringUtils.isEmpty(itemInfos)) {
            htmlContent += "<label style='text-align: center; color: rgb(128, 128, 128);'>" + emptyMessage + "</label>";
        } else {

            htmlContent += "<ol class='ui-datalist-data'>";
            for (String itemInfo : itemInfos) {
                htmlContent += "<li class='ui-datalist-item'>";
                htmlContent += "<label>" + itemInfo + "</label>";
                htmlContent += "<hr style='border-style: ridge'/>";
                htmlContent += "</li>";
            }
            htmlContent += "</ol>";
        }

        htmlContent += "</div>";
        return htmlContent;
    }

    // ============================================================================================================
    // 人員.部門異動比對訊息
    // ============================================================================================================

    /**
     * 兜組部門異動比對訊息
     *
     * @param beforeDepSids   異動前
     * @param afterDepSids    異動後
     * @param addPartTitle    新增資訊 title
     * @param removePartTitle 移除資訊 title
     * @return result content
     */
    public static String prepareDiffMessageForDepartment(
            Collection<Integer> beforeDepSids,
            Collection<Integer> afterDepSids,
            String addPartTitle,
            String removePartTitle) {
        return prepareDiffMessage(beforeDepSids, afterDepSids, addPartTitle, removePartTitle, true);
    }

    /**
     * 兜組人員異動比對訊息
     *
     * @param beforeUserSids  異動前
     * @param afterUserSids   異動後
     * @param addPartTitle    新增資訊 title
     * @param removePartTitle 移除資訊 title
     * @return result content
     */
    public static String prepareDiffMessageForUser(
            Collection<Integer> beforeUserSids,
            Collection<Integer> afterUserSids,
            String addPartTitle,
            String removePartTitle) {

        return prepareDiffMessage(beforeUserSids, afterUserSids, addPartTitle, removePartTitle, false);
    }


    /**
     *兜組人員異動比對訊息
     * @param beforeItemSids 異動前
     * @param afterItemSids 異動後
     * @param addPartTitle 新增資訊 title
     * @param removePartTitle 移除資訊 title
     * @param isDep  true 部門比對/ false 人員比對
     * @return result content
     */
    private static String prepareDiffMessage(
            Collection<Integer> beforeItemSids,
            Collection<Integer> afterItemSids,
            String addPartTitle,
            String removePartTitle,
            boolean isDep) {

        // ====================================
        // 比對
        // ====================================
        ItemsCollectionDiffTo<Integer> diffTo = WkCommonUtils.itemCollectionDiff(beforeItemSids, afterItemSids);

        // ====================================
        // 兜組內容
        // ====================================
        String style = "font-weight:bold;text-decoration:underline;";
        Integer maxWidth = 300;

        // 增加的項目
        List<String> messages = Lists.newArrayList();
        if (WkStringUtils.notEmpty(diffTo.getPlusItems())) {
            messages.add("<font style='" + style + "'>" + addPartTitle + ":</font>");
            List<Integer> addItemSids = Lists.newArrayList(diffTo.getPlusItems());
            if (isDep) {
                if (diffTo.getPlusItems()
                        .size() > 3) {
                    messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(addItemSids, 10) + "<br/>");
                } else {
                    messages.add(WkOrgUtils.findNameBySid(addItemSids, "、", maxWidth) + "<br/>");
                }
            } else {
                messages.add(WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(addItemSids,
                                                                                        OrgLevel.MINISTERIAL, true, ">&nbsp;", "", false));
            }
        }

        // 減少的項目
        if (WkStringUtils.notEmpty(diffTo.getReduceItems())) {
            List<Integer> removeItemSids = Lists.newArrayList(diffTo.getReduceItems());
            messages.add("<font style='" + style + "'>" + removePartTitle + ":</font>");
            if (isDep) {
                if (removeItemSids.size() > 3) {
                    messages.add(WkOrgUtils.prepareDepsNameByTreeStyle(removeItemSids, 10) + "<br/>");
                } else {
                    messages.add(WkOrgUtils.findNameBySid(removeItemSids, "、", maxWidth) + "<br/>");
                }
            } else {
                messages.add(WkCommonContentUtils.prepareUserNameWithDepByDataListStyle(removeItemSids,
                                                                                        OrgLevel.MINISTERIAL, true, ">&nbsp;", "", false));
            }
        }

        if (WkStringUtils.isEmpty(messages)) {
            return "";
        }

        return String.join("<br/>", messages);
    }

    // ============================================================================================================
    // for User
    // ============================================================================================================

    /**
     * 兜組 部門+使用者暱稱字串 by data list style
     *
     * @param userSids       使用者 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param depSplitStr    部門、使用者中間的分隔文字
     * @param emptyMessage   當傳入人員為空時, 要顯示的訊息
     * @param isWithUserSid  是否顯示 user sid
     * @return result content
     */
    public static String prepareUserNameWithDepByDataListStyle(
            Collection<Integer> userSids,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String depSplitStr,
            String emptyMessage,
            boolean isWithUserSid) {

        if (userSids == null) {
            userSids = Lists.newArrayList();
        }

        // ====================================
        // 兜組所有傳入 user 的資訊
        // ====================================
        List<String> userInfos = WkUserUtils.prepareUserNameWithDep_core(userSids, topLevel, isForceUpLevel,
                                                                         depSplitStr, isWithUserSid);

        // ====================================
        // 組 data list 樣式
        // ====================================
        return WkCommonContentUtils.makeupByDataListStyle(userInfos, emptyMessage);

    }

    // ============================================================================================================
    // for Org
    // ============================================================================================================

    /**
     * 兜組 部門名稱 by data list style
     *
     * @param depSids        部門 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param depSplitStr    部門、使用者中間的分隔文字
     * @param emptyMessage   當傳入人員為空時, 要顯示的訊息
     * @param isWithOrgSid   是否顯示 user sid
     * @return result content
     */
    public static String prepareDepNameWithDepByDataListStyle(
            Collection<Integer> depSids,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String depSplitStr,
            String emptyMessage,
            boolean isWithOrgSid) {

        if (depSids == null) {
            depSids = Lists.newArrayList();
        }

        // 排序
        depSids = WkOrgUtils.sortByOrgTree(depSids);

        // ====================================
        // 兜組所有傳入 dep 的資訊
        // ====================================
        List<String> depInfos = Lists.newArrayList();
        for (Integer depSid : depSids) {
            // 麵包屑樣式
            String depInfo = WkOrgUtils.prepareBreadcrumbsByDepName(depSid, topLevel, isForceUpLevel, depSplitStr);
            if (isWithOrgSid) {
                depInfo += " (" + depSid + ")";
            }
            depInfos.add(depInfo);
        }

        // ====================================
        // 組 data list 樣式
        // ====================================
        return WkCommonContentUtils.makeupByDataListStyle(depInfos, emptyMessage);
    }

}
