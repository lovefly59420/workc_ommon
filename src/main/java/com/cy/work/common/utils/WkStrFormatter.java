/**
 * 
 */
package com.cy.work.common.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.User;
import com.cy.employee.rest.client.simple.SimpleEmployeeClient;
import com.cy.employee.vo.SimpleEmployee;
import com.cy.work.common.enums.WkStrFormatterType;
import com.google.common.collect.Lists;

import lombok.extern.slf4j.Slf4j;

/**
 * 將傳入字串中的參數，換為對應字串
 * 
 * @author allen1214_wu
 */
@Slf4j
@Component
public class WkStrFormatter implements Serializable, InitializingBean {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8695640245918864816L;

    // ========================================================================
    // implement InitializingBean
    // ========================================================================
    private static WkStrFormatter instance;

    /**
     * get WkStrFormatter Instance
     *
     * @return WkCommonCache
     */
    public static WkStrFormatter getInstance() { return instance; }

    /**
     * fix for fireBugs warning
     *
     * @param instance
     */
    private static void setInstance(WkStrFormatter instance) { WkStrFormatter.instance = instance; }

    @Override
    public void afterPropertiesSet() throws Exception {
        setInstance(this);
    }

    // ========================================================================
    // 服務
    // ========================================================================
    @Autowired
    private transient SimpleEmployeeClient simpleEmployeeClient;

    // ========================================================================
    // 方法
    // ========================================================================
    public String process(
            final String targetStr,
            final User user) {
        return this.process(targetStr, user, true);
    }

    public String processWithoutErrorLog(
            final String targetStr,
            final User user) {
        return this.process(targetStr, user, false);
    }

    private String process(
            final String targetStr,
            final User user,
            final boolean isShowErrorLog) {

        if (WkStringUtils.isEmpty(targetStr)) {
            return targetStr;
        }

        if (!targetStr.contains("#")) {
            return targetStr;
        }

        // 結果
        StringBuilder resultStrs = new StringBuilder();
        // 收集變數字串
        StringBuilder paramStrs = new StringBuilder();
        boolean findFirstKeyPoint = false;

        List<String> warnMessageLogs = Lists.newArrayList();

        for (int i = 0; i < targetStr.length(); i++) {
            char currChar = targetStr.charAt(i);
            if ("#".equals(currChar + "")) {
                if (findFirstKeyPoint) {
                    // 為變數結尾 (符合#xxx#, 故currChar也加入)
                    paramStrs.append(currChar);

                    String tempStr = paramStrs.toString();
                    // 處理變數轉換結果
                    String trnsResult = trnsParamKeyToContent(
                            paramStrs.toString(), user, warnMessageLogs);
                    // 重置
                    paramStrs.setLength(0);

                    // 比對轉換成功
                    if (!WkCommonUtils.compareByStr(tempStr, trnsResult)) {
                        // 重置辨識旗標
                        findFirstKeyPoint = false;
                    } else {
                        // 辨識不出變數時視此次的結尾井號為變數頭 (重新開始計算)
                        paramStrs.append(currChar);
                        findFirstKeyPoint = true;
                        trnsResult = trnsResult.substring(0, trnsResult.length() - 1);
                    }

                    // 加入結果字串
                    resultStrs.append(trnsResult);

                    continue;
                } else {
                    // 為變數頭
                    // 初始化變數容器 (由下一個字開始記錄變數)
                    paramStrs.setLength(0);
                    paramStrs.append(currChar);
                    findFirstKeyPoint = true;
                    continue;
                }
            } else {
                if (findFirstKeyPoint) {
                    paramStrs.append(currChar);
                } else {
                    resultStrs.append(currChar);
                }
            }
        }

        // 有 # 但找不到結尾
        if (paramStrs.length() > 0) {
            resultStrs.append(paramStrs);
        }

        // 有警告訊息時印出
        if (isShowErrorLog
                && WkStringUtils.notEmpty(warnMessageLogs)) {
            log.warn("\r\n" + warnMessageLogs.stream().collect(Collectors.joining("\r\n")));
        }

        return resultStrs.toString();

    }

    private String trnsParamKeyToContent(
            final String paramKey,
            final User user,
            final List<String> warnMessageLogs) {

        String currParamKey = WkStringUtils.safeTrim(paramKey);
        // 去除web編輯器被加上的 HTML 內容
        currParamKey = WkJsoupUtils.getInstance().clearCssTag(currParamKey);

        // ====================================
        // 轉為 WkStrFormatterType
        // ====================================
        WkStrFormatterType strFormatterType = WkStrFormatterType.trnsFromTypeKeyword(currParamKey);
        if (strFormatterType == null) {
            warnMessageLogs.add("fail parse:【" + currParamKey + "】");
            return paramKey;
        }

        // ====================================
        // 參數轉換實做
        // ====================================
        try {
            switch (strFormatterType) {
            // -----------------
            // 使用者代號
            // -----------------
            case USER_ID:
                if (user == null) {
                    warnMessageLogs.add("傳入User 為空!");
                    return paramKey;
                }
                return user.getId();
            // -----------------
            // 使用者暱稱
            // -----------------
            case USER_NAME:
                if (user == null) {
                    warnMessageLogs.add("傳入User 為空!");
                    return paramKey;
                }

                return WkUserUtils.safetyGetName(user);
            // -----------------
            // 使用者部門
            // -----------------
            case DEPT_0:
            case DEPT_1:
            case DEPT_2:
                if (user == null) {
                    warnMessageLogs.add("傳入User 為空!");
                    return paramKey;
                }
                if (user.getPrimaryOrg() == null) {
                    warnMessageLogs.add("傳入User 的主要部門為空!userSid:[" + user.getSid() + "]");
                    return paramKey;
                }

                OrgLevel orgLevel = OrgLevel.THE_PANEL;
                switch (strFormatterType) {
                case DEPT_0: // 最高串到組
                    orgLevel = OrgLevel.THE_PANEL;
                    break;
                case DEPT_1: // 最高串到部
                    orgLevel = OrgLevel.MINISTERIAL;
                    break;
                case DEPT_2: // 最高串到處
                    orgLevel = OrgLevel.DIVISION_LEVEL;
                    break;
                default:
                    log.error("開發時期錯誤!未實做的類別【{}】", strFormatterType);
                }

                return WkOrgUtils.prepareBreadcrumbsByDepName(
                        user.getPrimaryOrg().getSid(),
                        orgLevel,
                        false,
                        "-");

            // -----------------
            // 系統日
            // -----------------
            case SYS_DATE_1:
                return WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD);
            case SYS_DATE_2:
                return WkDateUtils.formatDate(new Date(), WkDateUtils.YYYY_MM_DD2);

            // -----------------
            // 系統時間
            // -----------------
            case SYS_TIME_1:
                return WkDateUtils.formatDate(new Date(), WkDateUtils.HH24_mm_ss);

            // -----------------
            // 使用者分機
            // -----------------
            case USER_EXT:
                if (user == null) {
                    warnMessageLogs.add("傳入User 為空!");
                    return paramKey;
                }

                // 取得 Employee 資料
                Optional<SimpleEmployee> simpleEmployee = simpleEmployeeClient.findByMappedUserSid(
                        user.getSid());

                // 檢核無 Employee
                if (!simpleEmployee.isPresent()) {
                    return "";
                }

                // 無分機資料
                if (WkStringUtils.isEmpty(simpleEmployee.get().getExtension())) {
                    return "";
                }

                return "#" + simpleEmployee.get().getExtension();

            default:
                log.error("未實做的參數方法【{}】", paramKey);
                return paramKey;
            }

        } catch (Exception e) {
            log.error("處理解析字串失敗:【" + e.getMessage() + "】", e);
            return paramKey;
        }
    }

    /**
     * 回傳參數數轉換使用說明文字
     * 
     * @return 回傳參數數轉換使用說明文字
     */
    public String getUserManual() {

        String result = ""
                + "<table>"
                + "<tr><td colspan='2' style='text-align:center'>『參數轉換』使用說明</td></tr>";

        for (WkStrFormatterType strFormatterType : WkStrFormatterType.values()) {
            result += "<tr>";
            result += "<td>" + strFormatterType.getTypeKeyword() + "</td>";
            result += "<td>" + strFormatterType.getDescr() + "</td>";
            result += "</tr>";
        }
        result += "</table>";

        return result;
    }

}
