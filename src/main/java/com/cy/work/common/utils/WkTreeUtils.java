package com.cy.work.common.utils;

import com.cy.work.common.vo.WkItem;
import com.cy.work.common.vo.WkNode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 雜項工具
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkTreeUtils {

    /**
     * 移除項目中不需要的項目
     *
     * @return WkItem list
     */
    public static List<WkItem> cleanItemsByUnnecessary(
            List<WkItem> allItem,
            List<String> trueItemSids) {

        // 建立乾淨的樹
        WkNode rootNode = bulidCleanTree(allItem, trueItemSids);

        // 收集所有 item
        return collectAllItemByTreeNode(rootNode);

    }

    /**
     * 收集樹節點下所有資料項目
     *
     * @param parentNode parent Node
     * @return WkItem list
     */
    public static List<WkItem> collectAllItemByTreeNode(WkNode parentNode) {

        List<WkItem> results = Lists.newArrayList();

        if (parentNode == null) {
            return results;
        }

        // 加入傳入層
        if (parentNode.getNodeData() != null && (parentNode.getNodeData() instanceof WkItem)) {
            WkItem item = (WkItem) parentNode.getNodeData();
            // 排除虛擬 root
            if (WkStringUtils.notEmpty(item.getSid())) {
                results.add((WkItem) parentNode.getNodeData());
            }
        }

        // 遞迴加入子節點
        if (WkStringUtils.isEmpty(parentNode.getChildNodes())) {
            return results;
        }

        for (WkNode childNode : parentNode.getChildNodes()) {
            // 加入子節點的子節點
            List<WkItem> subChildItems = collectAllItemByTreeNode(childNode);
            if (WkStringUtils.notEmpty(subChildItems)) {
                results.addAll(subChildItems);
            }
        }
        return results;
    }

    /**
     * 建立簡略樹 (僅回傳路徑節點+真實節點)
     *
     * @param allItem      全部項目
     * @param trueItemSids 真實項目 SID
     * @return WkNode
     */
    public static WkNode bulidCleanTree(
            List<WkItem> allItem,
            Collection<String> trueItemSids) {

        // ====================================
        // 建立根節點
        // ====================================
        WkNode rootNode = new WkNode(new WkItem("", "", false));

        // ====================================
        // 將 item 資料轉回 node
        // ====================================
        List<WkNode> allNode = Lists.newArrayList();
        Map<String, WkNode> nodesMapBySid = Maps.newHashMap();
        for (WkItem wkItem : allItem) {
            if (nodesMapBySid.containsKey(wkItem.getSid())) {
                log.error("傳入資料有重複的 sid!");
                log.error("資料A:\r\n" + WkJsonUtils.getInstance().toPettyJson(nodesMapBySid.get(wkItem.getSid())));
                log.error("資料B:\r\n" + WkJsonUtils.getInstance().toPettyJson(wkItem));
                continue;
            }
            WkNode node = new WkNode(wkItem);
            allNode.add(node);
            nodesMapBySid.put(wkItem.getSid(), node);
        }
        // ====================================
        // 建立父子關係
        // ====================================
        for (WkNode wkNode : allNode) {
            WkItem wkItem = (WkItem) wkNode.getNodeData();
            WkItem parentItem = wkItem.getParent();
            if (parentItem == null) {
                // 無父類別時, 加到根節點
                rootNode.addChild(wkNode);
            } else if (!nodesMapBySid.containsKey(parentItem.getSid())) {
                // 有設定父類別, 但是傳入資料找不到時, 加到根節點
                rootNode.addChild(wkNode);
            } else {
                // 將此節點加到設定的父節點
                nodesMapBySid.get(parentItem.getSid()).addChild(wkNode);
            }
        }

        // ====================================
        // 遞迴處理所有節點
        // ====================================
        List<WkNode> childNodes = rootNode.getChildNodes();
        if (WkStringUtils.isEmpty(childNodes)) {
            return rootNode;
        }

        // 原始排序
        for (int i = 0; i < childNodes.size(); i++) {
            WkItem currWkItem = (WkItem) childNodes.get(i).getNodeData();
            currWkItem.getOtherInfo().put("SRC_SEQ", i + "");
        }
        // 先排item序號
        Comparator<WkNode> comparator = Comparator.comparing(currWkNode -> ((WkItem) currWkNode.getNodeData()).getItemSeq());
        // 再排原始排序
        comparator = comparator.thenComparing(
                Comparator.comparing(currWkNode -> ((WkItem) currWkNode.getNodeData()).getOtherInfo().get("SRC_SEQ")));

        childNodes = childNodes.stream()
                .sorted(Comparator.comparing(currWkNode -> ((WkItem) currWkNode.getNodeData()).getItemSeq()))
                .collect(Collectors.toList());

        // 遞迴處理所有子node
        for (WkNode wkNode : Lists.newArrayList(childNodes)) {
            bulidCleanTree_recursion(wkNode, trueItemSids);
        }
        return rootNode;
    }

    /**
     * bulidCleanTree 遞迴使用
     * 
     * @param node         node
     * @param trueItemSids true Item sid list
     * @return true / false
     */
    private static boolean bulidCleanTree_recursion(
            WkNode node,
            Collection<String> trueItemSids) {

        // ====================================
        // 防呆
        // ====================================
        if (node == null || node.getNodeData() == null || !(node.getNodeData() instanceof WkItem)) {
            return false;
        }

        // ====================================
        // 取得項目資料
        // ====================================
        // 取得項目資料
        WkItem item = (WkItem) node.getNodeData();

        // ====================================
        // 處理子節點
        // ====================================
        // 子節點包含『已選擇節點』
        boolean isHasSelectedChildNode = false;
        if (node.getChildNodes() != null) {
            List<WkNode> childNodes = Lists.newArrayList(node.getChildNodes());
            // 逐筆下鑽子節點
            for (WkNode childNode : childNodes) {
                // 遞迴處理子節點
                if (bulidCleanTree_recursion(childNode, trueItemSids)) {
                    // 標記子節點包含『已選擇節點』
                    isHasSelectedChildNode = true;
                }
            }
        }

        // ====================================
        // 判斷節點屬性
        // ====================================
        // 此節點是『真實節點』
        boolean isSelectedNode = trueItemSids.contains(item.getSid());

        // 為『真實節點』時直接回傳
        if (isSelectedNode) {
            return true;
        }

        // 不是『真實節點』, 但是下層有真實節點
        if (!isSelectedNode && isHasSelectedChildNode) {
            // 此節點賦予路徑屬性 (不是真的可用, 只是路過)
            node.setPathNode(true);
            item.setDisable(true);
            return true;
        }

        // 不是『真實節點』, 也沒有子節點為『真實節點』 (直系以下都沒有)
        // 將此節點移除 (無須顯示)
        node.setParent(null);
        return false;
    }

    // ========================================================================
    // 樹狀清單顯示
    // ========================================================================

    /**
     * 以階層樹方式，兜組傳入項目名稱
     *
     * @param allItems         所有項目 (包含已選擇未選擇)
     * @param selectedItemSids 已選擇項目
     * @param maxLine          最大顯示行數 (避免階層樹往下排過長,超過時會往右邊排)
     * @return HTML content
     */
    public static String prepareSelectedItemNameByTreeStyle(
            List<WkItem> allItems,
            Collection<String> selectedItemSids,
            int maxLine) {

        if (WkStringUtils.isEmpty(selectedItemSids)) {
            return "";
        }
        // ====================================
        // 防呆
        // ====================================
        if (allItems == null) {
            allItems = Lists.newArrayList();
        }
        if (maxLine < 1) {
            maxLine = 0;
        }

        // ====================================
        // 建立項目節點樹
        // ====================================
        WkNode rootNode = bulidCleanTree(allItems, selectedItemSids);
        
        // ====================================
        // 兜組單位顯示文字
        // ====================================
        List<String> contentLines = prepareSelectedItemNameByTreeStyle_recursion(-1, rootNode);

        // ====================================
        // 必免清單過長, 依據最大行數分組
        // ====================================
        //
        List<List<String>> contentParts = Lists.newArrayList();
        // 初始值訂為超過最大值, 讓第一次進入迴圈就產生一個新的 part
        int number = maxLine + 1;
        // 目前 part 在容器中的 index
        int currPartIndex = 0;
        for (String contentLine : contentLines) {
            // 超過行數時, 放到下一個 part
            if (number > maxLine) {
                contentParts.add(Lists.newArrayList());
                // 更新目前正在使用的 part index
                currPartIndex = contentParts.size() - 1;
                number = 1;
            }
            contentParts.get(currPartIndex)
                    .add(contentLine);
            number++;
        }

        // ====================================
        // 兜組全部內容
        // ====================================
        String hetmContent = "";
        // 外框線
        hetmContent += "<table style='border:2px #cccccc solid;'><tr>";
        if (WkStringUtils.notEmpty(contentParts)) {
            for (List<String> eachContentLinePart : contentParts) {
                // 強制不折行、文字置頂
                hetmContent += "<td style='white-space:nowrap;vertical-align:text-top;'>";
                hetmContent += String.join("<br/>", eachContentLinePart);
                hetmContent += "</td>";
            }
        }
        hetmContent += "</tr></table>";

        return hetmContent;
    }

    /**
     * prepareDepsNameByTreeStyle 遞迴使用
     * 
     * @param level 階層
     * @return string
     */
    private static String prepareDepsNameByTreeStyle_getPreContent(int level) {
        if (level == 0) {
            return "";
        }
        String preContent = "";
        for (int i = 0; i < level; i++) {
            preContent += "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        preContent += "|__";
        return preContent;
    }

    /**
     * prepareSelectedItemNameByTreeStyle 遞迴使用
     * 
     * @param level 階層
     * @param node  node
     * @return string list
     */
    private static List<String> prepareSelectedItemNameByTreeStyle_recursion(
            int level,
            WkNode node) {

        List<String> resultContents = Lists.newArrayList();

        // ====================================
        // 防呆
        // ====================================
        if (node == null || node.getNodeData() == null || !(node.getNodeData() instanceof WkItem)) {
            return resultContents;
        }

        // ====================================
        // 項目名稱
        // ====================================
        WkItem item = (WkItem) node.getNodeData();

        String itemName = item.getShowTreeName();
        if (!item.isActive()) {
            itemName = "(停用)" + itemName;
        }

        // ====================================
        // 兜組完整 CSS 內容
        // ====================================
        String nodeContent = "";
        if (level > -1) {
            nodeContent += prepareDepsNameByTreeStyle_getPreContent(level);
            if (node.isPathNode()) {
                // 非真實節點(階層路徑),不醒目處理
                nodeContent += "<span style='color:gray;opacity:0.5;'>" + itemName + "</span>";
            } else {
                // 真實節點, 醒目顯示
                String style = "font-weight:bold;";
                if (!item.isActive()) {
                    style += "text-decoration:line-through;color:red;opacity:0.7;";
                }
                nodeContent += "<i class=\"fa fa-star\" aria-hidden=\"true\"></i>";
                nodeContent += "<span style='" + style + "'>" + itemName + "</span>";
            }
        }

        if (node.getParentNode() != null) {
            resultContents.add(nodeContent);
        }

        // ====================================
        // 遞迴處理子節點
        // ====================================
        if (WkStringUtils.isEmpty(node.getChildNodes())) {
            return resultContents;
        }

        for (WkNode childNode : node.getChildNodes()) {
            resultContents.addAll(prepareSelectedItemNameByTreeStyle_recursion(level + 1, childNode));
        }
        return resultContents;
    }
}
