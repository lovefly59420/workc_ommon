package com.cy.work.common.utils;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author kasim
 */
@Slf4j
@Component
public class WkFileUtils implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1381120683268664912L;

    private static WkFileUtils instance;

    public static WkFileUtils getInstance() {

        if (instance == null) {
            instance = new WkFileUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        // WkFileUtils.instance = this;
    }

    /**
     * 建立路徑並取得檔案在伺服器端的實際路徑
     *
     * @param parent
     * @param child
     * @param fileName
     * @return File
     */
    public File createDirectoryIfNotExistAndGetServerSideFile(
            String parent,
            String child,
            String fileName) {

        this.createDirectoryIfNotExist(parent, child);
        return this.getServerSideFile(parent, child, fileName);
    }

    /**
     * 取得檔案在伺服器端的實際路徑
     *
     * @param parent
     * @param child
     * @param fileName
     * @return File
     */
    public File getServerSideFile(
            String parent,
            String child,
            String fileName) {

        File serverSideDirectory = new File(parent, child);
        return new File(serverSideDirectory, fileName);
    }

    /**
     * 用來建立各單據種類的子目錄
     *
     * @param parent
     * @param child
     */
    private void createDirectoryIfNotExist(
            String parent,
            String child) {

        File directory = new File(parent, child);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    /**
     * 寫入文字檔
     *
     * @param parent
     * @param child
     * @param fileName
     * @param text
     */
    public void writeFileByText(
            String parent,
            String child,
            String fileName,
            String text) {

        try {
            File file = this.createDirectoryIfNotExistAndGetServerSideFile(parent, child, fileName);
            if (text == null) {
                text = "";
            }
            Files.write(text.getBytes(Charsets.UTF_8.toString()), file);
        } catch (IOException ex) {
            log.error("建立文字檔發生錯誤 檔案資料夾：" + child + "， 檔案名稱：" + fileName, ex);
        }
    }

    /**
     * 讀取文字檔 (by UTF-8)
     *
     * @param parent
     * @param child
     * @param fileName
     * @param defaultText 若讀取錯誤時，return 此傳入字串
     * @return file context
     */
    public String readFileByText(
            String parent,
            String child,
            String fileName,
            String defaultText) {

        try {
            File file = this.getServerSideFile(parent, child, fileName);
            return Files.toString(file, Charsets.UTF_8);
        } catch (IOException ex) {
            log.warn("取回文字檔發生錯誤 檔案資料夾：" + child + "， 檔案名稱：" + fileName);
            return defaultText;
        }
    }

    /**
     * 讀取文字檔並輸出格式化HTML字串
     *
     * @param parent
     * @param child
     * @param fileName
     * @param defaultText 若讀取錯誤時，return 此傳入字串
     * @return file context
     */
    public String readFileToPrettyHTMLByText(
            String parent,
            String child,
            String fileName,
            String defaultText) {

        try {
            File file = this.getServerSideFile(parent, child, fileName);
            Document doc = Jsoup.parse(Files.toString(file, Charsets.UTF_8));
            return doc.body()
                    .html();
        } catch (IOException ex) {
            log.warn("取回文字檔發生錯誤 檔案資料夾：" + child + "， 檔案名稱：" + fileName);
            return defaultText;
        }
    }

}
