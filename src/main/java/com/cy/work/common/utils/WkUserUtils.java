package com.cy.work.common.utils;

import com.cy.commons.enums.Activation;
import com.cy.commons.enums.OrgLevel;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;
import com.cy.commons.vo.simple.SimpleUser;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.cache.WkUserCache;
import com.cy.work.common.cache.WkUserWithRolesCache;
import com.cy.work.common.enums.InfomationLevel;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 工具類：user
 *
 * @author allen1214_wu
 */
@Slf4j
public class WkUserUtils {

    /**
     * 比對傳入使用者的隸屬關係
     *
     * @param srcUserSid    來源使用者 SID
     * @param targrtUserSid 目標使用者 SID
     * @param bigBossSids   公司老闆 SIDs
     * @return 0：無隸屬關係 <br/>
     *             -1：srcUser 為 targrtUser 直屬單位『成員』srcUser < targrtUser <br/>
     *             1：srcUser 為 targrtUser 直屬單位『主管』srcUser > targrtUser
     */
    public static int compareUserRelation(
            Integer srcUserSid,
            Integer targrtUserSid,
            Set<Integer> bigBossSids) {

        // ====================================
        // 大老闆同為平級
        // ====================================
        if (bigBossSids.contains(srcUserSid) && bigBossSids.contains(targrtUserSid)) {
            return 0;
        } else if (bigBossSids.contains(srcUserSid)) {
            return 1;
        } else if (bigBossSids.contains(targrtUserSid)) {
            return -1;
        }

        // ====================================
        // 依據傳入 sid , 取得使用者資料
        // ====================================
        User srcUser = WkUserCache.getInstance().findBySid(srcUserSid);
        if (srcUser == null || srcUser.getPrimaryOrg() == null) {
            return 0;
        }
        Org srcUserDep = WkOrgCache.getInstance().findBySid(srcUser.getPrimaryOrg().getSid());
        if (srcUserDep == null) {
            return 0;
        }

        User targrtUser = WkUserCache.getInstance().findBySid(targrtUserSid);
        if (targrtUser == null || targrtUser.getPrimaryOrg() == null) {
            return 0;
        }
        Org targrtUserDep = WkOrgCache.getInstance().findBySid(targrtUser.getPrimaryOrg().getSid());
        if (targrtUserDep == null) {
            return 0;
        }

        // ====================================
        // 單位相等 (同單位)
        // ====================================
        if (srcUserDep.getSid().equals(targrtUserDep.getSid())) {
            // 此次處理的部門 (srcUserDep 同 targrtUserDep)
            Org currDep = srcUserDep;

            // 使用者層級 (0:成員,1:副主管,2:主管)
            Integer srcUserLevel = 0;
            Integer targetUserLevel = 0;

            // 檢查副主管 (需早於主管檢查, 若同時為主管，以主管優先)
            if (WkStringUtils.notEmpty(currDep.getDeputyManagers())) {
                for (SimpleUser deputyManager : currDep.getDeputyManagers()) {
                    // 比對 srcUser
                    if (WkCommonUtils.compareByStr(srcUserSid, deputyManager.getSid())) {
                        srcUserLevel = 1;
                    }

                    // 比對 targetUser
                    if (WkCommonUtils.compareByStr(targrtUserSid, deputyManager.getSid())) {
                        targetUserLevel = 1;
                    }
                }
            }

            // 檢查是否為主管
            // 比對 srcUser
            if (isInSimpleUserList(currDep.getManagers(), srcUserSid)) {
                srcUserLevel = 2;
            }
            // 比對 targetUser
            if (isInSimpleUserList(currDep.getManagers(), targrtUserSid)) {
                targetUserLevel = 2;
            }

            if (srcUserLevel == targetUserLevel) {
                // 平級
                return 0;
            } else if (srcUserLevel > targetUserLevel) {
                // 來源使用者為上層
                return 1;
            } else {
                // 標的使用者為上層 (targetUserLevel > srcUserLevel)
                return -1;
            }

        }

        // ====================================
        // src user > target user
        // ====================================
        if (isUsersManager(targrtUserSid, srcUserSid)) {
            return 1;
        }

        // ====================================
        // target user > src user
        // ====================================
        if (isUsersManager(srcUserSid, targrtUserSid)) {
            return -1;
        }

        // ====================================
        // 無隸屬關係
        // ====================================
        return 0;

    }

    /**
     * 以使用者暱稱，做模糊查詢
     *
     * @param userNameKeyWord 關鍵字
     * @param compID          查詢的公司別
     * @return user sid list
     */
    public static List<Integer> findByNameLike(
            String userNameKeyWord,
            String compID) {

        // 查詢所有 user
        List<User> users = WkUserCache.getInstance()
                .findAllUserByCompID(compID);

        // 關鍵字忽略大小寫
        final String finalUserNameKeyWord = WkStringUtils.safeTrim(userNameKeyWord)
                .toUpperCase();

        // 依據 user 暱稱過濾
        return users.stream()
                .filter(user -> {
                    String userName = WkStringUtils.safeTrim(user.getName())
                            .toUpperCase();
                    return userName.contains(finalUserNameKeyWord);
                })
                .map(User::getSid)
                .collect(Collectors.toList());
    }

    /**
     * 查詢部門以下所有 user (含以下部門)
     *
     * @param depSid org sid
     * @return user list
     */
    public static List<User> findAllUserByDepSid(Integer depSid) {

        WkOrgCache wkOrgCache = WkOrgCache.getInstance();

        // 查詢傳入單位資料
        Org currDep = wkOrgCache.findBySid(depSid);
        if (currDep == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "WkUserUtils.findAllUserByDepSid: 傳入查詢的 depSid ,找不到資料! :[" + depSid + "]", 4);
            return Lists.newArrayList();
        }

        // 本次選取單位
        List<Integer> findDepSids = Lists.newArrayList(currDep.getSid());

        // 加入傳入單位所有的子單位 SID
        findDepSids.addAll(
                WkOrgCache.getInstance()
                        .findAllChild(depSid)
                        .stream()
                        .map(Org::getSid)
                        .collect(Collectors.toList()));

        return WkUserCache.getInstance()
                .findUserWithManagerByOrgSids(findDepSids, null);

    }

    /**
     * 查詢部門以下所有 user (含以下部門)
     * 
     * @param orgId 單位/公司ID
     * @return user list
     */
    public static List<User> findAllUserByOrgId(String orgId) {

        Org org = WkOrgCache.getInstance().findById(orgId);
        if (orgId == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "WkUserUtils.findAllUserByOrgId: 傳入查詢的 orgId ,找不到資料! :[" + orgId + "]", 4);
            return Lists.newArrayList();
        }

        return findAllUserByDepSid(org.getSid());

    }

    /**
     * 以UserSid 查詢使用者名稱
     *
     * @param userSid user sid
     * @return username
     */
    public static String findNameBySid(Integer userSid) {
        if (userSid == null) {
            return "";
        }

        // 查詢 user 資料
        User user = WkUserCache.getInstance()
                .findBySid(userSid);
        if (user == null) {
            return "";
        }
        return user.getName();
    }

    /**
     * 兜組多使用者名稱字串
     *
     * @param userSids
     * @param delimiter 分隔號
     * @return users name
     */
    public static String findNameBySid(
            Collection<Integer> userSids,
            String delimiter) {
        return findNameBySid(userSids, delimiter, false);
    }

    /**
     * 兜組多使用者名稱字串
     *
     * @param userSids
     * @param delimiter     分隔號
     * @param isWithUserSid 回傳結果是否加上 userSid
     * @return users name
     */
    public static String findNameBySid(
            Collection<Integer> userSids,
            String delimiter,
            boolean isWithUserSid) {

        if (WkStringUtils.isEmpty(userSids)) {
            return "";
        }

        List<String> userNames = Lists.newArrayList();
        for (Integer userSid : userSids) {
            // 查詢 user 資料
            User user = WkUserCache.getInstance()
                    .findBySid(userSid);
            if (user != null) {
                String userName = user.getName();
                if (isWithUserSid) {
                    userName = userSid + "-" + userName;
                }
                userNames.add(userName);
            }
        }

        return String.join(delimiter, userNames);
    }

    /**
     * 以 User ID 查詢使用者名稱
     *
     * @param userID user ID
     * @return username
     */
    public static String findNameByID(String userID) {
        if (WkStringUtils.isEmpty(userID)) {
            return "";
        }

        // 查詢 user 資料
        User user = WkUserCache.getInstance()
                .findById(WkStringUtils.safeTrim(userID));
        if (user == null) {
            return "";
        }
        return user.getName();
    }

    /**
     * 兜組多使用者名稱字串
     *
     * @param userSidStrs
     * @param delimiter   分隔號
     * @return users name
     */
    public static String findNameBySidStrs(
            Collection<String> userSidStrs,
            String delimiter) {

        // ====================================
        // String 轉 Integer
        // ====================================
        List<Integer> userSids = Lists.newArrayList();

        for (String userSid : userSidStrs) {
            userSid = WkStringUtils.safeTrim(userSid);
            if (WkStringUtils.isEmpty(userSid)) {
                continue;
            }
            if (!WkStringUtils.isNumber(userSid)) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "傳入的 org sid 不是數字:[" + userSid + "]", 10);
                continue;
            }
            userSids.add(Integer.parseInt(userSid));
        }

        // ====================================
        // findNameBySid
        // ====================================
        return findNameBySid(userSids, delimiter, false);
    }

    /**
     * 取得 user 的主要部門 SID
     * 
     * @param userSid user sid
     * @return user 的主要部門 SID
     */
    public static Integer findUserPrimaryOrgSid(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null) {
            return null;
        }
        return user.getPrimaryOrg().getSid();
    }

    /**
     * 取得USER 的公司
     * 
     * @param userSid user sid
     * @return USER 的公司 Org
     */
    public static Org findUserCompany(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null || user.getPrimaryOrg() == null) {
            return null;
        }

        Org org = WkOrgCache.getInstance().findBySid(user.getPrimaryOrg().getSid());

        if (org == null || org.getCompany() == null) {
            return null;
        }

        return WkOrgCache.getInstance().findBySid(org.getCompany().getSid());
    }

    /**
     * 以 User ID 反查 SID
     * 
     * @param userId user ID
     * @return user sid
     */
    public static Integer findSidById(String userId) {
        User user = WkUserCache.getInstance().findById(userId);
        if (user == null) {
            return null;
        }
        return user.getSid();
    }

    /**
     * 以 User SID 查詢 ID
     * 
     * @param userSid user sid
     * @return user ID
     */
    public static String findUserIDBySid(Integer userSid) {
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return null;
        }
        return user.getId();
    }

    /**
     * 是否為啟用
     *
     * @param userSid
     * @return y/n
     */
    public static boolean isActive(Integer userSid) {
        User user = WkUserCache.getInstance()
                .findBySid(userSid);
        return isActive(user);
    }

    /**
     * 是否為啟用
     *
     * @param user User
     * @return y/n
     */
    public static boolean isActive(User user) {
        if (user == null) {
            return false;
        }
        return Activation.ACTIVE.equals(user.getStatus());
    }

    /**
     * 使用者資料是否存在
     *
     * @param userSid
     * @return true/false
     */
    public static boolean isExsit(Integer userSid) {
        return WkUserCache.getInstance()
                .isExsit(userSid);
    }

    /**
     * 檢查使用者是否擁有某公司別的角色權限
     *
     * @param userSid         user sid
     * @param loginCompanyID  登入公司 ID
     * @param targetRoleNames 比對角色名稱
     * @return true/false
     */
    public static boolean isUserHasRole(
            Integer userSid,
            String loginCompanyID,
            List<String> targetRoleNames) {

        if (WkStringUtils.isEmpty(targetRoleNames)) {
            return false;
        }

        // 取得所有使用者擁有的角色名稱
        List<String> userRoleNames = WkUserWithRolesCache.getInstance()
                .findRoleNamesByUserAndLoginCompID(
                        userSid,
                        loginCompanyID);

        // 若未有任一角色, 直接 return false
        if (WkStringUtils.isEmpty(userRoleNames)) {
            return false;
        }

        // 逐筆比較
        for (String targetRoleName : targetRoleNames) {

            // 處理原本有些角色判斷使用 security 時. 角色名稱後方會被加上 『:*』
            if ((targetRoleName + "").endsWith(":*")) {
                targetRoleName = targetRoleName.substring(0, targetRoleName.length() - 2);
            }

            // 使用者角色與目標角色名稱
            if (userRoleNames.contains(targetRoleName)) {
                return true;
            }
        }
        return false;

    }

    /**
     * 以角色名稱比對使用者登入的公司別是否有特定角色
     *
     * @param userSid        使用者 sid
     * @param loginCompanyID 登入公司
     * @param roleName       比對角色名稱
     * @return true/false
     */
    public static boolean isUserHasRole(
            Integer userSid,
            String loginCompanyID,
            String roleName) {

        // 處理原本有些角色判斷使用 security 時. 角色名稱後方會被加上 『:*』
        if ((roleName + "").endsWith(":*")) {
            roleName = roleName.substring(0, roleName.length() - 2);
        }

        List<String> userRoleNames = WkUserWithRolesCache.getInstance()
                .findRoleNamesByUserAndLoginCompID(
                        userSid,
                        loginCompanyID);

        // 若未有任一角色, 直接 return false
        if (WkStringUtils.isEmpty(userRoleNames)) {
            // log.debug("比對角色:{} -> false", roleName);
            return false;
        }

        // log.debug("比對角色:{} -> {}", roleName, userRoleNames.contains(roleName));
        // 比對傳入角色名稱
        return userRoleNames.contains(roleName);
    }

    /**
     * targrtUser 是否為 srcUser 的直系上層單位主管 (不包含直系上層部門成員[沒有掛主管者])
     * 需排除同步門狀況 (會有主副主管比對問題)
     *
     * @param srcUserSid
     * @param targrtUserSid
     * @return true/false
     */
    private static boolean isUsersManager(
            Integer srcUserSid,
            Integer targrtUserSid) {

        // 取得傳入 srcUser 資料
        User srcUser = WkUserCache.getInstance().findBySid(srcUserSid);
        if (srcUser == null || srcUser.getPrimaryOrg() == null) {
            return false;
        }
        Integer srcUserDepSid = srcUser.getPrimaryOrg().getSid();

        // ====================================
        // 比對 targrtUser 管理的部門
        // ====================================
        // 取得 targrtUser 管理的部門
        Set<Integer> targrtUserManagerDepSids = WkOrgCache.getInstance().findManagerOrgSids(targrtUserSid);

        // 沒有管理的部門, 不可能是 srcUser 的主管
        if (WkStringUtils.isEmpty(targrtUserManagerDepSids)) {
            return false;
        }
        // srcUser 為被 targrtUser 管理部門的成員
        if (targrtUserManagerDepSids.contains(srcUserDepSid)) {
            return true;
        }

        // ====================================
        // 比對 targrtUser 管理部門的下層部門
        // ====================================
        Set<Integer> childDepSids = WkOrgCache.getInstance().findAllChildSids(targrtUserManagerDepSids);
        if (childDepSids.contains(srcUserDepSid)) {
            return true;
        }

        return false;
    }

    /**
     * 兜組 部門+使用者暱稱字串 (ex E化發展部-專案管理組-Yifan)
     *
     * @param userSid        使用者 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param splitStr       部門間分隔符號
     * @return result content
     */
    public static String prepareUserNameWithDep(
            Integer userSid,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String splitStr) {

        // 查詢 user 資料
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return "";
        }

        // 防呆
        if (user.getPrimaryOrg() == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "user:[" + user.getSid() + "] 找不到主要單位");
            return user.getName();
        }

        try {

            // 組單位字串
            String depName = WkOrgUtils.prepareBreadcrumbsByDepName(
                    user.getPrimaryOrg().getSid(),
                    topLevel,
                    isForceUpLevel,
                    splitStr);

            // 加上使用者暱稱
            return depName + splitStr + user.getName();

        } catch (Exception e) {
            log.error(e.getMessage());

        }
        return user.getName();
    }

    /**
     * 兜組 部門+使用者暱稱字串 (ex: E化發展部-專案管理組-Yifan)
     *
     * @param userSid
     * @param splitStr
     * @return result content
     */
    public static String prepareUserNameWithDep(
            Integer userSid,
            String splitStr) {

        // 查詢 user 資料
        User user = WkUserCache.getInstance().findBySid(userSid);
        if (user == null) {
            return "";
        }

        // 防呆
        if (user.getPrimaryOrg() == null) {
            WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "user:[" + user.getSid() + "] 找不到主要單位");
            return user.getName();
        }

        return String.format("%s%s%s",
                (user.getPrimaryOrg() != null)?user.getPrimaryOrg().getOrgNameWithParentIfDuplicate():"",
                splitStr,
                user.getName());
    }

    /**
     * 兜組 部門+使用者暱稱字串 (內部核心方法)
     *
     * @param userSids       使用者 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param depSplitStr    部門、使用者中間的分隔文字
     * @param isWithUserSid  名稱後方帶使用者 sid
     * @return Username with dep name
     */
    protected static List<String> prepareUserNameWithDep_core(
            Collection<Integer> userSids,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String depSplitStr,
            boolean isWithUserSid) {

        if (WkStringUtils.isEmpty(userSids)) {
            return Lists.newArrayList();
        }

        // ====================================
        // 準備基本資料
        // ====================================
        List<UserModel> users = Lists.newArrayList();

        for (Integer userSid : userSids) {

            UserModel userModel = new UserModel();
            users.add(userModel);

            // 取得 user 顯示名稱(含部門)
            userModel.name = prepareUserNameWithDep(userSid, topLevel, isForceUpLevel, depSplitStr);
            if (isWithUserSid) {
                userModel.name += " (" + userSid + ")";
            }

            // 取得部門排序
            userModel.orgOrder = Integer.MAX_VALUE;

            User user = WkUserCache.getInstance()
                    .findBySid(userSid);
            if (user == null || user.getPrimaryOrg() == null) {
                continue;
            }

            Org primaryOrg = WkOrgCache.getInstance()
                    .findBySid(user.getPrimaryOrg()
                            .getSid());
            if (primaryOrg == null) {
                continue;
            }

            userModel.orgOrder = WkOrgCache.getInstance()
                    .findOrgOrderSeqBySid(primaryOrg.getSid());
        }

        // ====================================
        // 排序 + get name
        // ====================================
        // 先排部門, 再排 user 暱稱
        Comparator<UserModel> comparator = Comparator.comparing(UserModel::getOrgOrder);
        comparator = comparator.thenComparing(Comparator.comparing(UserModel::getName));

        // 取得暱稱
        List<String> userNames = users.stream()
                .sorted(comparator)
                .map(UserModel::getName)
                .collect(Collectors.toList());

        return userNames;
    }

    /**
     * 兜組 部門+使用者暱稱字串 (ex: E化發展部-專案管理組-Yifan) (可傳入多 user sid)
     *
     * @param userSids       使用者 sid
     * @param topLevel       最上層部門
     * @param isForceUpLevel 開啟非循序性部門, 最上層缺損時, 往上一層
     * @param depSplitStr    部門、使用者中間的分隔文字
     * @param userSplitStr   多使用者間的分隔文字
     * @param isWithUserSid  名稱後方帶使用者 sid
     * @return Username with dep name
     */
    public static String prepareUserNameWithDep(
            Collection<Integer> userSids,
            OrgLevel topLevel,
            boolean isForceUpLevel,
            String depSplitStr,
            String userSplitStr,
            boolean isWithUserSid) {

        // ====================================
        // 兜組所有傳入 user 的資訊
        // ====================================
        List<String> usersInfos = prepareUserNameWithDep_core(
                userSids, topLevel, isForceUpLevel, depSplitStr, isWithUserSid);

        // ====================================
        // 組成單一字串
        // ====================================
        return String.join(
                userSplitStr,
                usersInfos);
    }

    /**
     * 取得使用者名稱 (null exception 排除)
     *
     * @param user User
     * @return user name
     */
    public static String safetyGetName(User user) {
        if (user == null) {
            return "";
        }

        if (WkStringUtils.isEmpty(user.getName())) {
            return user.getSid() + "";
        }

        return user.getName();
    }

    /**
     * 比對 user 是否在清單中
     * 
     * @param sorceUsers    比對來源 user list
     * @param targetUserSid 目標 user sid
     * @return true/false
     */
    public static boolean isInSimpleUserList(List<SimpleUser> sorceUsers, Integer targetUserSid) {
        if (WkStringUtils.isEmpty(sorceUsers)) {
            return false;
        }

        for (SimpleUser simpleUser : sorceUsers) {
            if (WkCommonUtils.compareByStr(targetUserSid, simpleUser.getSid())) {
                return true;
            }
        }

        return false;
    }

    // ============================================================================================================
    //
    // ============================================================================================================

    @Getter
    @NoArgsConstructor
    private static class UserModel {

        public String name;
        public Integer orgOrder;
    }
}
