package com.cy.work.common.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Gson無法解析類別本身nested self，所以用Jackson解析<br/>
 * 被解析類別如果有引用本身類型要加入<br/>
 * 父類加入Anno @JsonBackReference <br/>
 * 子集合加入Anno @JsonManagedReference <br/>
 * 範例可看com.cy.tech.request.logic.vo.BasicDataCategoryTo.java
 *
 * @author shaun
 */
@Component
public class WkJsonUtils implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8391989321160286576L;
    
    // ========================================================================
    // InitializingBean
    // ========================================================================
    private static WkJsonUtils instance;

    public static WkJsonUtils getInstance() {

        if (instance == null) {
            instance = new WkJsonUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // WkJsonUtils.instance = this;
    }

    // ========================================================================
    // 方法區
    // ========================================================================
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Gson gson = new Gson();
    private final GsonBuilder gsonBuilder = new GsonBuilder();

    /**
     * 解析成java script object
     *
     * @param object
     * @return JSON string
     * @throws java.io.IOException
     */
    public String toJson(Object object) throws IOException {

        ObjectWriter ow = this.objectMapper.writer()
                .withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }

    /**
     * java script object 轉型為java 對應物件
     *
     * @param <T>
     * @param json
     * @param classOfT
     * @return object
     * @throws java.io.IOException
     */
    public <T> T fromJson(
            String json,
            Class<T> classOfT) throws IOException {

        return this.objectMapper.readValue(json, classOfT);
    }

    /**
     * JSON to List
     *
     * @param <T>
     * @param json
     * @param classOfT List內對應的物件型態
     * @return list
     * @throws java.io.IOException
     */
    public <T> List<T> fromJsonToList(
            String json,
            Class<T> classOfT) throws IOException {
        return this.objectMapper.readValue(json, this.objectMapper.getTypeFactory()
                .constructCollectionType(List.class, classOfT));
    }

    /**
     * JSON to HashMap
     *
     * @param json
     * @return Map<Integer, Object>
     * @throws IOException
     */
    public Map<Integer, Object> fromJsonToHashMapKeyInteger(String json) throws IOException {
        return this.objectMapper.readValue(json, new TypeReference<HashMap<Integer, Object>>() {
        });
    }

    /**
     * 轉為格式化的 JSON 字串
     *
     * @param obj
     * @return JSON String
     */
    public String toPettyJson(Object obj) {

        if (obj == null) {
            return "{}";
        }
        return this.gsonBuilder.setPrettyPrinting()
                .create()
                .toJson(obj);
    }

    /**
     * 轉為 JSON 字串
     *
     * @param obj
     * @return JSON string
     */
    public String toJsonWithOutPettyJson(Object obj) {
        return this.gson.toJson(obj);
    }

    /**
     * 轉為物件 by GSON
     *
     * @param jsonStr
     * @param type    new TypeToken<T>() {}.getType()
     * @return Object
     */
    public Object fromJsonByGson(
            String jsonStr,
            Type type) {
        return this.gson.fromJson(jsonStr, type);
    }

    /**
     * 格式化 JSON 字串
     *
     * @param uglyJSONString
     * @return JSON String
     */
    public String toPettyJson(String uglyJSONString) {

        Gson gson = this.gsonBuilder.setPrettyPrinting()
                .create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(uglyJSONString);
        String prettyJsonString = gson.toJson(je);
        return prettyJsonString;
    }

    public static boolean isJson(String Json) {
        Gson gson = new Gson();
        try {
            gson.fromJson(Json, Object.class);
            Object jsonObjType = gson.fromJson(Json, Object.class).getClass();
            if (jsonObjType.equals(String.class)) {
                return false;
            }
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
