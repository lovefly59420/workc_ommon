/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.utils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * @author shaun
 */
@Component
@Slf4j
public class WkJsoupUtils implements InitializingBean, Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4497251400865122760L;
    private static WkJsoupUtils instance;

    public static WkJsoupUtils getInstance() {
        if (instance == null) {
            instance = new WkJsoupUtils();
        }
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        // WkJsoupUtils.instance = this;
    }

    /**
     * 取得去除網頁標記的純文字內容
     *
     * @param cssContent 待處理字串
     * @return 處理完成字串
     */
    public String clearCssTag(String cssContent) {
        if (Strings.isNullOrEmpty(cssContent)) {
            return "";
        }
        Document doc = Jsoup.parse(cssContent);
        doc.body()
                .wrap("<pre></pre>");
        String text = doc.text();
        return removeRegexpByEditor(text.replaceAll("\u00A0", " "));
    }

    /**
     * 改名為 htmlToText
     *
     * @param cssContent html 格式內容
     * @return 處理結果
     */
    @Deprecated
    public String clearCssTag2(String cssContent) {
        return this.htmlToText(cssContent);
    }

    /**
     * 將HTML內容轉換為 text
     * 
     * @param htmlContent HTML內容
     * @return text content
     */
    public String htmlToText(String htmlContent) {
        if (WkStringUtils.isEmpty(htmlContent)) {
            return "";
        }
        htmlContent = WkStringUtils.safeTrim(htmlContent);

        Document doc = Jsoup.parse(htmlContent);
        doc.body().wrap("<pre></pre>");
        FormattingVisitor formatter = new FormattingVisitor();
        NodeTraversor.traverse(formatter, doc);
        return removeRegexpByEditor(formatter.toString().replaceAll("\u00A0", " "));
    }

    /**
     * 將純文字的斷行、tab ,轉為html 格式
     *
     * @param str 待處理字串
     * @return 回傳結果字串
     */
    public String toCss(String str) {

        if (WkStringUtils.isEmpty(str)) {
            return "";
        }

        // 去頭尾空白
        str = WkStringUtils.safeTrim(str);
        // 統一斷行符號
        str = str.replaceAll("\r\n", "\n");

        // 逐行處理
        String[] strAry = str.split("\n");

        List<String> lines = Lists.newArrayList();
        for (String strLine : strAry) {
            // 去頭尾空白
            strLine = WkStringUtils.safeTrim(strLine);
            // 轉空白
            // strLine = strLine.replaceAll(" ", "&nbsp;");
            // 轉 tab
            strLine = strLine.replaceAll("\t", "&nbsp;&nbsp;");

            lines.add(strLine);
        }

        return String.join("<br/>", lines);
    }

    /**
     * 移除editor 無法使用字元
     *
     * @param value 字串
     * @return string
     */
    public String removeRegexpByEditor(String value) {
        return WkStringUtils.removeCtrlChr(value);
    }

    /**
     * highlight 關鍵字 （自訂CSS style）
     *
     * @param targetStr      待處理字串
     * @param keyword        關鍵字
     * @param highlightStyle 標亮效果 style
     * @return 處理結果
     */
    public String highlightKeyWordByStyle(
            String targetStr,
            String keyword,
            String highlightStyle) {
        String styleContent = "style='" + highlightStyle + "'";
        return this.highlightKeyWord(targetStr, keyword, styleContent);
    }

    /**
     * highlight 關鍵字 （自訂CSS class）
     *
     * @param targetStr      待處理字串
     * @param keyword        關鍵字
     * @param highlightClass 高亮 class
     * @return 處理結果
     */
    public String highlightKeyWordByClass(
            String targetStr,
            String keyword,
            String highlightClass) {
        String styleContent = "class='" + highlightClass + "'";
        return this.highlightKeyWord(targetStr, keyword, styleContent);
    }

    /**
     * highlight 關鍵字
     *
     * @param targetStr    待處理字串
     * @param keyword      關鍵字
     * @param styleContent highlight 的 style
     * @return 處理結果
     */
    public String highlightKeyWord(
            String targetStr,
            String keyword,
            String styleContent) {

        // 傳入為空時, 直接回傳
        if (WkStringUtils.isEmpty(keyword) || WkStringUtils.isEmpty(targetStr)) {
            return targetStr;
        }
        // 未傳入 style 無法標亮
        if (WkStringUtils.isEmpty(styleContent)) {
            log.warn("開發時期錯誤，未傳入標記關鍵字的style");
            return targetStr;
        }

        // 去空白
        targetStr = (targetStr + "").trim();
        keyword = (keyword + "").trim();

        // 相等時直接標亮全部
        if (targetStr.equals(keyword)) {
            return this.highlightStr(targetStr, styleContent);
        }

        String displayNameForCompare = targetStr.toUpperCase();
        String queryKeywordForCompare = keyword.toUpperCase();

        StringBuilder highlightContext = new StringBuilder();
        int queryKeywordLength = keyword.length();

        // 逐字比對
        for (int i = 0; i < targetStr.length(); i++) {
            String currContext = displayNameForCompare.substring(i);
            if (currContext.startsWith(queryKeywordForCompare)) {
                highlightContext.append(this.highlightStr(targetStr.substring(i, i + queryKeywordLength), styleContent));
                i += queryKeywordLength - 1;
            } else {
                highlightContext.append(targetStr.charAt(i));
            }
        }
        return highlightContext.toString();
    }

    /**
     * 高亮字串
     *
     * @param targetStr    待處理字串
     * @param styleContent style content for html elm
     * @return 處理結果字串
     */
    private String highlightStr(
            String targetStr,
            String styleContent) {
        return this.prepareHighlightSpanPrefixTag(styleContent) + targetStr + HIGHLIGHT_SPAN_SUFFIX_TAG;
    }

    private String prepareHighlightSpanPrefixTag(String styleContent) {
        return "<span  highlightStr='true' " + styleContent + ">";
    }

    private final String HIGHLIGHT_SPAN_SUFFIX_TAG = "</span>";

    public String restoreHighlight(
            String displayContent,
            String styleContent) {
        if (WkStringUtils.isEmpty(displayContent)) {
            return displayContent;
        }

        displayContent = displayContent.replaceAll(prepareHighlightSpanPrefixTag(styleContent), "");
        displayContent = displayContent.replaceAll(HIGHLIGHT_SPAN_SUFFIX_TAG, "");
        return displayContent;
    }

    // the formatting rules, implemented in a breadth-first DOM traverse
    private static class FormattingVisitor implements NodeVisitor {
        private static final int maxWidth = 80;
        private int width = 0;
        private final StringBuilder accum = new StringBuilder(); // holds the accumulated text

        // hit when the node is first seen
        public void head(
                Node node,
                int depth) {
            String name = node.nodeName();
            if (node instanceof TextNode) {
                String text = ((TextNode) node).text();
                // 為超連結時， URL text 凸顯
                if (node.parentNode() != null && "a".equals(node.parentNode().nodeName())) {
                    text = "【" + text + "】";
                }
                append(text); // TextNodes carry all user-readable text in the DOM.
            } else if (name.equals("li")) {
                append("\n * ");
            } else if (name.equals("dt")) {
                append("  ");
            } else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5", "tr")) {
                append("\n");
            }
        }

        // hit when all the node's children (if any) have been visited
        public void tail(
                Node node,
                int depth) {
            String name = node.nodeName();
            if (StringUtil.in(name, "br", "div", "footer", "form", "header", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5", "h6")) {
                append("\n");
            } else if (name.equals("a")) {
                append(String.format(" <%s>", node.absUrl("href")));
            }
        }

        // appends text to the string builder with a simple word wrap method
        private void append(String text) {
            if (text.startsWith("\n")) {
                // reset counter if starts with a newline. only from formats above, not in natural text
                width = 0;
            }

            if (text.length() + width > maxWidth) { // won't fit, needs to wrap
                String[] words = text.split("\\s+");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    boolean last = i == words.length - 1;
                    if (!last) { // insert a space if not the last word
                        word = word + " ";
                    }
                    if (word.length() + width > maxWidth) { // wrap and reset counter
                        accum.append("\n")
                                .append(word);
                        width = word.length();
                    } else {
                        accum.append(word);
                        width += word.length();
                    }
                }
            } else { // fits as is, without need to wrap text
                accum.append(text);
                width += text.length();
            }
        }

        @Override
        public String toString() {
            return accum.toString();
        }
    }
}
