package com.cy.work.common.service;

import com.cy.employee.rest.client.simple.SimpleEmployeeClient;
import com.cy.employee.vo.SimpleEmployee;
import com.cy.work.common.config.MailConfig;
import com.cy.work.common.utils.WkStringUtils;
import com.cy.work.common.utils.WkUserUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author aken_kao
 */
@Slf4j
@Service
public class MailService {

    private static final String WERP_MAIL = "werp_system@mail.chungyo.net";
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private SimpleEmployeeClient employeeClient;

    /**
     * 發送HTML格式mail
     *
     * @param receiverSids 收件者 SID list
     * @param pSubject     信件主題
     * @param pContent     信件內容
     */
    public void sendHtmlMail(
            List<Integer> receiverSids,
            String pSubject,
            String pContent) {

        if (WkStringUtils.isEmpty(receiverSids)) {
            log.info("send mail:[{}], 傳入收件者為空，故不發送",
                     pSubject);
            return;
        }

        // ====================================
        // 過濾重複
        // ====================================
        receiverSids = receiverSids.stream()
                .distinct()
                .collect(Collectors.toList());

        // ====================================
        // 取得收件者 mail
        // ====================================
        Set<String> receiverMailAddress = Sets.newHashSet();
        List<String> userInfos = Lists.newArrayList();
        for (Integer receiverSid : receiverSids) {

            String warnMsg = "";
            String mail = "";

            // 取得 Employee 資料
            Optional<SimpleEmployee> simpleEmployee = employeeClient.findByMappedUserSid(receiverSid);

            // 檢核無 Employee
            if (!simpleEmployee.isPresent()) {
                warnMsg = "未設定 Employee 資料";
            }
            // 檢核未設定 mail 資料
            else if (WkStringUtils.isEmpty(simpleEmployee.get()
                                                   .getGmail())) {
                warnMsg = "未設定 mail 資料";
            }
            // 檢核 mail 正確性
            else if (!WkStringUtils.isEMail(simpleEmployee.get()
                                                    .getGmail())) {
                warnMsg = "設定的 mail 格式不對:" + simpleEmployee.get()
                        .getGmail();
                // 加入清單
            } else {
                mail = simpleEmployee.get()
                        .getGmail();
                receiverMailAddress.add(mail);
            }

            userInfos.add(String.format("[%s]:[%s]",
                                        WkUserUtils.prepareUserNameWithDep(receiverSid, "-"),
                                        WkStringUtils.isEmpty(warnMsg) ? mail : warnMsg));
        }

        log.debug("send mail:[{}], 取得收件者資訊如下：\r\n{}",
                  pSubject,
                  String.join("\r\n", userInfos));

        if (WkStringUtils.isEmpty(receiverMailAddress)) {
            log.warn("send mail:[{}],取不到有效的 mail, 故停止發送!",
                     pSubject);
            return;
        }

        String[] pMailAddresses = getAddressByUserSids(receiverSids);
        if (pMailAddresses.length == 0) {
            log.info("send mail Fail! cause could not find user mail address.. userSids : {}", receiverSids);
            return;
        }

        // ====================================
        // 發送郵件
        // ====================================
        this.sendMailByMailAddress(Sets.newHashSet(pMailAddresses), pSubject, pContent);
    }

    /**
     * 發送 mail
     *
     * @param mailAddresses 郵件地址
     * @param subject       主題
     * @param content       內容
     */
    public void sendMailByMailAddress(
            Set<String> mailAddresses,
            String subject,
            String content) {

        // ====================================
        // 開發環境：固定發送到同一個 mail 邏輯
        // ====================================
        String depEnvFixReceiver = WkStringUtils.safeTrim(MailConfig.getInstance()
                                                                  .getDepEnvFixReceiver());
        if (WkStringUtils.notEmpty(depEnvFixReceiver)
                && !depEnvFixReceiver.startsWith("${")
                && !"FALSE".equalsIgnoreCase(depEnvFixReceiver)
                && WkStringUtils.isEMail(depEnvFixReceiver)) {

            log.warn("send mail:觸發測試模式，信件將僅發送到 [" + depEnvFixReceiver + "]");
            content = "開發環境測試：原被發送者：" + new Gson().toJson(mailAddresses) + "<br/><br/>" + content;
            subject = "開發環境測試：" + subject;
            mailAddresses = Sets.newHashSet(depEnvFixReceiver);
        }

        // ====================================
        // 發送 mail
        // ====================================

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
            helper.setFrom(WERP_MAIL);
            helper.setTo(mailAddresses.stream()
                                 .toArray(String[]::new));
            helper.setSubject(subject);
            helper.setText(content, true);

            mailSender.send(message);
            log.info("發送 mail ({}) to {}.", subject, String.join("、", mailAddresses));
        } catch (MailException e) {
            log.warn("發送 mail ({}) to {} was failed.", subject, String.join("、", mailAddresses));
            log.warn(e.getMessage(), e);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 取得員工 mail
     *
     * @param userSids
     * @return
     */
    private String[] getAddressByUserSids(List<Integer> userSids) {
        if (WkStringUtils.isEmpty(userSids)) {
            return new String[0];
        }

        List<String> emails = Lists.newArrayList();

        for (Integer userSid : userSids) {
            // 取得 Employee 資料
            Optional<SimpleEmployee> simpleEmployee = employeeClient.findByMappedUserSid(userSid);
            // 檢核無 Employee
            if (!simpleEmployee.isPresent()) {
                log.debug("send mail: user:[" + userSid + "] 未設定 Employee 資料");
                continue;
            }
            // 檢核未設定 mail 資料
            if (WkStringUtils.isEmpty(simpleEmployee.get().getGmail())) {
                log.debug("user:[" + simpleEmployee.get().getId() + "] 未設定 mail 資料");
                continue;
            }
            // 加入清單
            emails.add(simpleEmployee.get().getGmail());
        }
        return emails.toArray(new String[emails.size()]);
    }
}
