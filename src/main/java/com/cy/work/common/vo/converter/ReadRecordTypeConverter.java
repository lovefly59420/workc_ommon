/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.enums.ReadRecordType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 閱讀狀態碼
 *
 * @author shaun
 */
@Slf4j
@Converter
public class ReadRecordTypeConverter implements AttributeConverter<ReadRecordType, String> {

    @Override
    public String convertToDatabaseColumn(ReadRecordType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public ReadRecordType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return ReadRecordType.valueOf(dbData);
        } catch (Exception e) {
            log.error("ReadRecordType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
