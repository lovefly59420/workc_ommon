/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo;

import com.cy.commons.enums.Activation;
import com.cy.work.common.utils.WkDateUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

/**
 * 附加檔案Servlet
 *
 * @author shaun
 */
public interface AttachmentServlet extends Serializable {

    //實作要覆寫 SERVLET_NAME 否則會出錯
    //public final static String SERVLET_NAME = AttachmentServlet.class.getSimpleName();
    public final static String PARAM_SID_KEY = "sid";
    public final static String PARAM_TIME_KEY = "time";
    public final static String PARAM_ATTACH_SERVICE_KEY = "service";

    @SuppressWarnings("rawtypes")
    public AttachmentService getAttachService(HttpServletRequest request);

    @SuppressWarnings({"unchecked", "rawtypes"})
    default void doGet(
            HttpServletRequest request,
            HttpServletResponse response,
            ServletContext servletContext) throws IOException {
        Object attachmentSid = this.findAttachSid(request);
        Date uploadTime = this.getUploadTimeParameter(request);

        Attachment attachment = this.getAttachService(request)
                .findBySid(attachmentSid);
        //找不到或已停用
        if (attachment == null || Activation.INACTIVE.equals(attachment.getStatus())) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        File attachmentFile = this.getAttachService(request)
                .getServerSideFile(attachment);
        if (!this.validateParameters(attachment, uploadTime, attachmentFile)) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        this.sendAttachment(request, response, servletContext, attachment, attachmentFile);
    }

    default Object findAttachSid(HttpServletRequest request) {
        return this.getAttachmentSidParameter(request);
    }

    default String getAttachmentServiceParameter(HttpServletRequest request) {
        return request.getParameter(PARAM_ATTACH_SERVICE_KEY);
    }

    default String getAttachmentSidParameter(HttpServletRequest request) {
        return request.getParameter(PARAM_SID_KEY);
    }

    default Date getUploadTimeParameter(HttpServletRequest request) {
        try {
            String timeString = request.getParameter(PARAM_TIME_KEY);
            return WkDateUtils.timeAnalysis(timeString);
        } catch (Exception e) {
            System.err.println("錯誤：" + e.getMessage());
            return new Date();
        }
    }

    @SuppressWarnings("rawtypes")
    default boolean validateParameters(
            Attachment attachment,
            Date uploadTime,
            File attachmentFile) {
        // 檢查 attachment.getCreated() 是為了預防使用者從 URL 修改 sid 即可取得其他檔案.
        return checkParam(attachment, uploadTime, attachmentFile)
                && uploadTime.equals(attachment.getCreatedDate())
                && checkAttachmentFile(attachmentFile);
    }

    default boolean checkParam(
            Attachment<?> attachment,
            Date uploadTime,
            File attachmentFile) {
        return attachment != null && uploadTime != null && attachmentFile != null;
    }

    default boolean checkAttachmentFile(File attachmentFile) {
        return attachmentFile.exists() && attachmentFile.isFile();
    }

    default void sendAttachment(
            HttpServletRequest request,
            HttpServletResponse response,
            ServletContext servletContext,
            Attachment<?> attachment,
            File attachmentFile) throws IOException {

        //清除特殊字元
        List<String> NOT_ALLOWED_CHARACTERS = Lists.newArrayList("\\", ":", "/", "*", "?", "\"", "<", ">", "|");
        StringBuilder cleanFileName = new StringBuilder();
        for (char ch : WkStringUtils.safeTrim(attachment.getFileName()).toCharArray()) {
            if (!NOT_ALLOWED_CHARACTERS.contains(ch + "")) {
                cleanFileName.append(ch);
            }
        }

        String encodedFileName = this.getAttachService(request).encodeFileName(cleanFileName.toString());
        String mimeType = this.getAttachService(request)
                .getNoneNullMimeType(servletContext, attachment.getFileName());
        String contentDispositionPattern
                = this.getAttachService(request)
                .isMSIE(request)
                ? "{0}; filename=\"{1}\""
                : "{0}; filename=\"{1}\"; filename*=UTF-8''''\"{1}\"";
        String contentDisposition
                = MessageFormat.format(contentDispositionPattern, "inline", encodedFileName);
        FileInputStream in = new FileInputStream(attachmentFile);
        OutputStream out = response.getOutputStream();

        response.setContentType(mimeType);
        response.setContentLength((int) attachmentFile.length());
        response.setHeader("Content-Disposition", contentDisposition);

        try {
            IOUtils.copy(in, out);
            out.flush();
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

}
