/**
 * 
 */
package com.cy.work.common.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
public class WkItemName implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4027987617055233029L;

    public WkItemName(String content, String styleContext) {
        this.content = content;
        this.showContent = content;
        this.styleContext = styleContext;
    }

    /**
     * 顯示文字
     */
    @Getter
    private String content;
    /**
     * style
     */
    @Getter
    private String styleContext = "%s";

    @Getter
    @Setter
    private String showContent;

    public void resetShowContent() {
        this.showContent = this.content;
    }

    /**
     * 取得顯示內容
     * 
     * @return Show content By style
     */
    public String getShowContentByStyle() { return String.format(this.styleContext, this.showContent); }
}
