/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo;

import com.cy.commons.enums.Activation;
import com.cy.commons.vo.Org;
import com.cy.commons.vo.User;

import java.io.Serializable;
import java.util.Date;

/**
 * 附加檔案 介面  <BR/>
 * sid type by T
 *
 * @param <T>
 */
public interface Attachment<T> extends Serializable {

    /**
     * sid
     *
     * @return sid
     */
    public T getSid();

    public void setSid(T sid);

    /**
     * 檔案名稱
     *
     * @return file name
     */
    public String getFileName();

    public void setFileName(String fileName);

    /**
     * 檔案描述
     *
     * @return 檔案描述 
     */
    public String getDesc();

    public void setDesc(String desc);

    /**
     * 上傳部門
     *
     * @return org
     */
    public Org getUploadDept();

    public void setUploadDept(Org uploadDept);

    /**
     * 狀態
     *
     * @return Activation
     */
    public Activation getStatus();

    public void setStatus(Activation status);

    /**
     * 建立日期
     *
     * @return Date
     */
    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    /**
     * 更新日期
     *
     * @return Date
     */
    public Date getUpdatedDate();

    public void setUpdatedDate(Date updatedDate);

    /**
     * 建立者
     *
     * @return User
     */
    public User getCreatedUser();

    public void setCreatedUser(User createdUser);

    /**
     * 更新者
     *
     * @return User
     */
    public User getUpdatedUser();

    public void setUpdatedUser(User updatedUser);

    /**
     * 是否編輯附加檔案
     *
     * @return Boolean
     */
    public Boolean getKeyCheckEdit();

    public void setKeyCheckEdit(Boolean checkEdit);

    /**
     * 是否選擇附加檔案
     *
     * @return Boolean
     */
    public Boolean getKeyChecked();

    public void setKeyChecked(Boolean checked);

}
