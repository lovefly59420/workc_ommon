package com.cy.work.common.vo;

import com.cy.commons.enums.Activation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * @author allen1214_wu
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEntityVO implements Serializable {


    /**
     * 狀態
     */
    @Getter
    @Setter
    private Activation status = Activation.ACTIVE;
    /**
     * 建立者
     */
    @Getter
    @Setter
    private Integer createdUser;
    /**
     * 建立日期
     */
    @Getter
    @Setter
    private Date createdDate;
    /**
     *
     */
    @Getter
    @Setter
    private Integer updatedUser;
    /**
     *
     */
    @Getter
    @Setter
    private Date updatedDate;

    /**
     * 是否啟用
     *
     * @return true/false
     */
    public boolean isActive() {
        return Activation.ACTIVE.equals(this.status);
    }
}
