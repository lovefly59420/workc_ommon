package com.cy.work.common.vo;

import com.google.common.collect.Lists;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * 階層式節點結構物件
 *
 * @author allen1214_wu
 */
@Slf4j
@EqualsAndHashCode(of = "uuid")
public class WkNode implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6533812725531225105L;

    /**
     * 建構時生成的 UUID
     */
    @Getter
    private String uuid;

    /**
     * 節點資料
     */
    @Getter
    @Setter
    private Object nodeData;

    /**
     *
     */
    @Getter
    private WkNode parentNode;

    /**
     * 僅為路徑節點, 非真實資料
     */
    @Getter
    @Setter
    private boolean isPathNode = false;

    /**
     * 子節點
     */
    @Getter
    private List<WkNode> childNodes = Lists.newArrayList();

    /**
     * @param nodeData
     */
    public WkNode(Object nodeData) {
        this.uuid = UUID.randomUUID().toString();
        this.nodeData = nodeData;
    }

    /**
     * 對外開放方法：新增子節點
     *
     * @param childNode
     */
    public void addChild(WkNode childNode) {
        if (childNode == null) {
            return;
        }

        childNode.onlySetParent(this);
        this.onlyAddChild(childNode);
    }

    /**
     * 對外開放方法：移除子節點
     *
     * @param childNode
     */
    public void removeChild(WkNode childNode) {
        if (childNode == null) {
            return;
        }
        childNode.onlySetParent(null);
        this.onlyRemoveChild(childNode);
    }

    /**
     * 對外開放方法：設定父節點
     *
     * @param newParentNode
     */
    public void setParent(WkNode newParentNode) {

        // 防呆
        if (newParentNode != null && this.uuid.equals(newParentNode.getUuid())) {
            log.warn("把自己設為父節點, 會造成無窮迴圈 -> 略過");
            return;
        }

        // 在原父節點中的子項, 將自己移移除
        if (this.parentNode != null) {
            this.parentNode.onlyRemoveChild(this);
        }

        if (newParentNode != null) {
            // 將本節點新增到新的父節點的子代
            newParentNode.onlyAddChild(this);
        }

        // 異動本節點的父節點
        this.parentNode = newParentNode;
    }

    /**
     * @param newParentNode
     */
    protected void onlySetParent(WkNode newParentNode) {
        this.parentNode = newParentNode;
    }

    /**
     * @param childNode
     */
    protected void onlyAddChild(WkNode childNode) {
        if (!this.childNodes.contains(childNode)) {
            this.childNodes.add(childNode);
        }
    }

    /**
     * @param childNode
     */
    protected void onlyRemoveChild(WkNode childNode) {
        if (this.childNodes.contains(childNode)) {
            this.childNodes.remove(childNode);
        }
    }

}
