/**
 *
 */
package com.cy.work.common.vo;

import com.cy.work.common.utils.WkStringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author allen1214_wu
 */
@EqualsAndHashCode(of = { "sid" })
@NoArgsConstructor
public class WkItem implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -1914857939106293465L;
    /**
     * SID
     */
    @Getter
    @Setter
    private String sid;
    /**
     * 是否啟用
     */
    @Getter
    @Setter
    private boolean active = false;
    /**
     * 父項
     */
    @Getter
    @Setter
    private WkItem parent;
    /**
     * 名稱
     */
    @Getter
    @Setter
    private String name;
    /**
     * 顯示名稱
     */
    @Getter
    @Setter
    private String showName;
    /**
     * 是否鎖定
     */
    @Getter
    @Setter
    private boolean disable = false;
    /**
     * icon (SingleSelectTree)
     */
    @Getter
    @Setter
    private boolean selectable = true;
    /**
     * 其他字串資料 (可依據情況，放入未在 WkItem 定義的欄位)
     */
    @Getter
    private Map<String, String> otherInfo = Maps.newHashMap();
    /**
     * 其他資料容器 (可依據情況，放入未在 WkItem 定義的物件欄位)
     */
    @Getter
    private Map<String, Object> otherObject = Maps.newHashMap();
    /**
     * 項目排序
     */
    @Getter
    @Setter
    private Long itemSeq = Long.MAX_VALUE;
    /**
     * 提供給搜尋用的名稱
     */
    @Getter
    private List<String> itemNamesForSearch = Lists.newArrayList();

    /**
     * 提供給搜尋用的名稱
     */
    @Getter
    private List<WkItemName> wkItemNames = Lists.newArrayList();

    /**
     * 兜組顯示資訊
     * 
     * @return 顯示資訊 string
     */
    public String prepareShowWkItemNames() {
        if (WkStringUtils.isEmpty(this.wkItemNames)) {
            return "";
        }

        return this.wkItemNames.stream()
                .map(WkItemName::getShowContentByStyle)
                .collect(Collectors.joining(""));
    }

    /**
     *
     */
    @Getter
    @Setter
    private String treeName;

    // ========================================================================
    //
    // ========================================================================
    /**
     *
     */
    @Getter
    @Setter
    private String showTreeName;
    /**
     *
     */
    @Getter
    @Setter
    private String showTreeClass;
    /**
     * 為樹節點時, 強制展開
     */
    @Getter
    @Setter
    private boolean forceExpand = false;

    /**
     * 建構子
     *
     * @param sid
     * @param name
     * @param active
     */
    public WkItem(
            String sid,
            String name,
            boolean active) {
        super();
        this.sid = sid;
        this.name = name;
        this.showName = name;
        this.treeName = name;
        this.showTreeName = name;
        this.active = active;
    }

    // ========================================================================
    //
    // ========================================================================

    @JsonIgnore
    public WkItem getThis() { return this; }

}
