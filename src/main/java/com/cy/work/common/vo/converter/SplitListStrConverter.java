package com.cy.work.common.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * String to integer list Converter (value)
 *
 * @author allen
 */
@Converter
public class SplitListStrConverter implements AttributeConverter<List<String>, String> {

    /*
     * (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.
     * Object)
     */
    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        if (WkStringUtils.isEmpty(attribute)) {
            return "";
        }

        // 轉為以逗點分隔的字串
        String json = attribute.stream()
                .map(WkStringUtils::safeTrim)
                .collect(Collectors.joining(","));

        if (WkStringUtils.notEmpty(json)) {
            json += ",";
        }

        return json;
    }

    /*
     * (non-Javadoc)
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.
     * Object)
     */
    @Override
    public List<String> convertToEntityAttribute(String dbData) {

        String contentValue = WkStringUtils.safeTrim(dbData);

        if (WkStringUtils.isEmpty(contentValue)) {
            return Lists.newArrayList();
        }

        // 以『,』分割，並去空白
        return Lists.newArrayList(contentValue.split(","))
                .stream()
                .map(WkStringUtils::safeTrim) // 去空白
                .filter(val -> WkStringUtils.notEmpty(val)) // 去除空值
                .collect(Collectors.toList());
    }
}
