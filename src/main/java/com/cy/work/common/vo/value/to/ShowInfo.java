package com.cy.work.common.vo.value.to;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 畫面欄位顯示容器
 *
 * @author allen1214_wu
 */
public class ShowInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3095636406648078287L;
    /**
     * 顯示文字
     */
    @Getter
    @Setter
    private String text;
    /**
     * tool tip content
     */
    @Getter
    @Setter
    private String tooltip;

    /**
     * 建構子
     *
     * @param text    顯示文字
     * @param tooltip tool tip
     */
    public ShowInfo(
            String text,
            String tooltip) {
        super();
        this.text = text;
        this.tooltip = tooltip;
    }

    /**
     * 建構子 - 資料內容一致時使用
     *
     * @param sameContent
     */
    public ShowInfo(String sameContent) {
        this.text = sameContent;
        this.tooltip = sameContent;
    }
}
