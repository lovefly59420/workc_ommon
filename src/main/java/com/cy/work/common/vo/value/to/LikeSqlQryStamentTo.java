package com.cy.work.common.vo.value.to;

import lombok.Getter;

public class LikeSqlQryStamentTo {

    /**
     * 加在 where like 語句末端的後綴
     */
    @Getter
    private String likeSqlSubFixStr;

    /**
     * 新的查詢字串
     */
    @Getter
    private String newConditionStr;

    /**
     * @param likeSqlSubFixStr
     * @param newConditionStr
     */
    public LikeSqlQryStamentTo(
            String likeSqlSubFixStr,
            String newConditionStr) {
        this.likeSqlSubFixStr = likeSqlSubFixStr;
        this.newConditionStr = newConditionStr;
    }

}
