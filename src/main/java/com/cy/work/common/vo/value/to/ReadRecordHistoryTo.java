/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.value.to;

import com.cy.work.common.enums.ReadRecordType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 閱讀狀態歷程
 *
 * @author shaun
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(of = "readDay")
public class ReadRecordHistoryTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -381619134625636874L;

    /**
     * 閱讀日期
     */
    @JsonProperty(value = "readDay")
    private Date readDay;
    /**
     * 閱讀狀態碼
     */
    @JsonProperty(value = "type")
    private ReadRecordType type;
}
