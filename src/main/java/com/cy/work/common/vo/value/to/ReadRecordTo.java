/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.value.to;

import com.cy.work.common.enums.ReadRecordType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;

/**
 * 閱讀記錄
 *
 * @author shaun
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString(exclude = {"history"})
@EqualsAndHashCode(of = "reader")
public class ReadRecordTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6700573486785872369L;

    /**
     * 閱讀人員Sid
     */
    @JsonProperty(value = "reader")
    private String reader;
    /**
     * 閱讀狀態碼
     */
    @JsonProperty(value = "type")
    private ReadRecordType type;
    /**
     * 閱讀日期
     */
    @JsonProperty(value = "readDay")
    private Date readDay;
    /**
     * 閱讀狀態歷程 - 最新的在最上面 history.addFirst() - 極限僅存放5個 history.removeLast()
     */
    @JsonProperty(value = "history")
    private LinkedList<ReadRecordHistoryTo> history = Lists.newLinkedList();

}
