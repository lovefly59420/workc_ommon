/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.bpm.rest.vo.ProcessTask;
import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.cy.bpm.rest.vo.ProcessTaskHistory;
import com.cy.work.common.utils.WkEntityUtils;
import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.vo.value.to.BpmTaskTo;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.List;

/**
 * BPM 流程節點物件轉換
 *
 * @author shaun
 */
@Slf4j
@Converter
public class BpmTaskToConverter implements AttributeConverter<BpmTaskTo, String> {

    @Override
    public String convertToDatabaseColumn(BpmTaskTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance()
                    .toJson(attribute.getTasks());
        } catch (IOException ex) {
            log.error("parser List<ProcessTaskBase> to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @Override
    public BpmTaskTo convertToEntityAttribute(String dbData) {
        BpmTaskTo to = new BpmTaskTo();
        if (Strings.isNullOrEmpty(dbData)) {
            return to;
        }
        try {
            //轉出時..以History型態轉出
            List<ProcessTaskHistory> historys = WkJsonUtils.getInstance()
                    .fromJsonToList(dbData, ProcessTaskHistory.class);
            if (historys == null || historys.isEmpty()) {
                return to;
            }
            //處理任務轉型，水管圖最後節點型態可能為ProcessTask型態...
            List<ProcessTaskBase> simulationChart = Lists.newArrayList();
            for (ProcessTaskBase each : historys) {
                if (Strings.isNullOrEmpty(((ProcessTaskHistory) each).getExecutorID())) {
                    ProcessTask news = new ProcessTask();
                    each = (ProcessTaskBase) WkEntityUtils.getInstance()
                            .copyProperties(each, news);
                }
                simulationChart.add(each);
            }
            to.setTasks(simulationChart);
            return to;
        } catch (IOException ex) {
            log.error("parser json to List<ProcessTaskBase> fail!!" + ex.getMessage());
            return to;
        }
    }

}
