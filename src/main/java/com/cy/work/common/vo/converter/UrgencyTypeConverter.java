package com.cy.work.common.vo.converter;

import com.cy.work.common.enums.UrgencyType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 緊急度轉換器
 *
 * @author shaun
 */
@Converter
public class UrgencyTypeConverter implements AttributeConverter<UrgencyType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(UrgencyType x) {
        return x.ordinal();
    }

    @Override
    public UrgencyType convertToEntityAttribute(Integer y) {
        for (UrgencyType each : UrgencyType.values()) {
            if (y.equals(each.ordinal())) {
                return each;
            }
        }
        return UrgencyType.GENERAL;
    }

}
