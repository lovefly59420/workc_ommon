/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.basic;

import com.cy.commons.enums.Activation;

import java.util.Date;

/**
 * 附加檔案 介面 <BR/>
 * sid type by T
 *
 * @param <T>
 * @author kasim
 */
public interface BasicAttachment<T> {

    /**
     * sid
     *
     * @return sid
     */
    public T getSid();

    public void setSid(T sid);

    /**
     * 檔案名稱
     *
     * @return 檔案名稱
     */
    public String getFileName();

    public void setFileName(String fileName);

    /**
     * 檔案描述
     *
     * @return 檔案描述
     */
    public String getDesc();

    public void setDesc(String desc);

    /**
     * 上傳部門
     *
     * @return 上傳部門 sid
     */
    public Integer getUploadDept();

    public void setUploadDept(Integer uploadDept);

    /**
     * 狀態
     *
     * @return 狀態
     */
    public Activation getStatus();

    public void setStatus(Activation status);

    /**
     * 建立日期
     *
     * @return 建立日期
     */
    public Date getCreatedDate();

    public void setCreatedDate(Date createdDate);

    /**
     * 更新日期
     *
     * @return 更新日期
     */
    public Date getUpdatedDate();

    public void setUpdatedDate(Date updatedDate);

    /**
     * 建立者
     *
     * @return 建立者 sid
     */
    public Integer getCreatedUser();

    public void setCreatedUser(Integer createdUser);

    /**
     * 更新者
     *
     * @return 更新者 sid
     */
    public Integer getUpdatedUser();

    public void setUpdatedUser(Integer updatedUser);

    /**
     * 是否編輯附加檔案
     *
     * @return 是/否
     */
    public Boolean getKeyCheckEdit();

    public void setKeyCheckEdit(Boolean checkEdit);

    /**
     * 是否選擇附加檔案
     *
     * @return 是/否
     */
    public Boolean getKeyChecked();

    public void setKeyChecked(Boolean checked);

}
