/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.basic;

import com.google.common.base.Strings;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * 附加檔案基本功能 + 實作功能 <BR/>
 * java 8 support
 *
 * @param <A> 附加檔案型態
 * @param <E> 附加檔案歸屬主體 entity
 * @param <T> sid 型態
 * @author kasim
 */
@SuppressWarnings("rawtypes")
public interface BasicAttachmentService<A extends BasicAttachment, E, T> {

    public static final int MAX_ALLOWED_FILENAME_LENGTH = 255;
    public static final char[] NOT_ALLOWED_CHARACTERS = { '\\', ':', '/', '*', '?', '\"', '<', '>', '|' };
    public static final char[] NOT_ALLOWED_CHARACTERS_REPLACE = { '＼', '：', '／', '＊', '？', '〝', '〈', '〉', '｜' };
    public static final String MULTIFILES_ZIP_FILENAME = "multi-files.zip";
    public static final String DEFAULT_MIME_TYPE = "application/octet-stream";

    /**
     * 透過Sid尋找附加檔案entity
     *
     * @param sid
     * @return entity
     */
    public A findBySid(T sid);

    /**
     * 依主體尋找附加檔案(需避免LazyException)
     *
     * @param e e
     * @return list
     */
    public List<A> findAttachsByLazy(E e);

    /**
     * 依主體尋找附加檔案
     *
     * @param e E
     * @return list
     */
    public List<A> findAttachs(E e);

    /**
     * 附加檔案資料路徑
     *
     * @return 附加檔案資料路徑
     */
    public String getAttachPath();

    /**
     * 檔案存放位置目錄名稱
     *
     * @return 檔案存放位置目錄名稱
     */
    public String getDirectoryName();

    /**
     * 建立附加檔案
     *
     * @param fileName 檔案名稱
     * @param executor 執行者 sid
     * @param dep      部門 sid
     * @return A
     */
    public A createEmptyAttachment(
            String fileName,
            Integer executor,
            Integer dep);

    /**
     * 更新
     *
     * @param attach attach
     * @return A
     */
    public A updateAttach(A attach);

    /**
     * 儲存
     *
     * @param attach
     */
    public void saveAttach(A attach);

    /**
     * 儲存
     *
     * @param attachs
     */
    public void saveAttach(List<A> attachs);

    /**
     * 附件上傳者及部門名稱<BR/>
     * ex: E化發展部 - Shaun
     *
     * @param attachment attachment
     * @return 附件上傳者及部門名稱
     */
    public String depAndUserName(A attachment);

    /**
     * 建立附加檔案時，與主檔建立關聯
     *
     * @param attachments attachments
     * @param entity      entity
     * @param login       login user Sid
     */
    public void linkRelation(
            List<A> attachments,
            E entity,
            Integer login);

    /**
     * 建立附加檔案時，與主檔建立關聯
     *
     * @param ra
     * @param entity
     * @param login
     */
    public void linkRelation(
            A ra,
            E entity,
            Integer login);

    /**
     * 附加檔案變更狀態
     *
     * @param attachments
     * @param attachment
     * @param entity
     * @param login
     */
    public void changeStatusToInActive(
            List<A> attachments,
            A attachment,
            E entity,
            Integer login);

    /**
     * 用來建立各單據種類的子目錄.
     *
     * @return 代表該子目錄的 File 物件.
     */
    default File createDirectoryIfNotExist() {
        File directory = new File(this.getAttachPath(), this.getDirectoryName());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }

    /**
     * 將輸入資料流轉存為暫存檔，然後關閉輸入資料流.
     *
     * @param in                輸入資料流.
     * @param filenameExtension 附檔名（不包含 "." ）.
     * @return 一個暫存檔案，該檔案放置於附加檔案最終所應該放置的目錄.
     * @throws IOException
     */
    default File saveToTemporaryFile(
            InputStream in,
            String filenameExtension) throws IOException {
        File tempFile = File.createTempFile("temp", "." + filenameExtension, new File(this.getAttachPath(), this.getDirectoryName()));
        BufferedOutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(tempFile));
            IOUtils.copy(in, out);
        } catch (IOException e) {
            tempFile.delete();
            throw e;
        } finally {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
        }
        return tempFile;
    }

    /**
     * 取得檔案在伺服器端的實際路徑.
     *
     * @param attachment 附加檔案 entity.
     * @return 附加檔案的 <code>File</code> 物件.
     */
    default File getServerSideFile(A attachment) {
        File serverSideDirectory = new File(this.getAttachPath(), this.getDirectoryName());
        return new File(serverSideDirectory, this.getServerSideFileName(attachment));
    }

    /**
     * server side 檔案名稱.
     *
     * @param attachment 附加檔案的資料庫實例.
     * @return 實際存放在 server 上的檔案名稱.
     */
    default String getServerSideFileName(A attachment) {
        return attachment.getSid() + "-" + attachment.getFileName();
    }

    /**
     * 替某些檔案改寫部分檔案內容，並儲存為最終標的.<br>
     * 請注意本方法不會擲出 Exception ，若內部發生 Exception 則回傳 false.
     *
     * @param source            輸入檔案，可能需要改寫部分內容.
     * @param target            最終標的檔案.
     * @param fileNameExtension 附檔名，用來初步過濾檔案格式.
     * @return true 代表輸入檔案經過修改並儲存為最終標的檔案；否則為 false.
     */
    default boolean rewriteJpg(
            File source,
            File target,
            String fileNameExtension) {
        if ("jpg".equalsIgnoreCase(fileNameExtension) || "jpeg".equalsIgnoreCase(fileNameExtension)) {
            try {
                return this.rewriteExif(source, target);
            } catch (ImageReadException | ImageWriteException | IOException e) {
                System.err.println("Failed to rewrite JPEG EXIF information." + e.getMessage());
            }
        }
        return false;
    }

    default String getNoneNullMimeType(
            ServletContext ctx,
            String fileName) {
        String mimeType = ctx.getMimeType(fileName);
        return (mimeType == null) ? DEFAULT_MIME_TYPE : mimeType;
    }

    /**
     * 列出被標記為無效附加檔案
     *
     * @param attachments 附加檔案.
     */
    default void logInvalidatedAttachments(List<A> attachments) {
        if (attachments.isEmpty()) {
            return;
        }
        this.startLogInvalid(attachments);
    }

    default void startLogInvalid(List<A> attachments) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("The following attachment has been invalidated: ");
        String separator = ",";
        attachments.stream()
                .map((attachment) -> {
                    strBuilder.append('(')
                            .append(attachment.getSid())
                            .append(')');
                    return attachment;
                })
                .forEach((attachment) -> {
                    strBuilder.append(attachment.getFileName())
                            .append(separator);
                });
        strBuilder.delete(strBuilder.length() - separator.length(), strBuilder.length());
    }

    /**
     * 檢查檔名是否符合規範，以及是否已經有相同檔名的檔案.
     *
     * @param attachments   attachments
     * @param tempFileNames tempFileNames
     * @param fileName      fileName
     * @param uploadMsg     uploadMsg
     * @param failMsg       failMsg
     * @return true/false
     */
    default boolean validateByFileNameAndResponse(
            List<A> attachments,
            List<String> tempFileNames,
            String fileName,
            StringBuilder uploadMsg,
            StringBuilder failMsg) {
        String validateMsg = this.createValidateMsg(fileName);
        if (!Strings.isNullOrEmpty(validateMsg)) {
            this.addFailMessage(fileName, "不接受，" + validateMsg, uploadMsg, failMsg);
            return false;
        }
        if (this.duplicateFileNameExists(attachments, fileName)) {
            this.addFailMessage(fileName, "不接受，因為檔名重覆.", uploadMsg, failMsg);
            return false;
        }
        if (this.duplicateTempFileNameExists(tempFileNames, fileName)) {
            this.addFailMessage(fileName, "不接受，因為檔名重覆.", uploadMsg, failMsg);
            return false;
        }
        tempFileNames.add(fileName);
        return true;
    }

    /**
     * 從現有的附加檔案中檢查是否有檔案的檔名跟參數相同.
     *
     * @param attachments
     * @param fileName    檢查要件.
     * @return true 代表有附加檔案的檔名跟參數相同；否則為 false.
     */
    default boolean duplicateFileNameExists(
            List<A> attachments,
            String fileName) {
        return attachments.stream()
                .anyMatch(attachment -> attachment.getFileName()
                        .equalsIgnoreCase(fileName));
    }

    /**
     * 從現有的附加檔案中檢查是否有檔案的檔名跟參數相同.
     *
     * @param tempFileNames
     * @param fileName      檢查要件.
     * @return true 代表有附加檔案的檔名跟參數相同；否則為 false.
     */
    default boolean duplicateTempFileNameExists(
            List<String> tempFileNames,
            String fileName) {
        return tempFileNames.stream()
                .anyMatch(name -> name.equalsIgnoreCase(fileName));
    }

    /**
     * 寫入上傳失敗訊息
     *
     * @param fileName
     * @param detail
     * @param uploadMsg
     * @param failMsg
     */
    default void addFailMessage(
            String fileName,
            String detail,
            StringBuilder uploadMsg,
            StringBuilder failMsg) {
        addMessage(fileName, uploadMsg, detail);
        addFailMsg(fileName, failMsg, detail);
    }

    /**
     * 寫入上傳訊息到uploadMessages
     *
     * @param fileName
     * @param uploadMsg
     * @param detail
     */
    default void addMessage(
            String fileName,
            StringBuilder uploadMsg,
            String detail) {
        if (uploadMsg.length() > 0) {
            uploadMsg.insert(0, "\n");
        }
        uploadMsg.insert(0, "\"");
        uploadMsg.insert(0, fileName);
        uploadMsg.insert(0, "\": ");
        uploadMsg.insert(0, detail);
    }

    /**
     * 寫入上傳訊息到uploadMessages
     *
     * @param fileName
     * @param failMsg
     * @param detail
     */
    default void addFailMsg(
            String fileName,
            StringBuilder failMsg,
            String detail) {
        if (failMsg.length() > 0) {
            failMsg.append("<br>");
        }
        failMsg.append("\"");
        failMsg.append(fileName);
        failMsg.append("\": ");
        failMsg.append(detail);
    }

    default String createValidateMsg(String fileName) {
        StringBuilder msg = new StringBuilder();

        if (fileName.length() > MAX_ALLOWED_FILENAME_LENGTH) {
            msg.append("檔名超過 ");
            msg.append(MAX_ALLOWED_FILENAME_LENGTH);
            msg.append(" 個字元！");
            return msg.toString();
        }

        for (char ch : NOT_ALLOWED_CHARACTERS) {
            if (fileName.indexOf(Character.getNumericValue(ch)) != -1) {
                msg.append("檔名含有特殊字元 ");
                msg.append(NOT_ALLOWED_CHARACTERS);
                break;
            }
        }

        return msg.toString();
    }

    // BrowseUtils
    default String getUserAgent(HttpServletRequest request) {
        return request.getHeader("User-Agent");
    }

    default boolean isMSIE(HttpServletRequest request) {
        String userAgent = getUserAgent(request);
        boolean msie = userAgent.contains("MSIE");
        boolean trident = userAgent.contains("Trident/");
        return msie || trident;
    }

    default boolean isMSIE10(HttpServletRequest request) {
        String userAgent = getUserAgent(request);
        boolean containsMSIE = userAgent.contains("MSIE");
        boolean containsTrident6 = userAgent.contains("Trident/6.0");
        return containsMSIE && containsTrident6;
    }

    default boolean isChromeFrame(HttpServletRequest request) {
        String userAgent = getUserAgent(request);
        return userAgent.contains("chromeframe");
    }

    default String encodeFileName(String fileName) {
        String encodedFileName = fileName;
        try {
            URI uri = new URI(null, null, fileName, null);
            encodedFileName = uri.toASCIIString();
        } catch (URISyntaxException e) {
            System.err.println(e.getMessage());
        }
        return encodedFileName;
    }

    // JpegExifRewriterUtils

    /**
     * 檢查輸入檔案的 EXIF 資訊，若有必要則用編輯過的 EXIF 資訊取代並輸出為新檔案.
     *
     * @param inputFile  輸入檔案.
     * @param outputFile 輸出目標.
     * @return true 代表輸入檔是一個 JPEG 檔案且包含 EXIF 資訊，而且 EXIF 資訊中包含 Orientation
     *             欄位，該欄位的數值非預設的「水平」，然後將該數值改為「水平」並輸出 至目標檔案；若不需要改寫 EXIF 資訊則為 false
     *             ，此時沒有產生新檔案.
     * @throws ImageReadException
     * @throws IOException
     * @throws ImageWriteException
     */
    default boolean rewriteExif(
            File inputFile,
            File outputFile)
            throws ImageReadException, IOException, ImageWriteException {
        ImageMetadata imageMetadata = Imaging.getMetadata(inputFile);
        if (imageMetadata == null) {
            return false;
        }

        if (!(imageMetadata instanceof JpegImageMetadata)) {
            return false;
        }
        JpegImageMetadata jpegMetadata = (JpegImageMetadata) imageMetadata;

        TiffImageMetadata tiffMetadata = jpegMetadata.getExif();
        if (tiffMetadata == null) {
            return false;
        }

        List<Integer> interestedDirTypes = getDirectoryTypesContaingAbnormalOrientation(tiffMetadata);
        if (interestedDirTypes.isEmpty()) {
            return false;
        }

        TiffOutputSet outputSet = updateOutputSet(tiffMetadata,
                interestedDirTypes);
        saveWithNewExif(inputFile, outputFile, outputSet);

        return true;
    }

    default List<Integer> getDirectoryTypesContaingAbnormalOrientation(
            TiffImageMetadata tiffImageMetadata) throws ImageReadException {
        List<? extends ImageMetadata.ImageMetadataItem> directories = tiffImageMetadata.getDirectories();
        List<Integer> interestedDirTypes = new ArrayList<>(directories.size());

        for (ImageMetadata.ImageMetadataItem dirItem : directories) {
            TiffImageMetadata.Directory dir = (TiffImageMetadata.Directory) dirItem;
            TiffField orientationField = dir
                    .findField(TiffTagConstants.TIFF_TAG_ORIENTATION);
            if (orientationField == null
                    || orientationField.getValue()
                            .equals((short) TiffTagConstants.ORIENTATION_VALUE_HORIZONTAL_NORMAL)) {
                continue;
            }
            interestedDirTypes.add(dir.type);
        }
        return interestedDirTypes;
    }

    default TiffOutputSet updateOutputSet(
            TiffImageMetadata tiffImageMetadata,
            List<Integer> interestedDirTypes) throws ImageWriteException {
        TiffOutputSet outputSet = tiffImageMetadata.getOutputSet();
        for (Integer dirType : interestedDirTypes) {
            TiffOutputDirectory outputDir = outputSet.findDirectory(dirType);
            outputDir.removeField(TiffTagConstants.TIFF_TAG_ORIENTATION);
            outputDir
                    .add(TiffTagConstants.TIFF_TAG_ORIENTATION,
                            (short) TiffTagConstants.ORIENTATION_VALUE_HORIZONTAL_NORMAL);
            outputDir.sortFields();
        }
        return outputSet;
    }

    default void saveWithNewExif(
            File inputFile,
            File outputFile,
            TiffOutputSet outputSet) throws ImageReadException,
            ImageWriteException, IOException {
        BufferedOutputStream out = null;
        ExifRewriter exifRewriter = new ExifRewriter();
        try {
            out = new BufferedOutputStream(new FileOutputStream(outputFile));
            exifRewriter.updateExifMetadataLossless(inputFile, out, outputSet);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }
}
