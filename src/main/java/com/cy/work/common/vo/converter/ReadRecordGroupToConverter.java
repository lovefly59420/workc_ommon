/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.vo.value.to.ReadRecordGroupTo;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

/**
 * 閱讀記錄群組轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class ReadRecordGroupToConverter implements AttributeConverter<ReadRecordGroupTo, String> {

    @Override
    public String convertToDatabaseColumn(ReadRecordGroupTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance()
                    .toJson(attribute)
                    .replace("\r", "")
                    .replace("\n", "")
                    .replace(" ", "");
        } catch (IOException ex) {
            log.error("parser ReadRecordGroupTo to json fail!!" + ex.getMessage());
            return "";
        }
    }

    @Override
    public ReadRecordGroupTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new ReadRecordGroupTo();
        }
        try {
            return WkJsonUtils.getInstance()
                    .fromJson(dbData, ReadRecordGroupTo.class);
        } catch (IOException ex) {
            log.error("parser json to ReadRecordGroupTo fail!!" + ex.getMessage());
            return new ReadRecordGroupTo();
        }
    }

}
