/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.value.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 閱讀記錄群組
 *
 * @author shaun
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadRecordGroupTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7236519214329106320L;

    /**
     * Key = userSid
     */
    @JsonProperty(value = "records")
    private Map<String, ReadRecordTo> records = Maps.newHashMap();

}
