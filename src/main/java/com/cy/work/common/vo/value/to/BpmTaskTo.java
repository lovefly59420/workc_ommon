/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.value.to;

import com.cy.bpm.rest.vo.ProcessTaskBase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author shaun
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpmTaskTo implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8382685682333004195L;

    @JsonProperty(value = "tasks")
    private List<ProcessTaskBase> tasks = Lists.newArrayList();
}
