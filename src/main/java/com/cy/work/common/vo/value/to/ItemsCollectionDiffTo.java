/**
 *
 */
package com.cy.work.common.vo.value.to;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author allen1214_wu
 *
 */
@AllArgsConstructor
public class ItemsCollectionDiffTo<T> implements Serializable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8789749088185389488L;

    /**
     * 增加項目
     */
    @Getter
    private Collection<T> plusItems;

    /**
     * 減少項目
     */
    @Getter
    private Collection<T> reduceItems;
}
