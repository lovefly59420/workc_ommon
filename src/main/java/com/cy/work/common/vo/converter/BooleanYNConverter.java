/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import javax.persistence.AttributeConverter;

/**
 * @author cosmo
 */
public class BooleanYNConverter implements AttributeConverter<Boolean, String> {

    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }

        switch (dbData.trim()) {
            case "Y":
                return Boolean.TRUE;
            case "N":
                return Boolean.FALSE;
            default:
                throw new IllegalArgumentException("無法轉換 " + dbData + " 為 Boolean");
        }
    }

    @Override
    public String convertToDatabaseColumn(Boolean javaType) {
        if (javaType == null) {
            return null;
        }
        return (javaType) ? "Y" : "N";
    }

}
