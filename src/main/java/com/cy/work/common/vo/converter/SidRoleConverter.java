/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.commons.vo.Role;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * system role 轉換器
 *
 * @author shaun
 */
@Converter
public class SidRoleConverter implements AttributeConverter<Role, Long> {

    @Override
    public Role convertToEntityAttribute(Long dbData) {
        return (dbData == null) ? null : new Role(dbData);
    }

    @Override
    public Long convertToDatabaseColumn(Role javaType) {
        return (javaType == null) ? null : javaType.getSid();
    }
}
