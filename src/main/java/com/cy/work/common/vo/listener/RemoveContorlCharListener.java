package com.cy.work.common.vo.listener;

import com.cy.work.common.utils.WkStringUtils;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.PrePersist;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * 移除 vo 中字串型態欄位值中的控制字元<br/>
 * 使用方法：@EntityListeners(RemoveContorlCharListener.class)
 *
 * @author allen
 */
@Slf4j
public class RemoveContorlCharListener {

    /**
     * 移除 vo 中字串型態欄位值中的控制字元<br/>
     * 此方法會在儲存Entity之前呼叫<br/>
     *
     * @param bean
     */
    @PrePersist
    public void removeContorlChar(Object bean) {

        try {
            // 依據傳入的 VO , 取得 BeanInfo
            BeanInfo info = Introspector.getBeanInfo(bean.getClass(), Object.class);

            // 取得 Property
            PropertyDescriptor[] props = info.getPropertyDescriptors();

            // 逐筆處理
            for (PropertyDescriptor pd : props) {

                if (pd == null) {
                    continue;
                }

                // 取得 Property 名稱
                String name = pd.getName();

                // 不為 string 類型時不處理
                if (!pd.getPropertyType()
                        .isAssignableFrom(String.class)) {
                    continue;
                }

                // 無 setter 可用時不處理
                Method setter = pd.getWriteMethod();
                if (setter == null) {
                    continue;
                }

                // 無 getter 可用時不處理
                Method getter = pd.getReadMethod();
                if (getter == null) {
                    continue;
                }

                // 字串為空時不處理
                String value = getter.invoke(bean) + "";
                if ("".equals(value)) {
                    continue;
                }

                // 重組欄位值 （移除控制字元）
                String newValue = WkStringUtils.removeCtrlChr(value);

                // 有判斷到控制字元時, 更新 Property 值
                if (!WkStringUtils.safeGetlength(value)
                        .equals(WkStringUtils.safeGetlength(newValue))) {
                    log.info("RemoveContorlCharListener： [" + name + "] 含有控制字元");
                    setter.invoke(bean, newValue);
                }
            }

        } catch (Exception e) {
            log.error("RemoveContorlCharListener", e);
        }
    }
}
