/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.vo.WkItem;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author kasim
 */
@Component("wkItemConverter")
@Slf4j
public class WkItemConverter implements Converter {

    @Override
    public Object getAsObject(
            FacesContext context,
            UIComponent component,
            String value) {
        if (Strings.isNullOrEmpty(value)) {
            return null;
        }

        try {
            return new Gson().fromJson(value, new TypeToken<WkItem>() {
            }.getType());
        } catch (Exception e) {
            log.error("解析失敗:\r\n" + value, e);
        }

        return null;
    }

    @Override
    public String getAsString(
            FacesContext context,
            UIComponent component,
            Object value) {
        if (value == null) {
            return null;
        }

        try {
            return new Gson().toJson(value);
        } catch (Exception e) {
            log.error("解析失敗", e);
        }

        return null;
    }

}
