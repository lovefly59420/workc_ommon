/**
 * 
 */
package com.cy.work.common.vo;

import java.io.Serializable;

import lombok.Getter;

/**
 * @author allen1214_wu
 */
public class CacheModel implements Serializable {
    /**
    *
    */
    private static final long serialVersionUID = -4906513357915214278L;
    @Getter
    private Long time;
    @Getter
    private Object data;

    /**
     * CacheModel
     * 
     * @param data
     */
    public CacheModel(Object data) {
        super();
        this.time = System.currentTimeMillis();
        this.data = data;
    }
}
