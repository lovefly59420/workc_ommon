package com.cy.work.common.vo.converter;

import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.utils.WkCommonUtils;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * String to integer list Converter (value)
 *
 * @author allen
 */
@Converter
public class SplitListIntConverter implements AttributeConverter<List<Integer>, String> {

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.
     * Object)
     */
    @Override
    public String convertToDatabaseColumn(List<Integer> attribute) {
        if (WkStringUtils.isEmpty(attribute)) {
            return "";
        }

        // 轉為以逗點分隔的字串
        return attribute.stream()
                .map(each -> each + "")
                .collect(Collectors.joining(","));
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.
     * Object)
     *
     */
    @Override
    public List<Integer> convertToEntityAttribute(String dbData) {

        String contentValue = WkStringUtils.safeTrim(dbData);

        if (WkStringUtils.isEmpty(contentValue)) {
            return Lists.newArrayList();
        }

        List<Integer> results = Lists.newArrayList();
        for (String strVal : contentValue.split(",")) {
            if (WkStringUtils.isEmpty(strVal)) {
                continue;
            }
            if (!WkStringUtils.isNumber(strVal)) {
                WkCommonUtils.logWithStackTrace(InfomationLevel.WARN, "DB取回的數值非數字:[" + strVal + "], 全字串[" + dbData + "]");
                continue;
            }
            results.add(Integer.valueOf(strVal));
        }

        return results;
    }
}
