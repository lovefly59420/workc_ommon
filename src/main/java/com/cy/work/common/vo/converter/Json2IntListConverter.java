package com.cy.work.common.vo.converter;

import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JSON to integer list Converter (value)
 *
 * @author allen
 */
@Slf4j
@Converter
public class Json2IntListConverter implements AttributeConverter<List<Integer>, String> {

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToDatabaseColumn(java.lang.
     * Object)
     */
    @Override
    public String convertToDatabaseColumn(List<Integer> attribute) {
        if (WkStringUtils.isEmpty(attribute)) {
            return "[]";
        }

        // 轉為字串形式存入
        List<String> strAttribute = attribute.stream()
                .map(each -> each + "")
                .collect(Collectors.toList());

        return new Gson().toJson(strAttribute);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.persistence.AttributeConverter#convertToEntityAttribute(java.lang.
     * Object)
     */
    @Override
    public List<Integer> convertToEntityAttribute(String dbData) {

        // 為空時直接回傳
        if (WkStringUtils.isEmpty(dbData)) {
            return Lists.newArrayList();
        }
        // 去空白
        String jsonStr = WkStringUtils.safeTrim(dbData);

        try {

            // 轉為 String List
            List<String> strs = new Gson().fromJson(
                    jsonStr,
                    new TypeToken<List<String>>() {
                    }.getType());

            // 1.去除非數字
            // 2.轉Integer
            return strs.stream()
                    .filter(WkStringUtils::isNumber)
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            log.error("Json2IntListConverter 轉型失敗。 dbData ：【" + dbData + "】");
            return Lists.newArrayList();
        }
    }
}
