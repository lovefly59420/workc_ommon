/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.utils.WkCommonUtils;

import javax.persistence.AttributeConverter;

public class IntBooleanConverter implements AttributeConverter<Boolean, Integer> {

    @Override
    public Boolean convertToEntityAttribute(Integer dbData) {
        //為1 時才回傳 true
        return WkCommonUtils.compareByStr(dbData, 1);
    }

    @Override
    public Integer convertToDatabaseColumn(Boolean attribute) {
        return attribute ? 1 : 0;
    }

}
