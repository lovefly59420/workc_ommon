/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.enums.WorkSourceType;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 送測來源
 *
 * @author shaun
 */
@Slf4j
@Converter
public class WorkSourceTypeConverter implements AttributeConverter<WorkSourceType, String> {

    @Override
    public String convertToDatabaseColumn(WorkSourceType attribute) {
        if (attribute == null) {
            return "";
        }
        return attribute.name();
    }

    @Override
    public WorkSourceType convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return null;
        }
        try {
            return WorkSourceType.valueOf(dbData);
        } catch (Exception e) {
            log.error("WorkSourceType Converter 轉型失敗。 dbData = " + dbData, e);
            return null;
        }
    }

}
