/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.enums.InstanceStatus;

import javax.persistence.AttributeConverter;

/**
 * @author cosmo
 */
public class InstanceStatusConverter implements AttributeConverter<InstanceStatus, String> {

    @Override
    public InstanceStatus convertToEntityAttribute(String paperCode) {
        return (paperCode == null) ? null : InstanceStatus.fromPaperCode(paperCode);
    }

    @Override
    public String convertToDatabaseColumn(InstanceStatus instanceStatus) {
        return (instanceStatus == null) ? null : instanceStatus.getValue();
    }

}
