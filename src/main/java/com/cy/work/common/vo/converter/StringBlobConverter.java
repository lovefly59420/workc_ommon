/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import java.nio.charset.StandardCharsets;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.common.base.Strings;

/**
 * 字串轉BLOB轉換器
 *
 * @author shaun
 */
@Converter
public class StringBlobConverter implements AttributeConverter<String, byte[]> {

    @Override
    public byte[] convertToDatabaseColumn(String attribute) {
        if (Strings.isNullOrEmpty(attribute)) {
            return "".getBytes();
        }
        return attribute.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String convertToEntityAttribute(byte[] dbData) {
        if (dbData == null) {
            return "";
        }
        return new String(dbData, StandardCharsets.UTF_8);
    }

}
