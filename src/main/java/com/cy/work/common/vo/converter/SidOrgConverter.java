/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.commons.vo.Org;
import com.cy.work.common.cache.WkOrgCache;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * system org 轉換器
 *
 * @author shaun
 */
@Converter
public class SidOrgConverter implements AttributeConverter<Org, Integer> {

    @Override
    public Org convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }

        Org org = WkOrgCache.getInstance()
                .findBySid(dbData);
        if (org != null) {
            return org;
        }
        return new Org(dbData);
    }

    @Override
    public Integer convertToDatabaseColumn(Org javaType) {
        return (javaType == null) ? null : javaType.getSid();
    }
}
