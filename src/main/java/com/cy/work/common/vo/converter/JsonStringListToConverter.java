/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.work.common.utils.WkJsonUtils;
import com.cy.work.common.vo.value.to.JsonStringListTo;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * 存放 集合 字串 的JSON物件 轉換器
 *
 * @author shaun
 */
@Slf4j
@Converter
public class JsonStringListToConverter implements AttributeConverter<JsonStringListTo, String> {

    @Override
    public String convertToDatabaseColumn(JsonStringListTo attribute) {
        if (attribute == null) {
            return "";
        }
        try {
            return WkJsonUtils.getInstance()
                    .toJson(attribute);
        } catch (IOException ex) {
            log.error("parser JsonStringListTo to json fail!!" + ex.getMessage());
            return "";
        }
    }

    /**
     *  convert To Database Column
     * @param values values
     * @return Database Column
     */
    public String convertToDatabaseColumn(Set<Integer> values) {
        if (values == null) {
            values = Sets.newHashSet();
        }

        JsonStringListTo jsonStringListTo = new JsonStringListTo();
        for (Integer value : values) {
            jsonStringListTo.getValue()
                    .add(value + "");
        }

        return this.convertToDatabaseColumn(jsonStringListTo);
    }

    @Override
    public JsonStringListTo convertToEntityAttribute(String dbData) {
        if (Strings.isNullOrEmpty(dbData)) {
            return new JsonStringListTo();
        }
        try {
            return WkJsonUtils.getInstance()
                    .fromJson(dbData, JsonStringListTo.class);
        } catch (IOException ex) {
            return tryRecovery(dbData);
        }
    }

    private JsonStringListTo tryRecovery(String dbData) {
        try {
            JsonStringListTo to = new JsonStringListTo();
            List<String> value = WkJsonUtils.getInstance()
                    .fromJsonToList(dbData, String.class);
            to.setValue(value);
            return to;
        } catch (IOException ex) {
            log.error("parser json to JsonStringListTo fail!!" + ex.getMessage());
            log.error("dbData = " + dbData);
            return new JsonStringListTo();
        }
    }

}
