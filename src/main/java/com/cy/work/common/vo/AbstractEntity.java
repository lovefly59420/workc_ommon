package com.cy.work.common.vo;

import com.cy.commons.enums.Activation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * @author allen1214_wu
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    /**
     * 狀態
     */
    @Getter
    @Setter
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;
    /**
     * 建立者
     */
    @Getter
    @Setter
    @Column(name = "create_usr", nullable = false)
    private Integer createdUser;
    /**
     * 建立時間
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createdDate;
    /**
     * 異動者
     */
    @Getter
    @Setter
    @Column(name = "update_usr")
    private Integer updatedUser;
    /**
     * 異動時間
     */
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updatedDate;

    /**
     * 是否啟用
     *
     * @return true/false
     */
    public boolean isActive() {
        return Activation.ACTIVE.equals(this.status);
    }
}
