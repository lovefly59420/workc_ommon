/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cy.work.common.vo.converter;

import com.cy.commons.vo.User;
import com.cy.work.common.cache.WkUserCache;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * 用於各專案之 createdUser, updatedUser 之轉型
 *
 * @author cosmo
 */
@Converter
public class SidUserConverter implements AttributeConverter<User, Integer> {

    @Override
    public User convertToEntityAttribute(Integer dbData) {
        if (dbData == null) {
            return null;
        }

        User user = WkUserCache.getInstance()
                .findBySid(dbData);
        if (user != null) {
            return user;
        }

        // 舊 code 在查不到資料時，也不回傳 null , 為避免影響程式, 先依舊規則
        return new User(dbData);
    }

    @Override
    public Integer convertToDatabaseColumn(User javaType) {
        return (javaType == null) ? null : javaType.getSid();
    }

}
