/**
 *
 */
package com.cy.work.common.exception.alert;

import com.cy.work.common.enums.WerpModuleType;

/**
 * 屬於系統處理時發生錯誤, 並發送 skype 警告訊息 （工作報告用）
 *
 * @author allen1214_wu
 */
public class WorkReportAlertException extends SystemAlertException {

    /**
     * 
     */
    private static final long serialVersionUID = -8234212680652917626L;

    /**
     * @param message
     */
    public WorkReportAlertException(String message, String compId) {
        super(WerpModuleType.WORKRPT, message, compId);
    }
    
    public WorkReportAlertException(String message) {
        super(WerpModuleType.WORKRPT, message, "");
    }
}
