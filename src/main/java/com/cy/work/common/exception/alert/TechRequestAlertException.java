/**
 *
 */
package com.cy.work.common.exception.alert;

import com.cy.work.common.enums.WerpModuleType;

/**
 * 屬於系統處理時發生錯誤, 並發送 skype 警告訊息 （需求單用）
 *
 * @author allen1214_wu
 */
public class TechRequestAlertException extends SystemAlertException {

    /**
     * 
     */
    private static final long serialVersionUID = -8234212680652917626L;

    /**
     * @param message
     */
    public TechRequestAlertException(String message, String compId) {
        super(WerpModuleType.REQ, message, compId);
    }
    
    public TechRequestAlertException(String message) {
        super(WerpModuleType.REQ, message, "");
    }
}
