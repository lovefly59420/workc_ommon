/**
 *
 */
package com.cy.work.common.exception;

import com.cy.work.common.enums.InfomationLevel;
import lombok.Getter;

/**
 * 屬於系統處理時發生錯誤
 *
 * @author allen1214_wu
 */
public class SystemOperationException extends Exception {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6076031684796590639L;

    @Getter
    protected InfomationLevel level;
    @Getter
    protected String exceptionMessage;

    /**
     * @param message
     */
    public SystemOperationException(String message) {
        super(message);

        level = InfomationLevel.INFO;
    }

    /**
     * @param message
     * @param level 訊息等級
     */
    public SystemOperationException(
            String message,
            InfomationLevel level) {
        super(message);
        this.level = level;
    }

    /**
     * @param message message
     * @param exceptionMessage 系統的 log 訊息
     * @param level 訊息等級
     */
    public SystemOperationException(
            String message,
            String exceptionMessage,
            InfomationLevel level) {
        super(message);
        this.exceptionMessage = exceptionMessage;
        this.level = level;
    }
}
