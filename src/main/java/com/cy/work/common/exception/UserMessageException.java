package com.cy.work.common.exception;

import com.cy.work.common.enums.InfomationLevel;
import lombok.Getter;

/**
 * 用於拋出前端要顯示給User的訊息
 *
 * @author allen1214_wu
 */
public class UserMessageException extends Exception {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2655216862577805220L;

    @Getter
    private InfomationLevel level;

    @Getter
    private String exceptionMessage;

    /**
     * @param userMessage
     */
    public UserMessageException(String userMessage) {
        super(userMessage);
        this.exceptionMessage = userMessage;
        level = InfomationLevel.INFO;
    }

    /**
     * @param userMessage
     * @param level       訊息等級
     */
    public UserMessageException(
            String userMessage,
            InfomationLevel level) {
        super(userMessage);
        this.exceptionMessage = userMessage;
        this.level = level;
    }

    /**
     * @param userMessage
     * @param exceptionMessage 系統的 log 訊息
     * @param level            訊息等級
     */
    public UserMessageException(
            String userMessage,
            String exceptionMessage,
            InfomationLevel level) {
        super(userMessage);
        this.exceptionMessage = exceptionMessage;
        this.level = level;
    }

    /**
     * @param e
     */
    public UserMessageException(
            SystemOperationException e) {
        super(e.getMessage());
        this.exceptionMessage = e.getExceptionMessage();
        this.level = e.getLevel();
    }

}
