/**
 *
 */
package com.cy.work.common.exception.alert;

import com.cy.work.common.enums.WerpModuleType;

/**
 * 屬於系統處理時發生錯誤, 並發送 skype 警告訊息 （案件單用）
 *
 * @author allen1214_wu
 */
public class TechIssueAlertException extends SystemAlertException {

    /**
     * 
     */
    private static final long serialVersionUID = -8234212680652917626L;

    /**
     * @param message
     */
    public TechIssueAlertException(String message, String compId) {
        super(WerpModuleType.TECH, message, compId);
    }
    
    public TechIssueAlertException(String message) {
        super(WerpModuleType.TECH, message, "");
    }
}
