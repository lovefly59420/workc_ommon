/**
 *
 */
package com.cy.work.common.exception;

import com.cy.work.common.enums.InfomationLevel;
import lombok.Getter;

/**
 * 屬於系統開發時期的異常 (檢核傳入資料, 或呼叫方式有誤等開發時期錯誤)
 *
 * @author allen1214_wu
 */
public class SystemDevelopException extends RuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 7409694828109095830L;

    @Getter
    private InfomationLevel level;

    /**
     * @param message
     */
    public SystemDevelopException(String message) {
        super(message);

        level = InfomationLevel.INFO;
    }

    /**
     * @param message
     * @param level 訊息等級
     */
    public SystemDevelopException(
            String message,
            InfomationLevel level) {
        super(message);
        this.level = level;
    }
}
