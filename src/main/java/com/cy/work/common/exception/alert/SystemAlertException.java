/**
 *
 */
package com.cy.work.common.exception.alert;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cy.commons.util.FusionUrlServiceUtils;
import com.cy.commons.vo.Org;
import com.cy.system.rest.client.vo.SecAuth;
import com.cy.work.common.cache.WkOrgCache;
import com.cy.work.common.client.skypebot.SkypeBotClient;
import com.cy.work.common.enums.InfomationLevel;
import com.cy.work.common.enums.WerpModuleType;
import com.cy.work.common.exception.SystemOperationException;
import com.cy.work.common.utils.WkStringUtils;
import com.google.common.collect.Lists;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * 屬於系統處理時發生錯誤
 *
 * @author allen1214_wu
 */
@Slf4j
public class SystemAlertException extends SystemOperationException {

    /**
     * 
     */
    @Setter
    private static boolean sendAlert = true;

    /**
     * 
     */
    private static final long serialVersionUID = -8234212680652917625L;

    /**
     * @param message
     */
    public SystemAlertException(WerpModuleType werpModuleType, String message, String compId) {
        super(message, InfomationLevel.ERROR);

        // ====================================
        // skype alert
        // ====================================
        if (sendAlert) {
            try {
                // 組警告訊息
                String sendMessage = this.prepareAlertMessage(werpModuleType, message, compId);
                // 發送
                SkypeBotClient.getInstance().sendMessageToChat(sendMessage);
            } catch (Exception e) {
                log.error("發送警告訊息失敗!" + e.getMessage(), e);
            }
        }
    }

    /**
     * 準備警示訊息
     * 
     * @param werpModuleType WERP 模組類別
     * @param message        訊息
     * @param compId         公司ID
     * @return 警示訊息
     * @throws SystemOperationException 錯誤時拋出
     */
    private String prepareAlertMessage(
            WerpModuleType werpModuleType,
            String message,
            String compId) throws SystemOperationException {
        List<String> alertMessages = Lists.newArrayList();

        alertMessages.add("\\================================");
        alertMessages.add("【" + werpModuleType.getLabel() + "】系統警示!");
        alertMessages.add("\\================================");
        alertMessages.add("\\--訊息--");
        alertMessages.add(message);
        alertMessages.add("");
        alertMessages.add(this.prepareSourceInfo(werpModuleType, compId));
        return alertMessages.stream().collect(Collectors.joining("\r\n "));
    }

    /**
     * 準備來源資訊
     * 
     * @param werpModuleType WERP 模組類別
     * @param compId         公司ID
     * @return 來源資訊
     * @throws SystemOperationException 錯誤時拋出
     */
    private String prepareSourceInfo(WerpModuleType werpModuleType, String compId) throws SystemOperationException {

        String sourceInfo = "\\--來源--\r\n";

        if (WkStringUtils.notEmpty(compId)) {
            if (!WkOrgCache.getInstance().isExistDomainUrlByCompId(compId)) {
                compId = "";
            }
        }

        // ====================================
        // 有傳入公司別
        // ====================================
        if (WkStringUtils.notEmpty(compId)) {
            sourceInfo += this.prepareCompAndUrlInfo(werpModuleType, compId, ".ap.url");
            return sourceInfo;
        }

        // ====================================
        // 未傳入公司別、但系統有TG資料 (CY本身，或和CY住一起)
        // ====================================
        if (WkOrgCache.getInstance().isExistDomainUrlByCompId("TG")) {
            sourceInfo += this.prepareCompAndUrlInfo(werpModuleType, "TG", ".rest.url");
            return sourceInfo;
        }

        // ====================================
        // 未傳入公司別、沒TG資料 (資料拆出去的公司)
        // ====================================
        Optional<SecAuth> opt = WkOrgCache.getInstance().findAllSecAuth().stream()
                .findFirst();

        if (opt.isPresent()) {
            sourceInfo += this.prepareCompAndUrlInfo(werpModuleType, opt.get().getCompId(), ".rest.url");
            return sourceInfo;
        }

        // ====================================
        // SecAuth 找不到資料?
        // ====================================
        log.error("sec_auth 無資料？");
        sourceInfo += FusionUrlServiceUtils.getWerpApDomain();
        return sourceInfo;

    }

    /**
     * 組公司資訊和對應URL
     * 
     * @param werpModuleType WERP 模組類別
     * @param compId         公司ID
     * @param propKeySuffix  URL prop key 後綴
     * @return 公司資訊和對應URL
     * @throws SystemOperationException 錯誤時拋出
     */
    private String prepareCompAndUrlInfo(WerpModuleType werpModuleType, String compId, String propKeySuffix) throws SystemOperationException {

        String sourceInfo = "";
        Org comp = WkOrgCache.getInstance().findById(compId);

        // 依據公司別取得網址
        String domainUrl = WkOrgCache.getInstance().findDomainUrlByCompId(compId);
        // 取得AP 路徑
        String propKey = werpModuleType.getUrlPropKeyPrefix() + ".ap.url";
        String apPath = FusionUrlServiceUtils.getUrlByPropKey(propKey);
        if (WkStringUtils.notEmpty(apPath)) {
            String[] apPaths = apPath.split("/");
            if (apPaths.length > 1) {
                apPath = apPaths[apPaths.length - 1];
            }
        }

        sourceInfo += "[" + comp.getName() + "]  ";
        sourceInfo += domainUrl + apPath;

        return sourceInfo;
    }
}
