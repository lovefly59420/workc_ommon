/**
 *
 */
package com.cy.work.common.exception.alert;

import com.cy.work.common.enums.WerpModuleType;

/**
 * 屬於系統處理時發生錯誤, 並發送 skype 警告訊息 （簽呈專案用）
 *
 * @author allen1214_wu
 */
public class PetitionAlertException extends SystemAlertException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -8234212680652917626L;

    /**
     * @param message
     */
    public PetitionAlertException(String message) {
        super(WerpModuleType.PET, message, "");
    }

    /**
     * @param message
     */
    public PetitionAlertException(String message, String compId) {
        super(WerpModuleType.PET, message, compId);
    }
}
