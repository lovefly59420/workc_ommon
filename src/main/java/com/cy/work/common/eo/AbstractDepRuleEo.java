/**
 * 
 */
package com.cy.work.common.eo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

import com.cy.work.common.vo.converter.SplitListIntConverter;

import lombok.Getter;
import lombok.Setter;

/**
 * @author allen1214_wu
 */
@MappedSuperclass
public abstract class AbstractDepRuleEo extends AbstractEo {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6792693506036662199L;

    /**
     * 公司別ID
     */
    @Getter
    @Setter
    @Column(name = "comp_id")
    private String compID;

    /**
     * 群組部門，以『,』分隔
     */
    @Getter
    @Setter
    @Column(name = "dep_sids")
    @Convert(converter = SplitListIntConverter.class)
    private List<Integer> depSids;

    /**
     * 群組部門是否含以下
     */
    @Getter
    @Setter
    @Column(name = "dep_is_below")
    private boolean depIsBelow;

    /**
     * 除外部門，以『,』分隔 (當群組含以下時可設定除外部門)
     */
    @Getter
    @Setter
    @Column(name = "except_dep_sids")
    @Convert(converter = SplitListIntConverter.class)
    private List<Integer> exceptDepSids;

    /**
     * 除外部門是否含以下
     */
    @Getter
    @Setter
    @Column(name = "except_dep_is_below")
    private boolean exceptDepIsBelow;

    /**
     * 群組名稱
     */
    @Getter
    @Setter
    @Column(name = "group_name")
    private String groupName;

    /**
     * 備註
     */
    @Getter
    @Setter
    @Column(name = "memo")
    private String memo;

    /**
     * 顯示排序
     */
    @Getter
    @Setter
    @Column(name = "show_seq")
    private Integer showSeq = Integer.MAX_VALUE;

}
