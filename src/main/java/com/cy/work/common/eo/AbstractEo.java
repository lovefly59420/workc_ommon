/**
 *
 */
package com.cy.work.common.eo;

import com.cy.commons.enums.Activation;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Eo 共通欄位
 *
 * @author allen
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEo implements Serializable {

    /**
     * 建立時間
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_dt", nullable = false)
    private Date createDate;

    /**
     * 建立者
     */
    @Column(name = "create_usr", nullable = false)
    private Integer createUser;

    /**
     * 異動時間
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_dt")
    private Date updateDate;

    /**
     * 異動者
     */
    @Column(name = "update_usr")
    private Integer updateUser;

    /**
     * 狀態
     */
    @Column(name = "status", nullable = false)
    private Activation status = Activation.ACTIVE;

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the createUser
     */
    public Integer getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the updateUser
     */
    public Integer getUpdateUser() {
        return updateUser;
    }

    /**
     * @param updateUser the updateUser to set
     */
    public void setUpdateUser(Integer updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * @return the status
     */
    public Activation getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Activation status) {
        this.status = status;
    }

    /**
     * 狀態是否為【啟用】
     *
     * @return 是/否
     */
    public boolean isActive() {
        return Activation.ACTIVE.equals(this.status);
    }
}
