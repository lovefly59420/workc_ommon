package com.cy.work.common.callback;

import com.cy.work.common.exception.SystemDevelopException;

import java.io.Serializable;

/**
 * @author allen1214_wu
 */
public class DialogCallback implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 2937850189737612649L;

    /**
     * 確認
     */
    public void confirm() {
        throw new SystemDevelopException("DialogCallback 未實做 confirm 方法!");
    }

    /**
     * 取消
     */
    public void cencel() {
        throw new SystemDevelopException("DialogCallback 未實做 cencel 方法!");
    }

}
